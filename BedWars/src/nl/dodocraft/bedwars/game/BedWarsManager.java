package nl.dodocraft.bedwars.game;

import javax.naming.ConfigurationException;

import cn.nukkit.AdventureSettings.Type;
import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.Listener;
import cn.nukkit.event.player.PlayerChatEvent;
import cn.nukkit.event.player.PlayerTeleportEvent;
import cn.nukkit.item.Item;
import cn.nukkit.item.ItemFirework.FireworkExplosion.ExplosionType;
import cn.nukkit.level.Sound;
import cn.nukkit.scheduler.NukkitRunnable;
import cn.nukkit.utils.DyeColor;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.bedwars.game.generators.GeneratorManager;
import nl.dodocraft.bedwars.game.respawn.RespawnManager;
import nl.dodocraft.bedwars.game.rewards.RewardType;
import nl.dodocraft.bedwars.game.rewards.RewardsManager;
import nl.dodocraft.bedwars.game.shop.ItemManager;
import nl.dodocraft.bedwars.game.shop.ShopCurrency;
import nl.dodocraft.bedwars.game.shop.ShopManager;
import nl.dodocraft.bedwars.game.shop.specials.SpecialItemManager;
import nl.dodocraft.bedwars.game.team.TeamManager;
import nl.dodocraft.bedwars.player.GamePlayerManager;
import nl.dodocraft.bedwars.player.PlayerInfo;
import nl.dodocraft.bedwars.player.perks.BedWarsPerkManager;
import nl.dodocraft.bedwars.server.BedWarsServerManager;
import nl.dodocraft.common.Rank;
import nl.dodocraft.common.server.Server;
import nl.dodocraft.common.server.ServerStatus;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.game.levels.LevelManager;
import nl.dodocraft.utils.helpers.SoundHelper;
import nl.dodocraft.utils.minigame.GameManager;
import nl.dodocraft.utils.minigame.GameType;
import nl.dodocraft.utils.minigame.player.equipment.ArmorEquipmentManager;
import nl.dodocraft.utils.minigame.team.BaseTeam;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.visual.particle.DodoFirework;
import nl.dodocraft.utils.world.DodoLocation;
import nl.dodocraft.utils.world.WorldManager;

public class BedWarsManager extends GameManager implements Listener {
	
	private BaseTeam winner = null;
	private DodoLocation lobbySpawn = null;
	
	@Override
	public String getGameName() {
		return DodoCore.getInstance().getServerApi().getGamemode().getName();
	}

	@Override
	public String getMapName() {
		DodoConfig config = DodoCraft.services().get(BedWarsServerManager.class).getConfig();
		String mapName = "Unknown";
		
		if(config.exists("settings.mapname")) {
			mapName = config.getString("settings.mapname");
		}
		
		return mapName;
	}
	
	@Override
	public int getMinPlayers() {
		return getGameType().getMaxPlayers();
	}

	@Override
	public int getMaxPlayers() {
		return getGameType().getMaxPlayers();
	}
	
	public DodoLocation getLobbySpawn() {
		return lobbySpawn.clone().add(0,1,0);
	}
	
	@Override
	public GameType getGameType() {
		Server server = DodoCore.getInstance().getServerApi().getServer();
		return server == Server.BEDWARS_SOLO ? GameType.SOLO : GameType.DUO;
	}
	
	public int getAlivePlayers() {
		int alive = 0;

		for(Player player : DodoCraft.players()) {
			PlayerInfo info = DodoCraft.services().get(GamePlayerManager.class).getPlayerInfo(player);
			if(!info.isEliminated()) alive++;
		}
		
		return alive;
	}
	
	public void forceStartGame(Player player) {
		if(player != null) {
			String color = DodoCore.getInstance().getRank(player.getUniqueId()).getColor().toString();
			Message.MINIGAME_COUNTDOWN_SKIP_TIMER.build().replace(new MessageReplaceValue("%PLAYER%", color + player.getName())).broadcast();
		} else {
			Message.MINIGAME_COUNTDOWN_SKIP_TIMER.build().replace(new MessageReplaceValue("%PLAYER%", "&c&lConsole")).broadcast();
		}
		
		// Update ServerStatus
		DodoCore.getInstance().getServerApi().updateStatus(ServerStatus.IN_GAME);
		
		// Start Game
		startGame();
	}
	
	@Override
	protected void runSetup() throws ConfigurationException {
		DodoConfig config = DodoCraft.services().get(BedWarsServerManager.class).getConfig();
		
		// Validate important config options
		try {
			lobbySpawn = config.get("settings.lobbyspawn", DodoLocation.class).clone().add(0.5, 1, 0.5);
			if(!lobbySpawn.isValid()) throw new ConfigurationException("Invalid lobby location");
			DodoCraft.services().get(WorldManager.class).setSpawn(getLobbySpawn());
		} catch(Exception e) {
			throw new ConfigurationException(e.getMessage());
		}
		
		//Register services
		DodoCraft.services().register(GamePlayerManager.class);
		DodoCraft.services().register(RewardsManager.class);
		DodoCraft.services().register(TeamManager.class);
		DodoCraft.services().register(ShopManager.class);
		DodoCraft.services().register(ItemManager.class);
		DodoCraft.services().register(SpecialItemManager.class);
		DodoCraft.services().register(RespawnManager.class);
		DodoCraft.services().register(GeneratorManager.class);
		DodoCraft.services().register(BedWarsPerkManager.class);
		DodoCraft.services().register(ArmorEquipmentManager.class); //in DodoUtils
	}

	@Override
	protected void onCountDown(int count) {
	
	}
	
	@Override
	protected void onPlayerJoin(Player player) {
		player.teleport(getLobbySpawn(), PlayerTeleportEvent.TeleportCause.PLUGIN);
		// Update player settings
		updatePlayer(player);
	}

	@Override
	protected void onPlayerLeave(Player player) {
		GamePlayerManager manager = DodoCraft.services().get(GamePlayerManager.class);
		PlayerInfo info = manager.getPlayerInfo(player);
		
		if(getStatus() == ServerStatus.WAITING || getStatus() == ServerStatus.STARTING) {
			if(DodoCraft.services().get(TeamManager.class).hasTeam(player)) {
				DodoCraft.services().get(TeamManager.class).getTeam(player).removePlayer(player);
			}		
			
			return;
		}

		if(getStatus() == ServerStatus.IN_GAME) {
			// Cancel if player isn't alive or the game is already ended.
			if(info.isEliminated()) return;
			
			// Check if player has a last damager.
			killPlayer(player, true);
			
			// Remove player
			DodoCraft.services().get(TeamManager.class).getTeam(player).removePlayer(player);
		}
	}

	@EventHandler (ignoreCancelled = true)
	public void onChat(PlayerChatEvent event) {
		LevelManager levelManager = DodoCraft.services().get(LevelManager.class);
		TeamManager manager = DodoCraft.services().get(TeamManager.class);
		Player player = event.getPlayer();
		String message = event.getMessage();
		Rank rank = DodoCore.getInstance().getRank(player.getUniqueId());
		String levelPrefix = levelManager.getChatPrefix(player);
		BaseTeam team = manager.getTeam(player);
		
		event.setCancelled(true);
		
		if(getStatus() == ServerStatus.IN_GAME && this.getGameType() != GameType.SOLO) {
			boolean isDead = DodoCraft.services().get(GamePlayerManager.class).getPlayerInfo(player).isEliminated();
			// Global chat
			if(isDead || message.startsWith("!") || manager.getTeam(player).getMembers().size() <= 1) {
				String shoutMessage = Message.GAME_SHOUT_MESSAGE.build().replace(
					new MessageReplaceValue("%COLOR%", isDead ? "&7" : team.getColor()),
					new MessageReplaceValue("%INT/NAME%", team.getName().toUpperCase()),
					new MessageReplaceValue("%PLAYER%", rank.getColor() + player.getName()),
					new MessageReplaceValue("%LEVEL%", levelPrefix),
					new MessageReplaceValue("%CHATCOLOR%", rank.getChatColor())
				).get();
				
				DodoCraft.allPlayers().forEach(players -> players.sendMessage(shoutMessage + message.replaceFirst("!", "").trim()));
				return;
			}
			
			// Team chat
			String teamMessage = Message.GAME_TEAMCHAT_MESSAGE.build().replace(
				new MessageReplaceValue("%COLOR%", team.getColor()),
				new MessageReplaceValue("%MODE%", "TEAM"),
				new MessageReplaceValue("%PLAYER%", rank.getColor() + player.getName()),
				new MessageReplaceValue("%LEVEL%", levelPrefix),
				new MessageReplaceValue("%CHATCOLOR%", rank.getChatColor())
			).get();
			
			manager.getTeam(player).getMembers().forEach(member -> member.sendMessage(teamMessage + message.trim()));
		} else {
			String preGameMessage = Message.GAME_PREGAME_MESSAGE.build().replace(
				new MessageReplaceValue("%PLAYER%", rank.getColor() + player.getName()),
				new MessageReplaceValue("%LEVEL%", levelPrefix),
				new MessageReplaceValue("%CHATCOLOR%", rank.getChatColor())
			).get() + message.trim();
			
			DodoCraft.allPlayers().forEach(p -> p.sendMessage(preGameMessage));
		}
	}
		
	@Override
	protected void startGame() {
		TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
		
		// Update players
		DodoCraft.players().forEach(players -> updatePlayer(players));
		
		// Make sure all players are in a team
		teamManager.finalizeTeams();
		
		for(Player player : DodoCraft.players()) {
			BaseTeam team = teamManager.getTeam(player);
			
			// Teleport player
			player.teleport(team.getSpawn());
			
			String levelTag = DodoCraft.services().get(LevelManager.class).getLevelTag(player);
			boolean isStaff = DodoCore.getInstance().getRank(player.getUniqueId()).isPermanent();
			player.setNameTag(TextFormat.colorize('&', levelTag + team.getColor() + (isStaff ? "&l" : "") + player.getName()));
			
			updatePlayer(player);
			DodoCraft.services().get(GamePlayerManager.class).giveItems(player);
			
			// if TeamMode, send message with teammembers.
			if(this.getGameType() != GameType.SOLO && team.getMembers().size() >= 2) {
				MessageReplaceValue members = new MessageReplaceValue("%PLAYER%", team.getMembersList("&e", player));
				Message.GAME_START_TEAM.build().replace(members).send(player);;
			}
		}
		
		new NukkitRunnable() {
			@Override
			public void run() {
				// Broadcast Ender Dragon sound after 1 tick
				SoundHelper.broadcastSound(Sound.MOB_ENDERDRAGON_GROWL, 0.5f, 1f);
			}
		}.runTaskLater(DodoBedWars.getInstance(), 20);
		
		// Start Generators
		DodoCraft.services().get(GeneratorManager.class).startAll();
		
		// Spawn Shops
		DodoCraft.services().get(ShopManager.class).spawnShops();
		
		// Start game action bar
		DodoCraft.services().get(GamePlayerManager.class).startActionBar();
		
		for(BaseTeam team : DodoCraft.services().get(TeamManager.class).getTeams()) {
			if(team.getMembers().isEmpty()) {
				team.setPlace();
				team.setAlive(false);
			}
		}
	}
	
	@Override
	protected void stopGame() {
		TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
		RewardsManager rewardsManager = DodoCraft.services().get(RewardsManager.class);
		
		// Update players
		DodoCraft.players().forEach(players -> updatePlayer(players));
		
		// Set their place since they haven't died
		winner.setPlace();
		
		// Win/lose message
		for(Player player : DodoCraft.players()) {
			BaseTeam team = teamManager.getTeam(player);
			if(team == null) continue;
			
			// Send Title with TeamPlace to players.
			if(!teamManager.getTeam(player).equals(winner)) {
				Message line = getGameType() == GameType.DUO ? Message.GAME_TEAM_PLACE_TEAM : Message.GAME_TEAM_PLACE_SOLO;
				player.sendTitle(Message.GAME_END_MESSAGE_LOSE.build().get(), line.build().replace(
					new MessageReplaceValue("%PLACE%", team.getPlaceString())
				).get());
			} else {
				Message line = getGameType() == GameType.DUO ? Message.GAME_TEAM_PLACE_TEAM : Message.GAME_TEAM_PLACE_SOLO;
				player.sendTitle(Message.GAME_END_MESSAGE_WIN.build().get(), line.build().replace(
					new MessageReplaceValue("%PLACE%", team.getPlaceString())
				).get());
				
				// Add Experience & Coins
				rewardsManager.updateRewards(RewardType.WIN, player, null);
			}
			
			rewardsManager.sendDataCore(player);
		}
		
		// Firework for winners
		playFirework();

		// Sending Game End & Win- message
		sendGameEndMessages();
	}

	@Override
	protected boolean checkGameEnd() {
		// Return false if game is already ended / ServerStatus isn't IN_GAME.
		if(getStatus() != ServerStatus.IN_GAME) {
			return false;
		}
						
		TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
		int aliveTeams = 0;
						
		// Find the winning team
		for(BaseTeam team : teamManager.getTeams()) {
			if(!team.isAlive() && !team.hasAlivePlayers()) continue;
			aliveTeams++;
			winner = team;
		}
						
		// Return whether the game should end
		return aliveTeams <= 1;
	}
	
	public void killPlayer(Player player, boolean isLeavedGame) {
		TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
		RewardsManager rewardManager = DodoCraft.services().get(RewardsManager.class);
		GamePlayerManager gamePlayerManager = DodoCraft.services().get(GamePlayerManager.class);
		
		PlayerInfo info = gamePlayerManager.getPlayerInfo(player);
		if (info == null) return;
		info.setAlive(false);
		
		BaseTeam team = teamManager.getTeam(player);
		if (team == null) return;
		if (team.isAlive()) {
			DodoCraft.services().get(RespawnManager.class).startRespawn(player);
		}
		
		if(info.getLastDamager() != null) {
			Player killer = info.getLastDamager();
			info.setLastDamager(null);
			
			if (!killer.isAlive()) return;
			ShopManager shopManager = DodoCraft.services().get(ShopManager.class);
			for (ShopCurrency currency : ShopCurrency.values()) {
				int amount = shopManager.getCurrencyInInventory(player, currency);
				if (amount <= 0) continue;
				Item item = currency.getItem();
				item.setCount(amount);
				killer.getInventory().addItem(item);
			}
			
			if (team.isAlive()) { // Normal kill
				rewardManager.updateRewards(RewardType.KILL, killer, player);
				Message.GAME_DEATH_PLAYER_KILL.build().replace(
					new MessageReplaceValue("%KILLER%", teamManager.getTeam(killer).getColor() + killer.getName()),
					new MessageReplaceValue("%VICTIM%", team.getColor() + player.getName())
				).broadcast();
			} else { // Elimination kill
				info.setEliminated(true);
				rewardManager.updateRewards(RewardType.ELIMINATION_KILL, killer, player);
				Message.GAME_DEATH_PLAYER_KILL_ELIMINATION.build().replace(
					new MessageReplaceValue("%KILLER%", teamManager.getTeam(killer).getColor() + killer.getName()),
					new MessageReplaceValue("%VICTIM%", team.getColor() + player.getName())
				).broadcast();
			}
			
			killer.heal(8f);
		} else {
			if (!teamManager.getTeam(player).isAlive()) {
				info.setEliminated(true);
				Message.GAME_DEATH_PLAYER_SUICIDE_ELIMINATION.build().replace(
						new MessageReplaceValue("%PLAYER%", teamManager.getTeam(player).getColor() + player.getName())
				).broadcast();
			} else {
				Message.GAME_DEATH_PLAYER_SUICIDE.build().replace(
						new MessageReplaceValue("%PLAYER%", teamManager.getTeam(player).getColor() + player.getName())
				).broadcast();
			}
		}
		
		if (isLeavedGame) {
			info.setEliminated(true);
		} else {
			// Update player's settings
			updatePlayer(player);
		}
		
		if (!team.hasAlivePlayers()) {
			team.setAlive(false);
		}
		
		player.teleport(getLobbySpawn());
		
		if(!team.isAlive() && !team.hasAlivePlayers()) {
			if(isLeavedGame) {
				
				// Set Place for team
				team.setPlace();
				
				// Check if game can End
				runGameEndCheck();
				
				// Give rewards
				rewardManager.updateRewards(RewardType.LOSS, player, null);
				return;
			}
			
			// Send titles to player
			sendDeathTitles(player);
			
			// Check if Game can end
			runGameEndCheck();
		}
	}
	
	public void sendDeathTitles(Player player) {
		TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
		BaseTeam team = teamManager.getTeam(player);
		if(team.hasAlivePlayers()) {
			player.sendTitle(Message.GAME_DEATH_PLAYER_TITLE_LINE1.build().get(), Message.GAME_DEATH_PLAYER_TITLE_LINE2.build().get());
		} else {
			// Set place
			team.setPlace();
			
			Message subline = teamManager.getTeammate(player) != null ? Message.GAME_TEAM_PLACE_TEAM : Message.GAME_TEAM_PLACE_SOLO;
			team.getMembers().forEach(member -> {
				member.sendTitle(Message.GAME_DEATH_PLAYER_TITLE_TEAMMEMBERS.build().get(), subline.build().replace(
					new MessageReplaceValue("%PLACE%", team.getPlaceString() + "")
				).get());
			});
		}
	}
	
	public void updatePlayer(Player player) {
		if(getStatus() == ServerStatus.IN_GAME) {
			GamePlayerManager manager = DodoCraft.services().get(GamePlayerManager.class);
			if(manager.getPlayerInfo(player).isAlive() && !manager.getPlayerInfo(player).isEliminated()) {
				player.setGamemode(0);
				player.getAdventureSettings().set(Type.ALLOW_FLIGHT, false);
				player.getAdventureSettings().set(Type.FLYING, false);
				player.getAdventureSettings().set(Type.ATTACK_PLAYERS, true);
				player.setAllowModifyWorld(true);
				player.setAllowInteract(true, true);
				player.getFoodData().setLevel(20);
				player.getFoodData().sendFoodLevel(player.getFoodData().getMaxLevel());
				player.setHealth(player.getMaxHealth());
				player.removeAllEffects();
			} else {
				player.setGamemode(3);
				player.getFoodData().setLevel(20);
				player.getFoodData().sendFoodLevel(player.getFoodData().getMaxLevel());
				player.setHealth(player.getMaxHealth());
				player.removeAllEffects();
				player.getInventory().clearAll();
				player.getInventory().remove(player.getInventory().getItemInHand());
			}
		} else {
			if(getStatus() == ServerStatus.GAME_ENDED && player.isOnGround()) {
				player.getAdventureSettings().set(Type.FLYING, true);
			}
			player.setGamemode(2);
			player.getAdventureSettings().set(Type.ALLOW_FLIGHT, true);
			player.getAdventureSettings().set(Type.ATTACK_PLAYERS, false);
			player.setAllowModifyWorld(false);
			player.setAllowInteract(false, false);
			player.getFoodData().setLevel(20);
			player.getFoodData().sendFoodLevel(player.getFoodData().getMaxLevel());
			player.setHealth(player.getMaxHealth());
			player.removeAllEffects();
			player.getInventory().clearAll();
			player.getInventory().remove(player.getInventory().getItemInHand());
		}
	}
	
	public void sendGameEndMessages() {
		GamePlayerManager manager = DodoCraft.services().get(GamePlayerManager.class);
		LevelManager level = DodoCraft.services().get(LevelManager.class);
		
		for(Player player : DodoCraft.players()) {
			PlayerInfo info = manager.getPlayerInfo(player);
			
			int coins = info.getCoins();
			int kills = info.getKills();
			
			String message = "";
			
			message += "\n&r" + DodoCraft.services().get(TeamManager.class).getWinMessage(winner).get();
			message += "\n&r&e" + "Aantal Kills: " + (kills == 0 ? "&c&l" : "&a&l") + kills;
			message += "\n&r&e" + "Aantal Coins: " + (coins == 0 ? "&c&l" : "&6&l") + coins;
			message += "\n&r";
			message += "\n&7" + level.getLevelInfo(player);
			message += "\n&r";
			
			player.sendMessage(TextFormat.colorize('&', message));
 		}
	}
	
	private void playFirework() {
		new NukkitRunnable() {
			int ticks = 0;
			
			@Override
			public void run() {
				if (ticks >= 20) {
					this.cancel();
					return;
				}
				
				for(Player winners : winner.getMembers()) {
					if(!winners.isOnline()) continue;
					DodoFirework firework = new DodoFirework();
					firework.setColor(DyeColor.LIGHT_BLUE);
					firework.setType(ExplosionType.SMALL_BALL);
					firework.spawn(new DodoLocation(winners.getLocation().clone().add(0,1,0)));
				}
				
				ticks++;
			}
		}.runTaskTimer(DodoBedWars.getInstance(), 20, 20);
	}	

}
