package nl.dodocraft.bedwars.game.generators;

import java.util.ArrayList;
import java.util.List;

import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.EventPriority;
import cn.nukkit.event.block.BlockPlaceEvent;
import cn.nukkit.event.inventory.InventoryPickupItemEvent;
import cn.nukkit.event.player.PlayerMoveEvent;
import cn.nukkit.level.Location;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.team.TeamManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.helpers.LocationHelper;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.world.DodoLocation;

public class GeneratorManager extends DodoService {
	
	private List<Generator> registeredGenerators = new ArrayList<>();
	
	@Override
	public void init() throws Exception {
		DodoConfig config = new DodoConfig(DodoBedWars.getInstance());
		
		for (GeneratorType type : GeneratorType.values()) {
			List<String> locations = config.getStringList(type.name());
			if (locations == null) continue;
			
			for (String stringLocation : locations) {
				DodoLocation location = LocationHelper.deserialize(stringLocation);
				registeredGenerators.add(new Generator(type, location));
			}
		}
	}
	
	public List<Generator> getRegisteredGenerators() {
		return this.registeredGenerators;
	}
	
	public void startAll() {
		for (Generator generator : registeredGenerators) {
			generator.start();
		}
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		if (allEnabled()) return;
		
		Location location = event.getTo();
		for (Generator generator : registeredGenerators) {
			if (generator.getLocation().distance(location) > 3) continue;
			if (generator.isEnabled()) continue;
			
			generator.setEnabled(true);
		}
	}
	
	@EventHandler
	public void onPickUp(InventoryPickupItemEvent event) {
		Location location = event.getItem().getLocation();
		
		for (Generator generator : registeredGenerators) {
			if (generator.getLocation().distance(location) > 3) continue;
			generator.clearAmount();
			
			if (!generator.getGeneratorType().isShareDrops()) continue;
			
			Player player = event.getViewers()[0];
			if (player == null) continue;
			TeamManager manager = DodoCraft.services().get(TeamManager.class);
			Player teammate = manager.getTeammate(player);
			if (teammate == null) continue;
			
			if (teammate.getLocation().distance(generator.getLocation()) > 3) continue;
			teammate.getInventory().addItem(generator.getGeneratorType().getItem());
			return;
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPickUpRemoveCustomName(InventoryPickupItemEvent event) {
		if (!(event.getItem().getItem().hasCustomName() && event.getItem().getItem().getCustomName().equalsIgnoreCase(Generator.ITEM_NAME))) return;
		event.getItem().getItem().clearCustomName();
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Location location = event.getBlock().getLocation().clone();
		
		for (Generator generator : registeredGenerators) {
			if (location.distance(generator.getLocation()) > 3) continue;
			event.setCancelled(true);
			BedWarsMessage.BUILD_TOO_CLOSE.build().replace(
					new MessageReplaceValue("%TYPE%", "Generator")
			).send(event.getPlayer());
			return;
		}
	}
	
	private boolean allEnabled() {
		for (Generator generator : registeredGenerators) {
			if (generator.isEnabled()) continue;
			return false;
		}
		
		return true;
	}
	
}
