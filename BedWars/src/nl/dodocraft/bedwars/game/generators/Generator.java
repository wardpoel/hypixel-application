package nl.dodocraft.bedwars.game.generators;

import java.util.ArrayDeque;
import java.util.Queue;

import cn.nukkit.item.Item;
import cn.nukkit.math.Vector3;
import cn.nukkit.scheduler.NukkitRunnable;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.common.server.ServerStatus;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.helpers.RandomHelper;
import nl.dodocraft.utils.visual.hologram.DodoHologram;
import nl.dodocraft.utils.world.DodoLocation;

public class Generator {
	
	public static final String ITEM_NAME = "GENERATOR_ITEM";
	
	private GeneratorType type;
	private DodoLocation location;
	private DodoHologram hologram;
	private Queue<Integer> interval;
	private Queue<Integer> time;
	private int amount;
	private boolean enabled = false;
	
	public Generator(GeneratorType type, DodoLocation location) {
		this.type = type;
		this.location = location.clone().add(0, 2.5, 0);
		this.amount = 0;
		
		if (!isMidGenerator()) {
			interval = new ArrayDeque<>();
			interval.add(7);
			interval.add(10);
			interval.add(20);
			
			time = new ArrayDeque<>();
			time.add(30 * 20);
			time.add(60 * 20);
			time.add(Integer.MAX_VALUE);
		}
	}
	
	public void start() {
		if (isMidGenerator()) {
			hologram = new DodoHologram(location.clone().add(0, 2, 0), "");
			hologram.spawnToAll();
			midGenerator();
			return;
		}
		
		enabled = true;
		normalGenerator();
	}
	
	private void normalGenerator() {
		int tick = 20;
		if (!interval.isEmpty()) {
			tick = interval.poll();
		}
		
		int tickInterval = tick;
		new NukkitRunnable() {
			int itemTicks = 0;
			int ticksToDo = time.poll();
			int ticks = 0;
			@Override
			public void run() {
				if (!enabled) {
					return;
				}
				
				if (ticks > ticksToDo) {
					this.cancel();
					normalGenerator();
					return;
				}
				
				if (DodoCore.getInstance().getServerApi().getStatus() != ServerStatus.IN_GAME) {
					this.cancel();
					return;
				}
				
				if (itemTicks == type.getInterval() && amount < type.getMaxDrops()) {
					drop(location, type.getItem());
					amount++;
				}
				
				itemTicks++;
				if (isFull() || itemTicks > type.getInterval()) {
					itemTicks = 0;
				}
				
				ticks += tickInterval;
			}
		}.runTaskTimer(DodoBedWars.getInstance(), tickInterval, tickInterval);
	}
	
	private void midGenerator() {
		int blocks = 5;
		new NukkitRunnable() {
			int ticks = 0;
			@Override
			public void run() {
				if (!enabled) {
					return;
				}
				
				if (DodoCore.getInstance().getServerApi().getStatus() != ServerStatus.IN_GAME) {
					hologram.destroy();
					this.cancel();
					return;
				}
				
				String line = "&8[&b";
				for (int i = 0; i < ticks; i++) {
					line += Unicode.SQUARE;
				}
				line += (isFull() ? "&c" : "&7");
				for (int i = 0; i < blocks - ticks; i++) {
					line += Unicode.SQUARE;
				}
				line += "&8]";
				
				hologram.setLine(0, line);
				
				
				if (ticks == blocks && amount < type.getMaxDrops()) {
					drop(location, type.getItem());
					amount++;
				}
				
				ticks++;
				if (isFull() || ticks > blocks) {
					ticks = 0;
				}
			}
		}.runTaskTimer(DodoBedWars.getInstance(), (int) (type.getInterval() * 1.0 / blocks * 20), (int) (type.getInterval() * 1.0 / blocks * 20));
	}
	
	private void drop(DodoLocation location, Item item) {
		double x = RandomHelper.randomDouble() * 0.1 - 0.05;
		double z = RandomHelper.randomDouble() * 0.1 - 0.05;
		location.getLevel().dropItem(new Vector3(location.getX(), location.getY(), location.getZ()), type.getItem().setCustomName(ITEM_NAME), new Vector3(x,0.075,z));
	}
	
	private boolean isFull() {
		return amount == type.getMaxDrops();
	}
	
	public void clearAmount() {
		this.amount = 0;
	}
	public DodoLocation getLocation() {
		return this.location;
	}
	
	public GeneratorType getGeneratorType() {
		return this.type;
	}
	
	private boolean isMidGenerator() {
		return type == GeneratorType.DIAMOND;
	}
	
	public boolean isEnabled() {
		return this.enabled;
	}
	
	public void setEnabled(boolean value) {
		this.enabled = value;
	}

}
