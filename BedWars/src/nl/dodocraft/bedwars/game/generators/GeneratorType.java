package nl.dodocraft.bedwars.game.generators;

import cn.nukkit.item.Item;
import nl.dodocraft.utils.player.inventory.DodoItem;

public enum GeneratorType {

	IRON(Item.IRON_INGOT, true, 48, 2),
	GOLD(Item.GOLD_INGOT, true, 16, 5),
	DIAMOND(Item.DIAMOND, false, 5, 8);
	
	private int itemId;
	private boolean shareDrops;
	private int maxDrops;
	private int interval;

	GeneratorType(int itemId, boolean shareDrops, int maxDrops, int interval) {
		this.itemId = itemId;
		this.shareDrops = shareDrops;
		this.maxDrops = maxDrops;
		this.interval = interval;
	}
	
	public Item getItem() {
		return new DodoItem(itemId).toItemStack();
	}
	
	public boolean isShareDrops() {
		return shareDrops;
	}
	
	public int getMaxDrops() {
		return maxDrops;
	}
	
	public int getInterval() {
		return interval;
	}
	
}
