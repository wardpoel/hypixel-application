package nl.dodocraft.bedwars.game.shop;

public enum ShopValue {
	
	NONE(null),
	DEFAULT(new int[] {8, 16, 32, 64}),
	SPECIAL(new int[] {1, 4, 8});

	private int[] values;
	
	ShopValue(int[] values) {
		this.values = values;
	}
	
	public int[] getValues() {
		return this.values;
	}
	
}
