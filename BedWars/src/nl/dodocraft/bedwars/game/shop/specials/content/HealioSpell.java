package nl.dodocraft.bedwars.game.shop.specials.content;

import cn.nukkit.item.Item;
import cn.nukkit.scheduler.NukkitRunnable;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.BedWarsManager;
import nl.dodocraft.bedwars.game.shop.ShopCurrency;
import nl.dodocraft.bedwars.game.shop.specials.SpecialItem;
import nl.dodocraft.bedwars.game.shop.specials.SpecialItemManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.helpers.RoundHelper;
import nl.dodocraft.utils.player.inventory.DodoClickableItem;
import nl.dodocraft.utils.player.inventory.DodoItem;
import nl.dodocraft.utils.visual.particle.DodoParticle;
import nl.dodocraft.utils.world.DodoLocation;

import java.util.Random;

public class HealioSpell implements SpecialItem {
	
	@Override
	public String getName() {
		return "Healio Spreuk";
	}
	
	@Override
	public DodoItem getItem() {
		return new DodoClickableItem(new DodoItem(Item.PAPER).setName("&r&aHealio Spreuk"), whoClicked -> {
			SpecialItemManager manager = DodoCraft.services().get(SpecialItemManager.class);
			if (!manager.canActivateSpell(whoClicked, getName())) {
				return;
			}
			
			double add = whoClicked.getMaxHealth() * 0.3;
			double health = whoClicked.getHealth();
			if (health + add > whoClicked.getMaxHealth()) {
				add = whoClicked.getMaxHealth() - health;
			}
			whoClicked.setHealth((float) (health + add));
			
			new NukkitRunnable() {
				int ticks = 0;
				@Override
				public void run() {
					if (ticks == 20 || !whoClicked.isOnline()) {
						this.cancel();
						return;
					}
					ticks++;
					DodoParticle.HEART.setLocation(new DodoLocation(whoClicked.getLocation().clone().add(new Random().nextDouble() * 2 - 1, new Random().nextDouble() + 0.75, new Random().nextDouble() * 2 - 1))).display(DodoCraft.allPlayers());
				}
			}.runTaskTimer(DodoBedWars.getInstance(), 0, 1);
		});
	}
	
	@Override
	public int getPrice() {
		return 6;
	}
	
	@Override
	public ShopCurrency getCurrency() {
		return ShopCurrency.DIAMOND;
	}
	
	@Override
	public int getAmount() {
		return 1;
	}
	
}
