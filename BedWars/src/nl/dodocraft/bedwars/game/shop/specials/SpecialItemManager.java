package nl.dodocraft.bedwars.game.shop.specials;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.*;

import cn.nukkit.Player;
import cn.nukkit.entity.projectile.EntitySnowball;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.entity.ProjectileHitEvent;
import cn.nukkit.event.inventory.InventoryPickupItemEvent;
import cn.nukkit.event.player.PlayerQuitEvent;
import cn.nukkit.item.Item;
import cn.nukkit.potion.Effect;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.shop.specials.content.HealioSpell;
import nl.dodocraft.bedwars.game.shop.specials.content.Slowballs;
import nl.dodocraft.bedwars.game.team.TeamManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.helpers.RoundHelper;
import nl.dodocraft.utils.player.inventory.DodoClickableItem;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.world.DodoLocation;

public class SpecialItemManager extends DodoService {
	
	private LocalDate releaseDate = LocalDate.of(2019, Month.AUGUST, 28);
	private SpecialItem specialItemOfTheWeek;
	private List<SpecialItem> registeredSpecialItems = new ArrayList<>();
	
	private static final int COOLDOWN = 20;
	private Map<UUID, Long> spellCooldown = new HashMap<>();
	
	@Override
	public void init() throws Exception {
		// Register specials items
		registeredSpecialItems.add(new Slowballs());
		registeredSpecialItems.add(new HealioSpell());
		
		// Calculate wich item is the item of the week
		//todo remove plusDays()
		long weeksBetween = ChronoUnit.WEEKS.between(releaseDate, LocalDate.now().plusDays(1));
		specialItemOfTheWeek = registeredSpecialItems.get(((int)weeksBetween) % registeredSpecialItems.size());
	}
	
	public SpecialItem getItemOfTheWeek() {
		return this.specialItemOfTheWeek;
	}

	/* SLOWBALLS */
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		if (!(event.getEntity() instanceof EntitySnowball)) return;
		EntitySnowball slowball = (EntitySnowball) event.getEntity();
		if (!(slowball.shootingEntity instanceof Player)) return;
		Player shooter = (Player) slowball.shootingEntity;
		
		DodoLocation location = new DodoLocation(event.getEntity().getLocation());
		TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
		for (Player player : DodoCraft.players()) {
			if (location.distance(player.getLocation()) > 3.5) continue;
			if (teamManager.areTeammates(shooter, player)) continue;
			
			player.attack(1.5f);
			player.addEffect(Effect.getEffect(Effect.SLOWNESS).setAmplifier(2).setDuration(4 * 20));
		}
	}
	
	/* SPELLS */
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		this.spellCooldown.remove(event.getPlayer().getUniqueId());
	}
	
	@EventHandler
	public void onPickUp(InventoryPickupItemEvent event) {
		Item item = event.getItem().getItem();
		if (!(item.hasCustomName() && item.getCustomName().contains("Spreuk"))) return;
		if (!(specialItemOfTheWeek.getItem() instanceof DodoClickableItem)) return;
		
		event.getItem().kill();
		((DodoClickableItem)specialItemOfTheWeek.getItem()).give(event.getViewers()[0]);
	}
	
	public boolean canActivateSpell(Player player, String spellName) {
		if (getSpellCooldown(player) > System.currentTimeMillis()) {
			double remainingCooldown = (getSpellCooldown(player) - System.currentTimeMillis()) / 1000.0;
			remainingCooldown = RoundHelper.round(remainingCooldown, 1);
			
			BedWarsMessage.SPELL_IN_COOLDOWN.build().replace(
					new MessageReplaceValue("%SPELL%", "&a&r" + spellName),
					new MessageReplaceValue("%COOLDOWN%", "&a&r" + getColor(remainingCooldown))
			).send(player);
			
			return false;
		}
		
		setInSpellCooldown(player);
		return true;
	}
	
	private long getSpellCooldown(Player player) {
		if (!this.spellCooldown.containsKey(player.getUniqueId())) {
			return 0L;
		}
		
		return this.spellCooldown.get(player.getUniqueId());
	}
	
	private void setInSpellCooldown(Player player) {
		this.spellCooldown.put(player.getUniqueId(), System.currentTimeMillis() + COOLDOWN * 1000);
	}
	
	private String getColor(double value) {
		TextFormat color = TextFormat.DARK_GREEN;
		
		if (value > 20)
			color = TextFormat.DARK_RED;
		else if (value > 15)
			color = TextFormat.RED;
		else if (value > 10)
			color = TextFormat.GOLD;
		else if (value > 5)
			color = TextFormat.GREEN;
		
		return color + "" + TextFormat.BOLD + value;
	}
	
}
