package nl.dodocraft.bedwars.game.shop.specials.content;

import cn.nukkit.item.Item;
import nl.dodocraft.bedwars.game.shop.ShopCurrency;
import nl.dodocraft.bedwars.game.shop.specials.SpecialItem;
import nl.dodocraft.utils.player.inventory.DodoItem;

public class Slowballs implements SpecialItem {
	
	@Override
	public String getName() {
		return "Slowball";
	}
	
	@Override
	public DodoItem getItem() {
		return new DodoItem(Item.SNOWBALL).setAmount(5).setName("&r&aSlowball");
	}
	
	@Override
	public int getPrice() {
		return 5;
	}
	
	@Override
	public ShopCurrency getCurrency() {
		return ShopCurrency.GOLD;
	}
	
	@Override
	public int getAmount() {
		return 5;
	}
	
}
