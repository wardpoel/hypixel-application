package nl.dodocraft.bedwars.game.shop.specials;

import nl.dodocraft.bedwars.game.shop.ShopCurrency;
import nl.dodocraft.utils.player.inventory.DodoItem;

public interface SpecialItem {
	
	String getName();
	DodoItem getItem();
	int getPrice();
	ShopCurrency getCurrency();
	int getAmount();
	
}
