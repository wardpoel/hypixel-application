package nl.dodocraft.bedwars.game.shop;

import cn.nukkit.item.Item;
import nl.dodocraft.utils.player.inventory.DodoItem;

public enum ShopItem {

	STONE_SWORD("Stenen Zwaard", Item.STONE_SWORD, 5, ShopCurrency.GOLD, ShopType.GEAR, ShopValue.NONE, true),
	IRON_SWORD("IJzeren Zwaard", Item.IRON_SWORD, 20, ShopCurrency.GOLD, ShopType.GEAR, ShopValue.NONE, true),
	DIAMOND_SWORD("Diamanten Zwaard", Item.DIAMOND_SWORD, 8, ShopCurrency.DIAMOND, ShopType.GEAR, ShopValue.NONE,false),
	CHAIN_ARMOR("Maliën Harnas", -1, 32, ShopCurrency.IRON, ShopType.GEAR, ShopValue.NONE, false),
	IRON_ARMOR("IJzeren Harnas", -1, 32, ShopCurrency.GOLD, ShopType.GEAR, ShopValue.NONE, false),
	DIAMOND_ARMOR("Diamanten Harnas", -1, 15, ShopCurrency.DIAMOND, ShopType.GEAR, ShopValue.NONE, false),
	BOW("Boog", Item.BOW, 3, ShopCurrency.DIAMOND, ShopType.GEAR, ShopValue.NONE, false),
	ARROW("Pijl", Item.ARROW, 1, ShopCurrency.GOLD, ShopType.GEAR, ShopValue.DEFAULT, false),
	WOOL("Wol", Item.WOOL, 0.75, ShopCurrency.IRON, ShopType.BLOCKS, ShopValue.DEFAULT,true),
	CLAY("Aardewerk", Item.STAINED_HARDENED_CLAY, 1, ShopCurrency.IRON, ShopType.BLOCKS, ShopValue.DEFAULT,true),
	OBSIDIAN("Lavaglas", Item.OBSIDIAN, 5, ShopCurrency.GOLD, ShopType.BLOCKS, ShopValue.SPECIAL,false),
	IRON_PICKAXE("IJzeren Pikhouweel", Item.IRON_PICKAXE, 10, ShopCurrency.GOLD, ShopType.EXTRA, ShopValue.NONE,false),
	DIAMOND_PICKAXE("Diamanten Pikhouweel", Item.DIAMOND_PICKAXE, 10, ShopCurrency.DIAMOND, ShopType.EXTRA, ShopValue.NONE,false),
	SHEAR("Schaar", Item.SHEARS, 5, ShopCurrency.GOLD, ShopType.EXTRA, ShopValue.NONE,false),
	GOLDEN_APPLE("Gouden Appel", Item.GOLDEN_APPLE, 10, ShopCurrency.GOLD, ShopType.EXTRA, ShopValue.NONE,true),
	ENDERPEARL("Enderparel", Item.ENDER_PEARL, 8, ShopCurrency.DIAMOND, ShopType.EXTRA, ShopValue.NONE,true),
	EXPLOSIVE_EGG("Explosive Egg", Item.EGG, 20, ShopCurrency.IRON, ShopType.EXTRA, ShopValue.NONE, false);
	
	private String name;
	private int itemId;
	private double price;
	private ShopCurrency currency;
	private ShopType type;
	private ShopValue value;
	private boolean inQuickShop;
	
	ShopItem(String name, int itemId, double price, ShopCurrency currency, ShopType type, ShopValue value, boolean inQuickShop) {
		this.name = name;
		this.itemId = itemId;
		this.price = price;
		this.currency = currency;
		this.type = type;
		this.value = value;
		this.inQuickShop = inQuickShop;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Item getItem() {
		return new DodoItem(itemId).toItemStack();
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public ShopCurrency getCurrency() {
		return this.currency;
	}
	
	public ShopType getShopType() {
		return this.type;
	}
	
	public ShopValue getShopValue() {
		return this.value;
	}
	
	public boolean isInQuickShop() {
		return inQuickShop;
	}

}
