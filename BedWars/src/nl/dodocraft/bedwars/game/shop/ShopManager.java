package nl.dodocraft.bedwars.game.shop;

import java.util.ArrayList;
import java.util.List;

import javax.naming.ConfigurationException;

import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.block.BlockPlaceEvent;
import cn.nukkit.inventory.PlayerInventory;
import cn.nukkit.item.Item;
import cn.nukkit.level.Location;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.bedwars.BedWarsResource;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.BedWarsManager;
import nl.dodocraft.bedwars.game.shop.specials.SpecialItem;
import nl.dodocraft.bedwars.game.team.TeamManager;
import nl.dodocraft.bedwars.menu.BedWarsMenu;
import nl.dodocraft.bedwars.player.GamePlayerManager;
import nl.dodocraft.bedwars.player.PlayerInfo;
import nl.dodocraft.bedwars.server.BedWarsServerManager;
import nl.dodocraft.common.server.ServerStatus;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.helpers.LocationHelper;
import nl.dodocraft.utils.player.inventory.DodoClickableItem;
import nl.dodocraft.utils.player.inventory.DodoItem;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NPCManager;
import nl.dodocraft.utils.world.DodoLocation;

public class ShopManager extends DodoService{
	
	private static final int[] BLOCKS_TO_COLOR = {
			ShopItem.WOOL.getItem().getId(),
			ShopItem.CLAY.getItem().getId(),
			//ShopItem.GLASS.getItem().getId(),
	};
	
	private NPCManager npcManager = null;
	private List<DodoLocation> locations = new ArrayList<>();
	
	@Override
	public void init() throws Exception {
		npcManager = DodoCraft.services().get(NPCManager.class);
		DodoConfig config = DodoCraft.services().get(BedWarsServerManager.class).getConfig();
		
		List<String> locs = config.getStringList("shops");
		for (String loc : locs) {
			locations.add(LocationHelper.deserialize(loc));
		}
		
		if(locations.isEmpty()) {
			throw new ConfigurationException("No Shop Locations configured");
		}
	}
	
	public void spawnShops() {		
		for(DodoLocation location : locations) {
			DodoNPC shop = new DodoNPC(location, BedWarsResource.SHOP, TextFormat.colorize('&', "&b&lShop"), null, 1f, whoClicked -> {
				if(DodoCraft.services().get(BedWarsManager.class).getStatus() != ServerStatus.IN_GAME) return;
				BedWarsMenu.SHOP_OVERVIEW.open(whoClicked);
			});
			
			DodoCraft.services().get(NPCManager.class).register(shop);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Location location = event.getBlock().getLocation().clone();
		
		for (DodoNPC npc : npcManager.getRegistered()) {
			if (location.distance(npc.getLocation()) > 3) continue;
			event.setCancelled(true);
			BedWarsMessage.BUILD_TOO_CLOSE.build().replace(
					new MessageReplaceValue("%TYPE%", "Shop")
			).send(event.getPlayer());
			return;
		}
	}
	
	public boolean canBuy(Player player, ShopItem item) {
		return canBuy(player, item, 1);
	}
	
	public boolean canBuy(Player player, ShopItem item, int amount) {
		int price = (int) (item.getPrice() * amount);
		
		return getCurrencyInInventory(player, item.getCurrency()) >= price;
	}
	
	public boolean hasEnoughCurrency(Player player, ShopCurrency currency, int amount) {
		return getCurrencyInInventory(player, currency) >= amount;
	}
	
	public void handleBuy(Player player, ShopItem item) {
		handleBuy(player, item, 1);
	}
	
	public void handleBuy(Player player, ShopItem item, int amount) {
		int price = (int) (item.getPrice() * amount);
		if (!canBuy(player, item, amount)) {
			sendMessageNotEnoughCurrency(player, item.getCurrency(), price);
			return;
		}
		
		pay(player, item.getCurrency(), price);
		
		// Check if bought item is Armor
		if (item == ShopItem.CHAIN_ARMOR || item == ShopItem.IRON_ARMOR || item == ShopItem.DIAMOND_ARMOR) {
			PlayerInfo info = DodoCraft.services().get(GamePlayerManager.class).getPlayerInfo(player);
			info.unlockArmor(item);
			
			Item leggings = new DodoItem(Item.CHAIN_LEGGINGS).toItemStack();
			Item boots = new DodoItem(Item.CHAIN_BOOTS).toItemStack();
			
			if (item == ShopItem.DIAMOND_ARMOR) {
				leggings = new DodoItem(Item.DIAMOND_LEGGINGS).toItemStack();
				boots = new DodoItem(Item.DIAMOND_LEGGINGS).toItemStack();
			} else if (item == ShopItem.IRON_ARMOR) {
				leggings = new DodoItem(Item.IRON_LEGGINGS).toItemStack();
				boots = new DodoItem(Item.IRON_BOOTS).toItemStack();
			}
			
			player.getInventory().setLeggings(leggings);
			player.getInventory().setBoots(boots);
			return;
		}
		
		// Add bought item to inventory
		Item toAdd = item.getItem();
		boolean color = false;
		for (int id : BLOCKS_TO_COLOR) {
			if (toAdd.getId() == id) {
				color = true;
				break;
			}
		}
		
		if (color) {
			TeamManager manager = DodoCraft.services().get(TeamManager.class);
			toAdd.setDamage(manager.getBedId(manager.getTeam(player)));
		}
		
		if (item == ShopItem.EXPLOSIVE_EGG) {
			toAdd.setCustomName(TextFormat.colorize('&', "&r&cExplosive Egg"));
		}
		
		toAdd.setCount(amount);
		player.getInventory().addItem(toAdd);
	}
	
	public void handleSpecialItemBuy(Player player, SpecialItem item) {
		if (!hasEnoughCurrency(player, item.getCurrency(), item.getPrice())) {
			sendMessageNotEnoughCurrency(player, item.getCurrency(), item.getPrice());
			return;
		}
		
		pay(player, item.getCurrency(), item.getPrice());
		
		// Add bought item to inventory
		if (item.getItem() instanceof DodoClickableItem) {
			((DodoClickableItem) item.getItem()).give(player);
			return;
		}
		
		player.getInventory().addItem(item.getItem().toItemStack());
	}
	
	public int getCurrencyInInventory(Player player, ShopCurrency currency) {
		PlayerInventory inventory = player.getInventory();
		int amount = 0;
		int slots = inventory.getSize();
		for (int i = 0; i < slots; i++) {
			Item slotItem = inventory.getItem(i);
			if (slotItem == null) continue;
			if (slotItem.getId() != currency.getItem().getId()) continue;
			amount += slotItem.getCount();
		}
		
		return amount;
	}
	
	private void pay(Player player, ShopCurrency currency, int price) {
		PlayerInventory inventory = player.getInventory();
		
		// Remove correct amount from inventory
		int has = getCurrencyInInventory(player, currency);
		Item toRemove = currency.getItem();
		toRemove.setCount(price);
		inventory.remove(toRemove);
		
		Item cur = currency.getItem();
		cur.setCount(has - price);
		inventory.addItem(cur);
	}
	
	private void sendMessageNotEnoughCurrency(Player player, ShopCurrency currency, int price) {
		BedWarsMessage.MENU_SHOP_NOT_ENOUGH_MONEY.build().replace(
				new MessageReplaceValue("%COLOR%", currency.getColor()),
				new MessageReplaceValue("%AMOUNT%", price - getCurrencyInInventory(player, currency) + ""),
				new MessageReplaceValue("%CURRENCY%", currency.getName())
		).send(player);
	}
	
}
