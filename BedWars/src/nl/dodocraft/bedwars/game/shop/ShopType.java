package nl.dodocraft.bedwars.game.shop;

public enum ShopType {

	QUICK_SHOP("Quick Shop", "&5"),
	GEAR("Uitrusting", "&1"),
	BLOCKS("Blokken", "&1"),
	EXTRA("Overig", "&1");
	
	private String name;
	private String color;
	
	ShopType(String name, String color) {
		this.name = name;
		this.color = color;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}

}
