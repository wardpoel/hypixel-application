package nl.dodocraft.bedwars.game.shop;

import java.util.ArrayList;
import java.util.List;

import cn.nukkit.Player;
import cn.nukkit.block.Block;
import cn.nukkit.entity.projectile.EntityEgg;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.entity.EntityExplodeEvent;
import cn.nukkit.event.entity.ProjectileHitEvent;
import cn.nukkit.level.Explosion;
import nl.dodocraft.bedwars.player.GamePlayerManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.world.DodoLocation;

public class ItemManager extends DodoService {
	
	@Override
	public void init() throws Exception {
	
	}
	
	// Explosive eggs
	@EventHandler
	public void onExplosiveEgg(ProjectileHitEvent event) {
		if (!(event.getEntity() instanceof EntityEgg)) return;
		EntityEgg egg = (EntityEgg) event.getEntity();
		if (!(egg.shootingEntity instanceof Player)) return;
		Player shooter = (Player) egg.shootingEntity;
		
		Explosion explosion = new Explosion(egg.getLocation(), 1.8f, shooter);
		explosion.explodeA();
		explosion.explodeB();
	}
	
	@EventHandler
	public void onExplode(EntityExplodeEvent event) {
		List<Block> blocks = event.getBlockList();
		List<Block> newBlocks = new ArrayList<>(blocks);
		GamePlayerManager manager = DodoCraft.services().get(GamePlayerManager.class);
		
		for (Block block : blocks) {
			if (block.getId() != Block.STAINED_HARDENED_CLAY && manager.getPlacedBlocks().contains(new DodoLocation(block.getLocation()))) continue;
			newBlocks.remove(block);
		}
		
		event.setBlockList(newBlocks);
	}
	
}
