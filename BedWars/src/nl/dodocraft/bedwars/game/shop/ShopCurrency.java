package nl.dodocraft.bedwars.game.shop;

import cn.nukkit.item.Item;
import nl.dodocraft.utils.player.inventory.DodoItem;

public enum ShopCurrency {

	IRON("IJzer", Item.IRON_INGOT, "&f"),
	GOLD("Goud", Item.GOLD_INGOT, "&6"),
	DIAMOND("Diamanten", Item.DIAMOND, "&9");
	
	private String name;
	private int itemId;
	private String color;
	
	ShopCurrency(String name, int itemId, String color) {
		this.name = name;
		this.itemId = itemId;
		this.color = color;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Item getItem() {
		return new DodoItem(itemId).toItemStack();
	}
	
	public String getColor() {
		return this.color;
	}

}
