package nl.dodocraft.bedwars.game.rewards;

public enum RewardType {
	
	KILL(0, 0),
	BED_DESTROY(15, 15),
	WIN(25, 60),
	ELIMINATION_KILL(8, 10),
	LOSS(0, 30);
	
	private int coins;
	private int experience;
	
	RewardType(int coins, int experience) {
		this.coins = coins;
		this.experience = experience;
	}
	
	public int getCoins() {
		return coins;
	}
	
	public int getExperience() {
		return experience;
	}
	
}
