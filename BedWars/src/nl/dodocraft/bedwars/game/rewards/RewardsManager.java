package nl.dodocraft.bedwars.game.rewards;

import cn.nukkit.Player;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.player.GamePlayerManager;
import nl.dodocraft.bedwars.player.PlayerInfo;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.game.boosters.BoosterManager;
import nl.dodocraft.utils.game.levels.LevelManager;
import nl.dodocraft.utils.service.DodoService;

public class RewardsManager extends DodoService{
	
	@Override
	public void init() throws Exception {}
	
	public void updateRewards(RewardType type, Player player, Player victim) {
		PlayerInfo info = DodoCraft.services().get(GamePlayerManager.class).getPlayerInfo(player);
		LevelManager manager = DodoCraft.services().get(LevelManager.class);
		
		int coins = DodoCraft.services().get(BoosterManager.class).getBoostedAmount(type.getCoins());
		info.addCoins(coins);
		
		// Sending new experience to Core.
		if (type.getExperience() > 0) {
			manager.addExperience(player, DodoGamemode.BEDWARS, type.getExperience());
		}
		
		if (type == RewardType.KILL) {
			info.addKills(1);
		}
		
		if(type == RewardType.ELIMINATION_KILL) {
			info.addKills(1);
			Message.GAME_REWARD_KILL_ELIMINATION.build().replace(
					new MessageReplaceValue("%COINS%", "" + coins),
					new MessageReplaceValue("%PLAYER%", victim.getName())
			).send(player);
		}
		
		if(type == RewardType.WIN) {
			Message.GAME_REWARD_WIN.build().replace(
				new MessageReplaceValue("%COINS%", "" + coins)
			).send(player);			
		}
		
		if (type == RewardType.BED_DESTROY) {
			BedWarsMessage.BED_KILL_REWARD.build().replace(
					new MessageReplaceValue("%COINS%", "" + coins)
			).send(player);
		}
	}
	
	public void sendDataCore(Player player) {
		PlayerInfo info = DodoCraft.services().get(GamePlayerManager.class).getPlayerInfo(player);
		if (info == null) return;
		DodoCore.getInstance().changeCoins(player.getUniqueId(), info.getCoins(), null);
	}
	
}
