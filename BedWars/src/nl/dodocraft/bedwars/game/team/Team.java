package nl.dodocraft.bedwars.game.team;

import cn.nukkit.Player;
import nl.dodocraft.bedwars.player.GamePlayerManager;
import nl.dodocraft.bedwars.player.PlayerInfo;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.minigame.team.BaseTeam;
import nl.dodocraft.utils.minigame.team.BaseTeamManager;
import nl.dodocraft.utils.world.DodoLocation;

public class Team extends BaseTeam{

	private DodoLocation bedLocation;
	
	public Team(int number, DodoLocation spawnLocation, DodoLocation bedLocation) {
		super(number, spawnLocation);
		this.bedLocation = bedLocation;
	}
	
	@Override
	public BaseTeamManager getTeamManager() {
		return DodoCraft.services().get(TeamManager.class);
	}
	
	@Override
	public int getAliveCount() {
		int alive = 0;

		for(Player player : this.getMembers()) {
			// Ignore if player isn't online.
			if(!(player.isOnline())) continue;
						
			PlayerInfo info = DodoCraft.services().get(GamePlayerManager.class).getPlayerInfo(player);			
			if(!info.isEliminated()) alive++;
		}

		return alive;
	}

	@Override
	public String getColor() {
		switch(getNumber()) {
			case 1:
				return "&c";
			case 2:
				return "&e";
			case 3:
				return "&a";
			case 4:
				return "&9";
			default:
				return null;
		}
	}

	public String getName() {
		switch(getNumber()) {
			case 1:
				return "Rood";
			case 2:
				return "Geel";
			case 3:
				return "Groen";
			case 4:
				return "Blauw";
			default:
				return null;
		}
	}
	
	public DodoLocation getBedLocation() {
		return this.bedLocation;
	}

}
