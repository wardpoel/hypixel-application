package nl.dodocraft.bedwars.game.team;

import java.util.concurrent.atomic.AtomicInteger;

import javax.naming.ConfigurationException;

import nl.dodocraft.bedwars.game.BedWarsManager;
import nl.dodocraft.bedwars.server.BedWarsServerManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.minigame.GameType;
import nl.dodocraft.utils.minigame.team.BaseTeam;
import nl.dodocraft.utils.minigame.team.BaseTeamManager;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class TeamManager extends BaseTeamManager {
	
	@Override
	protected void runSetup() throws ConfigurationException {
		// Load teams
		for(int i = 0; i <= this.getMaxTeams(); i++) {
			String key = "teams.team" + i + ".spawn";
					
			// Ignore team if no spawn configured
			if(!getConfig().exists(key)) continue;
					
			DodoLocation location = getConfig().get(key, DodoLocation.class);
			if(location == null) {
				throw new ConfigurationException("Invalid spawn location");
			}
			
			key = "teams.team" + i + ".bed";
			if(!getConfig().exists(key)) continue;
			
			DodoLocation bedLocation = getConfig().get(key, DodoLocation.class);
			if(bedLocation == null) {
				throw new ConfigurationException("Invalid bed location");
			}
					
			this.teams.add(new Team(i, location.clone().add(0.5, 1, 0.5), bedLocation));
		}
				
		if(this.teams.isEmpty()) {
			throw new ConfigurationException("No teams configured");
		}
				
		// Set current place
		this.currentPlace = new AtomicInteger(this.teams.size());
	}

	@Override
	public int getTeamSize() {
		return getGameType().getTeamSize();
	}

	@Override
	public DodoConfig getConfig() {
		return DodoCraft.services().get(BedWarsServerManager.class).getConfig();
	}

	@Override
	public GameType getGameType() {
		return DodoCraft.services().get(BedWarsManager.class).getGameType();
	}
	
	public BaseTeam getTeamByNumber(int id) {
		for (BaseTeam team : teams) {
			if (team.getNumber() == id) {
				return team;
			}
		}
		
		return null;
	}
	
	public BaseTeam getTeamByBedId(int bedId) {
		for (BaseTeam team : teams) {
			if (getBedId(team) == bedId) {
				return team;
			}
		}
		
		return null;
	}
	
	public int getBedId(BaseTeam team) {
		switch(team.getNumber()) {
			case 1:
				return 14;
			case 2:
				return 4;
			case 3:
				return 5;
			case 4:
				return 11;
			default:
				return 0;
		}
	}
	
}
