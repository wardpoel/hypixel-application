package nl.dodocraft.bedwars.game.respawn;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import cn.nukkit.Player;
import cn.nukkit.level.Sound;
import cn.nukkit.potion.Effect;
import cn.nukkit.scheduler.NukkitRunnable;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.BedWarsManager;
import nl.dodocraft.bedwars.game.team.TeamManager;
import nl.dodocraft.bedwars.player.GamePlayerManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.helpers.RoundHelper;
import nl.dodocraft.utils.helpers.SoundHelper;
import nl.dodocraft.utils.minigame.team.BaseTeam;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.world.DodoLocation;

public class RespawnManager extends DodoService{

	private Set<UUID> inRespawn = new HashSet<>();
	
	@Override
	public void init() throws Exception {}

	public void startRespawn(Player player) {
		if (!player.isOnline()) return;
		
		inRespawn.add(player.getUniqueId());
		
		new NukkitRunnable() {
            double time = 7.5;
           
			@Override
			public void run() {
				if(time <= 0.0) {
					TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
					BaseTeam team = teamManager.getTeam(player);
					if (team == null) return;
					
					// Teleport player to spawnpoint
					DodoLocation location = team.getSpawn();
					player.teleport(location);
					
					// Send actionbar
					BedWarsMessage.GAME_RESPAWNED_ACTIONBAR.build().sendActionBar(player);
					
					// setAlive and give items
					GamePlayerManager manager = DodoCraft.services().get(GamePlayerManager.class);
					manager.getPlayerInfo(player).setAlive(true);
					manager.giveItems(player);
					
					// Update player's settings
					DodoCraft.services().get(BedWarsManager.class).updatePlayer(player);
					
					// Notify teamplayers with sound
					for (Player teamPlayer : team.getMembers()) {
						SoundHelper.playSound(teamPlayer, Sound.RANDOM_POTION_BREWED, 0.5f, 1f);
					}
					
					// Give spawn protection
					player.addEffect(Effect.getEffect(Effect.DAMAGE_RESISTANCE).setAmplifier(10).setDuration(6 * 20));
					new NukkitRunnable() {
						@Override
						public void run() {
							if (!player.isOnline()) return;
							player.addEffect(Effect.getEffect(Effect.DAMAGE_RESISTANCE).setAmplifier(2).setDuration(4 * 20));
						}
					}.runTaskLater(DodoBedWars.getInstance(), 6 * 20);
					
					// Remove player from respawn after 1 tick.
					new NukkitRunnable() {
						@Override
						public void run() {
							inRespawn.remove(player.getUniqueId());
						}
					}.runTaskLater(DodoBedWars.getInstance(), 20);
					
					// Cancel task
					this.cancel();
					return;
				}
				
                double rounded = RoundHelper.round(this.time, 1);
                String colorCode = "&4";
        		if (rounded <= 1) {
        			colorCode = "&c";
        		} else if (rounded <= 3) {
        			colorCode = "&6";
        		} else if (rounded <= 9) {
        			colorCode = "&a";
        		}
                
        		if(rounded == 1.0 || rounded == 2.0 || rounded == 3.0 || rounded == 4.0 || rounded == 5.0) {
	        		/** BedWarsMessage.GAME_RESPAWN_TIME_TITLE.build().replace(
	        				new MessageReplaceValue("%TIME%", colorCode + "&l" + rounded)
	        		).sendTitle(player); **/
        		}
        		
        		BedWarsMessage.GAME_RESPAWN_TIME_ACTIONBAR.build().replace(
        				new MessageReplaceValue("%TIME%", colorCode + "&l" + rounded)
        		).sendActionBar(player);
                
                time -= 0.1;
				
			}
		}.runTaskTimer(DodoBedWars.getInstance(), 2, 2);
	}
	
	public boolean inRespawn(UUID uuid) {
		return inRespawn.contains(uuid);
	}
	
}
