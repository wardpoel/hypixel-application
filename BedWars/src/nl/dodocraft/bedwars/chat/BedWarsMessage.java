package nl.dodocraft.bedwars.chat;

import nl.dodocraft.utils.chat.DodoMessageBuilder;
import nl.dodocraft.utils.chat.MessageEnum;
import nl.dodocraft.utils.chat.Unicode;

public enum BedWarsMessage implements MessageEnum {

	// COMMANDS
	COMMAND_SETSPAWN_INCORRECT_ID("&c&l" + Unicode.DOUBLE_RIGHT + " Ongeldig ID. &r&cKies een nummer tussen 1 en %MAX%!"),
	COMMAND_SETSPAWN_SUCCESS("&a&l" + Unicode.DOUBLE_RIGHT + " &r&7Het Spawnpoint met &a&lID[=%ID%] &r&7is succesvol neergezet op jouw huidige locatie."),
	COMMAND_SETSPAWN_LOBBY_SUCCESS("&a&l" + Unicode.DOUBLE_RIGHT + " &r&7Je hebt de Lobbyspawn succesvol neergezet op jouw huidige locatie."),
	COMMAND_SETMAPNAME_SUCCESS("&a&l" + Unicode.DOUBLE_RIGHT + " &r&7Je hebt succesvol de map naam aangepast naar &a&l%NAME%&r&7."),
	COMMAND_ADDGENERATOR_SUCCESS("&a&l" + Unicode.DOUBLE_RIGHT + " &r&7Je hebt succesvol een nieuwe generator toegevoegd."),
	COMMAND_ADDSHOP_SUCCESS("&a&l" + Unicode.DOUBLE_RIGHT + " &r&7Je hebt succesvol een nieuwe shop toegevoegd."),
	
	// RESPAWN
	GAME_RESPAWN_TIME_ACTIONBAR("&e&lOVER %TIME%&r&e&ls RESPAWN JE!"),
	GAME_RESPAWNED_ACTIONBAR("&a&lJE LEEFT WEER!"),
	
	ACTIONBAR_TOPLAYER_ALIVE("&c&lEliminatie! &rEr leven nog &a&l%ALIVE_COUNT% &rspeler%S% van %TEAM%&r."),
	ACTIONBAR_TOPLAYER_ALIVE_ONE("&c&lEliminatie! &rEr leeft nog &a&l%ALIVE_COUNT% &rspeler%S% van %TEAM%&r."),
	ACTIONBAR_TOPLAYER_ELIMINATED("&c&lEliminatie! &rTeam &e%TEAM% &ris uitgeschakeld."),
	
	BED_KILL_TITLE("%COLOR%&lR.I.P."),
	BED_KILL_SUBTITLE("&7Je bed is gesloopt."),
	BED_KILL_MESSAGE("&c&l" + Unicode.DOUBLE_RIGHT + " &r&7Het bed van %TEAMCOLOR%&l%TEAMNAME% &r&7is gesloopt door %COLOR%&l%NAME%&r&7."),
	BED_KILL_TEAMMESSAGE("&r\n&c&l" + Unicode.DOUBLE_RIGHT + " &r&7Jouw bed is gesloopt door %COLOR%&l%NAME%&r&7.\n&r&7Als je nu dood gaat respawn je niet meer!\n&r"),
	BED_KILL_OWN_BED("&c&l" + Unicode.DOUBLE_RIGHT + " &r&7Je kan je eigen bed niet slopen."),
	BED_KILL_REWARD("&6+ %COINS% Coins (Bed Gesloopt!)"),
	
	MENU_SHOP_ITEM_BUTTON("&8&l%AMOUNT%%NAME%\n&r%COLOR%%PRICE%%CURRENCY%"),
	MENU_SHOP_ITEM_SPECIAL_BUTTON("&d&lSpecial: &r&8&l%AMOUNT%x %NAME%\n&r%COLOR%%PRICE% %CURRENCY%"),
	MENU_SHOP_NOT_ENOUGH_MONEY("&c&l" + Unicode.DOUBLE_RIGHT + " &r&7Je mist %COLOR%&l%AMOUNT% %CURRENCY% &r&7om dit item te kopen."),
	
	BUILD_TOO_CLOSE("&c&l" + Unicode.DOUBLE_RIGHT + " &r&7Je kan niet zo dicht bij een %TYPE% bouwen."),
	
	SPELL_IN_COOLDOWN("&c&l" + Unicode.DOUBLE_RIGHT + " &r%SPELL% &r&7zit in Cooldown! Je moet nog %COOLDOWN%s &r&7wachten.");
	
	private String message;
	
	private BedWarsMessage(String message) {
		this.message = message;
	}
	
	@Override
	public DodoMessageBuilder build() {
		return new DodoMessageBuilder(message);
	}

}