package nl.dodocraft.bedwars.player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import cn.nukkit.Player;
import cn.nukkit.block.Block;
import cn.nukkit.block.BlockBed;
import cn.nukkit.entity.Entity;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.block.BlockBreakEvent;
import cn.nukkit.event.block.BlockPlaceEvent;
import cn.nukkit.event.entity.EntityDamageByEntityEvent;
import cn.nukkit.event.entity.EntityDamageEvent;
import cn.nukkit.event.inventory.CraftItemEvent;
import cn.nukkit.event.inventory.InventoryPickupItemEvent;
import cn.nukkit.event.player.PlayerBedEnterEvent;
import cn.nukkit.event.player.PlayerDeathEvent;
import cn.nukkit.event.player.PlayerDropItemEvent;
import cn.nukkit.event.player.PlayerFoodLevelChangeEvent;
import cn.nukkit.event.player.PlayerInteractEvent;
import cn.nukkit.event.player.PlayerMoveEvent;
import cn.nukkit.inventory.Inventory;
import cn.nukkit.item.Item;
import cn.nukkit.item.ItemBootsLeather;
import cn.nukkit.item.ItemChestplateLeather;
import cn.nukkit.item.ItemHelmetLeather;
import cn.nukkit.item.ItemLeggingsLeather;
import cn.nukkit.level.Sound;
import cn.nukkit.network.protocol.ContainerClosePacket;
import cn.nukkit.potion.Effect;
import cn.nukkit.scheduler.NukkitRunnable;
import cn.nukkit.utils.DyeColor;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.BedWarsManager;
import nl.dodocraft.bedwars.game.respawn.RespawnManager;
import nl.dodocraft.bedwars.game.rewards.RewardType;
import nl.dodocraft.bedwars.game.rewards.RewardsManager;
import nl.dodocraft.bedwars.game.shop.ShopItem;
import nl.dodocraft.bedwars.game.team.Team;
import nl.dodocraft.bedwars.game.team.TeamManager;
import nl.dodocraft.common.log.CoreLogger;
import nl.dodocraft.common.log.LogType;
import nl.dodocraft.common.server.ServerStatus;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.helpers.SoundHelper;
import nl.dodocraft.utils.minigame.team.BaseTeam;
import nl.dodocraft.utils.player.inventory.DodoItem;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.world.DodoLocation;

public class GamePlayerManager extends DodoService {
	
	private final static int[] ALLOWED_TO_BREAK = {Block.GLASS, Block.GLASS_PANE, Block.STAINED_GLASS, Block.STAINED_GLASS_PANE};
	
	private Map<UUID, PlayerInfo> players = new HashMap<>();
	private Set<DodoLocation> placedBlocks = new HashSet<>();
	private BaseTeam lastEliminated = null;
	
	@Override
	public void init() throws Exception {
		
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		
		if (manager.getStatus() != ServerStatus.IN_GAME) {
			event.setCancelled(true);
			return;
		}
		
		if (!(event.getDamager() instanceof Player) || !(event.getEntity() instanceof Player)) return;
		Player damager = (Player) event.getDamager();
		Player victim = (Player) event.getEntity();
		
		// Get Player info for victim
		PlayerInfo info = getPlayerInfo(victim);
		
		// Ignore if on the same team or damager isn't alive.
		TeamManager teams = DodoCraft.services().get(TeamManager.class);
		if (teams.areTeammates(damager, victim) || getPlayerInfo(damager).isEliminated()) {
			event.setCancelled(true);
			return;
		}
		
		info.setLastDamager(damager);
		
		if (victim.getHealth() - event.getDamage() <= 0.0) {
			event.setCancelled(true);
			manager.killPlayer(victim, false);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		
		if (manager.getStatus() != ServerStatus.IN_GAME) {
			event.setCancelled(true);
			return;
		}
		
		if (!(event.getEntity() instanceof Player)) return;
		Player player = (Player) event.getEntity();
		if (player.getHealth() - event.getDamage() <= 0.0) {
			event.setCancelled(true);
			manager.killPlayer(player, false);
		}
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		event.setCancelled(true);
		event.setDeathMessage("");
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		manager.killPlayer(event.getEntity(), false);
	}
	
	@EventHandler
	public void onPlayerCraftingItem(CraftItemEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onEnterBed(PlayerBedEnterEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onEnterBedClick(PlayerInteractEvent event) {
		if (!(event.getBlock() instanceof BlockBed)) return;
		if (event.getAction() != PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onFoodLevelChange(PlayerFoodLevelChangeEvent event) {
		event.setCancelled(true);
		event.setFoodLevel(20);
	}
	
	@EventHandler
	public void onMoveBeforeGame(PlayerMoveEvent event) {
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		if (manager.getStatus() == ServerStatus.IN_GAME) return;
		
		Player player = event.getPlayer();
		if (player.getLocation().getY() < (manager.getLobbySpawn().getY() - 30)) {
			player.teleport(manager.getLobbySpawn());
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		
		// Ignore if game hasn't started.
		if (manager.getStatus() != ServerStatus.IN_GAME) {
			event.setCancelled(true);
			return;
		}
		
		// Ignore if player isn't alive
		if (getPlayerInfo(event.getPlayer()).isEliminated()) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPickup(InventoryPickupItemEvent event) {
		Inventory inventory = event.getInventory();
		if (!(inventory.getHolder() instanceof Player)) return;
		Player player = (Player) inventory.getHolder();
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		
		// Ignore if game hasn't started.
		if (manager.getStatus() != ServerStatus.IN_GAME) {
			event.setCancelled(true);
			return;
		}
		
		// Ignore if player isn't alive
		if (getPlayerInfo(player).isEliminated()) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		placedBlocks.add(new DodoLocation(event.getBlock().getLocation()));
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if (event.getPlayer().getGamemode() == 1) return;
		if (event.getBlock() instanceof BlockBed) return;
		
		for (int id : ALLOWED_TO_BREAK) {
			if (id != event.getBlock().getId()) continue;
			return;
		}
		
		DodoLocation location = new DodoLocation(event.getBlock().getLocation());
		if (!placedBlocks.contains(location)) {
			event.setCancelled(true);
			return;
		}
		
		placedBlocks.remove(location);
	}
	
	@EventHandler
	public void onBedKill(BlockBreakEvent event) {
		if (!(event.getBlock() instanceof BlockBed)) {
			return;
		}
		
		event.setDrops(new Item[]{});
		
		Player breaker = event.getPlayer();
		BlockBed bed = (BlockBed) event.getBlock();
		int bedId = bed.getDyeColor().getWoolData();

		if (bedId == 0) return;

		TeamManager manager = DodoCraft.services().get(TeamManager.class);
		BaseTeam team = manager.getTeamByBedId(bedId);
		if (team == null) return;
		if (!team.isAlive()) return;

		// Can't kill own bed
		if (team.isMember(breaker)) {
			event.setCancelled(true);
			BedWarsMessage.BED_KILL_OWN_BED.build().send(breaker);
			return;
		}

		team.setAlive(false);
		
		lastEliminated = team;
		
		new NukkitRunnable() {
			@Override
			public void run() {
				// reset lastEliminated team, when lastEliminated team is still t.
				if(lastEliminated != null && lastEliminated.equals(team)) {
					lastEliminated = null;
				}
			}
		}.runTaskLater(DodoBedWars.getInstance(), 10 * 20); // remove after 10 ticks

		BaseTeam breakerTeam = manager.getTeam(breaker);
		if (breakerTeam == null) return;

		for (Player player : team.getMembers()) {
			if (!player.isOnline()) continue;
			
			// Notify team players with a sound
			SoundHelper.playSound(player, Sound.MOB_ENDERDRAGON_GROWL, 0.5f, 1f);
			
			player.sendTitle(BedWarsMessage.BED_KILL_TITLE
							.build()
							.replace(new MessageReplaceValue("%COLOR%", breakerTeam.getColor()))
							.get(),
					BedWarsMessage.BED_KILL_SUBTITLE.build().get()
			);

			BedWarsMessage.BED_KILL_TEAMMESSAGE.build().replace(
					new MessageReplaceValue("%COLOR%", breakerTeam.getColor()),
					new MessageReplaceValue("%NAME%", breaker.getName())
			).send(player);
		}

		for (BaseTeam t : manager.getTeams()) {
			if (t.getNumber() == team.getNumber()) continue;
			
			for (Player player : t.getMembers()) {
				if (!player.isOnline()) continue;
				
				// Play sound to notify a bed has been killed
				SoundHelper.playSound(player, Sound.MOB_ENDERDRAGON_GROWL, 0.2f, 1f);

				BedWarsMessage.BED_KILL_MESSAGE.build().replace(
						new MessageReplaceValue("%TEAMCOLOR%", team.getColor()),
						new MessageReplaceValue("%TEAMNAME%", team.getName()),
						new MessageReplaceValue("%COLOR%", breakerTeam.getColor()),
						new MessageReplaceValue("%NAME%", breaker.getName())
				).send(player);
			}
		}

		RewardsManager rewardsManager = DodoCraft.services().get(RewardsManager.class);
		rewardsManager.updateRewards(RewardType.BED_DESTROY, breaker, null);
	}
	
	private static final int[] DISABLED_BLOCK = {
			Block.WORKBENCH,
			Block.ANVIL,
			Block.HOPPER_BLOCK,
	};
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		for (int id : DISABLED_BLOCK) {
			if (id == event.getBlock().getId()) {
				event.setCancelled(true);
				ContainerClosePacket pk = new ContainerClosePacket();
				pk.windowId = -1;
				event.getPlayer().dataPacket(pk);
				return;
			}
		}
	}
	
	public TextFormat getHealthColor(Player target) {
		TextFormat color = TextFormat.GRAY;
		
		// Gray Color if target isn't online.
		if (!target.isOnline()) return color;
		
		// Gray Color if target isn't alive.
		PlayerInfo info = getPlayerInfo(target);
		if (info.isEliminated()) return color;
		
		float health = target.getHealth();
		if (health == 20)
			return TextFormat.DARK_GREEN;
		else if (health > 16)
			return TextFormat.GREEN;
		else if (health > 12)
			return TextFormat.YELLOW;
		else if (health > 8)
			return TextFormat.GOLD;
		else if (health > 4)
			return TextFormat.RED;
		else if (health < 4)
			return TextFormat.DARK_RED;
		
		return color;
	}
	
	public void startActionBar() {
		TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
		RespawnManager respawnManager = DodoCraft.services().get(RespawnManager.class);
		
		new NukkitRunnable() {
			@Override
			public void run() {
				if (DodoCraft.services().get(BedWarsManager.class).getStatus() != ServerStatus.IN_GAME) {
					this.cancel();
					return;
				}
				
				for (UUID uuid : players.keySet()) {
					if (respawnManager.inRespawn(uuid)) continue;
					Player player = DodoCraft.getPlayer(uuid);
					if (player == null) continue;
					
					BaseTeam team = teamManager.getTeam(player);
					Player teammate = teamManager.getTeammate(player);
					PlayerInfo info = null;
					if (teammate != null) {
						info = getPlayerInfo(teammate);
					}
					
					String bar = team.getColor() + (info != null ? (info.isEliminated() ? "" + TextFormat.STRIKETHROUGH : "") + teammate.getName() : team.getName());
					bar += "&r   ";
					
					for (BaseTeam t : teamManager.getTeams()) {
						String bed = (!t.isAlive() && !t.hasAlivePlayers() ? "&7" : t.getColor()) + (t.isAlive() ? Unicode.SQUARE : Unicode.DOTTED_SQUARE);
						bar += bed;
					}
					
					bar += "&r   " + (team.isAlive() ? "&aBed" : "&cGeen Bed");
					
					player.sendActionBar(TextFormat.colorize('&', lastEliminated != null ? getEliminatedTeamLine() + "\n" + bar : getNearestPlayer(player) + "\n" + bar));
				}
			}
		}.runTaskTimer(DodoBedWars.getInstance(), 0, 2);
	}
	
	private String getEliminatedTeamLine() {
		BaseTeam team = this.lastEliminated;
		
		if(!team.isAlive() && !team.hasAlivePlayers()) {
			return BedWarsMessage.ACTIONBAR_TOPLAYER_ELIMINATED.build().replace(
				new MessageReplaceValue("%TEAM%", team.getColor() + team.getName())
			).get();
		} else if(team.getAliveCount() == 1) {
			return BedWarsMessage.ACTIONBAR_TOPLAYER_ALIVE_ONE.build().replace(
				new MessageReplaceValue("%ALIVE_COUNT%", team.getAliveCount() + ""),
				new MessageReplaceValue("%S%", ""),
				new MessageReplaceValue("%TEAM%", team.getColor() + team.getName())
			).get();
		} else if(team.getAliveCount() >= 2) {
			return BedWarsMessage.ACTIONBAR_TOPLAYER_ALIVE.build().replace(
				new MessageReplaceValue("%ALIVE_COUNT%", team.getAliveCount() + ""),
				new MessageReplaceValue("%S%", "s"),
				new MessageReplaceValue("%TEAM%", team.getColor() + team.getName())
			).get();
		} else {}
		
		return "";
	}
	
	private String getNearestPlayer(Player player) {
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		TeamManager teamManager = DodoCraft.services().get(TeamManager.class);
		
		String s = "";
		int aliveEnemies = manager.getAlivePlayers() - teamManager.getTeam(player).getAliveCount();
		if(aliveEnemies >= 3) {
			return s;
		}
		
		Player target = this.getNearest(player);
		if(target != null) {
			int distance = (int) player.distance(target.getLocation());
			
			s = Message.GAME_NEAREST_PLAYER.build().replace(
				new MessageReplaceValue("%PLAYER%", teamManager.getTeam(target).getColor() + target.getName()),
				new MessageReplaceValue("%BLOCKS%", (distance < 10 ? "&c" : "&a") + distance + (distance == 1 ? " blok" : " blokken"))
			).get();
		}
		
		return s;
	}
	
	public void giveItems(Player player) {
		if (!player.isOnline()) return;
		
		player.getInventory().clearAll();
		player.getCraftingGrid().clearAll();
		player.getCursorInventory().clearAll();
		
		PlayerInfo info = getPlayerInfo(player);
		if (info == null) return;
		
		TeamManager manager = DodoCraft.services().get(TeamManager.class);
		BaseTeam team = manager.getTeam(player);
		if (team == null) return;
		DyeColor color = DyeColor.getByWoolData(manager.getBedId(team));
		
		player.getInventory().setItem(0, new DodoItem(Item.WOODEN_SWORD).toItemStack());
		
		player.getInventory().setHelmet(new DodoItem(new ItemHelmetLeather().setColor(color)).toItemStack());
		player.getInventory().setChestplate(new DodoItem(new ItemChestplateLeather().setColor(color)).toItemStack());
		
		if (info.getUnlockedArmor() == null) {
			player.getInventory().setLeggings(new DodoItem(new ItemLeggingsLeather().setColor(color)).toItemStack());
			player.getInventory().setBoots(new DodoItem(new ItemBootsLeather().setColor(color)).toItemStack());
			return;
		}
		
		Item leggings = new DodoItem(Item.CHAIN_LEGGINGS).toItemStack();
		Item boots = new DodoItem(Item.CHAIN_BOOTS).toItemStack();
		
		if (info.getUnlockedArmor() == ShopItem.DIAMOND_ARMOR) {
			leggings = new DodoItem(Item.DIAMOND_LEGGINGS).toItemStack();
			boots = new DodoItem(Item.DIAMOND_LEGGINGS).toItemStack();
		} else if (info.getUnlockedArmor() == ShopItem.IRON_ARMOR) {
			leggings = new DodoItem(Item.IRON_LEGGINGS).toItemStack();
			boots = new DodoItem(Item.IRON_BOOTS).toItemStack();
		}
		
		player.getInventory().setLeggings(leggings);
		player.getInventory().setBoots(boots);
	}
	
	public Player getNearest(Player player) {
		TeamManager manager = DodoCraft.services().get(TeamManager.class);
		double distance = Double.POSITIVE_INFINITY;
		Player target = null;
		
		for (Entity entity : player.getLevel().getEntities()) {
			// Ignore if entity isn't EntityPlayer.
			if (!(entity instanceof Player)) continue;
			Player playerTarget = (Player) entity;
			
			// Ignore if player and targets are teammates or target isn't alive.
			PlayerInfo info = getPlayerInfo(playerTarget);
			if (info == null) continue;
			if (manager.areTeammates(player, playerTarget) || info.isEliminated() || !info.isAlive()) continue;
			double distanceTo = player.getLocation().distance(playerTarget.getLocation());
			
			if (distanceTo > distance) {
				continue;
			}
			
			distance = distanceTo;
			target = playerTarget;
		}
		
		return target;
	}
	
	
	public PlayerInfo getPlayerInfo(Player player) {
		PlayerInfo info = players.get(player.getUniqueId());
		
		if (info == null) {
			info = new PlayerInfo(player);
			players.put(player.getUniqueId(), info);
		}
		
		return info;
	}
	
	public Set<DodoLocation> getPlacedBlocks() {
		return this.placedBlocks;
	}
	
}
