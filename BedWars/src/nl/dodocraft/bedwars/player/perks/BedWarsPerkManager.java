package nl.dodocraft.bedwars.player.perks;

import java.util.HashMap;
import java.util.Map;

import cn.nukkit.Player;
import cn.nukkit.entity.Entity;
import cn.nukkit.entity.mob.EntitySilverfish;
import cn.nukkit.entity.projectile.EntityArrow;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.block.BlockBreakEvent;
import cn.nukkit.event.block.BlockPlaceEvent;
import cn.nukkit.event.entity.ProjectileHitEvent;
import cn.nukkit.event.entity.ProjectileLaunchEvent;
import cn.nukkit.event.inventory.InventoryPickupItemEvent;
import cn.nukkit.event.player.PlayerEatFoodEvent;
import cn.nukkit.item.Item;
import cn.nukkit.item.food.Food;
import cn.nukkit.level.Explosion;
import cn.nukkit.potion.Effect;
import nl.dodocraft.bedwars.game.generators.Generator;
import nl.dodocraft.bedwars.game.generators.GeneratorManager;
import nl.dodocraft.bedwars.game.team.Team;
import nl.dodocraft.bedwars.game.team.TeamManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.game.perks.Perk;
import nl.dodocraft.utils.game.perks.PerkManager;
import nl.dodocraft.utils.helpers.RandomHelper;
import nl.dodocraft.utils.minigame.team.BaseTeam;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.visual.entity.DodoEntity;
import nl.dodocraft.utils.world.DodoLocation;

public class BedWarsPerkManager extends DodoService {
	
	private PerkManager perkManager = DodoCraft.services().get(PerkManager.class);
	
	private Map<EntityArrow, Player> shooters = new HashMap<>();
	
	@Override
	public void init() throws Exception {
	
	}
	
	// BedBugs
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Perk perk = Perk.BEDBUGS;
		DodoLocation location = new DodoLocation(event.getBlock().getLocation());
		
		for (BaseTeam baseTeam : DodoCraft.services().get(TeamManager.class).getTeams()) {
			if (!(baseTeam instanceof Team)) continue;
			Team team = (Team) baseTeam;
			if (!team.isAlive()) continue;
			if (team.isMember(event.getPlayer())) continue;
			if (team.getBedLocation().distance(location) > 8) continue;
			
			int percentage = 0;
			for (Player teamPlayer : team.getMembers()) {
				int perkLevel = perkManager.getLevel(teamPlayer.getUniqueId(), perk);
				if (perkLevel == 0) continue;
				
				percentage += perk.getInstance().getPercentage(perkLevel);
			}
			
			if (!RandomHelper.randomPercentage(percentage)) return;
			
			DodoLocation spawnLocation = new DodoLocation(event.getBlock().getLocation().clone().add(0.5, 1.2, 0.5));
			DodoEntity entity = new DodoEntity(EntitySilverfish.NETWORK_ID).setName(team.getColor() + "Bedwants");
			entity.spawn(spawnLocation);
			return;
		}
	}
	
	// Greedy
	@EventHandler
	public void onItemPickUp(InventoryPickupItemEvent event) {
		Perk perk = Perk.GREEDY;
		if (!(event.getItem().getItem().hasCustomName() && event.getItem().getItem().getCustomName().equalsIgnoreCase(Generator.ITEM_NAME))) return;
		DodoLocation location = new DodoLocation(event.getItem().getLocation());
		
		for (Generator generator : DodoCraft.services().get(GeneratorManager.class).getRegisteredGenerators()) {
			if (generator.getLocation().distance(location) >= 3) continue;
			
			// Pickup is nearby a generator
			Player player = event.getViewers()[0];
			if (player == null) return;
			
			int perkLevel = perkManager.getLevel(player.getUniqueId(), perk);
			if (perkLevel == 0) return;
			
			int percentage = perk.getInstance().getPercentage(perkLevel);
			if (!RandomHelper.randomPercentage(percentage)) return;
			
			event.getItem().getItem().setCount(2);
			return;
		}
	}
	
	// Builder
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Perk perk = Perk.BUILDER;
		Player player = event.getPlayer();
		
		int perkLevel = perkManager.getLevel(player.getUniqueId(), perk);
		if (perkLevel == 0) return;
		
		int percentage = perk.getInstance().getPercentage(perkLevel);
		if (!RandomHelper.randomPercentage(percentage)) return;
		
		Item item = event.getItem().clone();
		item.setCount(1);
		player.getInventory().addItem(item);
	}
	
	// Bomber
	@EventHandler
	public void onArrowShoot(ProjectileLaunchEvent event) {
		Perk perk = Perk.BOMBER;
		if (!(event.getEntity() instanceof EntityArrow)) return;
		EntityArrow arrow = (EntityArrow) event.getEntity();
		
		if (!(arrow.shootingEntity instanceof Player)) return;
		Player shooter = (Player) arrow.shootingEntity;
		
		int perkLevel = perkManager.getLevel(shooter.getUniqueId(), perk);
		if (perkLevel == 0) return;
		
		int percentage = perk.getInstance().getPercentage(perkLevel);
		if (!RandomHelper.randomPercentage(percentage)) return;
		
		shooters.put(arrow, shooter);
	}
	
	@EventHandler
	public void onArrowHit(ProjectileHitEvent event) {
		Entity arrow = event.getEntity();
		DodoLocation location = new DodoLocation(arrow.getLocation());
		
		if (!shooters.containsKey(arrow)) return;
		Player shooter = shooters.get(arrow);
		shooters.remove(arrow);
		
		Explosion explosion = new Explosion(location, 1.5F, shooter);
		explosion.explodeA();
		explosion.explodeB();
	}
	
	// Sugar Rush
	@EventHandler
	public void onGoldenAppleEat(PlayerEatFoodEvent event) {
		if (event.getFood() != Food.apple_golden) return;
		Perk perk = Perk.SUGAR_RUSH;
		Player player = event.getPlayer();
		
		int perkLevel = perkManager.getLevel(player.getUniqueId(), perk);
		if (perkLevel == 0) return;
		
		int percentage = perk.getInstance().getPercentage(perkLevel);
		if (!RandomHelper.randomPercentage(percentage)) return;
		
		player.addEffect(Effect.getEffect(Effect.SPEED).setAmplifier(2).setDuration(10 * 20));
	}
	
}
