package nl.dodocraft.bedwars.player;

import java.util.Optional;
import java.util.UUID;

import cn.nukkit.Player;
import cn.nukkit.scheduler.NukkitRunnable;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.bedwars.game.shop.ShopItem;
import nl.dodocraft.utils.DodoCraft;

public class PlayerInfo {

	private UUID owner;
	
	private Runnable damagerTask;
	private boolean isAlive = true;
	private boolean isEliminated = false;
	private Player lastDamager = null;
	
	private int coins = 0;
	private int kills = 0;
	private float health;
	private ShopItem armor = null;
	
	public PlayerInfo(Player player) {
		this.owner = player.getUniqueId();
	}
	
	public boolean isAlive() {
		return isAlive;
	}
	
	public boolean isEliminated() {
		return isEliminated;
	}
	
	public void setEliminated(boolean eliminated) {
		this.isEliminated = eliminated;
	}
	
	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	
	public Player getLastDamager() {
		return lastDamager;
	}
	
	public void setLastDamager(Player damager) {
		this.lastDamager = damager;
		startDamagerTimeout();
	}
	
	private void startDamagerTimeout() {
	    if(damagerTask != null) {
	        ((NukkitRunnable) damagerTask).cancel();
	    }

	    damagerTask = new NukkitRunnable() {
	        @Override
	        public void run() {
	            setLastDamager(null);
	        }
	    }.runTaskLater(DodoBedWars.getInstance(), 20 * 30);
	}
	
	public void addCoins(int amount) {
		this.coins += amount;
		
		Optional<Player> player = DodoCraft.server().getPlayer(this.owner);
		if(player.isPresent()) {
			//DodoCore.getInstance().getServerApi().addCoins(player.get(), amount);
		}
	}
	
	public int getCoins() {
		return coins;
	}

	public void addKills(int amount) {
		this.kills += amount;
	}
	
	public int getKills() {
		return kills;
	}
	
	public void setKills(int amount) {
		this.kills = amount;
	}
	
	public float getHealth() {
		return this.health;
	}
	
	public void setHealth(float health) {
		this.health = health;
	}
	
	public void changeHealth(float health) {
		this.health += health;
	}
	
	public void unlockArmor(ShopItem item) {
		this.armor = item;
	}
	
	public boolean hasUnlockedArmor(ShopItem shopItem) {
		return this.armor == shopItem;
	}
	
	public ShopItem getUnlockedArmor() {
		return this.armor;
	}
	
}
