package nl.dodocraft.bedwars.server;

import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.common.log.CoreLogger;
import nl.dodocraft.common.log.LogType;
import nl.dodocraft.common.server.Server;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.service.DodoService;

public class BedWarsServerManager extends DodoService {

	private DodoConfig config = new DodoConfig(DodoBedWars.getInstance());
	
	@Override
	public void init() throws Exception {
		Server current = DodoCore.getInstance().getServerApi().getServer();
		boolean found = false;
		
		for (BedWarsServerType type : BedWarsServerType.values()) {
			if (type.getServer() != current) continue;
			
			DodoCraft.services().register(type.getGameManager());
			found = true;
			break;
		}
		
		if(!found) {
			CoreLogger.log(LogType.ERROR, "Unable to find manager for server " + current.name() + "!");
		}
	}
	
	public DodoConfig getConfig() {
		return config;
	}

}
