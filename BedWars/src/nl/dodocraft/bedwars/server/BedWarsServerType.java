package nl.dodocraft.bedwars.server;

import nl.dodocraft.bedwars.game.BedWarsManager;
import nl.dodocraft.common.server.Server;
import nl.dodocraft.utils.service.DodoService;

public enum BedWarsServerType {

	SOLO(Server.BEDWARS_SOLO, BedWarsManager.class),
	DUO(Server.BEDWARS_TEAM, BedWarsManager.class);
	
	private Server server;
	private Class<? extends DodoService> gameManager;
	
	BedWarsServerType(Server server, Class<? extends DodoService> gameManager) {
		this.server = server;
		this.gameManager = gameManager;
	}
	
	public Server getServer() {
		return this.server;
	}
	
	public Class<? extends DodoService> getGameManager() {
		return this.gameManager;
	}
}
