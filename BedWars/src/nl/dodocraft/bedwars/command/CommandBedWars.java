package nl.dodocraft.bedwars.command;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.bedwars.command.admin.CommandAddGenerator;
import nl.dodocraft.bedwars.command.admin.CommandAddShop;
import nl.dodocraft.bedwars.command.admin.CommandForceStart;
import nl.dodocraft.bedwars.command.admin.CommandSetMapName;
import nl.dodocraft.bedwars.command.admin.CommandSetSpawn;
import nl.dodocraft.common.Rank;
import nl.dodocraft.utils.command.DodoCommand;
import nl.dodocraft.utils.command.DodoSubCommand;

public class CommandBedWars extends DodoCommand {

	@Override
	public String getCommand() {
		return "bedwars";
	}
	
	@Override
	public DodoSubCommand[] getSubCommands() {
		return new DodoSubCommand[]{
			new CommandSetSpawn(),
			new CommandSetMapName(),
			new CommandForceStart(),
			new CommandAddGenerator(),
			new CommandAddShop()
		};
	}

	@Override
	public Rank getRequiredRank() {
		return Rank.ADMIN;
	}

	@Override
	public String getUsage() {
		return "/bedwars";
	}

	@Override
	public String getDescription() {
		return "BedWars Commands";
	}

	@Override
	public void onPlayerUse(Player player, String[] args) throws Exception {
		player.sendMessage(TextFormat.colorize('&', "&3&l" + getDescription()));
		for(DodoSubCommand cmd : getSubCommands()) {
			player.sendMessage(TextFormat.colorize('&', "&f - &3" + cmd.getUsage() + " &7" + cmd.getDescription()));
		}
	}

	@Override
	public void onConsoleUse(CommandSender console, String[] args) throws Exception {
		
	}

}
