package nl.dodocraft.bedwars.command.admin;

import java.util.List;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.generators.GeneratorType;
import nl.dodocraft.common.Rank;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.command.DodoSubCommand;
import nl.dodocraft.utils.helpers.LocationHelper;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class CommandAddGenerator extends DodoSubCommand {
	@Override
	public String getCommand() {
		return "addgenerator";
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "addgen" };
	}
	
	@Override
	public Rank getRequiredRank() {
		return Rank.ADMIN;
	}
	
	@Override
	public String getUsage() {
		return "/bedwars addgenerator <type>";
	}
	
	@Override
	public String getDescription() {
		return "Voeg een nieuwe generator toe (type = IRON/GOLD/DIAMOND)";
	}
	
	@Override
	public void onPlayerUse(Player player, String[] strings) throws Exception {
		if (strings.length != 1) return;
		
		DodoConfig config = new DodoConfig(DodoBedWars.getInstance());
		
		GeneratorType type = null;
		try {
			type = GeneratorType.valueOf(strings[0]);
		} catch (IllegalArgumentException e) {
			Message.COMMAND_ERROR.build().replace(
					new MessageReplaceValue("%MESSAGE%", "Die generator ken ik niet")
			).send(player);
			return;
		}
		
		List<String> generators = config.getStringList(type.name());
		generators.add(LocationHelper.serialize(new DodoLocation(player)));
		config.set(type.name(), generators);
		config.save();
		
		BedWarsMessage.COMMAND_ADDGENERATOR_SUCCESS.build().send(player);
	}
	
	@Override
	public void onConsoleUse(CommandSender commandSender, String[] strings) throws Exception {
		Message.COMMAND_MUST_BE_PLAYER.build().send(commandSender);
	}
}
