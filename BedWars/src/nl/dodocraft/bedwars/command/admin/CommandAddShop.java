package nl.dodocraft.bedwars.command.admin;

import java.util.List;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import nl.dodocraft.bedwars.DodoBedWars;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.common.Rank;
import nl.dodocraft.utils.command.DodoSubCommand;
import nl.dodocraft.utils.helpers.LocationHelper;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class CommandAddShop extends DodoSubCommand {
	@Override
	public String getCommand() {
		return "addshop";
	}
	
	@Override
	public Rank getRequiredRank() {
		return Rank.ADMIN;
	}
	
	@Override
	public String getUsage() {
		return "/bedwars addshop";
	}
	
	@Override
	public String getDescription() {
		return "Voeg een nieuwe shop toe.";
	}
	
	@Override
	public void onPlayerUse(Player player, String[] strings) throws Exception {
		DodoConfig config = new DodoConfig(DodoBedWars.getInstance());
		
		List<String> shops = config.getStringList("shops");
		shops.add(LocationHelper.serialize(new DodoLocation(player)));
		config.set("shops", shops);
		config.save();
		
		BedWarsMessage.COMMAND_ADDSHOP_SUCCESS.build().send(player);
	}
	
	@Override
	public void onConsoleUse(CommandSender commandSender, String[] strings) throws Exception {
	
	}
}
