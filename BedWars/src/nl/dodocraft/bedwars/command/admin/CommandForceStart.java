package nl.dodocraft.bedwars.command.admin;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import nl.dodocraft.bedwars.game.BedWarsManager;
import nl.dodocraft.common.Rank;
import nl.dodocraft.common.server.ServerStatus;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.command.DodoSubCommand;

public class CommandForceStart extends DodoSubCommand{

	@Override
	public String getCommand() {
		return "forcestart";
	}

	@Override
	public String[] getAliases() {
		return new String[] { "fs" };
	}
	
	@Override
	public String[] getCommandAliases() {
		return new String[] {"fs", "forcestart"};
	}

	@Override
	public Rank getRequiredRank() {
		return Rank.ADMIN;
	}

	@Override
	public String getUsage() {
		return "/bedwars forcestart";
	}

	@Override
	public String getDescription() {
		return "Je kunt dit commando gebruiken om de game geforceerd te starten.";
	}

	@Override
	public void onPlayerUse(Player player, String[] args) throws Exception {
		onUniversalUse(player);
	}

	@Override
	public void onConsoleUse(CommandSender console, String[] args) throws Exception {
		onUniversalUse(console);
	}
	
	private void onUniversalUse(CommandSender sender) {
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		
		if(DodoCraft.players().size() <= 1 || manager.getStatus() != ServerStatus.WAITING) {
			Message.MINIGAME_COUNTDOWN_SKIP_TIMER_DENIED.build().send(sender);
			return;
		}
		
		if(sender instanceof Player) {
			Player player = (Player) sender;
			DodoCraft.services().get(BedWarsManager.class).forceStartGame(player);
		} else {
			DodoCraft.services().get(BedWarsManager.class).forceStartGame(null);
		}
	}
	
}
