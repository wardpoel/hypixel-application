package nl.dodocraft.bedwars.command.admin;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.server.BedWarsServerManager;
import nl.dodocraft.common.Rank;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.command.DodoSubCommand;
import nl.dodocraft.utils.server.config.DodoConfig;

public class CommandSetMapName extends DodoSubCommand {

	@Override
	public String getCommand() {
		return "setmap";
	}

	@Override
	public String getDescription() {
		return "Stel met dit commando de map naam in.";
	}

	@Override
	public Rank getRequiredRank() {
		return Rank.ADMIN;
	}

	@Override
	public String getUsage() {
		return "/bedwars setmap <name>";
	}

	@Override
	public void onConsoleUse(CommandSender sender, String[] args) throws Exception {
		Message.COMMAND_MUST_BE_PLAYER.build().send(sender);	
	}

	@Override
	public void onPlayerUse(Player player, String[] args) throws Exception {
		DodoConfig config = DodoCraft.services().get(BedWarsServerManager.class).getConfig();
		
		if(args.length == 0) {
			sendUsageMessage(player);
			return;
		}
		
		config.set("settings.mapname", args[0]);
		config.save(true);
		BedWarsMessage.COMMAND_SETMAPNAME_SUCCESS.build().replace(new MessageReplaceValue("%NAME%", args[0])).send(player);
	}

}