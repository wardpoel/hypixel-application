package nl.dodocraft.bedwars.command.admin;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.BedWarsManager;
import nl.dodocraft.bedwars.server.BedWarsServerManager;
import nl.dodocraft.common.Rank;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.command.DodoSubCommand;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class CommandSetSpawn extends DodoSubCommand {

	@Override
	public String getCommand() {
		return "setspawn";
	}

	@Override
	public String getDescription() {
		return "Met dit commando kun je de team en lobby spawns instellen (1 = rood, 2 = geel, 3 = groen, 4 = blauw)";
	}

	@Override
	public Rank getRequiredRank() {
		return Rank.ADMIN;
	}

	@Override
	public String getUsage() {
		return "/bedwars setspawn <nummer/lobby>";
	}

	@Override
	public void onConsoleUse(CommandSender sender, String[] args) throws Exception {
		Message.COMMAND_MUST_BE_PLAYER.build().send(sender);	
	}

	@Override
	public void onPlayerUse(Player player, String[] args) throws Exception {
		DodoConfig config = DodoCraft.services().get(BedWarsServerManager.class).getConfig();
		BedWarsManager manager = DodoCraft.services().get(BedWarsManager.class);
		
		if(args.length == 0 || args.length >= 2) {
			sendUsageMessage(player);
			return;
		}
		
		if(args[0].equals("lobby")) {
			BedWarsMessage.COMMAND_SETSPAWN_LOBBY_SUCCESS.build().send(player);
			config.set("settings.lobbyspawn", new DodoLocation(player));
			config.save(true);
			return;
		}
		
		int id = 0;
		try {
			id = Integer.valueOf(args[0]);
		} catch(Exception e) {
			Message.COMMAND_INVALID_ARGUMENT.build().replace(
				new MessageReplaceValue("%VAR%", "het ID"), 
				new MessageReplaceValue("%CORRECT%", "nummer")
			).send(player);
			return;
		}
		
		if(id >= 1 && id <= manager.getMaxPlayers()) {
			BedWarsMessage.COMMAND_SETSPAWN_SUCCESS.build().replace(
				new MessageReplaceValue("%ID%", "" + id)
			).send(player);
			
			// Set spawn
			config.set("teams.team" + id + ".spawn", new DodoLocation(player));
			config.save(true);
		} else {
			BedWarsMessage.COMMAND_SETSPAWN_INCORRECT_ID.build().replace(
				new MessageReplaceValue("%MAX%", "" + manager.getMaxPlayers())
			).send(player);	
		}
		
	}

}