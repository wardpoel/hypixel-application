package nl.dodocraft.bedwars;

import cn.nukkit.plugin.PluginBase;
import nl.dodocraft.bedwars.command.CommandBedWars;
import nl.dodocraft.bedwars.server.BedWarsServerManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.command.CommandManager;

public class DodoBedWars extends PluginBase {
	
	private static DodoBedWars instance;
	
	@Override
	public void onEnable() {
		instance = this;
		
		// Register services
		DodoCraft.services().register(BedWarsServerManager.class);
		
		//Register commands
		DodoCraft.services().get(CommandManager.class).register(new CommandBedWars());
		
		// Register levels
		// LevelManager levelManager = DodoCraft.services().get(LevelManager.class);
		// levelManager.registerLevel(DodoGamemode.LUCKYLEGENDS, new Level1());
	}
	
	@Override
	public void onDisable() {}
	
	public static DodoBedWars getInstance() {
		return instance;
	}

}
