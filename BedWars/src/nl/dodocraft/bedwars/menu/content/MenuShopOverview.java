package nl.dodocraft.bedwars.menu.content;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.bedwars.game.shop.ShopType;
import nl.dodocraft.bedwars.menu.BedWarsMenu;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuShopOverview extends MenuFormat {
	
	@Override
	public void open(Player player, DataValue... dataValues) {
		
		DodoMenu menu = new DodoMenu(TextFormat.colorize('&', "&8Shop"));
		
		for (ShopType type : ShopType.values()) {
			String text = TextFormat.colorize('&', type.getColor() + "&l" + type.getName() + "\n&r&8Klik om te openen.");
			menu.addButton(text, whoClicked -> {
				BedWarsMenu.SHOP_ITEM.open(whoClicked, type);
			});
		}
		
		menu.open(player);
		
	}
	
}
