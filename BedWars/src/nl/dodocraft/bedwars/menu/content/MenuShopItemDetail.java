package nl.dodocraft.bedwars.menu.content;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.shop.ShopItem;
import nl.dodocraft.bedwars.game.shop.ShopManager;
import nl.dodocraft.bedwars.menu.BedWarsMenu;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuShopItemDetail extends MenuFormat {
	@Override
	public void open(Player player, DataValue... dataValues) {
		if (dataValues.length != 1) return;
		ShopManager manager = DodoCraft.services().get(ShopManager.class);
		ShopItem item = dataValues[0].as(ShopItem.class);
		
		DodoMenu menu = new DodoMenu(TextFormat.colorize('&', "&8Hoeveel wil je er kopen?"));
		
		int[] amounts = item.getShopValue().getValues();
		
		for (int amount : amounts) {
			String text = BedWarsMessage.MENU_SHOP_ITEM_BUTTON.build().replace(
					new MessageReplaceValue("%AMOUNT%", amount + "x "),
					new MessageReplaceValue("%NAME%", item.getName()),
					new MessageReplaceValue("%COLOR%", manager.canBuy(player, item, amount) ? "&2" : "&c"),
					new MessageReplaceValue("%PRICE%", (int) (item.getPrice() * amount) + " "),
					new MessageReplaceValue("%CURRENCY%", item.getCurrency().getName())
			).get();
			menu.addButton(text, whoClicked -> {
				manager.handleBuy(whoClicked, item, amount);
				BedWarsMenu.SHOP_ITEM_DETAIL.open(whoClicked, item);
			});
		}
		
		menu.addButton(TextFormat.colorize('&', "&8&l" + Unicode.DOUBLE_LEFT + " Terug"), whoClicked -> {
			BedWarsMenu.SHOP_ITEM.open(whoClicked, item.getShopType());
		});
		
		menu.open(player);
	}
}
