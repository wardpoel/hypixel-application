package nl.dodocraft.bedwars.menu.content;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.bedwars.chat.BedWarsMessage;
import nl.dodocraft.bedwars.game.shop.ShopItem;
import nl.dodocraft.bedwars.game.shop.ShopManager;
import nl.dodocraft.bedwars.game.shop.ShopType;
import nl.dodocraft.bedwars.game.shop.ShopValue;
import nl.dodocraft.bedwars.game.shop.specials.SpecialItem;
import nl.dodocraft.bedwars.game.shop.specials.SpecialItemManager;
import nl.dodocraft.bedwars.menu.BedWarsMenu;
import nl.dodocraft.bedwars.player.GamePlayerManager;
import nl.dodocraft.bedwars.player.PlayerInfo;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuShopItem extends MenuFormat {
	
	@Override
	public void open(Player player, DataValue... dataValues) {
		if (dataValues.length != 1) return;
		
		ShopManager manager = DodoCraft.services().get(ShopManager.class);
		PlayerInfo info = DodoCraft.services().get(GamePlayerManager.class).getPlayerInfo(player);
		ShopType type = dataValues[0].as(ShopType.class);
		
		DodoMenu menu = new DodoMenu(TextFormat.colorize('&', "&8" + type.getName()));
		
		if (type == ShopType.QUICK_SHOP) {
			SpecialItem item = DodoCraft.services().get(SpecialItemManager.class).getItemOfTheWeek();
			
			String text = BedWarsMessage.MENU_SHOP_ITEM_SPECIAL_BUTTON.build().replace(
					new MessageReplaceValue("%AMOUNT%", item.getAmount() + ""),
					new MessageReplaceValue("%NAME%", item.getName() + ""),
					new MessageReplaceValue("%COLOR%", manager.hasEnoughCurrency(player, item.getCurrency(), item.getPrice()) ? "&2" : "&c"),
					new MessageReplaceValue("%PRICE%", item.getPrice() + ""),
					new MessageReplaceValue("%CURRENCY%", item.getCurrency().getName())
			).get();
			
			menu.addButton(text, whoClicked -> {
				manager.handleSpecialItemBuy(player, item);
				BedWarsMenu.SHOP_ITEM.open(whoClicked, type);
			});
		}
		
		for (ShopItem item : ShopItem.values()) {
			if (!inShop(type, item)) continue;
			
			final int amount = type == ShopType.QUICK_SHOP && item.getShopValue() != ShopValue.NONE ? 16 : 1;
			
			String color = "";
			if (item.getShopValue() != ShopValue.NONE && type != ShopType.QUICK_SHOP) {
				color = item.getCurrency().getColor();
			} else {
				color = manager.canBuy(player, item, amount) ? "&2" : "&c";
			}
			
			boolean showPrice = item.getShopValue() == ShopValue.NONE || type == ShopType.QUICK_SHOP;
			String text = BedWarsMessage.MENU_SHOP_ITEM_BUTTON.build().replace(
					new MessageReplaceValue("%AMOUNT%", (amount == 1 ? "" : amount + "x ")),
					new MessageReplaceValue("%NAME%", item.getName()),
					new MessageReplaceValue("%COLOR%", color),
					new MessageReplaceValue("%PRICE%", showPrice ? (int) (item.getPrice() * amount) + " " : ""),
					new MessageReplaceValue("%CURRENCY%", item.getCurrency().getName())
			).get();
			
			boolean has = false;
			if (info != null && info.hasUnlockedArmor(item)) {
				has = true;
				text = TextFormat.colorize('&', "&8&l" + item.getName() + "\n&r&cVrijgespeeld");
			}
			
			final boolean hasArmor = has;
			menu.addButton(text, whoClicked -> {
				if (hasArmor) {
					BedWarsMenu.SHOP_ITEM.open(whoClicked, type);
					return;
				}
				
				if (showPrice) {
					manager.handleBuy(player, item, amount);
					BedWarsMenu.SHOP_ITEM.open(whoClicked, type);
					return;
				}
				
				BedWarsMenu.SHOP_ITEM_DETAIL.open(whoClicked, item);
			});
		}
		
		menu.addButton(TextFormat.colorize('&', "&8&l" + Unicode.DOUBLE_LEFT + " Terug"), whoClicked -> {
			BedWarsMenu.SHOP_OVERVIEW.open(whoClicked);
		});
		
		menu.open(player);
	}
	
	private boolean inShop(ShopType shop, ShopItem item) {
		if (shop == item.getShopType()) {
			return true;
		}
		
		return shop == ShopType.QUICK_SHOP && item.isInQuickShop();
	}
	
}
