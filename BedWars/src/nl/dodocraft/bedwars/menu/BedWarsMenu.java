package nl.dodocraft.bedwars.menu;

import java.util.ArrayList;
import java.util.List;

import cn.nukkit.Player;
import nl.dodocraft.bedwars.menu.content.MenuShopItem;
import nl.dodocraft.bedwars.menu.content.MenuShopItemDetail;
import nl.dodocraft.bedwars.menu.content.MenuShopOverview;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.utils.visual.menu.MenuEnum;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public enum BedWarsMenu implements MenuEnum {
	
	SHOP_OVERVIEW(MenuShopOverview.class),
	SHOP_ITEM(MenuShopItem.class),
	SHOP_ITEM_DETAIL(MenuShopItemDetail.class);

	private Class<? extends MenuFormat> format;
	
	BedWarsMenu(Class<? extends MenuFormat> format) {
		this.format = format;
	}
	
	@Override
	public void open(Player player) {
		getInstance().open(player);
	}
	
	@Override
	public void open(Player player, Object... objects) {
		List<DataValue> values = new ArrayList<>();
		
		for(Object obj : objects) {
			values.add(new DataValue(obj));
		}
		
		getInstance().open(player, values.toArray(new DataValue[values.size()]));
	}
	
	@Override
	public MenuFormat getInstance(Object... constructorArguments) {
		final Class<?>[] classes = new Class<?>[constructorArguments.length];
		for (int i = 0; i < constructorArguments.length; i++) {
			classes[i] = constructorArguments[i].getClass();
		}
		
		final Object[] parameters = new Object[constructorArguments.length];
		for (int i = 0; i < constructorArguments.length; i++) {
			parameters[i] = constructorArguments[i];
		}
		
		try {
			return format.getConstructor(classes).newInstance(parameters);
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
