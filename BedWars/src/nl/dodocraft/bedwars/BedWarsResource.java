package nl.dodocraft.bedwars;

import cn.nukkit.entity.data.Skin;
import nl.dodocraft.utils.server.ResourceList;

public class BedWarsResource extends ResourceList {

	private static final DodoBedWars PLUGIN = DodoBedWars.getInstance();
	
	public static final Skin SHOP = asSkin(PLUGIN, "skins/shop.png");
	
}
