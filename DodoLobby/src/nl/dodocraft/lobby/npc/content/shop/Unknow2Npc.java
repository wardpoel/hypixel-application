package nl.dodocraft.lobby.npc.content.shop;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NpcConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class Unknow2Npc implements NpcConfig {
	
	@Override
	public DodoNPC getNPC() {
		DodoLocation location = new DodoLocation(-5.486700057983398, 65.5, -6.492000102996826, 316.2341613769531, 13.752598762512207);
		location.setLevel(DodoCraft.server().getLevelByName("world"));
		
		return new DodoNPC(
			location, 
			LobbyResource.NPC_UNKNOWN, 
			TextFormat.colorize('&', "&7&l???"), 
			null, 
			1.05f
		);
	}
	
}
