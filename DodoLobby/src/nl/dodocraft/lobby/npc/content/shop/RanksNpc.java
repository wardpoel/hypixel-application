package nl.dodocraft.lobby.npc.content.shop;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NpcConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class RanksNpc implements NpcConfig {
	
	@Override
	public DodoNPC getNPC() {
		DodoLocation location = new DodoLocation(-11.552499771118164, 65.5, -0.4991999864578247, 271.15509033203125, 8.437555313110352);
		location.setLevel(DodoCraft.server().getLevelByName("world"));
		
		return new DodoNPC(
			location, 
			LobbyResource.NPC_RANKS, 
			TextFormat.colorize('&', "&b&lRanks\n&d&lHero &r&erank\n&evoor maar &64,99&e!"),
			null,
			1.15f,
			whoClicked -> {
				LobbyMessage.SHOP_NPC_RANKS.build().send(whoClicked);
			}
		);
	}
	
}
