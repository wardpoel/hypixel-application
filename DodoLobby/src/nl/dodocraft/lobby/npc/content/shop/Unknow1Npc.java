package nl.dodocraft.lobby.npc.content.shop;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NpcConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class Unknow1Npc implements NpcConfig {
	
	@Override
	public DodoNPC getNPC() {
		DodoLocation location = new DodoLocation(-5.441299915313721, 65.5, 5.511300086975098, 225.41946411132812, 13.030759811401367);
		location.setLevel(DodoCraft.server().getLevelByName("world"));
		
		return new DodoNPC(
			location,
			LobbyResource.NPC_UNKNOWN, 
			TextFormat.colorize('&', "&7&l???"), 
			null, 
			1.05f
		);
	}
	
}
