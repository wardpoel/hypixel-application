package nl.dodocraft.lobby.npc.content.shop;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NpcConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class CreditsNpc implements NpcConfig {
	
	@Override
	public DodoNPC getNPC() {
		DodoLocation location = new DodoLocation(-9.458200454711914, 65.5, 3.472899913787842, 248.9763641357422, 10.930984497070312);
		location.setLevel(DodoCraft.server().getLevelByName("world"));
		
		return new DodoNPC(
			location,
			LobbyResource.NPC_CREDITS,
			TextFormat.colorize('&', "&b&lCredits\n&r&eHaal &6&lCredits\n&r&evoor maar &64,99&e!"),
			null,
			1.15f,
			whoClicked -> {
				LobbyMessage.SHOP_NPC_CREDITS.build().send(whoClicked);
			}
		);
	}
	
}
