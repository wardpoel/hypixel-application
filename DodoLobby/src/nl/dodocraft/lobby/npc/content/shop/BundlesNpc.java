package nl.dodocraft.lobby.npc.content.shop;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NpcConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class BundlesNpc implements NpcConfig {
	
	@Override
	public DodoNPC getNPC() {
		DodoLocation location = new DodoLocation(-9.498200416564941, 65.5, -4.443399906158447, 293.5960998535156, 9.946784973144531);
		location.setLevel(DodoCraft.server().getLevelByName("world"));
		
		return new DodoNPC(
			location, 
			LobbyResource.NPC_BUNDELS,
			TextFormat.colorize('&', "&b&lBundels\n&6&lDodo &r&eRank & &6&l250 &r&eCredits\n" +
					"&evoor maar &619,99&e!"),
			null,
			1.15f,
			whoClicked -> {
				LobbyMessage.SHOP_NPC_BUNDLES.build().send(whoClicked);
			}
		);
	}
	
}
