package nl.dodocraft.lobby.npc.content;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.player.cosmetics.Cosmetic;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NpcConfig;
import nl.dodocraft.utils.world.DodoLocation;

public class PostmanNPC implements NpcConfig {
	
	@Override
	public DodoNPC getNPC() {
		DodoLocation location = new DodoLocation(15, 62, 6.2, 140.6, 0.87);
		location.setLevel(DodoCraft.server().getLevelByName("world"));
		
		return new DodoNPC(
			location, 
			LobbyResource.NPC_POSTMAN,
			TextFormat.colorize('&', "&b&lPostbode\n"),
			null,
			1.15f,
			whoClicked -> {
				Rank rank = DodoCore.getInstance().getRank(whoClicked.getUniqueId());
				if(!(Rank.hasPermission(Rank.HERO, rank))) return;
				DodoCraft.services().get(CosmeticManager.class).unlock(whoClicked.getUniqueId(), Cosmetic.PIXEL_DODO_CAKE);
				LobbyMessage.NPC_POSTMAN.build().send(whoClicked);
			}
		);
	}
	
}
