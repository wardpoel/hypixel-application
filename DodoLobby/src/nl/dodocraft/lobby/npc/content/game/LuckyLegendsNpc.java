package nl.dodocraft.lobby.npc.content.game;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.common.server.Server;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.game.ReleaseState;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NpcConfig;
import nl.dodocraft.utils.visual.npc.game.GameNPC;
import nl.dodocraft.utils.world.DodoLocation;

public class LuckyLegendsNpc implements NpcConfig {
	
	private static final DodoGamemode GAME = DodoGamemode.LUCKYLEGENDS;
	
	@Override
	public DodoNPC getNPC() {
		DodoLocation location = new DodoLocation(18.5, 62.5, 4.5, 105, -1);
		location.setLevel(DodoCraft.server().getLevelByName("world"));
		
		GameNPC npc = new GameNPC(
			location,
			LobbyResource.NPC_LUCKY_LEGENDS,
			TextFormat.colorize('&', GAME.getColorCode() + "&l" + GAME.getName()),
			1.3f,
			null,
			GAME,
			new Server[] {
					Server.LUCKYLEGENDS_SOLO, Server.LUCKYLEGENDS_TEAM
			},
			ReleaseState.UPDATED,
			true,
			player -> {
				LobbyMenu.GAME_SELECTOR_MODE.open(player, GAME);
			}
		);
		
		return npc;
	}

}
