package nl.dodocraft.lobby.npc.content.game;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.common.server.Server;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.game.ReleaseState;
import nl.dodocraft.utils.server.redirect.RedirectManager;
import nl.dodocraft.utils.server.redirect.ServerRedirectMode;
import nl.dodocraft.utils.visual.npc.DodoNPC;
import nl.dodocraft.utils.visual.npc.NpcConfig;
import nl.dodocraft.utils.visual.npc.game.GameNPC;
import nl.dodocraft.utils.world.DodoLocation;

public class BedWarsNpc implements NpcConfig {
	
	private static final DodoGamemode GAME = DodoGamemode.BEDWARS;
	
	@Override
	public DodoNPC getNPC() {
		DodoLocation location = new DodoLocation(20.5, 62.5, -0.5, 90, -1);
		location.setLevel(DodoCraft.server().getLevelByName("world"));
		
		return new GameNPC(
				location,
				LobbyResource.NPC_BEDWARS,
				TextFormat.colorize('&', GAME.getColorCode() + "&l" + GAME.getName()),
				1.3f,
				null,
				GAME,
				new Server[] {
						Server.BEDWARS_SOLO, Server.BEDWARS_TEAM
				},
				ReleaseState.NEW,
				true,
				player -> {
					// TODO remove when BedWars SOLO is ready for launch
					DodoCraft.services().get(RedirectManager.class).redirect(player.getUniqueId(), Server.BEDWARS_TEAM, ServerRedirectMode.MOST_PLAYERS);
//					LobbyMenu.GAME_SELECTOR_MODE.open(player, GAME);
				}
		);
	}
	
	
}
