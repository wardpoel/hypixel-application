package nl.dodocraft.lobby.npc;

import nl.dodocraft.common.boosters.ActiveBooster;
import nl.dodocraft.lobby.npc.content.game.BedWarsNpc;
import nl.dodocraft.lobby.npc.content.game.LuckyLegendsNpc;
import nl.dodocraft.lobby.npc.content.game.SurvivalNpc;
import nl.dodocraft.lobby.npc.content.shop.*;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.visual.npc.NPCManager;

public class LobbyNpcManager extends DodoService {
	
	@Override
	public void init() throws Exception {
		NPCManager manager = DodoCraft.services().get(NPCManager.class);
		
		manager.register(new SurvivalNpc());
		manager.register(new BedWarsNpc());
		manager.register(new LuckyLegendsNpc());
		manager.register(new RanksNpc());
		manager.register(new CreditsNpc());
		manager.register(new BundlesNpc());
		manager.register(new Unknow1Npc());
		manager.register(new Unknow2Npc());
	}
	
	public void addBoosterTag(ActiveBooster booster) {
	
	}
	
}
