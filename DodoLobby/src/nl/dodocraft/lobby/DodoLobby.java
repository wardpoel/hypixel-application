package nl.dodocraft.lobby;

import cn.nukkit.plugin.PluginBase;
import nl.dodocraft.lobby.chat.ChatManager;
import nl.dodocraft.lobby.minigame.dodoclicker.DodoClickerManager;
import nl.dodocraft.lobby.npc.LobbyNpcManager;
import nl.dodocraft.lobby.player.LobbyPlayerManager;
import nl.dodocraft.lobby.player.ScoreboardManager;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.lobby.server.LobbyManager;
import nl.dodocraft.lobby.server.NewsManager;
import nl.dodocraft.utils.DodoCraft;

public class DodoLobby extends PluginBase {

	private static DodoLobby instance;

	@Override
	public void onEnable() {
		instance = this;

		/* Register services */
		DodoCraft.services().register(LobbyManager.class);
		DodoCraft.services().register(LobbyPlayerManager.class);
		DodoCraft.services().register(ChatManager.class);
		DodoCraft.services().register(DodoClickerManager.class);
		DodoCraft.services().register(ScoreboardManager.class);
		DodoCraft.services().register(LobbyNpcManager.class);
		DodoCraft.services().register(NewsManager.class);
		DodoCraft.services().register(CosmeticManager.class);
	}

	public static DodoLobby getInstance() {
		return instance;
	}

}
