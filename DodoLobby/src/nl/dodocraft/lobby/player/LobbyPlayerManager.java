package nl.dodocraft.lobby.player;

import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.EventPriority;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.event.player.PlayerLoginEvent;
import cn.nukkit.event.player.PlayerMoveEvent;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.player.inventory.items.LobbyItem;
import nl.dodocraft.lobby.server.LobbyManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.game.GameItem;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.world.DodoLocation;
import nl.dodocraft.utils.world.WorldManager;

@SuppressWarnings("deprecation")
public class LobbyPlayerManager extends DodoService {
	
	private DodoLocation lobbySpawn;
	private static final int MAX_DISTANCE_AWAY = 200;
	
	@Override
	public void init() {
		lobbySpawn = new DodoLocation(DodoCraft.services().get(LobbyManager.class).getLobbySpawn());
		DodoCraft.services().get(WorldManager.class).setSpawn(lobbySpawn);
	}
	
	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		
		// Ignore if not cancelled for full server
		if(!event.isCancelled() || !event.getKickMessage().equals(Message.SERVER_KICK_FULL.build().get())) {
			return;
		}
		
		// Ignore if player isn't ranked
		if(!Rank.hasPermission(Rank.HERO, DodoCore.getInstance().getRank(player.getUniqueId()))) {
			return;
		}
		
		// Allow login
		event.setCancelled(false);
	}
	
	@EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		// Basic player settings
		float speed = player.getMovementSpeed() * 1.25f;
		player.setMovementSpeed(speed, true);
		player.setGamemode(2);
		player.getFoodData().setLevel(player.getFoodData().getMaxLevel());
		player.setFoodEnabled(false);
		player.setHealth(player.getMaxHealth());
		
		player.setAllowFlight(Rank.hasPermission(Rank.DODO, DodoCore.getInstance().getRank(player.getUniqueId())));
		
		// Give Lobby items
		giveLobbyItems(player);
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		DodoLocation to = new DodoLocation(event.getTo());
		
		if (to.distance(lobbySpawn) >= MAX_DISTANCE_AWAY) {
			player.teleport(lobbySpawn);
		}
	}
	
	private void giveLobbyItems(Player player) {
		// Clear players inventory
		player.getInventory().clearAll();
		
		// Give Lobby Items
		GameItem.GAMES.addToInventory(player, 1);
		LobbyItem.GAME_SELECTOR.addToInventory(player, 0);
		LobbyItem.COSMETICS.addToInventory(player, 4);
		LobbyItem.BOOSTER_SELECTOR.addToInventory(player, 7);
		LobbyItem.LOBBY_SELECTOR.addToInventory(player, 8);
	}

}
