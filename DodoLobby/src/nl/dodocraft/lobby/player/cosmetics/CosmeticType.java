package nl.dodocraft.lobby.player.cosmetics;

public enum CosmeticType {

	TITLE("Titles", "Title"),
	CAPE("Capes", "Cape"),
	PIXEL("Pixels", "Pixel");

	private String name;
	private String nameSingular;
	
	CosmeticType(String name, String nameSingular) {
		this.name = name;
		this.nameSingular = nameSingular;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getNameSingular() { 
		return this.nameSingular; 
	}
	
}
