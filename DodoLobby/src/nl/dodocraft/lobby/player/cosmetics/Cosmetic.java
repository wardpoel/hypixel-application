package nl.dodocraft.lobby.player.cosmetics;

import nl.dodocraft.lobby.player.cosmetics.cape.content.Crown;
import nl.dodocraft.lobby.player.cosmetics.cape.content.Jeb;
import nl.dodocraft.lobby.player.cosmetics.pixel.content.DodoCake;
import nl.dodocraft.lobby.player.cosmetics.pixel.content.DodoEgg;
import nl.dodocraft.lobby.player.cosmetics.title.content.*;

public enum Cosmetic {
	
	NONE(null, null),
	
	// Titles
	TITLE_EARLY_BIRD(CosmeticType.TITLE, EarlyBird.class),
	
	// LuckyLegends Title Rewards
	TITLE_QUESTION(CosmeticType.TITLE, Question.class),
	TITLE_LUCKY_BIRD(CosmeticType.TITLE, LuckyBird.class),
	TITLE_UNLUCKY_BIRD(CosmeticType.TITLE, UnLuckyBird.class),
	TITLE_HAPPY_LOOK(CosmeticType.TITLE, HappyLook.class),
	
	// BedWars Title Rewards
	TITLE_ZZZZZZ(CosmeticType.TITLE, Zzzzzz.class),
	TITLE_SLEEPYHEAD(CosmeticType.TITLE, Sleepyhead.class),
	
	// Capes
	CAPE_CROWN(CosmeticType.CAPE, Crown.class),
	CAPE_JEB(CosmeticType.CAPE, Jeb.class),
	
	// Pixels
	PIXEL_DODO_EGG(CosmeticType.PIXEL, DodoEgg.class),
	PIXEL_DODO_CAKE(CosmeticType.PIXEL, DodoCake.class);
	
	
	private CosmeticType type;
	private Class<? extends CosmeticFormat> cosmetic;
	
	Cosmetic(CosmeticType type, Class<? extends CosmeticFormat> cosmetic) {
		this.type = type;
		this.cosmetic = cosmetic;
	}
	
	public CosmeticFormat getInstance(Object... constructorArguments) {
		final Class<?>[] classes = new Class<?>[constructorArguments.length];
		for (int i = 0; i < constructorArguments.length; i++) {
			classes[i] = constructorArguments[i].getClass();
		}
		
		final Object[] parameters = new Object[constructorArguments.length];
		for (int i = 0; i < constructorArguments.length; i++) {
			parameters[i] = constructorArguments[i];
		}
		
		try {
			return cosmetic.getConstructor(classes).newInstance(parameters);
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public CosmeticType getType() {
		return this.type;
	}
	
}
