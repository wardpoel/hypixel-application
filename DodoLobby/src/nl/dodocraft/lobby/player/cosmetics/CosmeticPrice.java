package nl.dodocraft.lobby.player.cosmetics;

public class CosmeticPrice {
	
	public static final int EXCLUSIVE = -1;
	
	public static final int TITLE_LOW = 50;
	public static final int TITLE_MEDIUM = 200;
	public static final int TITLE_HIGH = 500;
	
	public static final int CAPE_LOW = 100;
	public static final int CAPE_MEDIUM = 250;
	public static final int CAPE_HIGH = 600;
	
	public static final int PIXEL_LOW = 1000;
	public static final int PIXEL_MEDIUM = 2000;
	public static final int PIXEL_HIGH = 4000;
	
}
