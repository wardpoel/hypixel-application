package nl.dodocraft.lobby.player.cosmetics.pixel;

import cn.nukkit.Player;
import cn.nukkit.entity.data.Skin;
import nl.dodocraft.lobby.player.cosmetics.CosmeticFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticType;
import nl.dodocraft.lobby.player.cosmetics.pixel.effects.PixelEffect;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.skull.DodoSkull;
import nl.dodocraft.utils.visual.skull.SkullManager;
import nl.dodocraft.utils.world.DodoLocation;

public abstract class CosmeticPixel extends CosmeticFormat {
	
	public abstract Skin getTexture();
	public abstract PixelEffect getEffect();
	
	@Override
	public CosmeticType getType() {
		return CosmeticType.PIXEL;
	}
	
	@Override
	public void equip(Player player) {
		DodoLocation location = new DodoLocation(player.getLocation());
		location.yaw = 0;
		
		DodoPixel pixel = new DodoPixel(getTexture(), location.clone().subtract(0, 1.5, 0), player, getEffect());
		pixel.setRemoveAfterClick(false);
		DodoCraft.services().get(SkullManager.class).register(pixel);
	}
	
	@Override
	public void unequip(Player player) {
		DodoSkull skull = DodoCraft.services().get(SkullManager.class).getSkullByOwner(player);
		if (skull == null) return;
		if (!(skull instanceof DodoPixel)) return;
		despawn((DodoPixel) skull);
	}
	
	private void despawn(DodoPixel pixel) {
		if (pixel == null) return;
		DodoCraft.services().get(SkullManager.class).unregister(pixel);
	}
	
}
