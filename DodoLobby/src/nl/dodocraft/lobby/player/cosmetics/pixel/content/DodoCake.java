package nl.dodocraft.lobby.player.cosmetics.pixel.content;

import cn.nukkit.entity.data.Skin;
import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.lobby.player.cosmetics.CosmeticPrice;
import nl.dodocraft.lobby.player.cosmetics.pixel.CosmeticPixel;
import nl.dodocraft.lobby.player.cosmetics.pixel.effects.PixelEffect;

public class DodoCake extends CosmeticPixel {
	
	@Override
	public Skin getTexture() {
		return LobbyResource.PIXEL_DODO_CAKE;
	}
	
	@Override
	public PixelEffect getEffect() {
		return PixelEffect.WAVE;
	}
	
	@Override
	public String getName() {
		return "Dodo Cake";
	}
	
	@Override
	public int getPrice() {
		return CosmeticPrice.EXCLUSIVE;
	}
	
}
