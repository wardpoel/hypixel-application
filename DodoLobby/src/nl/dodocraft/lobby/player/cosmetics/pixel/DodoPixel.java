package nl.dodocraft.lobby.player.cosmetics.pixel;

import cn.nukkit.Player;
import cn.nukkit.entity.data.Skin;
import cn.nukkit.event.player.PlayerTeleportEvent;
import cn.nukkit.scheduler.NukkitRunnable;
import nl.dodocraft.lobby.DodoLobby;
import nl.dodocraft.lobby.player.cosmetics.pixel.effects.PixelEffect;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.skull.DodoSkull;
import nl.dodocraft.utils.visual.skull.SkullManager;
import nl.dodocraft.utils.world.DodoLocation;

public class DodoPixel extends DodoSkull {
	
	private PixelEffect effect;
	
	public DodoPixel(Skin skin, DodoLocation location, Player owner, PixelEffect effect) {
		super(skin, location);
		setOwner(owner);
		this.effect = effect;
	}
	
	@Override
	public void init() {
		super.init();
		doEffect();
	}
	
	public void doEffect() {
		if (effect == PixelEffect.ROTATE) {
			rotate();
		} else if (effect == PixelEffect.WAVE) {
			rotateWave();
		} else {
			none();
		}
	}
	
	private void unregister() {
		DodoCraft.services().get(SkullManager.class).unregister(this);
	}
	
	private void none() {
		new NukkitRunnable() {
			@Override
			public void run() {
				if (!getSkull().isAlive()) {
					this.cancel();
					return;
				}
				
				if (!getOwner().isOnline()) {
					unregister();
					this.cancel();
					return;
				}
				
				DodoLocation location = new DodoLocation(getOwner().getLocation()).add(getOwner().getDirectionVector().multiply(1));
				location.pitch = 0;
				
				getSkull().teleport(location.clone().subtract(0, 1.5, 0), PlayerTeleportEvent.TeleportCause.PLUGIN);
			}
		}.runTaskTimer(DodoLobby.getInstance(), 20, 1);
	}
	
	private void rotate() {
		new NukkitRunnable() {
			float step = 0f;
			@Override
			public void run() {
				if (!getSkull().isAlive()) {
					this.cancel();
					return;
				}
				
				if (!getOwner().isOnline()) {
					unregister();
					this.cancel();
					return;
				}
				
				double dx = -Math.cos(step / 120.0F * 6.283185307179586D);
				double dz = -Math.sin(step / 120.0F * 6.283185307179586D);
				
				DodoLocation location = new DodoLocation(getOwner().getLocation());
				location.yaw = 0;
				location.pitch = 0;
				location.add(dx, 0.75, dz);
				
				getSkull().teleport(location.clone().subtract(0, 1.5, 0), PlayerTeleportEvent.TeleportCause.PLUGIN);
				step += 5f;
				if (step > 360f) {
					step = 0f;
				}
			}
		}.runTaskTimer(DodoLobby.getInstance(), 20, 1);
	}
	
	private void rotateWave() {
		new NukkitRunnable() {
			float step = 0f;
			double dy = 0.40;
			@Override
			public void run() {
				if (!getSkull().isAlive()) {
					this.cancel();
					return;
				}
				
				if (!getOwner().isOnline()) {
					unregister();
					this.cancel();
					return;
				}
				
				double dx = -Math.cos(step / 120.0F * 6.283185307179586D);
				double dz = -Math.sin(step / 120.0F * 6.283185307179586D);
				
				DodoLocation location = new DodoLocation(getOwner().getLocation());
				location.yaw = 0;
				location.pitch = 0;
				location.add(dx, dy, dz);
				
				if (dy >= 0.8) {
					dy -= 0.02;
				} else if (dy <= 0.4) {
					dy += 0.02;
				}
				
				getSkull().teleport(location.clone().subtract(0, 1.5, 0), PlayerTeleportEvent.TeleportCause.PLUGIN);
				step += 5f;
				if (step > 360f) {
					step = 0f;
				}
			}
		}.runTaskTimer(DodoLobby.getInstance(), 20, 1);
	}
	
}
