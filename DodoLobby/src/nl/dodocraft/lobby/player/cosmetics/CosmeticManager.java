package nl.dodocraft.lobby.player.cosmetics;

import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.event.player.PlayerQuitEvent;
import nl.dodocraft.common.Rank;
import nl.dodocraft.common.currency.PurchaseResult;
import nl.dodocraft.common.log.CoreLogger;
import nl.dodocraft.common.log.LogType;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.player.info.CosmeticInfo;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.server.stafftools.info.InfoManager;
import nl.dodocraft.utils.service.DodoService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

public class CosmeticManager extends DodoService {
	
	private static final String COSMETIC_UNLOCKED_KEY = "COSMETICS_UNLOCKED";
	private static final String COSMETIC_SELECTED_TYPE_KEY = "COSMETIC_SELECTED_";
	
	@Override
	public void init() throws Exception {
		DodoCraft.services().get(InfoManager.class).register(new CosmeticInfo());
		DodoCore.getInstance().cachePlayerData(DodoGamemode.LOBBY, COSMETIC_UNLOCKED_KEY);
		
		for (CosmeticType type : CosmeticType.values()) {
			DodoCore.getInstance().cachePlayerData(DodoGamemode.LOBBY, COSMETIC_SELECTED_TYPE_KEY + type.name());
		}
	}
	
	@EventHandler (ignoreCancelled = true)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		for (CosmeticType type : CosmeticType.values()) {
			Cosmetic cosmetic = getSelected(player, type);
			if (cosmetic == Cosmetic.NONE) continue;
			
			cosmetic.getInstance().equip(player);
		}
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		for (CosmeticType type : CosmeticType.values()) {
			Cosmetic cosmetic = getSelected(player, type);
			if (cosmetic == Cosmetic.NONE) continue;
			
			cosmetic.getInstance().unequip(player);
		}
	}
	
	public boolean has(UUID uuid, Cosmetic cosmetic) {
		if (Rank.hasPermission(Rank.ADMIN, DodoCore.getInstance().getRank(uuid))) return true;
		return getUnlocked(uuid).contains(cosmetic);
	}
	
	@SuppressWarnings("unchecked")
	public List<Cosmetic> getUnlocked(UUID uuid) {
		Player player = DodoCraft.getPlayer(uuid);
		
		if (player == null) {
			AtomicReference<List<Cosmetic>> cosmetics = new AtomicReference<>();
			cosmetics.set(new ArrayList<>());
			DodoCore.getInstance().getPlayerData(uuid, DodoGamemode.LOBBY, COSMETIC_UNLOCKED_KEY, cosmeticsRaw -> {
				if (cosmeticsRaw.getValue() == null) return;
				
				List<String> raw = cosmeticsRaw.asList(String.class);
				
				for (String rawCosmetic : raw) {
					try {
						cosmetics.get().add(Cosmetic.valueOf(rawCosmetic));
					} catch (Exception e) {
						CoreLogger.log(LogType.WARNING, "Tried to load unknown cosmetic: " + rawCosmetic);
					}
				}
				
				if (cosmetics.get().size() != raw.size()) {
					save(uuid, cosmetics.get());
				}
			});
			
			return cosmetics.get();
		}
		
		List<Cosmetic> cosmetics = new ArrayList<>();
		List<String> raw = DodoCore.getInstance().getPlayerData(uuid, DodoGamemode.LOBBY, COSMETIC_UNLOCKED_KEY, ArrayList.class);
		if(raw == null) {
			raw = new ArrayList<>();
		}
		
		for(String rawCosmetic : raw) {
			try {
				cosmetics.add(Cosmetic.valueOf(rawCosmetic));
			} catch(Exception e) {
				CoreLogger.log(LogType.WARNING, "Tried to load unknown cosmetic: " + rawCosmetic);
			}
		}
		
		if (cosmetics.size() != raw.size()) {
			save(uuid, cosmetics);
		}
			
		return cosmetics;
	}
	
	public void save(UUID uuid, List<Cosmetic> cosmetics) {
		List<String> raw = new ArrayList<>();
		
		for(Cosmetic cosmetic : cosmetics) {
			raw.add(cosmetic.name());
		}
		
		DodoCore.getInstance().setPlayerData(uuid, DodoGamemode.LOBBY, COSMETIC_UNLOCKED_KEY, raw);
	}
	
	public void purchase(Player player, Cosmetic cosmetic) {
		if (has(player.getUniqueId(), cosmetic)) {
			LobbyMessage.COSMETIC_ALREADY_PURCHASED.build().send(player);
			return;
		}
		
		// Check balance
		int balance = DodoCore.getInstance().getCredits(player.getUniqueId());
		int price = cosmetic.getInstance().getPrice();
		if (balance < price) {
			LobbyMessage.COSMETIC_PURCHASE_NOT_ENOUGH_CREDITS.build().replace(new MessageReplaceValue("%AMOUNT%", (price - balance) + "")).send(player);
			return;
		}
		
		// Remove credits
		DodoCore.getInstance().changeCredits(player.getUniqueId(), -price, purchaseResult -> {
			if (purchaseResult != PurchaseResult.SUCCESS) {
				LobbyMessage.COSMETIC_PURCHASE_ERROR.build().send(player);
				return;
			}
			
			List<Cosmetic> cosmetics = getUnlocked(player.getUniqueId());
			cosmetics.add(cosmetic);
			save(player.getUniqueId(), cosmetics);
		});
	}
	
	public void unlock(UUID uuid, Cosmetic cosmetic) {
		if (has(uuid, cosmetic)) return;
		
		List<Cosmetic> cosmetics = getUnlocked(uuid);
		cosmetics.add(cosmetic);
		save(uuid, cosmetics);
	}
	
	public void lock(UUID uuid, Cosmetic cosmetic) {
		if (!has(uuid, cosmetic)) return;
		
		List<Cosmetic> cosmetics = getUnlocked(uuid);
		cosmetics.remove(cosmetic);
		save(uuid, cosmetics);
	}
	
	public Cosmetic getSelected(Player player, CosmeticType type) {
		String cosmetic = DodoCore.getInstance().getPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, COSMETIC_SELECTED_TYPE_KEY + type.name(), String.class);
		
		if (cosmetic == null) {
			return Cosmetic.NONE;
		}
		
		Cosmetic c = null;
		try {
			c = Cosmetic.valueOf(cosmetic);
		} catch (IllegalArgumentException e) {
			return Cosmetic.NONE;
		}
		
		return c;
	}
	
	public void setSelected(Player player, Cosmetic cosmetic) {
		unequip(player, cosmetic.getType(), false);
		cosmetic.getInstance().equip(player);
		DodoCore.getInstance().setPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, COSMETIC_SELECTED_TYPE_KEY + cosmetic.getType().name(), cosmetic.name());
	}
	
	public void unequip(Player player, CosmeticType type) {
		unequip(player, type, true);
	}
	
	public void unequip(Player player, CosmeticType type, boolean save) {
		Cosmetic cosmetic = getSelected(player, type);
		if (cosmetic == Cosmetic.NONE) return;
		cosmetic.getInstance().unequip(player);
		if (!save) return;
		DodoCore.getInstance().setPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, COSMETIC_SELECTED_TYPE_KEY + type.name(), Cosmetic.NONE.name());
	}
	
	public int getUnlockedAmount(UUID uuid, CosmeticType type) {
		if (Rank.hasPermission(Rank.ADMIN, DodoCore.getInstance().getRank(uuid))) {
			return getMaxAmount(type);
		}
		
		int amount = 0;
		for (Cosmetic cosmetic : getUnlocked(uuid)) {
			if (cosmetic.getType() != type) continue;
			amount++;
		}
		return amount;
	}
	
	public int getMaxAmount(CosmeticType type) {
		int amount = 0;
		for (Cosmetic c : Cosmetic.values()) {
			if (type != c.getType()) continue;
			amount++;
		}
		return amount;
	}
	
}
