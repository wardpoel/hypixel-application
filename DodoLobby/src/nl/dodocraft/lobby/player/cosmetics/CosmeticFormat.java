package nl.dodocraft.lobby.player.cosmetics;

import cn.nukkit.Player;

public abstract class CosmeticFormat {

	public abstract String getName();
	public abstract CosmeticType getType();
	public abstract int getPrice();
	public abstract void equip(Player player);
	public abstract void unequip(Player player);
	
}
