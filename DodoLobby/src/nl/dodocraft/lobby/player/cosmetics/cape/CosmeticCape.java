package nl.dodocraft.lobby.player.cosmetics.cape;

import java.awt.image.BufferedImage;

import cn.nukkit.Player;
import cn.nukkit.entity.data.Skin;
import nl.dodocraft.lobby.player.cosmetics.CosmeticFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticType;

public abstract class CosmeticCape extends CosmeticFormat {

	public abstract BufferedImage getTexture();
	
	@Override
	public CosmeticType getType() {
		return CosmeticType.CAPE;
	}
	
	@Override
	public void equip(Player player) {
		Skin skin = player.getSkin();
		skin.setCapeData(getTexture());
		player.setSkin(skin);
	}
	
	@Override
	public void unequip(Player player) {
		Skin skin = player.getSkin();
		skin.setCapeData(new byte[0]);
		player.setSkin(skin);
	}
}
