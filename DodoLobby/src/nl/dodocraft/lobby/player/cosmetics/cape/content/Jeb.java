package nl.dodocraft.lobby.player.cosmetics.cape.content;

import java.awt.image.BufferedImage;

import nl.dodocraft.lobby.LobbyResource;
import nl.dodocraft.lobby.player.cosmetics.CosmeticPrice;
import nl.dodocraft.lobby.player.cosmetics.cape.CosmeticCape;

public class Jeb extends CosmeticCape {
	
	@Override
	public BufferedImage getTexture() {
		return LobbyResource.CAPE_JEB;
	}
	
	@Override
	public int getPrice() {
		return CosmeticPrice.CAPE_HIGH;
	}
	
	@Override
	public String getName() {
		return "Jeb";
	}
}
