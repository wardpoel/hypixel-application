package nl.dodocraft.lobby.player.cosmetics.title.content;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticPrice;
import nl.dodocraft.lobby.player.cosmetics.title.CosmeticTitle;

public class EarlyBird extends CosmeticTitle {
	
	@Override
	public String getTitle() {
		return "Vroege Vogel";
	}
	
	@Override
	public String getColoredTitle() {
		return TextFormat.colorize('&', "&bVroege Vogel");
	}
	
	@Override
	public int getPrice() {
		return CosmeticPrice.EXCLUSIVE;
	}
	
	@Override
	public String getName() {
		return "Vroege Vogel";
	}
	
}
