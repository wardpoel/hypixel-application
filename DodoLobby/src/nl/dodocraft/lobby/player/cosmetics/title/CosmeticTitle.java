package nl.dodocraft.lobby.player.cosmetics.title;

import cn.nukkit.Player;
import nl.dodocraft.lobby.player.cosmetics.CosmeticFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticType;

public abstract class CosmeticTitle extends CosmeticFormat {

	public abstract String getTitle();
	public abstract String getColoredTitle();
	
	@Override
	public CosmeticType getType() {
		return CosmeticType.TITLE;
	}
	
	@Override
	public void equip(Player player) {
		player.setScoreTag(getColoredTitle());
	}
	
	@Override
	public void unequip(Player player) {
		player.setScoreTag("");
	}
	
}
