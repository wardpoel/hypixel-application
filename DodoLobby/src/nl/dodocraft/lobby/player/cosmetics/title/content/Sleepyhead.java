package nl.dodocraft.lobby.player.cosmetics.title.content;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticPrice;
import nl.dodocraft.lobby.player.cosmetics.title.CosmeticTitle;

public class Sleepyhead extends CosmeticTitle {
	@Override
	public String getTitle() {
		return "Slaapkop";
	}
	
	@Override
	public String getColoredTitle() {
		return TextFormat.colorize('&', "&b" + getTitle());
	}
	
	@Override
	public String getName() {
		return "Slaapkop";
	}
	
	@Override
	public int getPrice() {
		return CosmeticPrice.EXCLUSIVE;
	}
}
