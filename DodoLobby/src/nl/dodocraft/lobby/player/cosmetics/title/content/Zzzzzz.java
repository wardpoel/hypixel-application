package nl.dodocraft.lobby.player.cosmetics.title.content;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticPrice;
import nl.dodocraft.lobby.player.cosmetics.title.CosmeticTitle;

public class Zzzzzz extends CosmeticTitle {
	@Override
	public String getTitle() {
		return "zZZzZz";
	}
	
	@Override
	public String getColoredTitle() {
		return TextFormat.colorize('&', "&7" + getTitle());
	}
	
	@Override
	public String getName() {
		return "zZZzZz";
	}
	
	@Override
	public int getPrice() {
		return CosmeticPrice.EXCLUSIVE;
	}
}
