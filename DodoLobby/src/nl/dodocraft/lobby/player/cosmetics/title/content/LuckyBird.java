package nl.dodocraft.lobby.player.cosmetics.title.content;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticPrice;
import nl.dodocraft.lobby.player.cosmetics.title.CosmeticTitle;

public class LuckyBird extends CosmeticTitle {
	
	@Override
	public String getTitle() {
		return "Geluksvogel";
	}
	
	@Override
	public String getColoredTitle() {
		return TextFormat.colorize('&', "&6Geluksvogel");
	}
	
	@Override
	public int getPrice() {
		return CosmeticPrice.EXCLUSIVE;
	}
	
	@Override
	public String getName() {
		return "Geluksvogel";
	}
	
}
