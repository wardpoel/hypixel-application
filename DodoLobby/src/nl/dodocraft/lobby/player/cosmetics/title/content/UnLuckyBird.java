package nl.dodocraft.lobby.player.cosmetics.title.content;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticPrice;
import nl.dodocraft.lobby.player.cosmetics.title.CosmeticTitle;

public class UnLuckyBird extends CosmeticTitle {
	@Override
	public String getTitle() {
		return "Pechvogel";
	}
	
	@Override
	public String getColoredTitle() {
		return TextFormat.colorize('&', "&cPechvogel");
	}
	
	@Override
	public String getName() {
		return "Pechvogel";
	}
	
	@Override
	public int getPrice() {
		return CosmeticPrice.EXCLUSIVE;
	}
}
