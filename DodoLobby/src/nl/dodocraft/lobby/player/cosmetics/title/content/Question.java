package nl.dodocraft.lobby.player.cosmetics.title.content;

import cn.nukkit.utils.TextFormat;
import nl.dodocraft.lobby.player.cosmetics.CosmeticPrice;
import nl.dodocraft.lobby.player.cosmetics.title.CosmeticTitle;

public class Question extends CosmeticTitle {
	
	@Override
	public String getTitle() {
		return "???";
	}
	
	@Override
	public String getColoredTitle() {
		return TextFormat.colorize('&', "&7???");
	}
	
	@Override
	public int getPrice() {
		return CosmeticPrice.EXCLUSIVE;
	}
	
	@Override
	public String getName() {
		return "???";
	}
	
}
