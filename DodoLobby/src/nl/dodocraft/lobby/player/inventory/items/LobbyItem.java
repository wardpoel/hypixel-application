package nl.dodocraft.lobby.player.inventory.items;

import cn.nukkit.item.Item;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.player.inventory.DodoClickableItem;
import nl.dodocraft.utils.player.inventory.DodoItem;

public class LobbyItem {
	
	public static final DodoClickableItem GAME_SELECTOR = gameSelector();
	public static final DodoClickableItem LOBBY_SELECTOR = lobbySelector();
	public static final DodoClickableItem COSMETICS = cosmetics();
	public static final DodoClickableItem BOOSTER_SELECTOR = boosterSelector();

	private static DodoClickableItem gameSelector() {
		DodoItem item = new DodoItem(Item.COMPASS).setName(LobbyMessage.LOBBY_ITEM_NAME.build().replace(new MessageReplaceValue("%NAME%", "Welke game wil je spelen?")).get());
		return new DodoClickableItem(item, LobbyMenu.GAME_SELECTOR::open).setMovable(false);
	}
	private static DodoClickableItem lobbySelector() {
		DodoItem item = new DodoItem(Item.BOOK).setName(LobbyMessage.LOBBY_ITEM_NAME.build().replace(new MessageReplaceValue("%NAME%", "Naar welke lobby wil je?")).get());
		return new DodoClickableItem(item, LobbyMenu.LOBBY_SELECTOR::open).setMovable(false);
	}
	
	private static DodoClickableItem boosterSelector() {
		DodoItem item = new DodoItem(Item.TOTEM).setName(LobbyMessage.LOBBY_ITEM_NAME.build().replace(new MessageReplaceValue("%NAME%", "Mijn Boosters")).get());
		return new DodoClickableItem(item, LobbyMenu.BOOSTERS::open).setMovable(false);
	}

	private static DodoClickableItem cosmetics() {
		DodoItem item = new DodoItem(Item.BLAZE_POWDER).setName(LobbyMessage.LOBBY_ITEM_NAME.build().replace(new MessageReplaceValue("%NAME%", "Mijn Cosmetics")).get());
		return new DodoClickableItem(item, LobbyMenu.COSMETIC_SELECTOR::open).setMovable(false);
	}
	
}
