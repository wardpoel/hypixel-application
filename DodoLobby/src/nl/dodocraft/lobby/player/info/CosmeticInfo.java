package nl.dodocraft.lobby.player.info;

import cn.nukkit.Player;
import nl.dodocraft.common.Rank;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.server.stafftools.info.Info;

public class CosmeticInfo implements Info {
	@Override
	public String getName() {
		return "Cosmetic Info";
	}
	
	@Override
	public Rank getRank() {
		return Rank.ADMIN;
	}
	
	@Override
	public void onClick(Player player, DataValue... dataValues) {
		LobbyMenu.COSMETICS_INFO.open(player, dataValues[0].asUUID(), dataValues[1].asString());
	}
}
