package nl.dodocraft.lobby.player;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.UUID;

import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.event.player.PlayerQuitEvent;
import cn.nukkit.scheduler.NukkitRunnable;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.DodoLobby;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.helpers.NumberHelper;
import nl.dodocraft.utils.helpers.TimeStringHelper;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.visual.scoreboard.network.DisplaySlot;
import nl.dodocraft.utils.visual.scoreboard.network.DodoScoreboard;
import nl.dodocraft.utils.visual.scoreboard.network.ScoreboardDisplay;

public class ScoreboardManager extends DodoService {

	private HashMap<UUID, DodoScoreboard> scoreboards = new HashMap<>();

	private String getTitle() {
		return TextFormat.colorize('&', "&3&l" + "DodoCraft");
	}
	
	private String getCredits(Player player) {
		int credits = DodoCore.getInstance().getCredits(player.getUniqueId());
		return (credits == 0 ? "&c" : "&6") + NumberHelper.format(credits);
	}
	
	private String getCoins(Player player) {
		int coins = DodoCore.getInstance().getCoins(player.getUniqueId());
		return (coins == 0 ? "&c" : "&6") + NumberHelper.format(coins);
	}
	
	@Override
	public void init() throws Exception {
		LocalDateTime now = LocalDateTime.now(ZoneId.systemDefault());
		LocalDateTime updateTime = LocalDateTime.now(ZoneId.systemDefault()).plusDays(1).withHour(0).withMinute(0).withSecond(1);
		
		long seconds = ChronoUnit.SECONDS.between(now, updateTime);
		
		if (seconds < 0) {
			seconds *= -1;
		}
		
		new NukkitRunnable() {
			@Override
			public void run() {
				for (Player players: DodoCraft.players()) {
					// Continue loop if player doesn't have an current Scoreboard.
					if(!(scoreboards.containsKey(players.getUniqueId()))) continue;
					
					// Get player's current Scoreboard, hide and remove current display.
					DodoScoreboard scoreboard = scoreboards.get(players.getUniqueId());
					scoreboard.hideFor(players);
					scoreboard.removeDisplay(DisplaySlot.SIDEBAR);
					
					// Set new Scoreboard
					setScoreboard(players);
				}
			}
		}.runTaskTimer(DodoLobby.getInstance(), (int)seconds * 20, 86400 * 20);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		scoreboards.remove(event.getPlayer().getUniqueId());
	}

	@EventHandler (ignoreCancelled = true)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		// Creating Scoreboard for player
		scoreboards.put(player.getUniqueId(), new DodoScoreboard());
		setScoreboard(player);
	}

	public void setScoreboard(Player player) {
		DodoScoreboard scoreboard = scoreboards.get(player.getUniqueId());
		
		// Get rank by Core
		Rank rank = DodoCore.getInstance().getRank(player.getUniqueId());
		
		// Creating new display
		ScoreboardDisplay display = scoreboard.addDisplay(DisplaySlot.SIDEBAR, "dumy", getTitle());
		display.addLine(TextFormat.colorize('&', " &7" + TimeStringHelper.scoreboardDate()), 0);
		display.addLine(TextFormat.colorize('&', " &1&l"), 1);
		if(rank.equals(Rank.DEFAULT)) {
			display.addLine(TextFormat.colorize('&', " &7Geen rank"), 2);
		} else {
			display.addLine(TextFormat.colorize('&', " Rank: " + rank.getColor() + rank.getLongName()), 2);
		}
	    display.addLine(TextFormat.colorize('&', " Credits: " + getCredits(player)), 3);
	    display.addLine(TextFormat.colorize('&', " Coins: " + getCoins(player)), 4);
	    display.addLine(TextFormat.colorize('&', " &3&l"), 5);
	    display.addLine(TextFormat.colorize('&', " &bplay.dodocraft.nl"), 6);
		
		scoreboard.showFor(player);
	    scoreboards.replace(player.getUniqueId(), scoreboard);
	}

}
