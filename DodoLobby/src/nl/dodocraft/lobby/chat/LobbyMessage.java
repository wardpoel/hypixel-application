package nl.dodocraft.lobby.chat;

import nl.dodocraft.utils.chat.DodoMessageBuilder;
import nl.dodocraft.utils.chat.MessageEnum;
import nl.dodocraft.utils.chat.Unicode;

public enum LobbyMessage implements MessageEnum {

	PLAYER_NO_PERMS_JOIN_FULL_LOBBY("&c&lDeze lobby is vol. &r&7Met een &d&lHero &r&7of hogere rank kan je deze lobby wel joinen."), //
	
	CHAT_NO_PERMS_DELAY("&c&lOeps! &r&7Je chat te snel."), //
	CHAT_NO_PERMS_RANKED_ONLY("&7Op dit moment kan je alleen chatten met een &d&lHero &r&7rank of hoger."), //
	CHAT_NO_PERMS_STAFF_ONLY("&7Op dit moment kan alleen &c&lStaff &r&7chatten."), //

	CHAT_MODE_UPDATED_NORMAL("&8" + Unicode.ARROW_1 + " &7Iedereen kan weer normaal chatten! :)"), //
	CHAT_MODE_UPDATED_DELAY("&c" + Unicode.ARROW_1 + " &eNormale spelers hebben een &4&l%DELAY%s &r&echat cooldown."), //
	CHAT_MODE_UPDATED_RANKED("&c" + Unicode.ARROW_1 + " &eAlleen &d&lHero &r&eof hoger kan nu chatten."), //
	CHAT_MODE_UPDATED_STAFF("&c" + Unicode.ARROW_1 + " &eAlleen &c&lStaff &r&ekan nu chatten."), //

	COSMETIC_ALREADY_PURCHASED("&a&l" + Unicode.DOUBLE_RIGHT + " &r&eJe hebt deze cosmetic al gekocht."),
	COSMETIC_EXCLUSIVE_ONLY("&d&lPoof! &r&eDit is een exclusieve cosmetic. Er zijn &bexclusieve cosmetics &eop &ashop.dodocraft.nl &ebij &a&lBundels&r&e."),
	COSMETIC_PURCHASE_NOT_ENOUGH_CREDITS("&c&l" + Unicode.DOUBLE_RIGHT + " Woepsie! &r&eJe mist &c%AMOUNT% Credits &eom deze cosmetic te kopen!"),
	COSMETIC_PURCHASE_ERROR("&c&l" + Unicode.DOUBLE_RIGHT + " Oei! &r&eEr ging iets fout tijdens het kopen. Probeer later nog eens!"),
	COSMETIC_EQUIP("&a&l" + Unicode.DOUBLE_RIGHT + " &r&7%NAME% %TYPE% aangezet."),
	COSMETIC_UNEQUIP("&c&l" + Unicode.DOUBLE_RIGHT + " &r&7%NAME% %TYPE% uitgezet."),
	COSMETIC_UNLOCKED("&a&l" + Unicode.DOUBLE_RIGHT + " &r&7Succesvol %NAME% aan %PLAYER% gegeven."),
	COSMETIC_LOCKED("&c&l" + Unicode.DOUBLE_RIGHT + " &r&7Succesvol %NAME% van %PLAYER% verwijdert."),
	
	DODO_CLICKER_PLAYER_CLICK("&e&l%MESSAGE%"), //
	
	SHOP_NPC_CREDITS("&eBezoek &b&lshop.dodocraft.nl &r&evoor &6&lCredits&r&e.\n&r&eBij &62000 Credits &ekrijg je er &6&l500 gratis&r&e!"),
	SHOP_NPC_RANKS("&eBezoek &b&lshop.dodocraft.nl &r&evoor &d&lRanks&r&e.\n&eAlle &b3 maanden ranks &ehebben standaard &a&l10% korting&r&e!"),
	SHOP_NPC_BUNDLES("&eBezoek &b&lshop.dodocraft.nl &r&evoor &a&lBundels&r&e.\n&r&eOm de zoveel tijd zijn er unieke bundels met &dexclusieve items&e!"),
	NPC_POSTMAN("&eAfgelopen dagen had Minecraft een update gedaan waardoor sommigen van jullie geen toegang hadden tot de server. Wij hebben zo snel mogelijk onze server weer werkend gemaakt zodat jullie kunnen spelen. Voor het ongemak hebben wij een exclusieve &b&lDodo Cake Pixel &r&egemaakt die alle trouwe beta testers als extra ontvangen. Veel plezier op de server!"),
	
	LOBBY_ITEM_NAME("&r&b&l%NAME%"), //

	MENU_COSMETIC_SELECTOR_TITLE("&8&lMijn Cosmetics"), //
	MENU_COSMETIC_SELECTOR_TYPE_BUTTON("&5&l%TYPE%\n&r&8%UNLOCKED%/%MAX% Ontgrendeld"),
	MENU_COSMETIC_SELECTOR_TYPE_BUTTON_HAS_NONE("&8&l%TYPE%\n&r&8Nog geen %TYPE% :("),
	MENU_COSMETIC_SELECTOR_TYPE_TITLE("&8&lWelke %TYPE%?"),
	MENU_COSMETIC_SELECTOR_COSMETIC_BUTTON_UNLOCKED("&1&l%NAME%\n&r&8Klik om te activeren"),
	MENU_COSMETIC_SELECTOR_COSMETIC_BUTTON_LOCKED("&8&l%NAME%\n&r&8%PRICE%"),
	MENU_COSMETIC_SELECTOR_COSMETIC_BUTTON_ACTIVE("&5&l%NAME%\n&r&2&l" + Unicode.DOUBLE_RIGHT + " &r&2Actief &r&2&l" + Unicode.DOUBLE_LEFT),
	MENU_COSMETIC_SELECTOR_CONFIRM_TITLE("&8&lWeet je het zeker?"), //
	MENU_GAME_SELECTOR_TITLE("&8&lWelke game wil je spelen?"), //
	MENU_GAME_SELECTOR_GAMENAME("%COLOR%&l%NAME%\n&r&8&l%PLAYERS% &r&8Speler%S%"), //

	MENU_GAME_SELECTOR_MODE_TITLE("%COLOR%&l%NAME%"), //

	MENU_LOBBY_SELECTOR_TITLE("&8&lNaar welke lobby wil je?"), //
	MENU_LOBBY_SELECTOR_LOBBY("&2&lLobby #%LOBBY_NUMBER%\n&r&8Spelers: %PLAYERS%/%MAX%"), //
	MENU_LOBBY_SELECTOR_LOBBY_OWN("&5&lLobby #%LOBBY_NUMBER%\n&r&8Je zit in deze lobby."), //
	MENU_LOBBY_SELECTOR_LOBBY_FULL("&4&lLobby #%LOBBY_NUMBER%\n&r&8Deze lobby zit vol."), //
	MENU_LOBBY_TELEPORT_SPAWN("&8&lTeleporteer naar Spawn"), //

	MENU_CHAT_MODE_TITLE("&8&lAdmin functie - Chat Mode"), //
	MENU_CHAT_MODE_CONTENT("&7Op dit moment geselecteerd: %CURRENT%"), //
	MENU_CHAT_MODE_BUTTON_NORMAL("&8&lNormale Chat\n&r&8Geen delays"), //
	MENU_CHAT_MODE_BUTTON_DELAY_3("&e&l3s Delay\n&r&83s tussen de berichten bij spelers"), //
	MENU_CHAT_MODE_BUTTON_DELAY_5("&c&l5s Delay\n&r&85s tussen de berichten bij spelers"), //
	MENU_CHAT_MODE_BUTTON_RANKED("&5&lRanked\n&r&8Alleen Hero+ kan berichten sturen"), //
	MENU_CHAT_MODE_BUTTON_STAFF("&4&lStaff\n&r&8Alleen Staff kan berichten sturen"), //
	
	MENU_BOOSTER_BUTTON("%COLOR%x%AMOUNT% %GAMEMODE% Booster\n&8&lKlik &r&8om te activeren."),
	MENU_BOOSTER_CONFIRM_CONTENT("%COLOR%x%AMOUNT% %GAMEMODE% Booster\n&r&7%DURATION% %UNIT% actief\n&r\n&r&f&lBeloning:\n  &r&6+ %REWARD_AMOUNT% %TYPE%\n  &r&9+ 10.000 %EXP_TYPE% Experience"),
	MENU_BOOSTER_CONTENT_NO_BOOSTER("&cJe hebt nog geen Boosters! :(\n&r\n&r&eKoop &d&lBoosters &r&eop\n&r&b&lshop.dodocraft.nl"),
	BOOSTER_ALREADY_ACTIVE("&cEr is op dit moment al een Booster actief op %GAMEMODE%.\n&r&cProbeer het later nog eens!"),
	BOOSTER_GAMEMODE_NOT_BOOSTABLE("&c&l" + Unicode.DOUBLE_RIGHT + " &r&eHet is niet mogelijk om een Booster te activeren voor deze Gamemode"),
	;
	
	private String message;

	private LobbyMessage(String message) {
		this.message = message;
	}

	@Override
	public DodoMessageBuilder build() {
		return new DodoMessageBuilder(message);
	}

}