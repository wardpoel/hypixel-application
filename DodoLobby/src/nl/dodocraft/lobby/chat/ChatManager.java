package nl.dodocraft.lobby.chat;

import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.player.PlayerChatEvent;
import cn.nukkit.scheduler.NukkitRunnable;
import nl.dodocraft.common.Rank;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.common.log.CoreLogger;
import nl.dodocraft.common.log.LogType;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.DodoLobby;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.service.DodoService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChatManager extends DodoService {

	private ChatMode chatMode = ChatMode.NORMAL;
	private List<UUID> delayedPlayers = new ArrayList<>();
	
	@Override
	public void init() {
		CoreLogger.log(LogType.DEBUG, "Requesting chatmode...");
		
		DodoCore.getInstance().cacheGlobalData(DodoGamemode.LOBBY, "CHAT_MODE", data -> {
			updateChatMode(data);
		});
		
		DodoCore.getInstance().addGlobalDataUpdateListener(DodoGamemode.LOBBY, "CHAT_MODE", data -> {
			updateChatMode(data);
		});
	}
	
	@EventHandler
	public void onChat(PlayerChatEvent event) {
		Player player = event.getPlayer();
		ChatManager manager = DodoCraft.services().get(ChatManager.class);
		if (!manager.canChat(player)) { //Player chats to fast or has not the permission to chat (based on rank)
			manager.getChatMode().getErrorMessage().build().send(player);
			event.setCancelled(true);
			return;
		}
		
		manager.addPlayer(player);
	}
	
	private void updateChatMode(DataValue value) {
		try {
			chatMode = ChatMode.valueOf(value.asString());
		} catch(Exception e) {
			System.out.println("Unknown chat mode " + value);
			chatMode = ChatMode.NORMAL;
		}
		
		broadcastUpdateMessage();
		
		CoreLogger.log(LogType.DEBUG, "Chat mode loaded: " + chatMode.name());
	}
	
	public void broadcastUpdateMessage() {
		if (chatMode.getDelay() != 0) {
			chatMode.getUpdateMessage().build().replace(new MessageReplaceValue("%DELAY%", "" + chatMode.getDelay())).broadcast();
		} else {
			chatMode.getUpdateMessage().build().broadcast();
		}
	}

	public boolean canChat(Player player) {
		// Normal, allow chat
		if (chatMode == ChatMode.NORMAL) {
			return true;
		}
		
		// Only allow not delayed players
		if (chatMode == ChatMode.DELAY_3 || chatMode == ChatMode.DELAY_5) {
			return !delayedPlayers.contains(player.getUniqueId());
		}
		
		// Only allow ranked chat
		if (chatMode == ChatMode.RANKED) { 
			return Rank.hasPermission(Rank.HERO, DodoCore.getInstance().getRank(player.getUniqueId()));
		}
		
		// Only allow staff to chat
		return Rank.hasPermission(Rank.BOUWER, DodoCore.getInstance().getRank(player.getUniqueId()));
	}
	
	public void addPlayer(Player player) {
		if (chatMode.getDelay() == 0) return;
		if (Rank.hasPermission(Rank.HERO, DodoCore.getInstance().getRank(player.getUniqueId()))) return;
		
		delayedPlayers.add(player.getUniqueId());
		
		new NukkitRunnable() {
			@Override 
			public void run() {
				delayedPlayers.remove(player.getUniqueId());
			}
		}.runTaskLater(DodoLobby.getInstance(), chatMode.getDelay() * 20);
	}
	
	public void setChatMode(ChatMode chatMode) {
		this.chatMode = chatMode;
		broadcastUpdateMessage();
		DodoCore.getInstance().setGlobalData(DodoGamemode.LOBBY, "CHAT_MODE", chatMode.name());
	}
	
	public ChatMode getChatMode() {
		return this.chatMode;
	}
	
}