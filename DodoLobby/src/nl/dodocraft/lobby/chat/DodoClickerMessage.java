package nl.dodocraft.lobby.chat;

public enum DodoClickerMessage {

	MESSAGE_1(1, "Klik niet te hard! Dadelijk breekt het ei!"),
	MESSAGE_100(100, "Ik heb gehoord dat er bij 1 miljoen clicks iets leuks uit het ei komt..."),
	MESSAGE_500(500, "Pro tip: Klik op het ei."),
	MESSAGE_1000(1000, "Ik denk dat wij een goede click hebben."),
	MESSAGE_10000(10000, "Clicking on the button.. like SKRR"),
	MESSAGE_50000(50000, "Ik heb een goede click met deze knop."),
	MESSAGE_100000(100000, "Wanneer ontvang ik mijn &r&7&lzilveren click button&r&e&l?"),
	MESSAGE_250000(250000, "Hoevaak moet ik nog klikken voordat het ei uitkomt?"),
	MESSAGE_500000(500000, "Het wordt nu toch wel tijd voor mijn eigen Dodo..."),
	MESSAGE_750000(750000, "Dodo Clicker.. ook wel het beste spel sinds &r&6&lHabbo Hotel&r&e&l."),
	MESSAGE_1000000(1000000, "Click.. dat ben ik! Je eigen &r&6&lDodo Egg Pixel&r&e&l!");
	
	private int clicks;
	private String message;
	
	DodoClickerMessage(int clicks, String message) {
		this.clicks = clicks;
		this.message = message;
	}
	
	public int getClicks() {
		return this.clicks;
	}
	
	public String getMessage() {
		return this.message;
	}
	
}
