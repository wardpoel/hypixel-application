package nl.dodocraft.lobby.chat;

import nl.dodocraft.utils.chat.DodoMessageBuilder;
import nl.dodocraft.utils.chat.MessageEnum;

public enum LobbyNewsMessage implements MessageEnum {
	
	HOLOGRAM_1("&e&lNIEUW!"),
	HOLOGRAM_2("&f&lN&e&lIEUW!"),
	HOLOGRAM_3("&f&lNI&6&lE&e&lUW!"),
	HOLOGRAM_4("&f&lNIE&6&lU&e&lW!"),
	HOLOGRAM_5("&f&lNIEU&6&lW&e&l!"),
	HOLOGRAM_6("&f&lNIEUW&6&l!"),
	HOLOGRAM_7("&f&lNIEUW!"),
	HOLOGRAM_8("&f&lNIEUW!"),
	HOLOGRAM_9("&e&lNIEUW!"),
	HOLOGRAM_10("&6&lNIEUW!"),
	HOLOGRAM_11("&3&lNIEUW!"),
	HOLOGRAM_12("&b&lNIEUW!"),
	HOLOGRAM_13("&f&lNIEUW!"),
	HOLOGRAM_14("&e&lNIEUW!");
	
	private String message;
	
	private LobbyNewsMessage(String message) {
		this.message = message;
	}
	
	@Override
	public DodoMessageBuilder build() {
		return new DodoMessageBuilder(message);
	}
}
