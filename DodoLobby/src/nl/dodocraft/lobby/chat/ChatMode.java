package nl.dodocraft.lobby.chat;

public enum ChatMode {

	NORMAL(0, null, LobbyMessage.CHAT_MODE_UPDATED_NORMAL, "&8&lNormal"),
	DELAY_3(3, LobbyMessage.CHAT_NO_PERMS_DELAY, LobbyMessage.CHAT_MODE_UPDATED_DELAY, "&e&l3s Delay"),
	DELAY_5(5, LobbyMessage.CHAT_NO_PERMS_DELAY, LobbyMessage.CHAT_MODE_UPDATED_DELAY, "&c&l5s Delay"),
	RANKED(0, LobbyMessage.CHAT_NO_PERMS_RANKED_ONLY, LobbyMessage.CHAT_MODE_UPDATED_RANKED, "&5&lRanked"),
	STAFF(0, LobbyMessage.CHAT_NO_PERMS_STAFF_ONLY, LobbyMessage.CHAT_MODE_UPDATED_STAFF, "&4&lStaff");
	
	private int delay;
	private LobbyMessage message;
	private LobbyMessage updateMessage;
	private String name;
	
	ChatMode(int delay, LobbyMessage message, LobbyMessage updateMessage, String name) {
		this.delay = delay;
		this.message = message;
		this.updateMessage = updateMessage;
		this.name = name;
	}
	
	public int getDelay() {
		return this.delay;
	}
	
	public LobbyMessage getErrorMessage() {
		return this.message;
	}
	
	public LobbyMessage getUpdateMessage() {
		return this.updateMessage;
	}
	
	public String getChatModeName() {
		return this.name;
	}

}
