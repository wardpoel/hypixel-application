package nl.dodocraft.lobby.server;

import cn.nukkit.event.EventHandler;
import cn.nukkit.event.block.BlockBreakEvent;
import cn.nukkit.event.block.BlockPlaceEvent;
import cn.nukkit.event.entity.EntityDamageByEntityEvent;
import cn.nukkit.event.entity.EntityDamageEvent;
import cn.nukkit.event.player.PlayerDropItemEvent;
import cn.nukkit.event.player.PlayerFoodLevelChangeEvent;
import cn.nukkit.event.player.PlayerGameModeChangeEvent;
import cn.nukkit.event.player.PlayerInteractEvent;
import cn.nukkit.event.player.PlayerKickEvent;
import cn.nukkit.event.player.PlayerKickEvent.Reason;
import cn.nukkit.event.player.PlayerQuitEvent;
import cn.nukkit.event.weather.WeatherChangeEvent;
import cn.nukkit.level.GameRule;
import cn.nukkit.level.Location;
import cn.nukkit.math.Vector3;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.DodoLobby;
import nl.dodocraft.lobby.server.management.ChatModeManagement;
import nl.dodocraft.lobby.server.management.NewsManagement;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.helpers.LocationHelper;
import nl.dodocraft.utils.server.ServerManager;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.server.stafftools.management.ManagementManager;
import nl.dodocraft.utils.service.DodoService;

public class LobbyManager extends DodoService {

	private DodoConfig config = new DodoConfig(DodoLobby.getInstance());
	private Location lobbySpawn;

	@Override
	public void init() {
		DodoCraft.services().get(ManagementManager.class).register(new ChatModeManagement());
		DodoCraft.services().get(ManagementManager.class).register(new NewsManagement());
		DodoCraft.services().get(ServerManager.class).setMaxPlayers(30);
		
		if (!DodoLobby.getInstance().getConfig().exists("lobbySpawn")) {
			DodoLobby.getInstance().getConfig().set("lobbySpawn", "2095.5_126.5_1181.5_270_0");
			DodoLobby.getInstance().getConfig().save();
		}

		this.lobbySpawn = LocationHelper.deserialize(DodoLobby.getInstance().getConfig().getString("lobbySpawn"));

		// Gamerules
		DodoLobby.getInstance().getServer().setPropertyBoolean(GameRule.DO_MOB_SPAWNING.getName(), false);
		DodoLobby.getInstance().getServer().setPropertyBoolean(GameRule.DO_DAYLIGHT_CYCLE.getName(), false);
		DodoLobby.getInstance().getServer().getPropertyBoolean(GameRule.DO_WEATHER_CYCLE.getName(), false);
		DodoLobby.getInstance().getServer().setPropertyBoolean(GameRule.DO_FIRE_TICK.getName(), false);
		DodoLobby.getInstance().getServer().setPropertyBoolean(GameRule.DROWNING_DAMAGE.getName(), false);
		DodoLobby.getInstance().getServer().setPropertyBoolean(GameRule.SHOW_DEATH_MESSAGE.getName(), false);
		DodoLobby.getInstance().getServer().setPropertyInt(GameRule.DO_FIRE_TICK.getName(), 0);

		DodoCraft.server().getLevelByName("world").setSpawnLocation(new Vector3(lobbySpawn.getX(), lobbySpawn.getY(), lobbySpawn.getZ()));
	}

	public Location getLobbySpawn() {
		return lobbySpawn.clone().add(0, 2, 0);
	}

	@EventHandler
	public void on(PlayerDropItemEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void on(BlockBreakEvent event) {
		event.setCancelled(event.getPlayer().getGamemode() != 1);
	}

	@EventHandler
	public void on(PlayerGameModeChangeEvent event) {
		event.setCancelled(!Rank.hasPermission(Rank.ADMIN, DodoCore.getInstance().getRank(event.getPlayer().getUniqueId())));
	}

	@EventHandler
	public void on(EntityDamageEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void on(EntityDamageByEntityEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void on(PlayerFoodLevelChangeEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void on(BlockPlaceEvent event) {
		event.setCancelled(event.getPlayer().getGamemode() != 1);
	}

	@EventHandler
	public void on(PlayerQuitEvent event) {
		event.setQuitMessage("");
	}

	@EventHandler
	public void on(PlayerKickEvent event) {
		event.setCancelled(event.getReasonEnum() == Reason.FLYING_DISABLED);
	}
	
	@EventHandler
	public void on(WeatherChangeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		double x = event.getBlock().getX();
		double y = event.getBlock().getY();
		double z = event.getBlock().getZ();
		
		event.setCancelled(event.getBlock().getLevel().getBlockEntity(new Vector3(x, y, z)) != null);
	}
	
	public DodoConfig getConfig() {
		return config;
	}

}
