package nl.dodocraft.lobby.server.management;

import cn.nukkit.Player;
import nl.dodocraft.common.Rank;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.server.stafftools.management.Management;

public class ChatModeManagement implements Management {
	
	@Override
	public String getName() {
		return "Chat Mode";
	}
	
	@Override
	public Rank getRank() {
		return Rank.ADMIN;
	}
	
	@Override
	public void onClick(Player player) {
		LobbyMenu.CHAT_MODE.open(player);
	}
	
}
