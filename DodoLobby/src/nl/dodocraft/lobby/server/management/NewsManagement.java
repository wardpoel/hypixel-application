package nl.dodocraft.lobby.server.management;

import cn.nukkit.Player;
import nl.dodocraft.common.Rank;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.server.stafftools.management.Management;

public class NewsManagement implements Management {
	@Override
	public String getName() {
		return "DodoCraft News";
	}
	
	@Override
	public Rank getRank() {
		return Rank.ADMIN;
	}
	
	@Override
	public void onClick(Player player) {
		LobbyMenu.NEWS.open(player);
	}
}
