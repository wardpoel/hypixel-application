package nl.dodocraft.lobby.server;

import cn.nukkit.event.EventHandler;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.scheduler.NukkitRunnable;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.DodoLobby;
import nl.dodocraft.lobby.chat.LobbyNewsMessage;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.visual.hologram.DodoHologram;
import nl.dodocraft.utils.world.DodoLocation;

public class NewsManager extends DodoService {
	
	public static final String DATA_KEY_ENABLED = "NEWS_ENABLED";
	private static final DodoLocation LOCATION = new DodoLocation(8.5, 66, -0.5);
	
	private DodoHologram hologram = null;
	
	@Override
	public void init() throws Exception {
		
		DodoCore.getInstance().cacheGlobalData(DodoGamemode.LOBBY, DATA_KEY_ENABLED, value -> {
			if (value.getValue() == null) return;
			update(value.asBoolean());
		});
		
		DodoCore.getInstance().addGlobalDataUpdateListener(DodoGamemode.LOBBY, DATA_KEY_ENABLED, value -> {
			if (value.getValue() == null) return;
			update(value.asBoolean());
		});
	}
	
	public void setEnabled(boolean enabled) {
		update(enabled);
		DodoCore.getInstance().setGlobalData(DodoGamemode.LOBBY, DATA_KEY_ENABLED, enabled);
	}
	
	private void update(boolean enabled) {
		if (!enabled) {
			if (hologram != null) {
				hologram.destroy();
				hologram = null;
			}
			
			return;
		}
		
		hologram = new DodoHologram(LOCATION, TextFormat.colorize('&', ""), TextFormat.colorize('&', "&d&lBoosters &r&evoor maar &66,99&e!"));
		hologram.spawnToAll();
		effect();
	}
	
	private void effect() {
		new NukkitRunnable() {
			int index = 0;
			@Override
			public void run() {
				if (hologram == null) {
					this.cancel();
					return;
				}
				
				hologram.setLine(0, LobbyNewsMessage.values()[index].build().get());
				index++;
				
				if (index >= LobbyNewsMessage.values().length) {
					index = 0;
				}
			}
		}.runTaskTimer(DodoLobby.getInstance(), 0, 5);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if (hologram == null) return;
		
		hologram.spawn(event.getPlayer());
	}
	
}
