package nl.dodocraft.lobby;

import cn.nukkit.entity.data.Skin;
import nl.dodocraft.utils.server.ResourceList;

import java.awt.image.BufferedImage;

public class LobbyResource extends ResourceList {

	private static final DodoLobby PLUGIN = DodoLobby.getInstance();
	
	public static final Skin NPC_SURVIVAL = asSkin(PLUGIN, "skins/npcs/survival.png");
	public static final Skin NPC_LUCKY_LEGENDS = asSkin(PLUGIN, "skins/npcs/luckylegends.png");
	public static final Skin NPC_BEDWARS = asSkin(PLUGIN, "skins/npcs/bedwars.png");
	public static final Skin NPC_RANKS = asSkin(PLUGIN, "skins/npcs/ranks.png");
	public static final Skin NPC_CREDITS = asSkin(PLUGIN, "skins/npcs/credits.png");
	public static final Skin NPC_BUNDELS = asSkin(PLUGIN, "skins/npcs/bundels.png");
	public static final Skin NPC_DIRECTOR = asSkin(PLUGIN, "skins/npcs/director.png");
	public static final Skin NPC_UNKNOWN = asSkin(PLUGIN, "skins/npcs/unknown.png");
	
	public static final Skin NPC_POSTMAN = asSkin(PLUGIN, "skins/npcs/postman.png");
	
	public static final BufferedImage CAPE_CROWN = asImage(PLUGIN, "skins/capes/crown.png");
	public static final BufferedImage CAPE_JEB = asImage(PLUGIN, "skins/capes/jeb.png");
	
	public static final Skin PIXEL_DODO_EGG = asSkin(PLUGIN, "skins/pixels/dodoEgg.png");
	public static final Skin PIXEL_DODO_CAKE = asSkin(PLUGIN, "skins/pixels/dodoCake.png");
	
}
