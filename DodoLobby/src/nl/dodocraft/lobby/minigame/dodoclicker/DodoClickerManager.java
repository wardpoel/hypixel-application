package nl.dodocraft.lobby.minigame.dodoclicker;

import cn.nukkit.Player;
import cn.nukkit.block.Block;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.player.PlayerInteractEvent;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.event.player.PlayerQuitEvent;
import cn.nukkit.scheduler.NukkitRunnable;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.DodoLobby;
import nl.dodocraft.lobby.chat.DodoClickerMessage;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.player.cosmetics.Cosmetic;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.lobby.server.LobbyManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.helpers.NumberHelper;
import nl.dodocraft.utils.server.config.DodoConfig;
import nl.dodocraft.utils.service.DodoService;
import nl.dodocraft.utils.visual.hologram.DodoHologram;
import nl.dodocraft.utils.visual.particle.DodoParticle;
import nl.dodocraft.utils.world.DodoLocation;

import javax.naming.ConfigurationException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class DodoClickerManager extends DodoService {

	private static final String CLICKS_KEY = "DODO_CLICKER_CLICKS";
	private static final String CLICKS_TODAY_KEY = "DODO_CLICKER_CLICKS_TODAY";
	private static final String NEXT_RESET_KEY = "DODO_CLICKER_NEXT_RESET";
	
	private static final int MAX_CLICKS_A_DAY = 25000;

	private DodoLocation location = null;
	private Map<UUID, Integer> clickers = new HashMap<>();
	private Map<UUID, Integer> clickersToday = new HashMap<>();
	private Set<UUID> allowed = new HashSet<>();
	private Map<UUID, DodoHologram> playerHolos = new HashMap<>();

	@Override
	public void init() throws Exception {
		DodoConfig config = DodoCraft.services().get(LobbyManager.class).getConfig();

		DodoCore.getInstance().cachePlayerData(DodoGamemode.LOBBY, CLICKS_KEY);
		DodoCore.getInstance().cachePlayerData(DodoGamemode.LOBBY, CLICKS_TODAY_KEY);
		DodoCore.getInstance().cachePlayerData(DodoGamemode.LOBBY, NEXT_RESET_KEY);
		
		LocalDateTime now = LocalDateTime.now(ZoneId.systemDefault());
		LocalDateTime updateTime = LocalDateTime.now(ZoneId.systemDefault()).withHour(6).withMinute(0).withSecond(1);
		
		if (now.isAfter(updateTime)) {
			updateTime = updateTime.plusDays(1);
		}
		
		long seconds = ChronoUnit.SECONDS.between(now, updateTime);
		
		if (seconds < 0) {
			seconds *= -1;
		}
		
		new NukkitRunnable() {
			@Override
			public void run() {
				for (Player player : DodoCraft.players()) {
					clickersToday.put(player.getUniqueId(), 0);
					allowed.add(player.getUniqueId());
				}
			}
		}.runTaskTimer(DodoLobby.getInstance(), (int) seconds * 20, 86400 * 20);
		
		// Unregister service if not configured
		if (!config.exists("dodoclicker")) {
			throw new ConfigurationException("Location not set");
		}

		location = config.get("dodoclicker.location", DodoLocation.class);
		if (location == null) {
			throw new ConfigurationException("Invalid location");
		}
	}

	@EventHandler (ignoreCancelled = true)
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		int clicks = getClicks(player);
		int clicksToday = getClicksToday(player);

		clickers.put(player.getUniqueId(), clicks);
		clickersToday.put(player.getUniqueId(), clicksToday);

		DodoHologram dodoClicker = new DodoHologram(new DodoLocation(location).add(0, 1, 0), "&b&lDodo Clicker", getPersonalHoloName(clicks, true));
		dodoClicker.spawn(player);

		playerHolos.put(player.getUniqueId(), dodoClicker);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();

		if (allowed.contains(player.getUniqueId())) {
			save(player);
			allowed.remove(player.getUniqueId());
		}
		
		clickers.remove(player.getUniqueId());
		clickersToday.remove(player.getUniqueId());
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		if (event.getBlock().getId() == Block.DRAGON_EGG) {
			event.setCancelled(true);
			Player player = event.getPlayer();
			
			if (!allowed.contains(player.getUniqueId())) {
				DodoLocation loc = new DodoLocation(event.getBlock().getLocation().clone().add(0.5, 0.5, 0.5));
				DodoParticle.LARGE_SMOKE.setScale(5).setLocation(loc).display(player);
				return;
			}
			
			click(player);
			
			if (clickersToday.get(player.getUniqueId()) >= MAX_CLICKS_A_DAY) {
				allowed.remove(player.getUniqueId());
				save(player);
			}
			
			int clicks = clickers.get(player.getUniqueId());
			playerHolos.get(player.getUniqueId()).setLine(1, getPersonalHoloName(clicks));

			String message = getMessage(clicks);
			if (message != null)
				player.sendMessage(message);
		}
	}
	
	private void click(Player player) {
		if (clickersToday.containsKey(player.getUniqueId())) {
			clickersToday.put(player.getUniqueId(), clickersToday.get(player.getUniqueId()) + 1);
		} else {
			clickersToday.put(player.getUniqueId(), getClicksToday(player) + 1);
		}
		
		if (clickers.containsKey(player.getUniqueId())) {
			clickers.put(player.getUniqueId(), clickers.get(player.getUniqueId()) + 1);
		} else {
			clickers.put(player.getUniqueId(), getClicks(player) + 1);
		}
		
		// Save every 20 clicks
		if (clickers.get(player.getUniqueId()) != null && clickers.get(player.getUniqueId()) % 20 == 0) {
			save(player);
		}
		
		// Give exclusive DodoPixelEgg
		if (clickers.get(player.getUniqueId()) != null && clickers.get(player.getUniqueId()) == 1000000) {
			DodoCraft.services().get(CosmeticManager.class).unlock(player.getUniqueId(), Cosmetic.PIXEL_DODO_EGG);
		}
	}
	
	private int getClicksToday(Player player) {
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, NEXT_RESET_KEY, Long.class) != null) {
			long millis = DodoCore.getInstance().getPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, NEXT_RESET_KEY, Long.class);
			
			LocalDateTime now = LocalDateTime.now(ZoneId.systemDefault());
			LocalDateTime nextReset = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault());
			
			if (nextReset.isAfter(now)) {
				if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, CLICKS_TODAY_KEY, Integer.class) != null) {
					int clicks = DodoCore.getInstance().getPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, CLICKS_TODAY_KEY, Integer.class);
					if (clicks < MAX_CLICKS_A_DAY) {
						allowed.add(player.getUniqueId());
					}
					return clicks;
				}
			}
		}
		allowed.add(player.getUniqueId());
		return 0;
	}

	private int getClicks(Player player) {
		int clicks = 0;
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, CLICKS_KEY, Integer.class) != null) {
			clicks = DodoCore.getInstance().getPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, CLICKS_KEY, Integer.class);
		}
		return clicks;
	}
	
	private String getMessage(int clicks) {
		for (DodoClickerMessage message : DodoClickerMessage.values()) {
			if (clicks == message.getClicks()) {
				return LobbyMessage.DODO_CLICKER_PLAYER_CLICK.build().replace(new MessageReplaceValue("%MESSAGE%", message.getMessage())).get();
			}
		}
		return null;
	}
	
	private String getPersonalHoloName(int clicks, boolean format) {
		String amount = format ? NumberHelper.format(clicks) : clicks + "";
		if (clicks >= 1000000) {
			return "&6" + Unicode.STAR_1 + " &e&lClicks: &r&3&l" + amount + " &r&6" + Unicode.STAR_1;
		} else if (clicks >= 1000) {
			return "&b" + Unicode.STAR_2 + " &e&lClicks: &r&3&l" + amount + " &r&b" + Unicode.STAR_2;
		} else if (clicks >= 1) {
			return "&e&lClicks: &3&l" + amount;
		}
		return "&eKlik op het ei voor clicks.";
	}
	
	private String getPersonalHoloName(int clicks) {
		return getPersonalHoloName(clicks, false);
	}

	private void save(Player player) {
		DodoCore.getInstance().setPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, CLICKS_KEY, clickers.get(player.getUniqueId()));
		DodoCore.getInstance().setPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, CLICKS_TODAY_KEY, clickersToday.get(player.getUniqueId()));
		DodoCore.getInstance().setPlayerData(player.getUniqueId(), DodoGamemode.LOBBY, NEXT_RESET_KEY, getNextReset());
	}
	
	private long getNextReset() {
		LocalDateTime resetTime = LocalDateTime.now(ZoneId.systemDefault());
		if (resetTime.getHour() >= 6) {
			resetTime = resetTime.plusDays(1);
		}
		resetTime = resetTime.withHour(6).withMinute(0).withNano(0);
		return resetTime.toInstant(ZoneOffset.UTC).toEpochMilli();
	}

}
