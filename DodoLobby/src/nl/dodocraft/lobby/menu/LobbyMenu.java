package nl.dodocraft.lobby.menu;

import cn.nukkit.Player;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.lobby.menu.content.MenuGameSelector;
import nl.dodocraft.lobby.menu.content.MenuGameSelectorMode;
import nl.dodocraft.lobby.menu.content.MenuLobbySelector;
import nl.dodocraft.lobby.menu.content.admin.MenuChatMode;
import nl.dodocraft.lobby.menu.content.admin.MenuNews;
import nl.dodocraft.lobby.menu.content.boosters.MenuBoosters;
import nl.dodocraft.lobby.menu.content.boosters.MenuBoostersConfirm;
import nl.dodocraft.lobby.menu.content.cosmetic.MenuCosmeticConfirmPurchase;
import nl.dodocraft.lobby.menu.content.cosmetic.MenuCosmeticSelector;
import nl.dodocraft.lobby.menu.content.cosmetic.MenuCosmeticSelectorType;
import nl.dodocraft.lobby.menu.content.cosmetic.info.MenuInfoCosmetics;
import nl.dodocraft.lobby.menu.content.cosmetic.info.MenuInfoCosmeticsCosmetic;
import nl.dodocraft.lobby.menu.content.cosmetic.info.MenuInfoCosmeticsType;
import nl.dodocraft.utils.visual.menu.MenuEnum;
import nl.dodocraft.utils.visual.menu.MenuFormat;

import java.util.ArrayList;
import java.util.List;

public enum LobbyMenu implements MenuEnum {

	GAME_SELECTOR(MenuGameSelector.class),
	GAME_SELECTOR_MODE(MenuGameSelectorMode.class),
	LOBBY_SELECTOR(MenuLobbySelector.class),
	COSMETIC_SELECTOR(MenuCosmeticSelector.class),
	COSMETIC_SELECTOR_TYPE(MenuCosmeticSelectorType.class),
	COSMETIC_CONFIRM_PURCHASE(MenuCosmeticConfirmPurchase.class),
	BOOSTERS(MenuBoosters.class),
	BOOSTSER_CONFIRM(MenuBoostersConfirm.class),
	
	// Admin
	CHAT_MODE(MenuChatMode.class),
	NEWS(MenuNews.class),
	COSMETICS_INFO(MenuInfoCosmetics.class),
	COSMETICS_INFO_TYPE(MenuInfoCosmeticsType.class),
	COSMETICS_INFO_COSMETIC(MenuInfoCosmeticsCosmetic.class);

	private Class<? extends MenuFormat> format;

	LobbyMenu(Class<? extends MenuFormat> format) {
		this.format = format;
	}

	@Override
	public void open(Player player) {
		getInstance().open(player);
	}

	@Override
	public void open(Player player, Object... objects) {
		List<DataValue> values = new ArrayList<>();
		
		for(Object obj : objects) {
			values.add(new DataValue(obj));
		}
		
		getInstance().open(player, values.toArray(new DataValue[values.size()]));
	}

	@Override
	public MenuFormat getInstance(Object... constructorArguments) {
		final Class<?>[] classes = new Class<?>[constructorArguments.length];
		for (int i = 0; i < constructorArguments.length; i++) {
			classes[i] = constructorArguments[i].getClass();
		}

		final Object[] parameters = new Object[constructorArguments.length];
		for (int i = 0; i < constructorArguments.length; i++) {
			parameters[i] = constructorArguments[i];
		}

		try {
			return format.getConstructor(classes).newInstance(parameters);
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
