package nl.dodocraft.lobby.menu.content;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.common.server.Server;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.server.ServerManager;
import nl.dodocraft.utils.server.redirect.RedirectManager;
import nl.dodocraft.utils.server.redirect.ServerRedirectMode;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

import java.util.UUID;

public class MenuGameSelectorMode extends MenuFormat {

	@Override
	public void open(Player player, DataValue... args) {
		DodoGamemode gamemode = args[0].as(DodoGamemode.class);
		UUID uuid = player.getUniqueId();
		
		DodoMenu menu = new DodoMenu(LobbyMessage.MENU_GAME_SELECTOR_MODE_TITLE.build().replace(//
			new MessageReplaceValue("%COLOR%", gamemode.getColorCode()), //
			new MessageReplaceValue("%NAME%", gamemode.getName())
		).get());

		// For every gamemode add a button (Solo, Duo, Team,...)
		for (Server server : Server.values()) {
			if (server.getGamemode() != gamemode) continue;

			int players = DodoCraft.services().get(ServerManager.class).getPlayerCount(server);
			menu.addButton(LobbyMessage.MENU_GAME_SELECTOR_GAMENAME.build().replace(
				new MessageReplaceValue("%COLOR%", server.getColorCode()),
				new MessageReplaceValue("%NAME%", server.getName()),
				new MessageReplaceValue("%PLAYERS%", "" + players),
				new MessageReplaceValue("%S%", (players == 1 ? "" : "s"))
			).get(), whoClicked -> {
				DodoCraft.services().get(RedirectManager.class).redirect(uuid, server, ServerRedirectMode.MOST_PLAYERS);
			});
		}
		
		menu.addButton(TextFormat.colorize('&', "&8&l" + Unicode.DOUBLE_LEFT + " Terug"), whoClicked -> {
			LobbyMenu.GAME_SELECTOR.open(player);
		});

		menu.open(player);
	}

}