package nl.dodocraft.lobby.menu.content.boosters;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.boosters.Booster;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Message;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.game.boosters.BoosterManager;
import nl.dodocraft.utils.helpers.TimeStringHelper;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuBoostersConfirm extends MenuFormat {

	@Override
	public void open(Player player, DataValue... dataValues) {
		if (dataValues.length != 1) return;
		
		Booster booster = dataValues[0].as(Booster.class);
		BoosterManager boosterManager = DodoCraft.services().get(BoosterManager.class);
		
		DodoMenu menu = new DodoMenu(TextFormat.colorize('&', "&8&lWeet je het zeker?"));

		String experienceType = booster.getGamemode().getName();
		
		String content = LobbyMessage.MENU_BOOSTER_CONFIRM_CONTENT.build().replace(
			new MessageReplaceValue("%COLOR%", booster.getGamemode().getColorCode() + "&l"),
			new MessageReplaceValue("%AMOUNT%", boosterManager.getAmount(booster)),
			new MessageReplaceValue("%GAMEMODE%", booster.getGamemode().getName()),
			new MessageReplaceValue("%DURATION%", booster.getDuration() + ""),
			new MessageReplaceValue("%UNIT%", TimeStringHelper.getDutchUnit(booster.getUnit(), booster.getDuration() != 1)),
			new MessageReplaceValue("%REWARD_AMOUNT%", booster.getGamemode() == DodoGamemode.SURVIVAL ? "2.500" : "5.000"),
			new MessageReplaceValue("%TYPE%", booster.getGamemode().getCurrency().getDisplayName()),
			new MessageReplaceValue("%EXP_TYPE%", experienceType)
		).get();
		
		menu.setContent(content);
		menu.addButton(TextFormat.colorize('&', "&2&lJa!\n&r&8Booster activeren."), whoClicked -> {
			if (DodoCore.getInstance().getActiveBooster(booster.getGamemode()) != null) {
				LobbyMessage.BOOSTER_ALREADY_ACTIVE.build().replace(
						new MessageReplaceValue("%GAMEMODE%", booster.getGamemode().getName())
				).send(whoClicked);
				return;
			}
			
			if (!booster.getGamemode().canBeBoosted()) {
				LobbyMessage.BOOSTER_GAMEMODE_NOT_BOOSTABLE.build().send(whoClicked);
				return;
			}
			
			DodoCore.getInstance().activateBooster(whoClicked.getUniqueId(), booster, success -> {
				if (success) {
					DodoCraft.services().get(BoosterManager.class).giveBoosterReward(whoClicked, booster);
					return;
				};
				
				Message.COMMAND_INTERNAL_ERROR.build().send(whoClicked);
			});
		});
		
		menu.addButton(TextFormat.colorize('&', "&c&lNee!\n&r&8Niet activeren."), whoClicked -> {
			LobbyMenu.BOOSTERS.open(player);
		});
		
		menu.open(player);
	}

}
