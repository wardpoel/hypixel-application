package nl.dodocraft.lobby.menu.content.boosters;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.boosters.Booster;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.game.boosters.BoosterManager;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

import java.util.List;

public class MenuBoosters extends MenuFormat {
	
	@Override
	public void open(Player player, DataValue... dataValues) {
		DodoMenu menu = new DodoMenu(TextFormat.colorize('&', "&8&lMijn Boosters"));
		List<Booster> boosters = DodoCore.getInstance().getBoosters(player.getUniqueId());
		
		if (boosters.isEmpty()) {
			menu.setContent(LobbyMessage.MENU_BOOSTER_CONTENT_NO_BOOSTER.build().get());
			menu.open(player);
			return;
		}
		
		BoosterManager manager = DodoCraft.services().get(BoosterManager.class);
		for (Booster booster : boosters) {
			if (!booster.getGamemode().canBeBoosted()) continue;
			
			String text = LobbyMessage.MENU_BOOSTER_BUTTON.build().replace(
					new MessageReplaceValue("%COLOR%", booster.getGamemode().getColorCode() + "&l"),
					new MessageReplaceValue("%AMOUNT%", manager.getAmount(booster)),
					new MessageReplaceValue("%GAMEMODE%", booster.getGamemode().getName())
			).get();
			
			menu.addButton(text, whoClicked -> {
				LobbyMenu.BOOSTSER_CONFIRM.open(whoClicked, booster);
			});
		}
		
		menu.open(player);
	}
	
}
