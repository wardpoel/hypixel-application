package nl.dodocraft.lobby.menu.content.cosmetic;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.lobby.player.cosmetics.Cosmetic;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.lobby.player.cosmetics.CosmeticType;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuCosmeticSelectorType extends MenuFormat {
	
	@Override
	public void open(Player player, DataValue... dataValues) {
		CosmeticManager manager = DodoCraft.services().get(CosmeticManager.class);
		CosmeticType type = dataValues[0].as(CosmeticType.class);
		
		DodoMenu menu = new DodoMenu(LobbyMessage.MENU_COSMETIC_SELECTOR_TYPE_TITLE.build().replace(new MessageReplaceValue("%TYPE%", type.getNameSingular())).get());
		
		for (Cosmetic cosmetic : Cosmetic.values()) {
			if (cosmetic.getType() != type) continue;
			String name = cosmetic.getInstance().getName();
			
			int price = cosmetic.getInstance().getPrice();
			int credits = DodoCore.getInstance().getCredits(player.getUniqueId());
			String priceInfo = "";
			if (price <= 0) {
				priceInfo = "&6&lExclusief";
			} else if (price >= credits) {
				priceInfo = "&7Prijs: &2" + price + " Credits";
			} else {
				priceInfo = "&7Prijs: &4" + price + " Credits";
			}
			
			String text = LobbyMessage.MENU_COSMETIC_SELECTOR_COSMETIC_BUTTON_LOCKED.build().replace(
					new MessageReplaceValue("%NAME%", name),
					new MessageReplaceValue("%PRICE%", priceInfo)).get();
			
			if (manager.getSelected(player, type) == cosmetic) {
				text = LobbyMessage.MENU_COSMETIC_SELECTOR_COSMETIC_BUTTON_ACTIVE.build().replace(
						new MessageReplaceValue("%NAME%", name)).get();
			} else if (manager.has(player.getUniqueId(), cosmetic)) {
				text = LobbyMessage.MENU_COSMETIC_SELECTOR_COSMETIC_BUTTON_UNLOCKED.build().replace(
						new MessageReplaceValue("%NAME%", name)).get();
			}
			
			menu.addButton(TextFormat.colorize('&', text), whoClicked -> {
				if (!manager.has(whoClicked.getUniqueId(), cosmetic)) {
					if (price <= 0) {
						LobbyMessage.COSMETIC_EXCLUSIVE_ONLY.build().send(whoClicked);
						return;
					}
					LobbyMenu.COSMETIC_CONFIRM_PURCHASE.open(whoClicked, cosmetic);
					return;
				}
				
				if (manager.getSelected(whoClicked, cosmetic.getType()) == cosmetic) { // Unequip
					manager.unequip(whoClicked, cosmetic.getType());
					LobbyMessage.COSMETIC_UNEQUIP.build().replace(
							new MessageReplaceValue("%NAME%", cosmetic.getInstance().getName()),
							new MessageReplaceValue("%TYPE%", cosmetic.getType().getNameSingular())
					).send(whoClicked);
					return;
				}
				
				// Equip
				manager.setSelected(whoClicked, cosmetic);
				LobbyMessage.COSMETIC_EQUIP.build().replace(
						new MessageReplaceValue("%NAME%", cosmetic.getInstance().getName()),
						new MessageReplaceValue("%TYPE%", cosmetic.getType().getNameSingular())
				).send(whoClicked);
			});
		}
		
		menu.addButton(TextFormat.colorize('&', "&8&l" + Unicode.DOUBLE_LEFT + " Terug"), whoClicked -> {
			LobbyMenu.COSMETIC_SELECTOR.open(player);
		});
		
		menu.open(player);
	}
	
}
