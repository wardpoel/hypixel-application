package nl.dodocraft.lobby.menu.content.cosmetic.info;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.lobby.player.cosmetics.CosmeticType;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

import java.util.UUID;

public class MenuInfoCosmetics extends MenuFormat {

	@Override
	public void open(Player player, DataValue... dataValues) {
		UUID uuid = dataValues[0].asUUID();
		CosmeticManager manager = DodoCraft.services().get(CosmeticManager.class);
		DodoMenu menu = new DodoMenu(TextFormat.colorize('&', "Cosmetics Info"));
		
		for (CosmeticType type : CosmeticType.values()) {
			String text = type.getName() + " (" + manager.getUnlockedAmount(uuid, type) + "/" + manager.getMaxAmount(type) + ")";
			
			menu.addButton(text, whoClicked -> {
				LobbyMenu.COSMETICS_INFO_TYPE.open(whoClicked, dataValues[0].asUUID(), dataValues[1].asString(), type);
			});
		}
		
		menu.open(player);
	}

}
