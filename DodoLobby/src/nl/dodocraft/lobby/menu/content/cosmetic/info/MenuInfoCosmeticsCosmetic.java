package nl.dodocraft.lobby.menu.content.cosmetic.info;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.lobby.player.cosmetics.Cosmetic;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

import java.util.UUID;

public class MenuInfoCosmeticsCosmetic extends MenuFormat {
	@Override
	public void open(Player player, DataValue... dataValues) {
		UUID uuid = dataValues[0].asUUID();
		String name = dataValues[1].asString();
		Cosmetic cosmetic = dataValues[2].as(Cosmetic.class);
		
		CosmeticManager manager = DodoCraft.services().get(CosmeticManager.class);
		
		DodoMenu menu = new DodoMenu(TextFormat.colorize('&', "Cosmetic Info"));
		
		String info = "&8Naam: " + cosmetic.getInstance().getName();
		info += "\n&8Type: " + cosmetic.getType().getNameSingular();
		
		menu.setContent(info);
		
		String text = "";
		boolean has = manager.has(uuid, cosmetic);
		
		if (has) {
			text = "&4Verijderen";
		} else {
			text = "&2Ontgrendelen";
		}
		
		menu.addButton(TextFormat.colorize('&', text), whoClicked -> {
			if (!has) {
				manager.unlock(uuid, cosmetic);
				LobbyMessage.COSMETIC_UNLOCKED.build().replace(
						new MessageReplaceValue("%NAME%", cosmetic.getInstance().getName()),
						new MessageReplaceValue("%PLAYER%", name)
				).send(whoClicked);
				return;
			}
			
			manager.lock(uuid, cosmetic);
			LobbyMessage.COSMETIC_LOCKED.build().replace(
					new MessageReplaceValue("%NAME%", cosmetic.getInstance().getName()),
					new MessageReplaceValue("%PLAYER%", name)
			).send(whoClicked);
		});
		
		menu.addButton(TextFormat.colorize('&', "&8&l" + Unicode.DOUBLE_LEFT + " Terug"), whoClicked -> {
			LobbyMenu.COSMETIC_SELECTOR_TYPE.open(player, uuid, name, cosmetic.getType());
		});
		
		menu.open(player);
	}
}
