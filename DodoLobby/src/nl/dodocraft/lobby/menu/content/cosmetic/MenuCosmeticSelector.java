package nl.dodocraft.lobby.menu.content.cosmetic;

import cn.nukkit.Player;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.lobby.player.cosmetics.CosmeticType;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuCosmeticSelector extends MenuFormat {
	
	private final static CosmeticType[] ENABLED_COSMETICS = new CosmeticType[]{CosmeticType.TITLE, CosmeticType.PIXEL};
	
	@Override
	public void open(Player player, DataValue... args) {
		CosmeticManager manager = DodoCraft.services().get(CosmeticManager.class);
		DodoMenu menu = new DodoMenu(LobbyMessage.MENU_COSMETIC_SELECTOR_TITLE.build().get());
		
		for (CosmeticType type : ENABLED_COSMETICS) {
			int unlocked = manager.getUnlockedAmount(player.getUniqueId(), type);
			int max = manager.getMaxAmount(type);
			
			String text = LobbyMessage.MENU_COSMETIC_SELECTOR_TYPE_BUTTON.build().replace(
					new MessageReplaceValue("%TYPE%", type.getName()),
					new MessageReplaceValue("%UNLOCKED%", unlocked + ""),
					new MessageReplaceValue("%MAX%", max + "")
			).get();
			
			if (unlocked == 0) {
				text = LobbyMessage.MENU_COSMETIC_SELECTOR_TYPE_BUTTON_HAS_NONE.build().replace(
						new MessageReplaceValue("%TYPE%", type.getName())).get();
			}
			
			menu.addButton(text, whoClicked -> {
				LobbyMenu.COSMETIC_SELECTOR_TYPE.open(whoClicked, type);
			});
		}
		
		menu.open(player);
	}
}