package nl.dodocraft.lobby.menu.content.cosmetic.info;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.lobby.player.cosmetics.Cosmetic;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.lobby.player.cosmetics.CosmeticType;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.Unicode;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

import java.util.UUID;

public class MenuInfoCosmeticsType extends MenuFormat {
	@Override
	public void open(Player player, DataValue... dataValues) {
		UUID uuid = dataValues[0].asUUID();
		String name = dataValues[1].asString();
		CosmeticType type = dataValues[2].as(CosmeticType.class);
		
		CosmeticManager manager = DodoCraft.services().get(CosmeticManager.class);
		
		DodoMenu menu = new DodoMenu(TextFormat.colorize('&', "Cosmetics Info - Type"));
		
		for (Cosmetic cosmetic : Cosmetic.values()) {
			if (cosmetic.getType() != type) continue;
			
			String text = (manager.has(uuid, cosmetic) ? "&2" : "&4") + cosmetic.getInstance().getName() ;
			menu.addButton(TextFormat.colorize('&', text), whoClicked -> {
				LobbyMenu.COSMETICS_INFO_COSMETIC.open(whoClicked, uuid, name, cosmetic);
			});
		}
		
		menu.addButton(TextFormat.colorize('&', "&8&l" + Unicode.DOUBLE_LEFT + " Terug"), whoClicked -> {
			LobbyMenu.COSMETICS_INFO.open(player, uuid, name);
		});
		
		menu.open(player);
	}
}
