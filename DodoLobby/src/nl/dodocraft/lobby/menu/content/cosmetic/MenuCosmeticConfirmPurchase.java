package nl.dodocraft.lobby.menu.content.cosmetic;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.lobby.player.cosmetics.Cosmetic;
import nl.dodocraft.lobby.player.cosmetics.CosmeticManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuCosmeticConfirmPurchase extends MenuFormat {
	@Override
	public void open(Player player, DataValue... dataValues) {
		Cosmetic cosmetic = dataValues[0].as(Cosmetic.class);
		
		DodoMenu menu = new DodoMenu(LobbyMessage.MENU_COSMETIC_SELECTOR_CONFIRM_TITLE.build().get());
		
		menu.addButton(TextFormat.colorize('&', "&2&lJa, koop!"), whoClicked -> {
			DodoCraft.services().get(CosmeticManager.class).purchase(whoClicked, cosmetic);
		});
		
		menu.addButton(TextFormat.colorize('&', "&4&lNee, annuleer :("), whoClicked -> {
			LobbyMenu.COSMETIC_SELECTOR_TYPE.open(whoClicked, cosmetic.getType());
		});
		
		menu.open(player);
	}
}
