package nl.dodocraft.lobby.menu.content;

import cn.nukkit.Player;
import cn.nukkit.event.player.PlayerTeleportEvent.TeleportCause;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.common.data.server.DodocraftServer;
import nl.dodocraft.common.server.Server;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.server.LobbyManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.server.ServerManager;
import nl.dodocraft.utils.server.redirect.RedirectManager;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuLobbySelector extends MenuFormat {

	@Override
	public void open(Player player, DataValue... args) {
		DodoMenu menu = new DodoMenu(LobbyMessage.MENU_LOBBY_SELECTOR_TITLE.build().get());
		
		int lobbyNumber = 1;
		for (DodocraftServer server : DodoCraft.services().get(ServerManager.class).getServers()) {
			if (server.getServer() != Server.LOBBY) continue;
			
			// Add button depending on playercount
			if (server.getCurrentPlayers() >= server.getMaxPlayers()) {
				menu.addButton(LobbyMessage.MENU_LOBBY_SELECTOR_LOBBY_FULL.build().replace(
					new MessageReplaceValue("%LOBBY_NUMBER%", lobbyNumber + "")).get(),
					whoClicked -> {
						LobbyMessage.PLAYER_NO_PERMS_JOIN_FULL_LOBBY.build().send(whoClicked);
					}
				); 
			} else {
				boolean isThisServer = DodoCore.getInstance().getIdentifier().equals(server.getUniqueId());
				
				String title = LobbyMessage.MENU_LOBBY_SELECTOR_LOBBY.build().replace(
						new MessageReplaceValue("%LOBBY_NUMBER%", lobbyNumber + ""),
						new MessageReplaceValue("%PLAYERS%", server.getCurrentPlayers() + ""),
						new MessageReplaceValue("%MAX%", server.getMaxPlayers() + "")).get();
				
				if (isThisServer) {
					title = LobbyMessage.MENU_LOBBY_SELECTOR_LOBBY_OWN.build().replace(
							new MessageReplaceValue("%LOBBY_NUMBER%", lobbyNumber + "")
					).get();
				}
				
				menu.addButton(title, whoClicked -> {
						DodoCraft.services().get(RedirectManager.class).redirect(whoClicked.getUniqueId(), server.getUniqueId());
					}
				);
			}
			
			lobbyNumber++;
		}
		
		menu.addButton(LobbyMessage.MENU_LOBBY_TELEPORT_SPAWN.build().get(), whoClicked -> {
			whoClicked.teleport(DodoCraft.services().get(LobbyManager.class).getLobbySpawn(), TeleportCause.PLUGIN);
		});
		
		menu.open(player);
	}

}