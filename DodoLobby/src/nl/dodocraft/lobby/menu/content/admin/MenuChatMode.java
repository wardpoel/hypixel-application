package nl.dodocraft.lobby.menu.content.admin;

import cn.nukkit.Player;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.lobby.chat.ChatManager;
import nl.dodocraft.lobby.chat.ChatMode;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuChatMode extends MenuFormat {

	@Override
	public void open(Player player, DataValue... args) {
		ChatManager manager = DodoCraft.services().get(ChatManager.class);
		String current = manager.getChatMode().getChatModeName();
		
		DodoMenu menu = new DodoMenu(LobbyMessage.MENU_CHAT_MODE_TITLE.build().get(), LobbyMessage.MENU_CHAT_MODE_CONTENT.build().replace(new MessageReplaceValue("%CURRENT%", current)).get());
		menu.addButton(LobbyMessage.MENU_CHAT_MODE_BUTTON_NORMAL.build().get(), pl -> {
			manager.setChatMode(ChatMode.NORMAL);
		});
		
		menu.addButton(LobbyMessage.MENU_CHAT_MODE_BUTTON_DELAY_3.build().get(), pl -> {
			manager.setChatMode(ChatMode.DELAY_3);
		});
		
		menu.addButton(LobbyMessage.MENU_CHAT_MODE_BUTTON_DELAY_5.build().get(), pl -> {
			manager.setChatMode(ChatMode.DELAY_5);
		});
		
		menu.addButton(LobbyMessage.MENU_CHAT_MODE_BUTTON_RANKED.build().get(), pl -> {
			manager.setChatMode(ChatMode.RANKED);
		});
		
		menu.addButton(LobbyMessage.MENU_CHAT_MODE_BUTTON_STAFF.build().get(), pl -> {
			manager.setChatMode(ChatMode.STAFF);
		});
		
		menu.open(player);
	}

}
