package nl.dodocraft.lobby.menu.content.admin;

import cn.nukkit.Player;
import cn.nukkit.form.element.ElementToggle;
import cn.nukkit.utils.TextFormat;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.lobby.server.NewsManager;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.visual.menu.DodoMenuAdvanced;
import nl.dodocraft.utils.visual.menu.MenuFormat;
import nl.dodocraft.utils.visual.menu.MenuManager;

public class MenuNews extends MenuFormat {
	@Override
	public void open(Player player, DataValue... dataValues) {
		
		DodoMenuAdvanced menu = new DodoMenuAdvanced(TextFormat.colorize('&', "&8&lNews"));
		boolean enabled = DodoCore.getInstance().getGlobalData(DodoGamemode.LOBBY, NewsManager.DATA_KEY_ENABLED, Boolean.class) == null ? false : DodoCore.getInstance().getGlobalData(DodoGamemode.LOBBY, NewsManager.DATA_KEY_ENABLED, Boolean.class);
		menu.addElement(new ElementToggle("Nieuws update enabled", enabled));
		
		menu.onClick(whoClicked -> {
			boolean response = menu.getResponse().getToggleResponse(0);
			NewsManager manager = DodoCraft.services().get(NewsManager.class);
			manager.setEnabled(response);
		});
		
		DodoCraft.services().get(MenuManager.class).registerMenu(menu);
		menu.open(player);
	}
}
