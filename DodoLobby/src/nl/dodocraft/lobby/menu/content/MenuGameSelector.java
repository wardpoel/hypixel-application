package nl.dodocraft.lobby.menu.content;

import cn.nukkit.Player;
import nl.dodocraft.common.data.DataValue;
import nl.dodocraft.common.data.server.DodocraftServer;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.common.server.Server;
import nl.dodocraft.lobby.chat.LobbyMessage;
import nl.dodocraft.lobby.menu.LobbyMenu;
import nl.dodocraft.utils.DodoCraft;
import nl.dodocraft.utils.chat.MessageReplaceValue;
import nl.dodocraft.utils.server.ServerManager;
import nl.dodocraft.utils.server.redirect.RedirectManager;
import nl.dodocraft.utils.server.redirect.ServerRedirectMode;
import nl.dodocraft.utils.visual.menu.DodoMenu;
import nl.dodocraft.utils.visual.menu.MenuFormat;

public class MenuGameSelector extends MenuFormat {

	private final static DodoGamemode[] ENABLED_GAMES = new DodoGamemode[]{DodoGamemode.SURVIVAL, DodoGamemode.LUCKYLEGENDS, DodoGamemode.BEDWARS};
	
	@Override
	public void open(Player player, DataValue... args) {
		DodoMenu menu = new DodoMenu(LobbyMessage.MENU_GAME_SELECTOR_TITLE.build().get());

		for (DodoGamemode gamemode : ENABLED_GAMES) {
			int players = 0;
			for (DodocraftServer server : DodoCraft.services().get(ServerManager.class).getServers()) {
				if (server.getDodoGamemode() == gamemode) {
					players += server.getCurrentPlayers();
				}
			}

			menu.addButton(
				LobbyMessage.MENU_GAME_SELECTOR_GAMENAME.build().replace(
					new MessageReplaceValue("%COLOR%", gamemode.getColorCode()), 
					new MessageReplaceValue("%NAME%", gamemode.getName()), 
					new MessageReplaceValue("%PLAYERS%", "" + players), 
					new MessageReplaceValue("%S%", (players == 1 ? "" : "s"))
				).get(), whoClicked -> {
					if (gamemode == DodoGamemode.BEDWARS) {
						// TODO remove when BedWars SOLO is ready for launch
						DodoCraft.services().get(RedirectManager.class).redirect(player.getUniqueId(), Server.BEDWARS_TEAM, ServerRedirectMode.MOST_PLAYERS);
						return;
					}
					LobbyMenu.GAME_SELECTOR_MODE.open(whoClicked, gamemode);
				});
		}

		menu.open(player);
	}

}