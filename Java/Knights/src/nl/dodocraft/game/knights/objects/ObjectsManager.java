package nl.dodocraft.game.knights.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.chat.messages.MessageController;
import nl.dodocraft.utils.game.world.util.LocationSerializer;

public class ObjectsManager {
	
	private Loader plugin;
	
	private final HashMap<Team, List<Tower>> towers = new HashMap<>();
	private final HashMap<Team, List<Gate>> gates = new HashMap<>();
	private final HashMap<Team, Nexus> nexuses = new HashMap<>();
	
	public ObjectsManager (Loader plugin) {
		this.plugin = plugin;
		
		loadTowersFromConfig();
		loadNexusesFromConfig();
		loadGatesFromConfig();		
	}
	
	private void loadGatesFromConfig() {
		if (plugin.getConfig().contains("gates")) {
            for (Team t : Team.values()) {
                this.gates.put(t, new ArrayList<>());
                if (plugin.getConfig().contains("gates." + t.name())) {
                    for (String string : plugin.getConfig().getConfigurationSection("gates." + t.name()).getKeys(false)) {
                        Gate gate = new Gate(Integer.valueOf(string), t, LocationSerializer.deserialize(plugin.getConfig().getString("gates." + t.name() + "." + string + ".location")));
                        gate.setHealth(plugin.getConfig().getDouble("gates." + t.name() + "." + string + ".health"));
                        gate.setMaxHealth((int)Math.round(plugin.getConfig().getDouble("gates." + t.name() + "." + string + ".health")));
                        List<Gate> ta = this.gates.get(t);
                        ta.add(gate);
                        this.gates.put(t, ta);
                        System.out.println("Loaded Gate " + string + ": " + t.name() + " " + plugin.getConfig().getString("gates." + t.name() + "." + string + ".location"));
                        System.out.println("Amount of gates is now: " + ta.size());
                    }
                }
            }
        }
	}

	private void loadNexusesFromConfig() {
		if (plugin.getConfig().contains("nexus")) {
            for (Team t : Team.values()) {
                if (plugin.getConfig().contains("nexus." + t.name())) {
                    Nexus nexus = new Nexus(t.getId(), t, LocationSerializer.deserialize(plugin.getConfig().getString("nexus." + t.name() + ".location")));
                    nexus.setHealth(plugin.getConfig().getDouble("nexus." + t.name() + ".health"));
                    nexus.setMaxHealth(plugin.getConfig().getDouble("nexus." + t.name() + ".health"));
                    if (plugin.getConfig().contains("nexus.playersToAttack")) {
                			nexus.setPlayersToAttack(plugin.getConfig().getInt("nexus.playersToAttack"));
                    }
                    this.nexuses.put(t, nexus);
                    System.out.println("Loaded Nexus " + t.name() + ": " + t.name() + " " + plugin.getConfig().getString("nexus." + t.name() + ".location"));
                }
            }
        }
	}

	private void loadTowersFromConfig() {
		if (plugin.getConfig().contains("towers")) {
			for (Team team : Team.values()) {
				this.towers.put(team, new ArrayList<>());
                if (plugin.getConfig().contains("towers." + team.name())) {
                    for (String string : plugin.getConfig().getConfigurationSection("towers." + team.name()).getKeys(false)) {
                        Tower tower = new Tower(Integer.valueOf(string), team, LocationSerializer.deserialize(plugin.getConfig().getString("towers." + team.name() + "." + string + ".location")));
                        tower.setHealth(plugin.getConfig().getDouble("towers." + team.name() + "." + string + ".health"));
                        tower.setMaxHealth(plugin.getConfig().getDouble("towers." + team.name() + "." + string + ".health"));
                        
                        if (plugin.getConfig().contains("towers.playersToAttack")) {
                        		tower.setPlayersToAttack(plugin.getConfig().getInt("towers.playersToAttack"));
                        }
                        
                        if (plugin.getConfig().contains("towers." + team.name() + "." + string + ".require")) {
                            List<Integer> list = new ArrayList<>();
                            if (plugin.getConfig().getString("towers." + team.name() + "." + string + ".require").contains("%")) {
                                for (String str : plugin.getConfig().getString("towers." + team.name() + "." + string + ".require").split("%")) {
                                    list.add(Integer.parseInt(str));
                                }
                            }
                            else {
                                list.add(plugin.getConfig().getInt("towers." + team.name() + "." + string + ".require"));
                            }
                        }

                        List<Tower> ta = this.towers.get(team);
                        ta.add(tower);
                        this.towers.put(team, ta);
                        System.out.println("Loaded Tower " + string + ": " + team.name() + " " + plugin.getConfig().getString("towers." + team.name() + "." + string + ".location"));
                        System.out.println("Amount of towers is now: " + ta.size());
                    }
                }
			}
		}
	}

	public void spawnAllObjects() {
		double towerRange = 16.0; //Default range;
		if (plugin.getConfig().contains("towers.range")) {
			towerRange = plugin.getConfig().getDouble("towers.range"); 
		}
		
		double nexusRange = 11; //Default range;
		if (plugin.getConfig().contains("nexus.range")) {
			nexusRange = plugin.getConfig().getDouble("nexus.range"); 
		}
		
		for (Team team : Team.values()) {
			if (team.isActive()) {
				if (this.towers.get(team) != null) {
					for (Tower tower : this.towers.get(team)) {
						tower.spawnCrystal();
						tower.attackListener(towerRange);
					}
				}
				
				if (this.gates.get(team) != null) {
					for (Gate gate : this.gates.get(team)) {
						gate.spawnGate();
					}
				}
				
				if (this.nexuses.get(team) != null) {
					Nexus nexus = this.nexuses.get(team);
					nexus.spawnCrystal();
					nexus.attackListener(nexusRange);
				}
				
			}
		}
	}

	public HashMap<Team, List<Tower>> getTowers() {
		return towers;
	}

	public HashMap<Team, List<Gate>> getGates() {
		return gates;
	}

	public HashMap<Team, Nexus> getNexuses() {
		return nexuses;
	}
	
	public boolean allTowersAreDownFromAllTeams() {
		for (Team team : Team.values()) {
			if (team.isActive()) {
				for (Tower tower : getTowers().get(team)) {
					if (!tower.isDead()) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public boolean allTowersAreDown(Team team) {
		if (getTowers() == null || getTowers().get(team) == null || getTowers().get(team).isEmpty()) {
			return true;
		}
		for (Tower tower : this.getTowers().get(team)) {
			if (!tower.isDead()) {
				return false;
			}
		}
		return true;
	}
	
	public boolean allGatesAreDownFromAllTeams() {
		for (Team team : Team.values()) {
			if (team.isActive()) {
				for (Gate gate : getGates().get(team)) {
					if (!gate.isDead()) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public boolean oneOfTheGatesIsDead(Team team) {
		for (Gate gate : getGates().get(team)) {
			if (gate.isDead()) {
				return true;
			}
		}
		return false;
	}
	
	public void autoDamage() {
		autoDamageTowers();
		autoDamageGates();
		autoDamageNexusses();
	}
	
	public void autoDamageTowers() {
		new BukkitRunnable() {
			@Override
			public void run() {
				if (plugin.getGameType() == GameType.MINI) {
					this.cancel();
					return;
				}
				for (Team t : Team.values()) {
					if (!t.isActive()) {
						continue;
					}
					if (plugin.getGameType() == GameType.MEGA && (t == Team.GREEN || t == Team.YELLOW)) {
						continue;
					}
					
					if (plugin.getGameType() == GameType.MEGA && allTowersAreDownFromAllTeams()) {
						this.cancel();
					}
					
					double damage = 2.0;
					
					if (towers.containsKey(t)) {
						if (towers.get(t) != null) {
							for (Tower tower : towers.get(t)) {
								System.out.println("");
								if (!tower.isDead() && tower.getHealth() > 50) {
									if (tower.getHealth() - damage < 50) {
										damage = tower.getHealth() - 50;
									}
									tower.damage(damage);
									tower.updateHoloGram();
									plugin.getScoreBoard().updateTeamObject(t);
									break;
								}
							}
						}
					}
				}
			}
		}.runTaskTimer(plugin, 100L, 100L);
	}
	
	public void autoDamageGates() {
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Team t : Team.values()) {
					if (!t.isActive()) {
						continue;
					}
					if (plugin.getGameType() == GameType.MEGA && (t == Team.GREEN || t == Team.YELLOW)) {
						continue;
					}
					
					if (allGatesAreDownFromAllTeams()) {
						this.cancel();
					}
					
					if (!allTowersAreDown(t)) {
						continue;
					}
					
					double damage = 3.0;
					
					if (gates.containsKey(t)) {
						for (Gate gate : gates.get(t)) {
							if (gate.isAlive() && gate.getHealth() > 50) {
								if (gate.getHealth() - damage < 50) {
									damage = gate.getHealth() - 50;
								}
								gate.damage(damage);
								gate.updateHoloGram();
								plugin.getScoreBoard().updateTeamObject(t);
								break;
							}
						}
					}
				}
			}
		}.runTaskTimer(plugin, 80L, 80L);
	}
	
	public void autoDamageNexusses() {
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Team t : Team.values()) {
					if (!t.isActive()) {
						continue;
					}
					
					if (plugin.getGameType() == GameType.MEGA && (t == Team.GREEN || t == Team.YELLOW)) {
						continue;
					}
					
					if (plugin.getEnd()) {
						this.cancel();
					}
					
					if (!oneOfTheGatesIsDead(t)) {
						continue;
					}
					
					double damage = 2.0;
					
					if (nexuses.containsKey(t)) {
						Nexus nexus = nexuses.get(t);
						if (!nexus.isDead() && nexus.getHealth() > 100) {
							if (nexus.getHealth() - damage < 100) {
								damage = nexus.getHealth() - 100;
							}
							nexus.damage(damage);
							nexus.updateHoloGram();
							plugin.getScoreBoard().updateTeamObject(t);
						}
					}
				}
			}
		}.runTaskTimer(plugin, 100L, 100L);
	}
	
	public void setActionBar() {
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers()) {
					if (player.isValid() && plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null && player.getGameMode() == GameMode.SURVIVAL) {
						if (plugin.getGameType() == GameType.MINI) { //MINI
							String message = createActionBarMini();
							MessageController.getInstance().sendActionBar(player, message);
						} else { //MEGA
							String message = createActionBarMega();
							MessageController.getInstance().sendActionBar(player, message);
						}
					}
				}
			}
		}.runTaskTimer(plugin, 0, 10);
	}
	
	private String createActionBarMini() {
		int teamsActive = 0;
		for (Team team : Team.values()) {
			if (team.isActive()) {
				teamsActive++;
			}
		}
		
		int c = 0;
		
		String message = "";
		for (Team team : Team.values()) {
			if (team.isActive()) {
				c++;
				if (getGates().get(team).get(0).isAlive()) {
					Gate gate = getGates().get(team).get(0);
					message += team.getColor().toString() + ChatColor.BOLD + "Gate: " + ChatColor.GRAY + gate.getHealth() + " HP";
				} else if (!getNexuses().get(team).isDead()) {
					Nexus nexus = getNexuses().get(team);
					message += team.getColor().toString() + ChatColor.BOLD  + "Nexus: " + ChatColor.GRAY + nexus.getHealth() + " HP";
				} else {
					String s = " Knights";
					if (plugin.getTeamManager().getTeamPlayers(team).size() == 1) {
						s = " Knight";
					}
					message += team.getColor().toString() + ChatColor.BOLD + plugin.getTeamManager().getTeamPlayers(team).size() + s;
				}
				if (c < teamsActive) {
					message += ChatColor.DARK_GRAY + " ❚ ";
				}
			}
		}
		return message;
	}
	
	private String createActionBarMega() {
		String message = "";
		int i = 0;
		for (Team team : Team.values()) {
			if (team == Team.GREEN || team == Team.YELLOW) {
				continue;
			}
			if (team.isActive()) {
				Tower alpha = getTowers().get(team).get(0);
				Tower beta = getTowers().get(team).get(1);
				Gate gate = getGates().get(team).get(0);
				Nexus nexus = getNexuses().get(team);
				if (!alpha.isDead() && !beta.isDead()) { //Tower Alpha en Beta zijn alive;
					message += team.getColor().toString() + ChatColor.BOLD + alpha.getName() + ": " + ChatColor.GRAY + alpha.getHealth() + " HP";
					message += team.getColor().toString() + ChatColor.BOLD + beta.getName() +  ChatColor.GRAY + beta.getHealth() + " HP";
				} else if (!alpha.isDead() && beta.isDead()) { //Tower Alpha alive en Beta dead;
					message += team.getColor().toString() + ChatColor.BOLD + alpha.getName() + ": "  + ChatColor.GRAY + alpha.getHealth() + " HP";
					message += team.getColor().toString() + ChatColor.BOLD + "Gate: " + ChatColor.GRAY + gate.getHealth() + " HP";
				} else if (alpha.isDead() && !beta.isDead()) { //Tower Alpha is dead en Beta alive;
					message += team.getColor().toString() + ChatColor.BOLD + beta.getName() + ": " + ChatColor.GRAY + beta.getHealth() + " HP";
					message += team.getColor().toString() + ChatColor.BOLD + "Gate: " + ChatColor.GRAY + gate.getHealth() + " HP";
				} else if (alpha.isDead() && beta.isDead() && gate.isAlive()) {
					message += team.getColor().toString() + ChatColor.BOLD + "Gate: " + ChatColor.GRAY + gate.getHealth() + " HP";
					message += team.getColor().toString() + ChatColor.BOLD + "Nexus: " + ChatColor.GRAY + nexus.getHealth() + " HP";
				} else {
					message += team.getColor().toString() + ChatColor.BOLD + "Nexus: " + ChatColor.GRAY + nexus.getHealth() + " HP";
				}
				if (i == 0) {
					message += ChatColor.DARK_GRAY + " ❚ ";
					i++;
				}
			}
		}
		return message;
	}

}
