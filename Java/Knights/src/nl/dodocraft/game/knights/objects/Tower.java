package nl.dodocraft.game.knights.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.particles.ParticleEffect;

public class Tower {

	private static HashMap<EnderCrystal, Tower> towers = new HashMap<>();
	
	private EnderCrystal crystal;
	private final int id;
	private final Team team;
	private final Location location;
	private double health;
	private double maxHealth;
	private boolean dead;
	private int playersToAttack;
	private BossBar bar = null;
	
	private ArmorStand stand;
	
	public Tower(int id, Team team, Location location) {
		this(id, team, location, 400.0, 400.0, 3);
	}
	
	public Tower(int id, Team team, Location location, double health, double maxHealth, int playersToAttack) {
		this.id = id;
		this.team = team;
		this.location = location;
		this.health = health;
		this.maxHealth = maxHealth;
		this.dead = false;
		this.playersToAttack = playersToAttack;
	}

	public EnderCrystal getCrystal() {
		return crystal;
	}
	
	public static HashMap<EnderCrystal, Tower> getTowers() {
		return towers;
	}

	public void spawnCrystal() {
		if (!getLocation().getChunk().isLoaded()) {
			getLocation().getChunk().load();
		}

		this.crystal = Bukkit.getWorlds().get(0).spawn(getLocation().clone().add(0, 0.2, 0), EnderCrystal.class);
		if (!crystal.getLocation().getChunk().isLoaded()) {
			crystal.getLocation().getChunk().load();
		}
		crystal.setShowingBottom(false);
		towers.put(crystal, this);
		addHoloGram();
	}

	public double getHealth() {
		return round(health);
	}

	public void setHealth(double health) {
		this.health = health;
	}

	public double getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(double maxHealth) {
		this.maxHealth = maxHealth;
	}
	
	public void damage(double d) {
		this.health = this.health - d;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public int getId() {
		return id;
	}

	public Team getTeam() {
		return team;
	}

	public Location getLocation() {
		return location;
	}
	
	public void setPlayersToAttack(int i) {
		this.playersToAttack = i;
	}

	public int getPlayersToAttack() {
		return this.playersToAttack;
	}
	
	public void attackListener(double range) {
		Location loc = getLocation().clone().add(0, 0.2, 0);
		attackListener(range, loc);
	}

	public void attackListener(double range, Location loc) {
		List<UUID> players = new ArrayList<>();
		new BukkitRunnable() {
			@Override
			public void run() {
				if (getHealth() <= 0.0) {
					this.cancel();
					return;
				}
				
				for (Player player : Bukkit.getOnlinePlayers()) {
					if (player.getLocation().distance(loc) <= range && (player.getGameMode() != GameMode.SPECTATOR || player.isDead() || !player.isOnline() || !player.isValid() || player.getGameMode() == GameMode.CREATIVE)) {
						if (Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId()) != team && getPlayersToAttack() >= players.size()) {
							if (!players.contains(player.getUniqueId())) {
								players.add(player.getUniqueId());
								
								new BukkitRunnable() {
									double damage = 3.4;
									
									int lastParticle = 0;
									
									@Override
									public void run() {
										if (getHealth() <= 0.0) { //Stop when nexus is dead
											this.cancel();
											players.clear();
											return;
										}
										if (!players.contains(player.getUniqueId())) { //Stop when player is not more in list
											this.cancel();
											return;
										}
										
										Location playerLoc = player.getLocation().add(0, 1, 0);
										if (player.isDead() || !player.isOnline() || !player.isValid() || playerLoc.distance(loc.clone()) > range || player.getGameMode() == GameMode.SPECTATOR || player.getGameMode() == GameMode.CREATIVE) { //Stop when player is to far, or gamemode or is dead,...
											this.cancel();
											players.remove(player.getUniqueId());
											return;
										}
										
										Vector vec = playerLoc.toVector().subtract(loc.clone().toVector()).normalize();
										if (lastParticle-- <= 0) { //nickcode
											lastParticle = 6;
											for (double i = 0; i < playerLoc.distance(loc.clone()); i += 0.2) {
												ParticleEffect.REDSTONE.display(0, 0, 0, 0, 1, loc.clone().add(vec.clone().multiply(i)));
											}
											for (double i = 0; i < playerLoc.distance(loc.clone()); i += 0.4) {
												ParticleEffect.FLAME.display(0, 0, 0, 0.005f, 1, loc.clone().add(vec.clone().multiply(i)));
											}
										}
										
										for (double i = 0; i <  playerLoc.distance(loc.clone()); i += 0.6) {
											ParticleEffect.FLAME.display((float) (damage / 10.0), (float) (damage / 10.0), (float) (damage / 10.0), 0.005f, (int) damage, loc.clone().add(vec.clone().multiply(i)));
										}
										
										player.damage(damage);
										
									}
								}.runTaskTimer(Loader.getInstance(), 0, 23); //1.15sec
								
							}
						}
					}
				}
				
			}
		}.runTaskTimer(Loader.getInstance(), 0, 20);
	}
	
	private Double round(double value) {
        long factor = (long) Math.pow(10.0, 2);
        long tmp = Math.round(value * (double) factor);
        return (double) tmp / (double) factor;
    }
	
	public void addHoloGram() {
		if (this.health > 0.0) {
			this.stand = this.crystal.getWorld().spawn(this.getLocation().clone().add(0, 0.23, 0), ArmorStand.class);
			this.stand.setVisible(false);
			this.stand.setGravity(false);
			this.stand.setCustomName(team.getColor().toString() + ChatColor.BOLD + getName() + ChatColor.GRAY + " ─ " + ChatColor.GRAY.toString() + ChatColor.BOLD + "HP: " + ChatColor.GREEN.toString() + ChatColor.BOLD + round(this.health));
			this.stand.setCustomNameVisible(true);
		}
	}
	
	public void removeHoloGram() {
		this.stand.remove();
	}
	
	public void updateHoloGram() {
		if (this.health > 0.0) {
			this.stand.setCustomName(team.getColor().toString() + ChatColor.BOLD + getName() + ChatColor.GRAY + " ─ " + ChatColor.GRAY.toString() + ChatColor.BOLD + "HP: " + ChatColor.GREEN.toString() + ChatColor.BOLD + round(this.health));
		} else {
			towerDown();
			removeHoloGram();
		}
	}

	private void towerDown() {
		this.crystal.remove();
		this.setDead(true);
		boolean doo = true;
        for (Entity e : getLocation().getWorld().getNearbyEntities(getLocation(), 3, 3, 3)) {
            if (e instanceof EnderCrystal) {
                if (doo) {
                		ParticleEffect.EXPLOSION_HUGE.display(0, 0, 0, 0, 1, getLocation());
                		e.getWorld().playSound(e.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 3F, 0F);
                		doo = false;
                }
                e.remove();
            }
        }
	}
	
	public String getName() {
		if (getId() == 1) {
			return "Alpha";
		} else {
			return "Beta";
		}
	}
	
	public void addWarnBossBar() {
		if (bar != null) {
			if (!this.bar.getPlayers().isEmpty()) {
				return;
			}
		}
		String warn1 = team.getColor().toString() + ChatColor.BOLD + "JE" + ChatColor.WHITE + ChatColor.BOLD + " " + getName().toUpperCase() + " " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + ChatColor.WHITE + ChatColor.BOLD + getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
		this.bar = Bukkit.createBossBar(warn1, team.getBarColor(), BarStyle.SOLID);
		this.bar.setProgress(getHealth() / getMaxHealth());
		for (UUID uuid : Loader.getInstance().getTeamManager().getTeamPlayers(team)) {
			Player p = Bukkit.getPlayer(uuid);
			if (p.getGameMode() == GameMode.SURVIVAL) {
				this.bar.addPlayer(p);
			}
		}
		
		new BukkitRunnable() {
			int ticks = 0;
			@Override
			public void run() {
				if (isDead() || ticks >= 8) {
					this.cancel();
					bar.removeAll();
				} else {
					if (ticks % 2 == 0) {
						String warn1 = team.getColor().toString() + ChatColor.BOLD + "JE" + ChatColor.WHITE + ChatColor.BOLD + " " + getName().toUpperCase() + " " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + ChatColor.WHITE + ChatColor.BOLD + getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
						bar.setTitle(warn1);
						bar.setProgress(getHealth() / getMaxHealth());
					} else {
						String warnColor = ChatColor.DARK_RED.toString() + ChatColor.BOLD;
						if (team == Team.BLUE) {
							warnColor = ChatColor.BLUE.toString() + ChatColor.BOLD;
						} else if (team == Team.YELLOW) {
							warnColor = ChatColor.GOLD.toString() + ChatColor.BOLD;
						} else if (team == Team.GREEN) {
							warnColor = ChatColor.DARK_GREEN.toString() + ChatColor.BOLD;
						}
						String warn2 = team.getColor().toString() + ChatColor.BOLD + "JE" + warnColor + " " + getName().toUpperCase() + " " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + warnColor + getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
						bar.setTitle(warn2);
						bar.setProgress(getHealth() / getMaxHealth());
					}
					ticks++;
				}
			}
		}.runTaskTimer(Loader.getInstance(), 0, 10L);
		
	}
	
}
