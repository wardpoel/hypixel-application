package nl.dodocraft.game.knights.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.fight.PlayerDamage;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.particles.ParticleEffect;
import nl.dodocraft.utils.scoreboard.DodoBoard;
import nl.dodocraft.utils.scoreboard.controller.ScoreboardController;

public class Nexus {

	private static HashMap<EnderCrystal, Nexus> nexuses = new HashMap<>();
	
	private EnderCrystal crystal;
	private final int id;
	private double health;
	private double maxHealth;
	private boolean dead;
	private final Location location;
	private final Team team;
	private int playersToAttack;
	private ArmorStand stand;
	private BossBar bar = null;
	
	public Nexus(int id, Team team, Location location) {
		this(id, team, location, 850.0, 850.0, 3);
	}

	public Nexus(int id, Team team, Location location, double health, double maxHealth, int playerToAttack) {
		this.id = id;
		this.team = team;
		this.location = location;
		this.health = health;
		this.maxHealth = maxHealth;
		this.dead = false;
		this.playersToAttack = playerToAttack;
	}

	public double getHealth() {
		return round(health);
	}

	public void setHealth(double health) {
		this.health = health;
	}

	public double getMaxHealth() {
		return maxHealth;
	}

	public static HashMap<EnderCrystal, Nexus> getNexuses() {
		return nexuses;
	}
	
	public void setMaxHealth(double maxHealth) {
		this.maxHealth = maxHealth;
	}
	
	public void addHealth(double health) {
		if (getHealth() + health <= getMaxHealth()) {
			this.health += health;
		}
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public int getId() {
		return id;
	}

	public Location getLocation() {
		return location;
	}

	public Team getTeam() {
		return team;
	}

	public void spawnCrystal() {
		if (!getLocation().getChunk().isLoaded()) {
			getLocation().getChunk().load();
		}
		
		this.crystal = Bukkit.getWorlds().get(0).spawn(getLocation().clone().add(0, 0.2, 0), EnderCrystal.class);
		if (!crystal.getLocation().getChunk().isLoaded()) {
            crystal.getLocation().getChunk().load();
        }
        crystal.setShowingBottom(false);
        crystal.setCustomName(team.getColor().toString() + ChatColor.BOLD + team.getBvName() + " Nexus");
        nexuses.put(crystal, this);
        addHoloGram();
	}

	public void setPlayersToAttack(int i) {
		this.playersToAttack = i;
	}
	
	public int getPlayersToAttack() {
		return this.playersToAttack;
	}
	
	public void attackListener(double nexusRange) {
		Location loc = getLocation().clone().add(0, 0.2, 0);
		attackListener(nexusRange, loc);
	}
	
	public void attackListener(double range, Location loc) {
		List<UUID> players = new ArrayList<>();
		new BukkitRunnable() {
			@Override
			public void run() {
				if (getHealth() <= 0.0) {
					this.cancel();
					return;
				}
				
				for (Player player : Bukkit.getOnlinePlayers()) {
					if (player.getLocation().distance(loc) <= range && (player.getGameMode() != GameMode.SPECTATOR || player.isDead() || !player.isOnline() || !player.isValid() || player.getGameMode() == GameMode.CREATIVE)) {
						if (Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId()) != team && getPlayersToAttack() >= players.size()) {
							if (!players.contains(player.getUniqueId())) {
								players.add(player.getUniqueId());
								
								new BukkitRunnable() {
									double damage = 3.4;
									
									int lastParticle = 0;
									
									@Override
									public void run() {
										if (getHealth() <= 0.0) { //Stop when nexus is dead
											this.cancel();
											players.clear();
											return;
										}
										if (!players.contains(player.getUniqueId())) { //Stop when player is not more in list
											this.cancel();
											return;
										}
										
										Location playerLoc = player.getLocation().add(0, 1, 0);
										if (player.isDead() || !player.isOnline() || !player.isValid() || playerLoc.distance(loc.clone()) > range || player.getGameMode() == GameMode.SPECTATOR || player.getGameMode() == GameMode.CREATIVE) { //Stop when player is to far, or gamemode or is dead,...
											this.cancel();
											players.remove(player.getUniqueId());
											return;
										}
										
										Vector vec = playerLoc.toVector().subtract(loc.clone().toVector()).normalize();
										if (lastParticle-- <= 0) { //nickcode
											lastParticle = 6;
											for (double i = 0; i < playerLoc.distance(loc.clone()); i += 0.2) {
												ParticleEffect.REDSTONE.display(0, 0, 0, 0, 1, loc.clone().add(vec.clone().multiply(i)));
											}
											for (double i = 0; i < playerLoc.distance(loc.clone()); i += 0.4) {
												ParticleEffect.FLAME.display(0, 0, 0, 0.005f, 1, loc.clone().add(vec.clone().multiply(i)));
											}
										}
										
										for (double i = 0; i <  playerLoc.distance(loc.clone()); i += 0.6) {
											ParticleEffect.FLAME.display((float) (damage / 10.0), (float) (damage / 10.0), (float) (damage / 10.0), 0.005f, (int) damage, loc.clone().add(vec.clone().multiply(i)));
										}
										
										player.damage(damage);
										if (player.getHealth() - damage <= 0) { //Nexus killed that player
											for (UUID uuid : Loader.getInstance().getTeamManager().getTeamPlayers(getTeam())) {
												if (Loader.getInstance().getPlayerDataManager().getPlayerData(Bukkit.getPlayer(uuid)) != null) {
													Loader.getInstance().getPlayerDataManager().addMoney(Bukkit.getPlayer(uuid), 1);
												}
											}
										}
										
										PlayerDamage d;
										if (Loader.getInstance().getFightListener().playerDamage.containsKey(player.getName())) {
											d = Loader.getInstance().getFightListener().playerDamage.get(player.getName());
										} else {
											d = new PlayerDamage(player);
											Loader.getInstance().getFightListener().playerDamage.put(player.getName(), d);
										}
										
										if (d != null) {
											d.addPlayerDamage(crystal, damage);
											d.setLastDamageTaken(System.currentTimeMillis());
										}
										
									}
								}.runTaskTimer(Loader.getInstance(), 0, 23); //1.15sec
								
							}
						}
					}
				}
			}
		}.runTaskTimer(Loader.getInstance(), 0, 20);
	}

	private Double round(double value) {
        long factor = (long) Math.pow(10.0, 2);
        long tmp = Math.round(value * (double) factor);
        return (double) tmp / (double) factor;
    }
	
	public void damage(double damage) {
		this.health = this.health - damage;
		if (this.health < 0) {
			nexusDown();
		}
	}
			
	private void nexusDown() {
		this.crystal.remove();
		this.setDead(true);

		boolean doo = true;
        for (Entity e : getLocation().getWorld().getNearbyEntities(getLocation(), 3, 3, 3)) {
            if (e instanceof EnderCrystal) {
                if (doo) {
                		ParticleEffect.EXPLOSION_HUGE.display(0, 0, 0, 0, 1, getLocation());
                		e.getWorld().playSound(e.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 3F, 0F);
                		doo = false;
                }
                e.remove();
            }
        }
		
        int size = Loader.getInstance().getTeamManager().getTeamPlayers(getTeam()).size();
		String scoreboardLine = this.getTeam().getColor().toString() + (size == 0 ? "✗" : ChatColor.BOLD.toString() + size) + " " + ChatColor.WHITE + getTeam().getName();
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (Loader.getInstance().getPlayerDataManager().getPlayerData(player) != null) {
				Loader.getInstance().getPlayerDataManager().addCoins(player, 15);
				DodoBoard sb = ScoreboardController.getInstance().getScoreboard(player);
				sb.setLine(this.getTeam().getScoreBoardLine(), scoreboardLine);
			}
		}
		
	}
	
	public void removeNexus() {
		this.crystal.remove();
		this.setDead(true);

		boolean doo = true;
        for (Entity e : getLocation().getWorld().getNearbyEntities(getLocation(), 3, 3, 3)) {
            if (e instanceof EnderCrystal) {
                if (doo) {
                		ParticleEffect.EXPLOSION_HUGE.display(0, 0, 0, 0, 1, getLocation());
                		e.getWorld().playSound(e.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 3F, 0F);
                		doo = false;
                }
                e.remove();
            }
        }
	}
	
	public void addHoloGram() {
		if (this.health > 0.0) {
			this.stand = this.crystal.getWorld().spawn(this.getLocation().clone().add(0, 0.23, 0), ArmorStand.class);
			this.stand.setVisible(false);
			this.stand.setGravity(false);
			this.stand.setCustomName(team.getColor().toString() + ChatColor.BOLD + "Nexus" + ChatColor.GRAY + " ─ " + ChatColor.GRAY.toString() + ChatColor.BOLD + "HP: " + ChatColor.GREEN.toString() + ChatColor.BOLD + round(this.health));
			this.stand.setCustomNameVisible(true);
		}
	}
	
	public void removeHoloGram() {
		this.stand.remove();
	}
	
	public void updateHoloGram() {
		if (this.health > 0.0) {
			this.stand.setCustomName(team.getColor().toString() + ChatColor.BOLD + "Nexus" + ChatColor.GRAY + " ─ " + ChatColor.GRAY.toString() + ChatColor.BOLD + "HP: " + ChatColor.GREEN.toString() + ChatColor.BOLD + round(this.health));
		} else {
			nexusDown();
			removeHoloGram();
		}
	}
	
	public void addWarnBossBar() {
		if (bar != null) {
			if (!this.bar.getPlayers().isEmpty()) {
				return;
			}
		}
		String warn1 = team.getColor().toString() + ChatColor.BOLD + "JE" + ChatColor.WHITE + ChatColor.BOLD + " NEXUS " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + ChatColor.WHITE + ChatColor.BOLD + getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
		this.bar = Bukkit.createBossBar(warn1, team.getBarColor(), BarStyle.SOLID);
		if (getHealth() < 0) {
			this.health = 0;
		}
		this.bar.setProgress(getHealth() / getMaxHealth());
		for (UUID uuid : Loader.getInstance().getTeamManager().getTeamPlayers(team)) {
			Player p = Bukkit.getPlayer(uuid);
			if (p.getGameMode() == GameMode.SURVIVAL) {
				this.bar.addPlayer(p);
			}
		}
		
		new BukkitRunnable() {
			int ticks = 0;
			@Override
			public void run() {
				if (isDead() || ticks >= 8) {
					this.cancel();
					bar.removeAll();
				} else {
					if (ticks % 2 == 0) {
						String warn1 = team.getColor().toString() + ChatColor.BOLD + "JE" + ChatColor.WHITE + ChatColor.BOLD + " NEXUS " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + ChatColor.WHITE + ChatColor.BOLD +  getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
						bar.setTitle(warn1);
						bar.setProgress(getHealth() / getMaxHealth());
					} else {
						String warnColor = ChatColor.DARK_RED.toString() + ChatColor.BOLD;
						if (team == Team.BLUE) {
							warnColor = ChatColor.BLUE.toString() + ChatColor.BOLD;
						} else if (team == Team.YELLOW) {
							warnColor = ChatColor.GOLD.toString() + ChatColor.BOLD;
						} else if (team == Team.GREEN) {
							warnColor = ChatColor.DARK_GREEN.toString() + ChatColor.BOLD;
						}
						String warn2 = team.getColor().toString() + ChatColor.BOLD + "JE" + warnColor + " NEXUS " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + warnColor + getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
						bar.setTitle(warn2);
						bar.setProgress(getHealth() / getMaxHealth());
					}
					ticks++;
				}
			}
		}.runTaskTimer(Loader.getInstance(), 0, 10L);
		
	}
	
}










































