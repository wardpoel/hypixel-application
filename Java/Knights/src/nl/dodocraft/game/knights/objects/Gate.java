package nl.dodocraft.game.knights.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.game.world.util.LocationSerializer;
import nl.dodocraft.utils.particles.ParticleEffect;

public class Gate {
	
	private static List<Gate> gates = new ArrayList<>();
	
	private final int id;
	private double health;
	private double maxHealth;
	private boolean dead;
	private final Location location;
	private final Team team;
	private ArmorStand stand;
	private BossBar bar = null;
	
	public Gate(int id, Team team, Location location) {
		this(id, team, location, 300.0, 300.0);	
	}
	
	public Gate(int id, Team team, Location location, double health, double maxHealth) {
		this.id = id;
		this.team = team;
		this.location = location;
		this.dead = false;
		this.health = health;
		this.maxHealth = maxHealth;
		this.dead = false;
	}
	
	public void spawnGate() {
		if (!getLocation().getChunk().isLoaded()) {
			getLocation().getChunk().load();
		}
		gates.add(this);
		addHoloGram();
	}
	
	public static List<Gate> getGates() {
		return gates;
	}
	
	public Location getLocation() {
		return this.location;
	}
	
	public void setHealth(double h) {
		this.health = h;
	}
	
	public void setMaxHealth(double h) {
		this.maxHealth = h;
	}
	
	public int getId() {
		return this.id;
	}
	
	public Team getTeam() {
		return this.team;
	}
	
	public double getHealth() {
		return this.health;
	}
	
	public double getMaxHealth() {
		return this.maxHealth;
	}
	
	public boolean isDead() {
		return this.dead;
	}
	
	public boolean isAlive() {
		return !this.dead;
	}
	
	public void damage(double damage) {
		this.health = this.health - damage;
		if (this.health <= 0.0) {
			if (this.dead == false) {
				openGate();
				this.dead = true;
				for (Player p : Bukkit.getOnlinePlayers()) {
					if (p.getGameMode() == GameMode.SURVIVAL) {
						Loader.getInstance().getItemManager().giveDynamiet(p);
					}
				}
			}
		}
	}

	private void openGate() {
		Location location = getLocation();
        ParticleEffect.EXPLOSION_HUGE.display(0, 0, 0, 0, 1, location);
        location.getWorld().createExplosion(location.getX(), location.getY(), location.getZ(), 0.0f, false, false);
        location.getWorld().playSound(location, Sound.BLOCK_ANVIL_DESTROY, 1.5f, 0.0f);

        new BukkitRunnable() {
            int radius = 1;

            @Override
            public void run() {
                for (Location loca : getArenaBlocks(location, radius++, 0)) {
                    if (loca.getBlock().getType().equals(Material.IRON_FENCE)) {
                        if (radius < 3 || new Random().nextInt(100) + 1 < 85) {
                            Block block = loca.getBlock();
                            block.setType(Material.AIR);

                            if (new Random().nextInt(100) + 1 < 15) {
                                ParticleEffect.EXPLOSION_HUGE.display(0, 0, 0, 0, 1, location);
                                location.getWorld().createExplosion(block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ(), 0.0f, false, false);

                            }
                        }
                    }
                }

                if (radius >= 5)
                    this.cancel();
            }
        }.runTaskTimer(Loader.getInstance(), 0, 8);
	}
	
	public void destroyGateByLeave() {
		Location location = getLocation();

        new BukkitRunnable() {
            int radius = 1;

            @Override
            public void run() {
                for (Location loca : getArenaBlocks(location, radius++, 0)) {
                    if (loca.getBlock().getType().equals(Material.IRON_FENCE)) {
                        if (radius < 3 || new Random().nextInt(100) + 1 < 85) {
                            Block block = loca.getBlock();
                            block.setType(Material.AIR);
                        }
                    }
                }

                if (radius >= 5)
                    this.cancel();
            }
        }.runTaskTimer(Loader.getInstance(), 0, 8);
	}
	
	private List<Location> getArenaBlocks(Location location, int radius, int minimalY) {
        World w = location.getWorld();
        final int xCoord = (int) location.getX();
        final int zCoord = (int) location.getZ();
        final int yCoord = (int) location.getY();

        List<Location> tempList = new ArrayList<Location>();
        for (int y = -radius; y <= radius; y++) {
            if (yCoord + y >= minimalY && yCoord + y < 256) {
                for (int x = -radius; x <= radius; x++) {
                    for (int z = -radius; z <= radius; z++) {
                        tempList.add(new Location(w, xCoord + x, yCoord + y, zCoord + z));
                    }
                }
            }
        }
        return tempList;
    }

	private Double round(double value) {
        long factor = (long) Math.pow(10.0, 2);
        long tmp = Math.round(value * (double) factor);
        return (double) tmp / (double) factor;
    }
	
	public void addHoloGram() {
		if (this.health > 0.0) {
			double x = 0;
			double z = 0;
			if (Loader.getInstance().getConfig().contains("center")) {
				Location center = LocationSerializer.deserialize(Loader.getInstance().getConfig().getString("center"));
				if (center.getX() == getLocation().getX()) {
					z = (center.getZ() > getLocation().getZ() ? 1 : -1);
				} else if (center.getZ() == getLocation().getZ()) {
					x = (center.getX() > getLocation().getX() ? 1 : -1);
				}
			}
			
			this.stand = this.getLocation().getWorld().spawn(this.getLocation().clone().add(x, 0.23, z), ArmorStand.class);
			this.stand.setVisible(false);
			this.stand.setGravity(false);
			this.stand.setCustomName(team.getColor().toString() + ChatColor.BOLD + "Gate" + ChatColor.GRAY + " ─ " + ChatColor.GRAY.toString() + ChatColor.BOLD + "HP: " + ChatColor.GREEN.toString() + ChatColor.BOLD + round(this.health));
			this.stand.setCustomNameVisible(true);
		}
	}
		
	public void removeHoloGram() {
		this.stand.remove();
	}
	
	public void updateHoloGram() {
		if (this.health > 0.0) {
			this.stand.setCustomName(team.getColor().toString() + ChatColor.BOLD + "Gate" + ChatColor.GRAY + " ─ " + ChatColor.GRAY.toString() + ChatColor.BOLD + "HP: " + ChatColor.GREEN.toString() + ChatColor.BOLD + round(this.health));
		} else {
			removeHoloGram();
		}
	}
	
	public void addWarnBossBar() {
		if (bar != null) {
			if (!this.bar.getPlayers().isEmpty()) {
				return;
			}
		}
		String warn1 = team.getColor().toString() + ChatColor.BOLD + "JE" + ChatColor.WHITE + ChatColor.BOLD + " GATE " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + ChatColor.WHITE + ChatColor.BOLD + getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
		this.bar = Bukkit.createBossBar(warn1, team.getBarColor(), BarStyle.SOLID);
		this.bar.setProgress(getHealth() / getMaxHealth());
		for (UUID uuid : Loader.getInstance().getTeamManager().getTeamPlayers(team)) {
			Player p = Bukkit.getPlayer(uuid);
			if (p.getGameMode() == GameMode.SURVIVAL) {
				this.bar.addPlayer(p);
			}
		}
		
		new BukkitRunnable() {
			int ticks = 0;
			@Override
			public void run() {
				if (isDead() || ticks >= 8) {
					this.cancel();
					bar.removeAll();
				} else {
					if (ticks % 2 == 0) {
						String warn1 = team.getColor().toString() + ChatColor.BOLD + "JE" + ChatColor.WHITE + ChatColor.BOLD + " GATE " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + ChatColor.WHITE + ChatColor.BOLD + getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
						bar.setTitle(warn1);
						bar.setProgress(getHealth() / getMaxHealth());
					} else {
						String warnColor = ChatColor.DARK_RED.toString() + ChatColor.BOLD;
						if (team == Team.BLUE) {
							warnColor = ChatColor.BLUE.toString() + ChatColor.BOLD;
						} else if (team == Team.YELLOW) {
							warnColor = ChatColor.GOLD.toString() + ChatColor.BOLD;
						} else if (team == Team.GREEN) {
							warnColor = ChatColor.DARK_GREEN.toString() + ChatColor.BOLD;
						}
						String warn2 = team.getColor().toString() + ChatColor.BOLD + "JE" + warnColor + " GATE " + team.getColor().toString() + ChatColor.BOLD + "WORDT AANGEVALLEN (" + warnColor + getHealth() + team.getColor().toString() + ChatColor.BOLD + "/" + getMaxHealth() + ")";
						bar.setTitle(warn2);
						bar.setProgress(getHealth() / getMaxHealth());
					}
					ticks++;
				}
			}
		}.runTaskTimer(Loader.getInstance(), 0, 10L);
		
	}
	
}
