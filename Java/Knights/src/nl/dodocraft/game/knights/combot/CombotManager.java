package nl.dodocraft.game.knights.combot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.scheduler.BukkitRunnable;

import nl.dodocraft.game.knights.Loader;

public class CombotManager implements Listener {
	
    private List<UUID> send = new ArrayList<>();

    public CombotManager() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (tag.containsKey(player) && tag2.containsKey(player)) {
                        if (tag.get(player) - System.currentTimeMillis() <= 0) {
                            tag.remove(player);
                            tag2.remove(player);
                            send.remove(player.getUniqueId());
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 0F);
                        } else {
                            if (!send.contains(player.getUniqueId())) {
                                send.add(player.getUniqueId());
                            }
                        }
                    }
                }
            }
        }.runTaskTimer(Loader.getInstance(), 20, 20);
    }

    public boolean isTagged(Player player) {
        if (tag.containsKey(player) && tag2.containsKey(player)) {
            if (tag.get(player) - System.currentTimeMillis() > 0) {
                return true;
            }
        }
        return false;
    }


    public HashMap<Player, Long> tag = new HashMap<>();
    public HashMap<Player, LivingEntity> tag2 = new HashMap<>();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onHit(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            if (e.getEntity() instanceof Player && (e.getDamager() instanceof LivingEntity || e.getDamager() instanceof Projectile)) {
                Player player = (Player) e.getEntity();
                Entity tagger = e.getDamager();

                tag.put(player, System.currentTimeMillis() + (10 * 1000));
                if (e.getDamager() instanceof Projectile) {
                    tag2.put(player, (LivingEntity) ((Projectile) e.getDamager()).getShooter());
                } else {
                    tag2.put(player, (LivingEntity) tagger);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDeath(PlayerDeathEvent e) {
    		if (tag.containsKey(e.getEntity())) {
    			tag.remove(e.getEntity());
    		}
        if (tag2.containsKey(e.getEntity())) {
        		tag2.remove(e.getEntity());
        }
    }
    
}
