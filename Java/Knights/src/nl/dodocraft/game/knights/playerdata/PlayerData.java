package nl.dodocraft.game.knights.playerdata;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import nl.dodocraft.common.boosters.Booster;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.core.internal.InternalAPI;
import nl.dodocraft.game.knights.team.Team;
import nl.thiemoboven.ncore.SimpleCore;

public class PlayerData {

	private final UUID uuid;
	private final String name;
	private final String color;
	private int gold = 0;
	private int kills = 0;
	private int ekills = 0;
	private int deaths = 0;
	private double nexusDamage = 0;
	private double gateDamage = 0;
	private double towerDamage = 0;
	private int assists = 0;
	private int mana = 0;
	private int abilities = 0;
	private long startTime;
	private long stopTime;
	private double kitDamage = 0;
	private double healing = 0;
	private String kit;
	private Team team;
	private HashMap<UUID, Integer> killsOnAPlayer = new HashMap<>();
	
	private HashMap<String, Integer> oldData = new HashMap<>();
	
	private int coins = 0;
	private int xp = 0;
	private int tntPlaced = 0;

	public PlayerData(Player player) {
		this.uuid = player.getUniqueId();
		this.color = DodoCore.getInstance().getRank(player.getUniqueId()).getColorPrefix().toString();
		this.name = color + player.getName();
		this.startTime = System.currentTimeMillis();
	}
	
	/* Getters */
	public Player getPlayer() {
		return Bukkit.getPlayer(getUUID());
	}
	
	public UUID getUUID() {
		return this.uuid;
	}
	
	public int getGold() {
		return this.gold;
	}
	
	public int getKills() {
		return this.kills;
	}
	
	public int getEliminationKills() {
		return this.ekills;
	}
	
	public int getAssists() {
		return this.assists;
	}
	
	public int getMana() {
		return this.mana;
	}
	
	public int getCoins() {
		return this.coins;
	}
	
	public int getXp() {
		return this.xp;
	}
	
	public int getTntPlaced() {
		return this.tntPlaced;
	}
	
	public int getKillsOnPlayer(Player killedPlayer) {
		if (this.killsOnAPlayer.containsKey(killedPlayer.getUniqueId())) {
			return this.killsOnAPlayer.get(killedPlayer.getUniqueId());
		}
		return 0;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public int getAbilities() {
		return this.abilities;
	}
	
	public long getStartTime() {
		return this.startTime;
	}
	
	public long getStopTime() {
		return this.stopTime;
	}
	
	public double getKitDamage() {
		return this.kitDamage;
	}
	
	public String getKit() {
		return this.kit;
	}
	
	public int getDeaths() {
		return this.deaths;
	}
	
	public double getNexusDamage() {
		return this.nexusDamage;
	}
	
	public double getGateDamage() {
		return this.gateDamage;
	}
	
	public double getTowerDamage() {
		return this.towerDamage;
	}
	
	public double getHealing() {
		return this.healing;
	}
	
	public Team getTeam() {
		return this.team;
	}
	
	/* Setters */
	public void addGold(int value) {
		this.gold += value;
	}
	
	public void removeGold(int value) {
		this.gold -= value;
	}
	
	public void addKill() {
		this.kills += 1;
	}
	
	public void addEliminationKill() {
		this.ekills += 1;
	}
	
	public void addAssist() {
		this.assists += 1;
	}
	
	public void addMana(int value) {
		if (this.mana + value > PlayerDataManager.MAX_MANA) {
			value = PlayerDataManager.MAX_MANA - this.mana;
		}
		this.mana += value;
	}
	
	public void resetMana() {
		this.mana = 0;
	}
	
	public void addCoins(int value) { 
		this.coins += value;
	}
	
	public void addXp(int value) {
		this.xp += value;
	}
	
	public void addTntPlaced(int value) {
		this.tntPlaced += value;
	}
	
	public void addKillOnPlayer(Player killedPlayer) {
		if (this.killsOnAPlayer.containsKey(killedPlayer.getUniqueId())) {
			int kills = this.killsOnAPlayer.get(killedPlayer.getUniqueId()) + 1;
			this.killsOnAPlayer.replace(killedPlayer.getUniqueId(), kills);
		} else {
			this.killsOnAPlayer.put(killedPlayer.getUniqueId(), 1);
		}
	}
	
	public void addAbility() {
		this.abilities++;
	}
	
	public void setStopTime() {
		this.stopTime = System.currentTimeMillis();
	}
	
	public void addKitDamage(double damage) {
		this.kitDamage += damage;
	}
	
	public void addHealing(double heal) {
		this.healing += heal;
	}
	
	public void setKit(String kit) {
		this.kit = kit;
	}
	
	public void setTeam(Team team) {
		this.team = team;
	}
	
	public void addDeath() {
		this.deaths++;
	}
	
	public void addNexusDamage(double damage) {
		this.nexusDamage += damage;
	}
	
	public void addGateDamage(double damage) {
		this.gateDamage += damage;
	}
	
	public void addTowerDamage(double damage) {
		this.towerDamage += damage;
	}
	
	String[] types = {"WINS", "GAMES_PLAYED", "KILLS", "ASSISTS", "DYNAMITE_PLACED"};
	String[] kitTypes = {"_WINS", "_HOURS", "_TIMES_PLAYED", "_KILLS", "_DEATHS", "_EKILLS", "_EDEATHS", "_ABILITIES", "_DAMAGE", "_NEXUS_DAMAGE", "_GATE_DAMAGE", "_TOWER_DAMAGE", "_HEALING"};
	public void saveDataFromCoreToMap() {
		if (Bukkit.getPlayer(uuid) != null) {
			for (String type : types) { //Overall
				int value = 0;
				if (DodoCore.getInstance().getPlayerData(uuid, "KNIGHTS", type, Integer.class) != null) {
					value = DodoCore.getInstance().getPlayerData(uuid, "KNIGHTS", type, Integer.class);
				}
				oldData.put(type, value);
			}
			for (String type : kitTypes) {
				int value = 0;
				if (DodoCore.getInstance().getPlayerData(uuid, "KNIGHTS", kit.toUpperCase() + type, Integer.class) != null) {
					value = DodoCore.getInstance().getPlayerData(uuid, "KNIGHTS", kit.toUpperCase() + type, Integer.class);
				}
				oldData.put(kit.toUpperCase() + type, value);
			}
		}
	}
	
	public int getOldCoreData(String type) {
		if (oldData.containsKey(type)) {
			return oldData.get(type);
		}
		return 0;
	}
	
}
