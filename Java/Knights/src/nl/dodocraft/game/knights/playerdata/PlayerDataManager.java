package nl.dodocraft.game.knights.playerdata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.common.boosters.Booster;
import nl.dodocraft.common.server.DodoGamemode;
import nl.dodocraft.core.internal.InternalAPI;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.thiemoboven.ncore.SimpleCore;

public class PlayerDataManager {

	private Loader plugin;

	private HashMap<UUID, PlayerData> playerData = new HashMap<>();

	public final static int MAX_MANA = 100;

	public PlayerDataManager(Loader plugin) {
		this.plugin = plugin;

	}

	public void createNewPlayerData(Player player) {
		if (!this.playerData.containsKey(player.getUniqueId())) {
			this.playerData.put(player.getUniqueId(), new PlayerData(player));
		}
	}

	public PlayerData getPlayerData(Player player) {
		if (this.playerData.containsKey(player.getUniqueId())) {
			return this.playerData.get(player.getUniqueId());
		}
		return null;
	}

	public int getPlayerMoney(Player player) {
		int money = 0;
		if (getPlayerData(player) != null) {
			money = getPlayerData(player).getGold();
		}
		return money;
	}

	public int getPlayerKills(Player player) {
		int kills = 0;
		if (getPlayerData(player) != null) {
			kills = getPlayerData(player).getKills();
		}
		return kills;
	}

	public int getPlayerAssists(Player player) {
		int assists = 0;
		if (getPlayerData(player) != null) {
			assists = getPlayerData(player).getAssists();
		}
		return assists;
	}

	public int getPlayerCoins(Player player) {
		int coins = 0;
		if (getPlayerData(player) != null) {
			coins = getPlayerData(player).getCoins();
		}
		return coins;
	}

	public int getPlayerExperience(Player player) {
		int xp = 0;
		if (getPlayerData(player) != null) {
			xp = getPlayerData(player).getXp();
		}
		return xp;
	}

	public void addMoney(Player player, int money) {
		if (getPlayerData(player) != null) {
			getPlayerData(player).addGold(money);
		}
		plugin.getScoreBoard().updateMoney(player);
		plugin.getItemManager().giveShop(player);
		player.sendMessage(ChatColor.GRAY + "Je hebt " + ChatColor.GREEN.toString() + ChatColor.BOLD + money + ChatColor.GOLD + " Gold " + ChatColor.GRAY + "ontvangen.");
	}

	public void addKill(Player player) {
		if (getPlayerData(player) != null) {
			getPlayerData(player).addKill();
		}
		plugin.getScoreBoard().updateKills(player);
	}

	public void removeMoney(Player player, int money) {
		if (getPlayerData(player) != null) {
			getPlayerData(player).removeGold(money);
		}
		plugin.getScoreBoard().updateMoney(player);
		plugin.getItemManager().giveShop(player);
	}

	public void startMana(Player player) {
		new BukkitRunnable() {
			boolean shineAdded = false;
			boolean shineRemoved = false;
			@Override
			public void run() {
				if (!player.isOnline() || plugin.getEnd()) {
					this.cancel();
				}
				int add = 2;
				if (player.getGameMode() != GameMode.SURVIVAL) {
					add = 0;
				}
				if (getPlayerData(player) != null) {
					if (getPlayerData(player).getMana() < MAX_MANA) {
						if (!shineRemoved) {
							removeShine(player);
							shineRemoved = true;
							shineAdded = false;
						}
						getPlayerData(player).addMana(add);
					} else {
						if (!shineAdded) {
							shineAdded = true;
							addShine(player);
							shineRemoved = false;
						}
					}
				}
				updateMana(player);
			}
		}.runTaskTimer(plugin, 0, 20L);
	}

	public void addShine(Player player) {
		int slot = 0;
		if (plugin.getKitManager().selectedKit.containsKey(player.getName())) {
			if (plugin.getKitManager().selectedKit.get(player.getName()).getName().equalsIgnoreCase("Hunter")) {
				slot = 1;
			}
		}
		if (player.getInventory().getItem(slot) != null) {
			ItemStack item = new ItemBuilder(player.getInventory().getItem(slot).clone()).hideAttributes().addEnchantGlow(Enchantment.MENDING, 1).toItemStack();
			player.getInventory().setItem(slot, item);
		}
	}

	public void removeShine(Player player) {
		int slot = 0;
		if (plugin.getKitManager().selectedKit.containsKey(player.getName())) {
			if (plugin.getKitManager().selectedKit.get(player.getName()).getName().equalsIgnoreCase("Hunter")) {
				slot = 1;
			}
		}
		if (player.getInventory().getItem(slot) != null && player.getInventory().getItem(slot).containsEnchantment(Enchantment.MENDING)) {
			ItemStack item = new ItemBuilder(player.getInventory().getItem(slot).clone()).hideAttributes().removeEnchantment(Enchantment.MENDING).toItemStack();
			player.getInventory().setItem(slot, item);
		}
	}

	public void resetMana(Player player) {
		if (getPlayerData(player) != null) {
			getPlayerData(player).resetMana();
			updateMana(player);
		}
	}

	public void updateMana(Player player) {
		if (getPlayerData(player) != null) {
			player.setLevel(getPlayerData(player).getMana());
			player.setExp(((float)getPlayerData(player).getMana()) / MAX_MANA);
		}
	}

	public void addMana(Player player, int value) {
		if (getPlayerData(player) != null) {
			if ((getPlayerData(player).getMana() + value) > MAX_MANA) {
				value = MAX_MANA - getPlayerData(player).getMana();
			}
			getPlayerData(player).addMana(value);
			updateMana(player);
		}
	}

	public int getMana(Player player) {
		int mana = 0;
		if (getPlayerData(player) != null) {
			mana = getPlayerData(player).getMana();
		}
		return mana;
	}

	public int getMaxMana() {
		return MAX_MANA;
	}

	public void addCoins(Player player, int coins) {
		if (getPlayerData(player) != null) {
			getPlayerData(player).addCoins(coins);
		}
		if (plugin.getPlayerDataManager().getPlayerData(player) != null) {
			SimpleCore.getInstance().getInternalAPI().addInGameCoins(player.getUniqueId(), coins, "KNIGHTCOINS", false);
		}
	}

	public void addExperience(Player player, int xp) {
		if (getPlayerData(player) != null) {
			getPlayerData(player).addXp(xp);
		}
	}

	public int getDynamitePlaced(Player player) {
		int dynamite = 0;
		if (getPlayerData(player) != null) {
			dynamite = getPlayerData(player).getTntPlaced();
		}
		return dynamite;
	}

	public List<String> getMostKills() {
		HashMap<String, Integer> mostKills = new HashMap<>();
		for (PlayerData data : playerData.values()) {
			mostKills.put(data.getName(), data.getKills());
		}
		mostKills = sortHashMapByValues(mostKills);
		
		List<String> top3Killers = new ArrayList<>();
		
		for (int i = 0; i < mostKills.size(); i++) {
			int kills = (int) mostKills.values().toArray()[i];
			String s = mostKills.keySet().toArray()[i] + ChatColor.GRAY.toString() + " - " + ChatColor.AQUA + ChatColor.BOLD + kills + (kills == 1 ? " kill" : " kills");
			top3Killers.add(s);
		}
		
		return top3Killers;
		
	}
	
	public HashMap<String, Integer> sortHashMapByValues(HashMap<String, Integer> passedMap) {
	    List<String> mapKeys = new ArrayList<>(passedMap.keySet());
	    List<Integer> mapValues = new ArrayList<>(passedMap.values());
	    Collections.sort(mapValues);
	    Collections.sort(mapKeys);

	    HashMap<String, Integer> sortedMap = new HashMap<>();

	    Iterator<Integer> valueIt = mapValues.iterator();
	    while (valueIt.hasNext()) {
	        Integer val = valueIt.next();
	        Iterator<String> keyIt = mapKeys.iterator();

	        while (keyIt.hasNext()) {
	            String key = keyIt.next();
	            Integer comp1 = passedMap.get(key);
	            Integer comp2 = val;

	            if (comp1.equals(comp2)) {
	                keyIt.remove();
	                sortedMap.put(key, val);
	                break;
	            }
	        }
	    }
	    return sortedMap;
	}

	public HashMap<UUID, PlayerData> getPlayerDataList() {
		return this.playerData;
	}

}
