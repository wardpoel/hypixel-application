package nl.dodocraft.game.knights;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import nl.dodocraft.common.server.ServerStatus;
import nl.dodocraft.common.server.ServerType;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.combot.CombotManager;
import nl.dodocraft.game.knights.commands.CmdSetCenter;
import nl.dodocraft.game.knights.commands.CmdSetCrystal;
import nl.dodocraft.game.knights.commands.CmdSetGate;
import nl.dodocraft.game.knights.commands.CmdSetSpawn;
import nl.dodocraft.game.knights.countdown.CountdownStart;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.game.knights.inventory.items.ItemManager;
import nl.dodocraft.game.knights.inventory.shop.PlayerShop;
import nl.dodocraft.game.knights.inventory.shop.upgrades.UpgradeManager;
import nl.dodocraft.game.knights.kits.KitManager;
import nl.dodocraft.game.knights.listener.CrystalListener;
import nl.dodocraft.game.knights.listener.FightListener;
import nl.dodocraft.game.knights.listener.GameListener;
import nl.dodocraft.game.knights.listener.GateListener;
import nl.dodocraft.game.knights.listener.ability.PlayerAbilityListener;
import nl.dodocraft.game.knights.listener.passive.PlayerPassiveListener;
import nl.dodocraft.game.knights.lobby.listener.ShopListener;
import nl.dodocraft.game.knights.lobby.shop.LobbyManager;
import nl.dodocraft.game.knights.objects.ObjectsManager;
import nl.dodocraft.game.knights.playerdata.PlayerDataManager;
import nl.dodocraft.game.knights.quests.RegisteredQuests;
import nl.dodocraft.game.knights.scoreboard.ScoreBoard;
import nl.dodocraft.game.knights.team.TeamManager;
import nl.dodocraft.utils.game.DodoGameManager;
import nl.dodocraft.utils.game.countdowns.GameCountdown;
import nl.thiemoboven.methods.Methods;
import nl.thiemoboven.ncore.SimpleCore;

public class Loader extends JavaPlugin {
	
	private static Loader instance;
	public final String KIT_SELECTOR = ChatColor.GREEN + "" + ChatColor.BOLD + "Welke kit wil je gebruiken?";
	public final String TEAM_SELECTOR = ChatColor.GREEN + "" + ChatColor.BOLD + "In welk team wil je?";
	private boolean end = false;
	private DodoGameManager gameManager;
	private GameCountdown countDown;
	
	private TeamManager teamManager;
	private ObjectsManager objectsManager;
	private ItemManager itemManager;
	private PlayerDataManager playerDataManager;
	private GameType gameType;
	private UpgradeManager upgradeManager;
	private FightListener fightListener;
	private KitManager kitManager;
	private ScoreBoard scoreBoard;	
	private CombotManager combotManager;
	private RegisteredQuests registeredQuests;
	private LobbyManager lobbyManager;
	private PlayerShop playerShop;
	
	@Override
	public void onEnable() {
		/* Initialize instance variable */
		instance = this;
		
		if(SimpleCore.getInstance().getInternalAPI().getServer().getPrefix().getServerType() == ServerType.LOBBY) {
	        DodoCore.getInstance().registerCurrency("KNIGHTCOINS");
	        lobbyManager = new LobbyManager(this);
			getServer().getPluginManager().registerEvents(new ShopListener(), this);			
		} else {
			/* Copy default config from project and create a plugin folder with this file */
			getConfig().options().copyDefaults(true);
			saveConfig();
			
			/* Instantiate GameManager located in DodoUtils */
			String map = getConfig().contains("map") ? getConfig().getString("map") : "Geen Naam";
			int minPlayers = getConfig().contains("minPlayers") ? getConfig().getInt("minPlayers") : 2;		
			
			if (minPlayers <= 8) {
				this.gameType = GameType.MINI;
			} else {
				this.gameType = GameType.MEGA;
			}
			
			gameManager = new DodoGameManager("Knights", map, minPlayers, getServer().getMaxPlayers(), false);

	        new CountdownStart(this);
			
	        objectsManager = new ObjectsManager(this);
	        itemManager = new ItemManager(this);
	        playerDataManager = new PlayerDataManager(this);
	        upgradeManager = new UpgradeManager(this);
	        scoreBoard = new ScoreBoard();
	        registeredQuests = new RegisteredQuests();
	        
			/* Update InternalAPI */
	        SimpleCore.getInstance().getInternalAPI().updateMapName(this.gameManager.getMapName());
	        SimpleCore.getInstance().getInternalAPI().updateStatus(ServerStatus.WAITING);
	        SimpleCore.getInstance().getInternalAPI().update();
	        
	        /* Register listeners */
	        getServer().getPluginManager().registerEvents(new GameListener(), this);
	        getServer().getPluginManager().registerEvents(playerShop = new PlayerShop(), this);
	        getServer().getPluginManager().registerEvents(new CrystalListener(), this);
	        getServer().getPluginManager().registerEvents(fightListener = new FightListener(this), this);
	        getServer().getPluginManager().registerEvents(kitManager = new KitManager(this), this);
	        getServer().getPluginManager().registerEvents(new GateListener(), this);
	        getServer().getPluginManager().registerEvents(new PlayerAbilityListener(this), this);
	        getServer().getPluginManager().registerEvents(new PlayerPassiveListener(this), this);
	        getServer().getPluginManager().registerEvents(teamManager = new TeamManager(this), this);
	        getServer().getPluginManager().registerEvents(combotManager = new CombotManager(), this);
	        
	        getCommand("setcrystal").setExecutor(new CmdSetCrystal());
	        getCommand("setspawn").setExecutor(new CmdSetSpawn());
	        getCommand("setgate").setExecutor(new CmdSetGate());
	        getCommand("setcenter").setExecutor(new CmdSetCenter());	     
	        getCommand("shop").setExecutor(playerShop);
		}
 
		
		SimpleCore.getInstance().getInternalAPI().setCoinName("KNIGHT");
		DodoCore.getInstance().registerCurrency("KNIGHTCOINS");
		
		String[] caches = {"KILLS", "EKILLS", "ASSISTS", "WINS", "KIT", "ELEKTRON_UNLOCKED", "GAMES_PLAYED", "DEATHS", "DYNAMITE_PLACED", "NEXUS_HITS", "MEGA_NEXUS_HITS", "MEGA_TOWER_HITS", "BLOCKS_PLACED", "BLOCKS_BROKEN", "BLOCKS_PICKED_UP", "TOTAL_DAMAGE_DONE", "TOTAL_HEALTH_GAINED", "TOTAL_GOLD_GAINED", "TOTAL_SWORD_UPGRADES", "TOTAL_GEAR_UPGRADES", "TOTAL_FASTFEET_UPGRADES", "TOTAL_HEALER_UPGRADES", "TOTAL_UPGRADES", "GOLDEN_APPLES_EATEN", "TIMES_PLAYED_NINJA", "TIMES_PLAYED_HUNTER", "TIMES_PLAYED_MENDER", "TIMES_PLAYED_PIRATE", "TIMES_PLAYED_ELEKTRON", "CANNONBALLS_HITS", "CANNONBALLS_FIRED", "TOTAL_CANNONBALLS_DAMAGE", "TIMES_SEARAGE_ACTIVATED", "TOTAL_DASHES", "TOTAL_DASHES_DAMAGE", "TIMES_COMBOSPEED_ACTIVATED", "FARM_EXPLOSION_ARROWS_FIRED", "TIMES_FARM_EXPLOSION_ACTIVATED", "PIGS_FIRED", "CHICKENS_FIRED", "SHEEPS_FIRED", "COWS_FIRED", "TOTAL_TRIPLE_SHOTS_FIRED", "TIMES_LIFESTEAL_ACTIVATED", "TOTAL_PERSONS_HITTED_LIFESTEAL", "GAINED_HEALTH_BY_LIFESTEAL", "DAMAGED_DEALED_BY_LIFESTEAL", "TOTAL_HEALTH_GAINED_BY_RECOVER", "TIMES_LASEREYES_ACTIVATED", "TIMES_LASERSCAN_ACTIVATED", "TOTAL_HITS_WITH_LASEREYES", "TOTAL_HITS_WITH_LASERSCAN", "TIMES_LASERUP_ACTIVATED"};
		
		for (String type : caches) {
			DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "KNIGHTS", type);
		}
		
		String[] classes = {"NINJA_", "ELEKTRON_", "HUNTER_", "MENDER_", "PIRATE_"};
		String[] classesCaches = {"KILLS", "DEATHS", "EKILLS", "EDEATHS", "ASSISTS", "WINS", "DAMAGE", "HEALING", "ABILITIES", "HOURS", "TIMES_PLAYED", "NEXUS_DAMAGE", "GATE_DAMAGE", "TOWER_DAMAGE"};
		for (String type : classes) {
			for (String c : classesCaches) {
				DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "KNIGHTS", type + c);
			}
		}
		
        DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "LOBBY", "HCOOLDOWN");
        DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "LOBBY", "CCOOLDOWN");
        DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "LOBBY", "HBOXES");
        DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "LOBBY", "CBOXES");
        
        SimpleCore.getInstance().getInternalAPI().ALLOW_THREAD_UPDATES = true; 
	}

	public static Loader getInstance() {
		return instance;
	}
	
	public DodoGameManager getGameManager() {
		return gameManager;
	}
	
	public void setCountdown(GameCountdown i) {
		this.countDown = i;
	}
	
	public GameCountdown getCountdown() {
		return this.countDown;
	}
	
	public ObjectsManager getObjectsManager() {
		return this.objectsManager;
	}
	
	public TeamManager getTeamManager() {
		return this.teamManager;
	}
	
	public ItemManager getItemManager() {
		return this.itemManager;
	}
	
	public PlayerDataManager getPlayerDataManager() {
		return this.playerDataManager;
	}
	
	public GameType getGameType() {
		return this.gameType;
	}
	
	public UpgradeManager getUpgradeManager() {
		return this.upgradeManager;
	}
	
	public FightListener getFightListener() {
		return this.fightListener;
	}
	
	public KitManager getKitManager() {
		return this.kitManager;
	}
	
	public LobbyManager getLobbyManager() {
		return this.lobbyManager;
	}
	
	public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
	
	public ScoreBoard getScoreBoard() {
		return this.scoreBoard;
	}
	
	public CombotManager getCombotManager() {
		return this.combotManager;
	}
	
	public RegisteredQuests getRegisteredQuests() {
		return this.registeredQuests;
	}
	
	public HashMap<UUID, HashMap<String, Integer>> quests = new HashMap<>();
    public void addValueToQuest(UUID uuid, String type) {
        quests.putIfAbsent(uuid, Maps.newHashMap());
        quests.get(uuid).putIfAbsent(type, 0);
        quests.get(uuid).put(type, quests.get(uuid).get(type) + 1);
    }
	
    public double getRespawnTime() {
    		double time = 6.5; //Default Mini
		if (getGameType() == GameType.MEGA) {
			time = 15.0; //Default Mega
		}
		if (getConfig().contains("respawnTime")) {
			time = getConfig().getDouble("respawnTime");
		}
		return time;
    }
    
    public boolean getEnd() {
    		return this.end;
    }
    
    public void setEnd(boolean bool) {
    		this.end = bool;
    }
    
}
