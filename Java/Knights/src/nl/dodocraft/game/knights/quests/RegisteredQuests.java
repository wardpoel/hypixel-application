package nl.dodocraft.game.knights.quests;

import java.util.ArrayList;
import java.util.List;

import nl.dodocraft.game.knights.quests.type.*;

/**
 * Created by ImSpooks on 25-7-2017
 * No part of this publication may be reproduced, distributed, or transmitted in any form or by any means.
 * Copyright © 2016 by ImSpooks
 */
public class RegisteredQuests {

    public List<QuestFormat> quests;

    public RegisteredQuests() {
        this.quests = new ArrayList<>();


        quests.add(new KQuestDefender());
        quests.add(new KQuestKnight());
        quests.add(new KQuestRusher());
        quests.add(new KQuestSilver());
        quests.add(new KQuestWizard());

        for (QuestFormat format : quests) {
            format.registerCacheData();
        }
    }
}
