package nl.dodocraft.game.knights.quests.type;

import nl.dodocraft.common.Rank;
import nl.dodocraft.game.knights.quests.QuestFormat;

public class KQuestSilver extends QuestFormat {

    @Override
    public int getId() {
        return 4;
    }

    @Override
    public String getName() {
        return "Silver+";
    }

    @Override
    public Rank getRequiredRank() {
        return Rank.SILVER;
    }

    @Override
    public String[] getRequirements() {
        return new String[] {"60 kills", "3000 Nexus/Gate damage"};
    }

    @Override
    public String[] getRequirementsData() {
        return new String[] {"60 KILLS", "3000 NDAMAGE"};
    }
}
