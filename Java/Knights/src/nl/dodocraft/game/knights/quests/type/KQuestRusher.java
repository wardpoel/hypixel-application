package nl.dodocraft.game.knights.quests.type;

import nl.dodocraft.common.Rank;
import nl.dodocraft.game.knights.quests.QuestFormat;

public class KQuestRusher extends QuestFormat {

    @Override
    public int getId() {
        return 1;
    }

    @Override
    public String getName() {
        return "Rusher";
    }

    @Override
    public Rank getRequiredRank() {
        return Rank.NORMAL;
    }

    @Override
    public String[] getRequirements() {
        return new String[] {"1200 Nexus/Gate damage"};
    }

    @Override
    public String[] getRequirementsData() {
        return new String[] {"1200 NDAMAGE"};
    }
}
