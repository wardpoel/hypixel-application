package nl.dodocraft.game.knights.quests;

import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.thiemoboven.methods.Methods;

import java.util.UUID;

/**
 * Created by ImSpooks on 24-7-2017
 * No part of this publication may be reproduced, distributed, or transmitted in any form or by any means.
 * Copyright © 2016 by ImSpooks
 */
public abstract class QuestFormat {

    public abstract int getId();

    public abstract String getName();

    public abstract Rank getRequiredRank();

    public abstract String[] getRequirements();

    public abstract String[] getRequirementsData();

    private String serverName() {
        return "KNIGHTS";
    }

    protected void registerCacheData() {
        DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, serverName(), getName().toUpperCase() + ".COOLDOWN");
        for (String string : getRequirementsData()) {
            String[] split = string.split(" ");
            DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, serverName(), getName().toUpperCase() + "." + split[1].toUpperCase());
        }
    }

    public long cooldownRemaining(UUID uuid) {
        long l = 0;
        if (DodoCore.getInstance().getPlayerData(uuid, serverName(), getName().toUpperCase() + ".COOLDOWN", Long.class) != null) {
            l = DodoCore.getInstance().getPlayerData(uuid, serverName(), getName().toUpperCase() + ".COOLDOWN", Long.class);
        }
        return l - System.currentTimeMillis();
    }

    public void add(UUID uuid, String type, int amount) {
        Rank rank = DodoCore.getInstance().getRank(uuid);
        if (rank == null || rank.getId() < getRequiredRank().getId()) {
            return;
        }

        if (cooldownRemaining(uuid) > 0)
            return;

        for (String string : getRequirementsData()) {
            String[] split = string.split(" ");

            int i = 0;

            if (DodoCore.getInstance().getPlayerData(uuid, serverName(), getName().toUpperCase() + "." + split[1].toUpperCase(), Integer.class) != null) {
                i = DodoCore.getInstance().getPlayerData(uuid, serverName(), getName().toUpperCase() + "." + split[1].toUpperCase(), Integer.class);
            }

            if (split[1].toUpperCase().equalsIgnoreCase(type)) {
                if (i < Integer.parseInt(split[0])) {
                    DodoCore.getInstance().setPlayerData(uuid, serverName(), getName().toUpperCase() + "." + split[1].toUpperCase(), ((i + amount) > Integer.parseInt(split[0]) ? Integer.parseInt(split[0]) : (i + amount)));
                }
            }
        }
    }
}
