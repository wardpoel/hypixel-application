package nl.dodocraft.game.knights.quests.type;

import nl.dodocraft.common.Rank;
import nl.dodocraft.game.knights.quests.QuestFormat;

public class KQuestWizard extends QuestFormat {

    @Override
    public int getId() {
        return 2;
    }

    @Override
    public String getName() {
        return "Wizard";
    }

    @Override
    public Rank getRequiredRank() {
        return Rank.NORMAL;
    }

    @Override
    public String[] getRequirements() {
        return new String[] {"60 Abilities gebruiken"};
    }

    @Override
    public String[] getRequirementsData() {
        return new String[] {"60 ABILITYU"};
    }
}
