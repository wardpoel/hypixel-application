package nl.dodocraft.game.knights.quests.type;

import nl.dodocraft.common.Rank;
import nl.dodocraft.game.knights.quests.QuestFormat;

public class KQuestDefender extends QuestFormat {

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public String getName() {
        return "Defender";
    }

    @Override
    public Rank getRequiredRank() {
        return Rank.NORMAL;
    }

    @Override
    public String[] getRequirements() {
        return new String[] {"15 Nexus defend kills"};
    }

    @Override
    public String[] getRequirementsData() {
        return new String[] {"15 NKILLS"};
    }
}
