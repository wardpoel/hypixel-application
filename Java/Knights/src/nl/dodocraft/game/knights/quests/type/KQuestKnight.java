package nl.dodocraft.game.knights.quests.type;

import nl.dodocraft.common.Rank;
import nl.dodocraft.game.knights.quests.QuestFormat;

public class KQuestKnight extends QuestFormat {

    @Override
    public int getId() {
        return 3;
    }

    @Override
    public String getName() {
        return "Knight";
    }

    @Override
    public Rank getRequiredRank() {
        return Rank.NORMAL;
    }

    @Override
    public String[] getRequirements() {
        return new String[] {"75 Nexus defend kills", "5 Eliminatie kills", "8000 Nexus/Gate damage", "350 Abilities gebruiken"};
    }

    @Override
    public String[] getRequirementsData() {
        return new String[] {"75 NKILLS", "5 EKILLS", "8000 NDAMAGE", "350 ABILITYU"};
    }
}
