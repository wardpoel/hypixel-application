package nl.dodocraft.game.knights.inventory.shop.upgrades;

import org.bukkit.Material;

public enum LifeSaver {
	LEVEL_0(0, 20D, "Life Saver I", 0, "20 Health"),
	LEVEL_1(1, 22D, "Life Saver II", 3, "22 Health"),
	LEVEL_2(2, 24D, "Life Saver III", 8, "24 Health"),
	LEVEL_3(3, 26D, "Life Saver IV", 12, "26 Health"),
	LEVEL_4(4, 28D, "Life Saver V", 16, "28 Health");
	
	private int level;
	private final static Material material = Material.SPLASH_POTION;
	private double lifes;
	private String name;
	private int price;
	private String description;
	
	LifeSaver(int level, double lifes, String name, int price, String description) {
		this.level = level;
		this.lifes = lifes;
		this.name = name;
		this.price = price;
		this.description = description;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public static Material getMaterial() {
		return material;
	}
	
	public double getLifes() {
		return this.lifes;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCurrentLevel() {
		if (getLevel() == 0) {
			return "Standaard level";
		} else if (getLevel() == 1) {
			return "Huidig level: I";
		} else if (getLevel() == 2) {
			return "Huidig level: II";
		} else if (getLevel() == 3) {
			return "Huidig level: III";
		} else {
			return "Huidig level: IV";
		}
	}
	
	public String getDescription() {
		return this.description;
	}
	
}
