package nl.dodocraft.game.knights.inventory.shop.upgrades;

import org.bukkit.Material;

public enum GoldenApple {
	LEVEL_0(0, "Golden Apple I", 0, 2, (20 * 32), "2x Golden Apple", "Elke 32s een nieuwe Apple"),
	LEVEL_1(1, "Golden Apple II", 3, 2, (20 * 30), "2x Golden Apple", "Elke 30s een nieuwe Apple"),
	LEVEL_2(2, "Golden Apple III", 6, 3, (20 * 28), "3x Golden Apple", "Elke 28s een nieuwe Apple"),
	LEVEL_3(3, "Golden Apple IV", 10, 4, (20 * 24), "4x Golden Apple", "Elke 24s een nieuwe Apple"),
	LEVEL_4(4, "Golden Apple V", 15, 5, (20 * 22), "5x Golden Apple", "Elke 22s een nieuwe Apple");
	
	private int level;
	private static Material material = Material.GOLDEN_APPLE;
	private String name;
	private int price;
	private int max;
	private int time;
	private String description;
	private String timeDescription;
	
	GoldenApple(int level, String name, int price, int max, int time, String description, String timeDescription) {
		this.level = level;
		this.name = name;
		this.price = price;
		this.max = max;
		this.time = time;
		this.description = description;
		this.timeDescription = timeDescription;
	}
	
	public static Material getMaterial() {
		return material;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public int getMax() {
		return this.max;
	}
	
	public int getTime() {
		return this.time;
	}
	
	public String getCurrentLevel() {
		if (getLevel() == 0) {
			return "Standaard level";
		} else if (getLevel() == 1) {
			return "Huidig level: I";
		} else if (getLevel() == 2) {
			return "Huidig level: II";
		} else if (getLevel() == 3) {
			return "Huidig level: III";
		} else {
			return "Huidig level: IV";
		}
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getTimeDescription() {
		return this.timeDescription;
	}
	
}
