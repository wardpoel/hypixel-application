package nl.dodocraft.game.knights.inventory.items;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.game.knights.inventory.shop.upgrades.Armor;
import nl.dodocraft.game.knights.inventory.shop.upgrades.FastFeet;
import nl.dodocraft.game.knights.inventory.shop.upgrades.GoldenApple;
import nl.dodocraft.game.knights.inventory.shop.upgrades.LifeSaver;
import nl.dodocraft.game.knights.inventory.shop.upgrades.Sword;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.inventory.ItemBuilder;

public class ItemManager {
	
	private Loader plugin;
	
	private HashMap<UUID, Sword> playerSword = new HashMap<>();
	private HashMap<UUID, Armor> playerArmor = new HashMap<>();
	private HashMap<UUID, LifeSaver> playerLifeSaver = new HashMap<>();
	private HashMap<UUID, FastFeet> playerFastFeet = new HashMap<>();
	private HashMap<UUID, GoldenApple> playerGoldenApple = new HashMap<>();
	
	public ItemManager(Loader plugin) {
		this.plugin = plugin;		
	}
	
	public HashMap<UUID, Sword> getPlayerSwords() {
		return this.playerSword;
	}
	
	public HashMap<UUID, Armor> getPlayerArmors() {
		return this.playerArmor;
	}
	
	public HashMap<UUID, LifeSaver> getPlayerLifeSavers() {
		return this.playerLifeSaver;
	}
	
	public HashMap<UUID, FastFeet> getPlayerFastFeets() {
		return this.playerFastFeet;
	}
	
	public HashMap<UUID, GoldenApple> getPlayerGoldenApples() {
		return this.playerGoldenApple;
	}
	
	public void giveItems(Player player) {
		if (player != null && player.isValid() && player.isOnline()) {
			player.getInventory().clear();
			giveSword(player);
			giveArmor(player);
			giveShop(player);
			giveLifeSaver(player);
			giveFastFeet(player);
			giveDynamiet(player);
			
			if (plugin.getGameType() == GameType.MINI) {
				giveBlocks(player);
			}
			
			if (plugin.getKitManager().selectedKit.containsKey(player.getName())) {
				if (plugin.getKitManager().selectedKit.get(player.getName()).getName().equalsIgnoreCase("Hunter")) {
					giveBow(player);
				}
			}
		}
	}
	
	private void giveBlocks(Player player) {
		ItemStack blocks = new ItemBuilder(Material.WOOL, 32).setDurability((short)plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getBlockData()).toItemStack();
		player.getInventory().setItem(8, blocks);
		
		int slot = 1;
		if (plugin.getKitManager().selectedKit.get(player.getName()).getName().equalsIgnoreCase("Hunter")) {
			slot = 2;
		}
		
		ItemStack shears = new ItemBuilder(Material.SHEARS).setUnbreakable(true).toItemStack();
		player.getInventory().setItem(slot, shears);
	}

	public void giveArmor(Player player) {
		player.getInventory().setHelmet(createHelmet(player));
		player.getInventory().setChestplate(createChestplate(player));
		player.getInventory().setLeggings(createLeggings(player));
		player.getInventory().setBoots(createBoots(player));
		
		int level = 0;
		if (this.playerArmor.containsKey(player.getUniqueId())) {
			level = this.playerArmor.get(player.getUniqueId()).getLevel();
			this.playerArmor.remove(player.getUniqueId());
		}
		this.playerArmor.put(player.getUniqueId(), Armor.values()[level]);
	}
	
	public void giveSword(Player player) {
		ItemStack item;
		int level = 0;
		if (playerSword.containsKey(player.getUniqueId())) {
			level = playerSword.get(player.getUniqueId()).getLevel();
			playerSword.remove(player.getUniqueId());
		}
		item = new ItemBuilder(Sword.values()[level].getItem()).setName(ChatColor.AQUA.toString() + ChatColor.BOLD + plugin.getKitManager().selectedKit.get(player.getName()).getName() + "'s Sword").setUnbreakable(true).toItemStack();
		player.getInventory().setItem(0, item);
		this.playerSword.put(player.getUniqueId(), Sword.values()[level]);
		if (this.playerSword.get(player.getUniqueId()).getLevel() > 0) {
			if (plugin.getPlayerDataManager().getMana(player) == plugin.getPlayerDataManager().getMaxMana()) {
				plugin.getPlayerDataManager().addShine(player);
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public void giveLifeSaver(Player player) {
		int level = 0;
		if (playerLifeSaver.containsKey(player.getUniqueId())) {
			level = playerLifeSaver.get(player.getUniqueId()).getLevel();
			playerLifeSaver.remove(player.getUniqueId());
		}
		double lifes = LifeSaver.values()[level].getLifes();
		player.setMaxHealth(lifes);
		this.playerLifeSaver.put(player.getUniqueId(), LifeSaver.values()[level]);
	}
	
	public void giveFastFeet(Player player) {
		int level = 0;
		if (playerFastFeet.containsKey(player.getUniqueId())) {
			level = playerFastFeet.get(player.getUniqueId()).getLevel();
			playerFastFeet.remove(player.getUniqueId());
		}
		float speed = (float) FastFeet.values()[level].getSpeed();
		player.setWalkSpeed(speed);
		this.playerFastFeet.put(player.getUniqueId(), FastFeet.values()[level]);
	}
	
	private void giveBow(Player player) {
		player.getInventory().setItem(1, new ItemBuilder(Material.BOW).setName(ChatColor.AQUA.toString() + ChatColor.BOLD + plugin.getKitManager().selectedKit.get(player.getName()).getName() + "'s Bow").setUnbreakable(true).hideAttributes().toItemStack());
		giveArrow(player);
	}
	
	public void giveArrow(Player player) {
		player.getInventory().setItem(9, new ItemBuilder(Material.ARROW).hideAttributes().toItemStack());
	}
	
	public void giveShop(Player player) {
		ItemStack shop = new ItemBuilder(Material.GOLD_INGOT).setAmount((plugin.getPlayerDataManager().getPlayerMoney(player) == 0 ? 1 : plugin.getPlayerDataManager().getPlayerMoney(player))).setName(ChatColor.GOLD.toString() + ChatColor.BOLD + "Upgrades " + ChatColor.GRAY + "(" + plugin.getPlayerDataManager().getPlayerMoney(player) + " Gold)").toItemStack();
		player.getInventory().setItem(4, shop);
	}
	
	public Sword getSword(Player player) {
		return this.playerSword.get(player.getUniqueId());
	}
	
	public Armor getArmor(Player player) {
		return this.playerArmor.get(player.getUniqueId());
	}
	
	public LifeSaver getLifeSaver(Player player) {
		return this.playerLifeSaver.get(player.getUniqueId());
	}
	
	public FastFeet getFastFeet(Player player) {
		return this.playerFastFeet.get(player.getUniqueId());
	}
	
	public GoldenApple getGoldenApple(Player player) {
		return this.playerGoldenApple.get(player.getUniqueId());
	}
		
	private ItemStack createHelmet(Player player) {
		int level = 0;
		if (playerArmor.containsKey(player.getUniqueId())) {
			level = playerArmor.get(player.getUniqueId()).getLevel();
		}
		ItemStack helmet = new ItemBuilder(Armor.values()[level].getHelmet()).setUnbreakable(true).setLeatherArmorColor(plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getArmorColor()).toItemStack();
		return helmet;
	}
	
	private ItemStack createChestplate(Player player) {
		int level = 0;
		if (playerArmor.containsKey(player.getUniqueId())) {
			level = playerArmor.get(player.getUniqueId()).getLevel();
		}
		ItemStack chestplate;
		if (level == 0) {
			chestplate = new ItemBuilder(Armor.values()[level].getChestplate()).setUnbreakable(true).setLeatherArmorColor(plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getArmorColor()).toItemStack();
		} else {
			chestplate = new ItemBuilder(Armor.values()[level].getChestplate()).setUnbreakable(true).toItemStack();
		}
		return chestplate;
	}
	
	private ItemStack createLeggings(Player player) {
		int level = 0;
		if (playerArmor.containsKey(player.getUniqueId())) {
			level = playerArmor.get(player.getUniqueId()).getLevel();
		}
		ItemStack leggings;
		if (level == 0) {
			leggings = new ItemBuilder(Armor.values()[level].getLeggings()).setUnbreakable(true).setLeatherArmorColor(plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getArmorColor()).toItemStack();
		} else {
			leggings = new ItemBuilder(Armor.values()[level].getLeggings()).setUnbreakable(true).toItemStack();
		}
		return leggings;
	}
	
	private ItemStack createBoots(Player player) {
		int level = 0;
		if (playerArmor.containsKey(player.getUniqueId())) {
			level = playerArmor.get(player.getUniqueId()).getLevel();
		}
		ItemStack boots;
		if (level == 0) {
			boots = new ItemBuilder(Armor.values()[level].getBoots()).setUnbreakable(true).setLeatherArmorColor(plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getArmorColor()).toItemStack();
		} else {
			boots = new ItemBuilder(Armor.values()[level].getBoots()).setUnbreakable(true).toItemStack();
		}
		return boots;
	}
	
	public void giveDynamiet(Player player) {
		if (thisPlayerNeedsDynamite(player)) {
			for (Team team : Team.values()) {
				if (team.isActive() && team != plugin.getTeamManager().getPlayerTeam(player.getUniqueId())) {
					if (plugin.getObjectsManager().allTowersAreDown(team)) {
						player.getInventory().setItem(7, new ItemBuilder(Material.SKULL_ITEM).setDurability((short) SkullType.PLAYER.ordinal()).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZWI5OTRiNDFmMDdmODdiMzI4MTg2YWNmY2JkYWJjNjk5ZDViMTg0N2ZhYmIyZTQ5ZDVhYmMyNzg2NTE0M2E0ZSJ9fX0=").setName(ChatColor.RED.toString() + ChatColor.BOLD + "Dynamiet").addLoreLines(new String[]{ChatColor.GRAY + "Plaats Dynamiet voor de",ChatColor.GRAY + "gate van jouw tegenstanders.",ChatColor.GRAY + "Per keer dat je dynamiet explodeert",ChatColor.GRAY + "krijgt de gate " + ChatColor.RED + "20 HP" + ChatColor.GRAY + " damage.",}).toItemStack());
						break;
					}
				}
			}
		} else {
			player.getInventory().remove(7);
			givePlayerTracker(player);
		}
	}
	
	public boolean thisPlayerNeedsDynamite(Player player) {
		for (Team team : Team.values()) {
			if (team.isActive() && team != plugin.getTeamManager().getPlayerTeam(player.getUniqueId())) {
				if (plugin.getObjectsManager().getGates().get(team).get(0).isAlive()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void givePlayerTracker(Player player) {
		ItemBuilder playerTracker = new ItemBuilder(Material.COMPASS).setName(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Player Tracker");
		player.getInventory().setItem(7, playerTracker.toItemStack());
	}
	
	public void autoBlocks(Player player) {
		new BukkitRunnable() {
			@Override
			public void run() {
				if (Loader.getInstance().getEnd() || !player.isOnline()) {
					this.cancel();
					return;
				} else if (player.getGameMode() == GameMode.SURVIVAL) {
					ItemBuilder item = new ItemBuilder(Material.WOOL).setDurability((byte) Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId()).getBlockData());
					if (!player.getInventory().contains(Material.WOOL)) {
    						player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 3F, 0F);
    						player.getInventory().setItem(8, item.toItemStack());
    					} else if (player.getInventory().getItem(8).getAmount() < 64) {
    						player.getInventory().addItem(item.toItemStack());
    					}
				}
			}
		}.runTaskTimer(plugin, 60L, 60L);
	}
	
	public void startGoldenApples(Player player) {
		int level = 0;
		if (playerGoldenApple.containsKey(player.getUniqueId())) {
			level = playerGoldenApple.get(player.getUniqueId()).getLevel();
			playerGoldenApple.remove(player.getUniqueId());
		}
		this.playerGoldenApple.put(player.getUniqueId(), GoldenApple.values()[level]);
		if (player.isValid() && player.getGameMode() == GameMode.SURVIVAL) {
			int delay = (getPlayerGoldenApples().get(player.getUniqueId()).getLevel() > 0 ? 0 : getPlayerGoldenApples().get(player.getUniqueId()).getTime());
			int time = getPlayerGoldenApples().get(player.getUniqueId()).getTime();
			new BukkitRunnable() {
				int currentLevel = getPlayerGoldenApples().get(player.getUniqueId()).getLevel();
				@Override
				public void run() {
					if (getPlayerGoldenApples().get(player.getUniqueId()).getLevel() > currentLevel || plugin.getEnd()) {
						this.cancel();
						return;
					} else {
						if (player.getGameMode() == GameMode.SURVIVAL) {
							giveGoldenApple(player);
						}
					}
				}
			}.runTaskTimer(plugin, delay, time);
		}
	}
	
	public void giveGoldenApple(Player player) {
		int slot = 2;
		if (plugin.getKitManager().selectedKit.get(player.getName()).getName().equalsIgnoreCase("Hunter")) {
			slot = 3;
		}
		
		int level = 0;
		if (playerGoldenApple.containsKey(player.getUniqueId())) {
			level = playerGoldenApple.get(player.getUniqueId()).getLevel();
			playerGoldenApple.remove(player.getUniqueId());
		}
		this.playerGoldenApple.put(player.getUniqueId(), GoldenApple.values()[level]);
		
		int currentAmountInInventory = 0;
		for (ItemStack item : player.getInventory().getContents()) {
			if (item != null) {
				if (item.getType() == Material.GOLDEN_APPLE) {
					currentAmountInInventory = item.getAmount();
				}
			}
		}

		if (currentAmountInInventory == 0) {
			player.getInventory().setItem(slot, new ItemBuilder(Material.GOLDEN_APPLE).hideAttributes().toItemStack());
		} else {
			if (currentAmountInInventory < this.playerGoldenApple.get(player.getUniqueId()).getMax()) {
				player.getInventory().addItem(new ItemBuilder(Material.GOLDEN_APPLE).hideAttributes().toItemStack());
			}
		}
	}
}
