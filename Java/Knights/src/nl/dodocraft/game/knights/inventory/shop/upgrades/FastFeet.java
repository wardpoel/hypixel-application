package nl.dodocraft.game.knights.inventory.shop.upgrades;

import org.bukkit.Material;

public enum FastFeet {
	LEVEL_0(0, 0.200, "Fast Feet I", 0, "4.31m/s Walkspeed", Material.LEATHER_BOOTS),
	LEVEL_1(1, 0.216, "Fast Feet II", 3, "4.83m/s Walkspeed", Material.GOLD_BOOTS),
	LEVEL_2(2, 0.232, "Fast Feet III", 8, "5.47m/s Walkspeed", Material.CHAINMAIL_BOOTS),
	LEVEL_3(3, 0.248, "Fast Feet IV", 12, "5.73m/s Walkspeed", Material.IRON_BOOTS),
	LEVEL_4(4, 0.264, "Fast Feet V", 16, "5.90m/s Walkspeed", Material.DIAMOND_BOOTS);
	
	private int level;
	private double speed;
	private String name;
	private int price;
	private String description;
	private Material material;
	
	FastFeet(int level, double speed, String name, int price, String description, Material material) {
		this.level = level;
		this.speed = speed;
		this.name = name;
		this.price = price;
		this.description = description;
		this.material = material;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public double getSpeed() {
		return this.speed;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public String getCurrentLevel() {
		if (getLevel() == 0) {
			return "Standaard level";
		} else if (getLevel() == 1) {
			return "Huidig level: I";
		} else if (getLevel() == 2) {
			return "Huidig level: II";
		} else if (getLevel() == 3) {
			return "Huidig level: III";
		} else {
			return "Huidig level: IV";
		}
	}
	
	public String getDescription() {
		return this.description;
	}
	
}
