package nl.dodocraft.game.knights.inventory.shop.upgrades;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum Sword {
	LEVEL_0(0, Material.WOOD_SWORD, 4, 4, "Sword I", 0, "Wooden Sword", "Sharpness 4"),
	LEVEL_1(1, Material.STONE_SWORD, 5, 5, "Sword II", 5,  "Stone Sword", "Sharpness 5"),
	LEVEL_2(2, Material.GOLD_SWORD, 6, 4, "Sword III", 10, "Gold Sword", "Sharpness 6"),
	LEVEL_3(3, Material.IRON_SWORD, 7, 6, "Sword IV", 15, "Iron Sword", "Sharpness 7"),
	LEVEL_4(4, Material.DIAMOND_SWORD, 8, 7, "Sword V", 20, "Diamond Sword", "Sharpness 8");
	
	private int level;
	private Material material;
	private int attackDamage;
	private int normalAttackDamage;
	private String name;
	private int price;
	private String description;
	private String attackDamageLore;
	
	Sword(int level, Material material, int attackDamage, int normalAttackDamage, String name, int price, String description, String attackDamageLore) {
		this.level = level;
		this.material = material;
		this.attackDamage = attackDamage;
		this.normalAttackDamage = normalAttackDamage;
		this.name = name;
		this.price = price;
		this.description = description;
		this.attackDamageLore = attackDamageLore;
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(getMaterial());
		return item;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public int getAttackDamage() {
		return this.attackDamage;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public int getNormalAttackDamage() {
		return this.normalAttackDamage;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public String getCurrentLevel() {
		if (getLevel() == 0) {
			return "Standaard level";
		} else if (getLevel() == 1) {
			return "Huidig level: I";
		} else if (getLevel() == 2) {
			return "Huidig level: II";
		} else if (getLevel() == 3) {
			return "Huidig level: III";
		} else {
			return "Huidig level: IV";
		}
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getAttackDamageLore() {
		return this.attackDamageLore;
	}
	
}
