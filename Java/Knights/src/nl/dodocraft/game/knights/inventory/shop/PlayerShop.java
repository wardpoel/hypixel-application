package nl.dodocraft.game.knights.inventory.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.inventory.shop.upgrades.Armor;
import nl.dodocraft.game.knights.inventory.shop.upgrades.FastFeet;
import nl.dodocraft.game.knights.inventory.shop.upgrades.GoldenApple;
import nl.dodocraft.game.knights.inventory.shop.upgrades.Sword;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.menu.DodoMenu;
import nl.dodocraft.utils.menu.MenuClick;

public class PlayerShop implements Listener, CommandExecutor {

	private List<UUID> cooldownShopMessage = new ArrayList<>();
	
	@EventHandler
	public void onPlayerClick(PlayerInteractEvent event) {
		if (event.getHand() == EquipmentSlot.HAND) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Player player = event.getPlayer();
				if (player.isValid()) {
					if (player.getInventory().getItemInMainHand() != null) {
						if (player.getInventory().getItemInMainHand().getType() == Material.GOLD_INGOT) {
							if (!Loader.getInstance().getCombotManager().isTagged(player)) {
								openShopMenu(player);
							} else {
								if (!cooldownShopMessage.contains(player.getUniqueId())) {
									int timeLeft = (int) ((Loader.getInstance().getCombotManager().tag.get(player) - System.currentTimeMillis()) / 1000);
									player.sendMessage(ChatColor.GRAY + "Over " + ChatColor.RED.toString() + ChatColor.BOLD + (timeLeft + 1) + "s" + ChatColor.GRAY + " kan je de shop weer openen.");
									player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
									cooldownShopMessage.add(player.getUniqueId());
									new BukkitRunnable() {
										@Override
										public void run() {
											if (cooldownShopMessage.contains(player.getUniqueId())) {
												cooldownShopMessage.remove(player.getUniqueId());
											}
										}
									}.runTaskLater(Loader.getInstance(), 20L);
								}
							}
						}
					}
				}
			}
		}
	}
		
	public void openShopMenu(Player player) {
		Sword currentSword = Loader.getInstance().getItemManager().getSword(player);
		Armor currentArmor = Loader.getInstance().getItemManager().getArmor(player);
		//LifeSaver currentLifeSaver = Loader.getInstance().getItemManager().getLifeSaver(player);
		FastFeet currentFastFeet = Loader.getInstance().getItemManager().getFastFeet(player);
		GoldenApple currentGoldenApple = Loader.getInstance().getItemManager().getGoldenApple(player);
		
		String green = ChatColor.GREEN.toString() + ChatColor.BOLD;
		String yellow = ChatColor.YELLOW.toString() + ChatColor.BOLD;
		String red = ChatColor.RED.toString() + ChatColor.BOLD;
		String gray = ChatColor.GRAY.toString() + ChatColor.BOLD;
				
		DodoMenu menu = new DodoMenu("Upgrades (" + Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + " Gold)", 4);
		
		//SWORD
		menu.addMenuClick(new ItemBuilder(currentSword.getMaterial()).setName(ChatColor.AQUA.toString() + ChatColor.BOLD + "Sword").hideAttributes().toItemStack(), new MenuClick() {
			@Override
			public boolean onItemClick(Player player) {
				player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
				return false;
			}
		}, 1);
		
		for (Sword sword : Sword.values()) {
			ItemStack item;
			if (sword.getLevel() <= currentSword.getLevel()) {
				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)5).setName(green + sword.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + sword.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + sword.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + sword.getAttackDamageLore(), "", ChatColor.GREEN + "Ontgrendeld"}).toItemStack(); //Heeft dit level al.
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
						player.sendMessage(ChatColor.RED + "Je hebt dit level al gekocht.");
						return false;
					}
					
				}, sword.getLevel() + 3);
			} else if (sword.getLevel() == currentSword.getLevel() + 1) {
				if (sword.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) {
					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 4).setName(yellow + sword.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + sword.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentSword.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + currentSword.getAttackDamageLore(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + sword.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + sword.getAttackDamageLore(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + sword.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")", ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om deze upgrade te ontgrendelen"} ).toItemStack();
				} else {
					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 14).setName(red + sword.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + sword.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentSword.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + currentSword.getAttackDamageLore(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + sword.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + sword.getAttackDamageLore(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + sword.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack();
				}
				
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						if (sword.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) { // Heeft genoeg geld
							player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 0F);
							Loader.getInstance().getPlayerDataManager().removeMoney(player, sword.getPrice());
							Loader.getInstance().getUpgradeManager().upgradeSword(player);
							player.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "Upgrade! " + ChatColor.GRAY + currentSword.getName() + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + sword.getName());
							openShopMenu(player);
						} else {
							player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
							player.sendMessage(ChatColor.GRAY + "Je mist " + ChatColor.RED + ChatColor.BOLD + (sword.getPrice() - Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) + ChatColor.GRAY + " Gold om dit item te kopen.");
						}
						return false;
					}
					
				}, sword.getLevel() + 3);
			} else {
				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)7).setName(gray + sword.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + sword.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + Sword.values()[sword.getLevel()-1].getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + Sword.values()[sword.getLevel()-1].getAttackDamageLore(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + sword.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + sword.getAttackDamageLore(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + sword.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack(); //Heeft dit level nog niet en kan dit niet kopen
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
						player.sendMessage(ChatColor.RED + "Je moet eerst het vorige level vrijspelen voordat je dit level kan kopen.");
						return false;
					}
				}, sword.getLevel() + 3);
			}
			
		}
		
		
		//ARMOR
		menu.addMenuClick(new ItemBuilder(currentArmor.getChestplate()).setName(ChatColor.AQUA.toString() + ChatColor.BOLD + "Armor").hideAttributes().toItemStack(), new MenuClick() {
			@Override
			public boolean onItemClick(Player player) {
				player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
				return false;
			}
		}, 10);
		
		for (Armor armor : Armor.values()) {
			ItemStack item;
			if (armor.getLevel() <= currentArmor.getLevel()) {
				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)5).setName(green + armor.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + armor.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + armor.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + armor.getProtection(), "", ChatColor.GREEN + "Ontgrendeld"}).toItemStack(); //Heeft dit level al.
				menu.addMenuClick(item, new MenuClick() {

					@Override
					public boolean onItemClick(Player player) {
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
						return false;
					}
					
				}, armor.getLevel() + 12);
			} else if (armor.getLevel() == currentArmor.getLevel() + 1) {
				if (armor.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) {
					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 4).setName(yellow + armor.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + armor.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentArmor.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + currentArmor.getProtection(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + armor.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + armor.getProtection(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + armor.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")", ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om deze upgrade te ontgrendelen"} ).toItemStack();
				} else {
					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 14).setName(red + armor.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + armor.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentArmor.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + currentArmor.getProtection(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + armor.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + armor.getProtection(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + armor.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack();
				}
				menu.addMenuClick(item, new MenuClick() {

					@Override
					public boolean onItemClick(Player player) {
						if (armor.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) { // Heeft genoeg geld
							player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 0F);
							Loader.getInstance().getPlayerDataManager().removeMoney(player, armor.getPrice());
							Loader.getInstance().getUpgradeManager().upgradeArmor(player);
							player.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "Upgrade! " + ChatColor.GRAY + currentArmor.getName() + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + armor.getName());
							openShopMenu(player);
						} else {
							player.sendMessage(ChatColor.GRAY + "Je mist " + ChatColor.RED + ChatColor.BOLD + (armor.getPrice() - Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) + ChatColor.GRAY + " Gold om dit item te kopen.");
						}
						return false;
					}
					
				}, armor.getLevel() + 12);
			} else {
				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)7).setName(gray + armor.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + armor.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + Armor.values()[armor.getLevel()-1].getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + Armor.values()[armor.getLevel()-1].getProtection(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + armor.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + armor.getProtection(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + armor.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack(); //Heeft dit level nog niet en kan dit niet kopen
				menu.addMenuClick(item, new MenuClick() {

					@Override
					public boolean onItemClick(Player player) {
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
						player.sendMessage(ChatColor.RED + "Je moet eerst het vorige level vrijspelen voordat je dit level kan kopen.");
						return false;
					}
					
				}, armor.getLevel() + 12);
			}
			
		}
		
		//GOLDEN APPLES
		menu.addMenuClick(new ItemBuilder(GoldenApple.getMaterial()).setName(ChatColor.AQUA.toString() + ChatColor.BOLD + "Golden Apple").hideAttributes().toItemStack(), new MenuClick() {
			@Override
			public boolean onItemClick(Player player) {
				player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
				return false;
			}
		}, 19);
			
		for (GoldenApple goldenApple : GoldenApple.values()) {
			ItemStack item;
			if (goldenApple.getLevel() <= currentGoldenApple.getLevel()) {
				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)5).setName(green + goldenApple.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + goldenApple.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + goldenApple.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + goldenApple.getTimeDescription(), "", ChatColor.GREEN + "Ontgrendeld"}).toItemStack(); //Heeft dit level al.
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
						return false;
					}
				}, goldenApple.getLevel() + 21);
			} else if (goldenApple.getLevel() == currentGoldenApple.getLevel() + 1) {
				if (goldenApple.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) {
					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 4).setName(yellow + goldenApple.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + goldenApple.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentGoldenApple.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + currentGoldenApple.getTimeDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + goldenApple.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + goldenApple.getTimeDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + goldenApple.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")", ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om deze upgrade te ontgrendelen"} ).toItemStack();
				} else {
					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 14).setName(red + goldenApple.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + goldenApple.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentGoldenApple.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + currentGoldenApple.getTimeDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + goldenApple.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + goldenApple.getTimeDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + goldenApple.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack();
				}
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						if (goldenApple.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) { // Heeft genoeg geld
							player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 0F);
							Loader.getInstance().getPlayerDataManager().removeMoney(player, goldenApple.getPrice());
							Loader.getInstance().getUpgradeManager().upgradeGoldenApple(player);
							player.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "Upgrade! " + ChatColor.GRAY + currentGoldenApple.getName() + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + goldenApple.getName());
							openShopMenu(player);
						} else {
							player.sendMessage(ChatColor.GRAY + "Je mist " + ChatColor.RED + ChatColor.BOLD + (goldenApple.getPrice() - Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) + ChatColor.GRAY + " Gold om dit item te kopen.");
						}
						return false;
					}
				}, goldenApple.getLevel() + 21);
			} else {
				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)7).setName(gray + goldenApple.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + goldenApple.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + GoldenApple.values()[goldenApple.getLevel()-1].getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + GoldenApple.values()[goldenApple.getLevel()-1].getTimeDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + goldenApple.getDescription(), ChatColor.DARK_GRAY + "   ▫ " + ChatColor.GRAY + goldenApple.getTimeDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + goldenApple.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack(); //Heeft dit level nog niet en kan dit niet kopen
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
						player.sendMessage(ChatColor.RED + "Je moet eerst het vorige level vrijspelen voordat je dit level kan kopen.");
						return false;
					}
				}, goldenApple.getLevel() + 21);
			}
		}
		
		//LIFESAVER
//		menu.addMenuClick(new ItemBuilder(LifeSaver.getMaterial()).setName(ChatColor.AQUA.toString() + ChatColor.BOLD + "Life Saver").hideAttributes().toItemStack(), new MenuClick() {
//			@Override
//			public boolean onItemClick(Player player) {
//				player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
//				return false;
//			}
//		}, 28);
//			
//		for (LifeSaver lifeSaver : LifeSaver.values()) {
//			ItemStack item;
//			if (lifeSaver.getLevel() <= currentLifeSaver.getLevel()) {
//				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)5).setName(green + lifeSaver.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + lifeSaver.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + lifeSaver.getDescription(), "", ChatColor.GREEN + "Ontgrendeld"}).toItemStack(); //Heeft dit level al.
//				menu.addMenuClick(item, new MenuClick() {
//					@Override
//					public boolean onItemClick(Player player) {
//						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
//						return false;
//					}
//				}, lifeSaver.getLevel() + 30);
//			} else if (lifeSaver.getLevel() == currentLifeSaver.getLevel() + 1) {
//				if (lifeSaver.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) {
//					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 4).setName(yellow + lifeSaver.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + lifeSaver.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentLifeSaver.getDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + lifeSaver.getDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + lifeSaver.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")", ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om deze upgrade te ontgrendelen"} ).toItemStack();
//				} else {
//					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 14).setName(red + lifeSaver.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + lifeSaver.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentLifeSaver.getDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + lifeSaver.getDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + lifeSaver.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack();
//				}
//				menu.addMenuClick(item, new MenuClick() {
//					@Override
//					public boolean onItemClick(Player player) {
//						if (lifeSaver.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) { // Heeft genoeg geld
//							player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 0F);
//							Loader.getInstance().getPlayerDataManager().removeMoney(player, lifeSaver.getPrice());
//							Loader.getInstance().getUpgradeManager().upgradeLifeSaver(player);
//							player.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "Upgrade! " + ChatColor.GRAY + currentLifeSaver.getName() + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + lifeSaver.getName());
//							openShopMenu(player);
//						} else {
//							player.sendMessage(ChatColor.GRAY + "Je mist " + ChatColor.RED + ChatColor.BOLD + (lifeSaver.getPrice() - Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) + ChatColor.GRAY + " Gold om dit item te kopen.");
//						}
//						return false;
//					}
//				}, lifeSaver.getLevel() + 30);
//			} else {
//				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)7).setName(gray + lifeSaver.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + lifeSaver.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + LifeSaver.values()[lifeSaver.getLevel()-1].getDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + lifeSaver.getDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + lifeSaver.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack(); //Heeft dit level nog niet en kan dit niet kopen
//				menu.addMenuClick(item, new MenuClick() {
//					@Override
//					public boolean onItemClick(Player player) {
//						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
//						player.sendMessage(ChatColor.RED + "Je moet eerst het vorige level vrijspelen voordat je dit level kan kopen.");
//						return false;
//					}
//				}, lifeSaver.getLevel() + 30);
//			}
//		}
		
		//FASTFEET
		menu.addMenuClick(new ItemBuilder(currentFastFeet.getMaterial()).setName(ChatColor.AQUA.toString() + ChatColor.BOLD + "Fast Feet").hideAttributes().toItemStack(), new MenuClick() {
			@Override
			public boolean onItemClick(Player player) {
				player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
				return false;
			}
		}, 28);
			
		for (FastFeet fastFeet : FastFeet.values()) {
			ItemStack item;
			if (fastFeet.getLevel() <= currentFastFeet.getLevel()) {
				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)5).setName(green + fastFeet.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + fastFeet.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + fastFeet.getDescription(), "", ChatColor.GREEN + "Ontgrendeld"}).toItemStack(); //Heeft dit level al.
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
						return false;
					}
				}, fastFeet.getLevel() + 30);
			} else if (fastFeet.getLevel() == currentFastFeet.getLevel() + 1) {
				if (fastFeet.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) {
					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 4).setName(yellow + fastFeet.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + fastFeet.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentFastFeet.getDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + fastFeet.getDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + fastFeet.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")", ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om deze upgrade te ontgrendelen"} ).toItemStack();
				} else {
					item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 14).setName(red + fastFeet.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + fastFeet.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + currentFastFeet.getDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + fastFeet.getDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + fastFeet.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack();
				}
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						if (fastFeet.getPrice() <= Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) { // Heeft genoeg geld
							player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 3F, 0F);
							Loader.getInstance().getPlayerDataManager().removeMoney(player, fastFeet.getPrice());
							Loader.getInstance().getUpgradeManager().upgradeFastFeet(player);
							player.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "Upgrade! " + ChatColor.GRAY + currentFastFeet.getName() + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + fastFeet.getName());
							openShopMenu(player);
						} else {
							player.sendMessage(ChatColor.GRAY + "Je mist " + ChatColor.RED + ChatColor.BOLD + (fastFeet.getPrice() - Loader.getInstance().getPlayerDataManager().getPlayerMoney(player)) + ChatColor.GRAY + " Gold om dit item te kopen.");
						}
						return false;
					}
				}, fastFeet.getLevel() + 30);
			} else {
				item = new ItemBuilder(Material.STAINED_CLAY).setDurability((short)7).setName(gray + fastFeet.getName()).addLoreLines(new String[] {ChatColor.DARK_GRAY + fastFeet.getCurrentLevel(), "", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + FastFeet.values()[fastFeet.getLevel()-1].getDescription(), ChatColor.GREEN + " ➜ ", ChatColor.DARK_GRAY + " ▪ " + ChatColor.GRAY + fastFeet.getDescription(), "", ChatColor.GRAY + "Kost: " + ChatColor.GOLD + fastFeet.getPrice() + " Gold" + ChatColor.GRAY + " (" +  Loader.getInstance().getPlayerDataManager().getPlayerMoney(player) + ")"} ).toItemStack(); //Heeft dit level nog niet en kan dit niet kopen
				menu.addMenuClick(item, new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 3F, 0F);
						player.sendMessage(ChatColor.RED + "Je moet eerst het vorige level vrijspelen voordat je dit level kan kopen.");
						return false;
					}
				}, fastFeet.getLevel() + 30);
			}
		}		
		menu.openMenu(player);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			openShopMenu((Player)sender);
		}
		return true;
	}
	
}
