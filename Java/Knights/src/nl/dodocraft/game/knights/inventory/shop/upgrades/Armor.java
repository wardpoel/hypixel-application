package nl.dodocraft.game.knights.inventory.shop.upgrades;

import org.bukkit.Material;


public enum Armor {
	LEVEL_0(0, Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS, "Armor I", 0, "Leather Armor", "Protection 1"),
	LEVEL_1(1, Material.LEATHER_HELMET, Material.GOLD_CHESTPLATE, Material.GOLD_LEGGINGS, Material.GOLD_BOOTS, "Armor II", 5, "Gold Armor", "Protection 2"),
	LEVEL_2(2, Material.LEATHER_HELMET, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_BOOTS, "Armor III", 10, "Chainmail Armor", "Protection 3"),
	LEVEL_3(3, Material.LEATHER_HELMET, Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS, Material.IRON_BOOTS, "Armor IV", 15, "Iron Armor", "Protection 4"),
	LEVEL_4(4, Material.LEATHER_HELMET, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_LEGGINGS, Material.DIAMOND_BOOTS, "Armor V", 20, "Diamond Armor", "Protection 5");
	
	private int level;
	private Material helmet;
	private Material chestplate;
	private Material leggings;
	private Material boots;
	private String name;
	private int price;
	private String description;
	private String protection;
	
	Armor(int level, Material helmet, Material chestplate, Material leggings, Material boots, String name, int price, String description, String protection) {
		this.level = level;
		this.helmet = helmet;
		this.chestplate = chestplate;
		this.leggings = leggings;
		this.boots = boots;
		this.name = name;
		this.price = price;
		this.description = description;
		this.protection = protection;
	}
	
	public Material getHelmet() {
		return this.helmet;
	}
	
	public Material getChestplate() {
		return this.chestplate;
	}
	
	public Material getLeggings() {
		return this.leggings;
	}
	
	public Material getBoots() {
		return this.boots;
	}
				
	public int getLevel() {
		return this.level;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public String getCurrentLevel() {
		if (getLevel() == 0) {
			return "Standaard level";
		} else if (getLevel() == 1) {
			return "Huidig level: I";
		} else if (getLevel() == 2) {
			return "Huidig level: II";
		} else if (getLevel() == 3) {
			return "Huidig level: III";
		} else {
			return "Huidig level: IV";
		}
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getProtection() {
		return this.protection;
	}
	
}
