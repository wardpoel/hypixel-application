package nl.dodocraft.game.knights.inventory.shop.upgrades;

import org.bukkit.entity.Player;

import nl.dodocraft.game.knights.Loader;

public class UpgradeManager {

	private Loader plugin;
	
	public UpgradeManager(Loader plugin) {
		this.plugin = plugin;
	}
	
	public void upgradeSword(Player player) {
		int level = 0;
		if (plugin.getItemManager().getPlayerSwords().containsKey(player.getUniqueId())) {
			level = plugin.getItemManager().getSword(player).getLevel();
			plugin.getItemManager().getPlayerSwords().remove(player.getUniqueId());
		}
		Sword sword = Sword.values()[level+1];
		plugin.getItemManager().getPlayerSwords().put(player.getUniqueId(), sword);
		plugin.getItemManager().giveSword(player);
	}
	
	public void upgradeArmor(Player player) {
		int level = 0;
		if (plugin.getItemManager().getPlayerArmors().containsKey(player.getUniqueId())) {
			level = plugin.getItemManager().getArmor(player).getLevel();
			plugin.getItemManager().getPlayerArmors().remove(player.getUniqueId());
		}
		Armor armor = Armor.values()[level+1];
		plugin.getItemManager().getPlayerArmors().put(player.getUniqueId(), armor);
		plugin.getItemManager().giveArmor(player);
	}
	
	public void upgradeLifeSaver(Player player) {
		int level = 0;
		if (plugin.getItemManager().getPlayerLifeSavers().containsKey(player.getUniqueId())) {
			level = plugin.getItemManager().getLifeSaver(player).getLevel();
			plugin.getItemManager().getPlayerLifeSavers().remove(player.getUniqueId());
		}
		LifeSaver lifeSaver = LifeSaver.values()[level+1];
		plugin.getItemManager().getPlayerLifeSavers().put(player.getUniqueId(), lifeSaver);
		plugin.getItemManager().giveLifeSaver(player);
	}
	
	public void upgradeFastFeet(Player player) {
		int level = 0;
		if (plugin.getItemManager().getPlayerFastFeets().containsKey(player.getUniqueId())) {
			level = plugin.getItemManager().getFastFeet(player).getLevel();
			plugin.getItemManager().getPlayerFastFeets().remove(player.getUniqueId());
		}
		FastFeet fastFeet = FastFeet.values()[level+1];
		plugin.getItemManager().getPlayerFastFeets().put(player.getUniqueId(), fastFeet);
		plugin.getItemManager().giveFastFeet(player);
	}
	
	public void upgradeGoldenApple(Player player) {
		int level = 0;
		if (plugin.getItemManager().getPlayerGoldenApples().containsKey(player.getUniqueId())) {
			level = plugin.getItemManager().getGoldenApple(player).getLevel();
			plugin.getItemManager().getPlayerGoldenApples().remove(player.getUniqueId());
		}
		GoldenApple goldenApple = GoldenApple.values()[level+1];
		plugin.getItemManager().getPlayerGoldenApples().put(player.getUniqueId(), goldenApple);
		plugin.getItemManager().startGoldenApples(player);
	}
	
}
