package nl.dodocraft.game.knights.countdown.gametype;

public enum GameType {
	MINI("Mini"),
	MEGA("Mega");
	
	private String name;
	
	GameType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
