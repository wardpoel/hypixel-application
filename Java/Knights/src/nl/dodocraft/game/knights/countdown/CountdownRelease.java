package nl.dodocraft.game.knights.countdown;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.utils.chat.messages.MessageController;
import nl.dodocraft.utils.game.countdowns.GameCountdown;
import nl.dodocraft.utils.game.countdowns.announce.CountdownType;
import nl.dodocraft.utils.game.countdowns.check.CountdownCheck;
import nl.dodocraft.utils.game.countdowns.check.CountdownEnd;
import nl.dodocraft.utils.game.countdowns.check.CountdownMethod;
import nl.dodocraft.utils.game.state.GameState;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class CountdownRelease {

    public CountdownRelease(Loader plugin) {
        GameCountdown countdown = new GameCountdown(0, -1, new CountdownCheck() {
            @Override
            public boolean onCountdownCheck() {
                return true;
            }
        }, new CountdownEnd() {
            @Override
            public void onCountdownEnd() {
            	/**
            	 * Game is gestart. 
            	 */
            	
                GameState.setState(GameState.IN_GAME);
            }
        }, CountdownType.RELEASING, "");
        
        countdown.addMethod(new CountdownMethod() {
            @Override
            public void execute() {
                if (countdown.getTimer() <= 5) {
                    switch (countdown.getTimer()) {
                        case 5:
                            for (Player p : Bukkit.getOnlinePlayers()) {
                                MessageController.sendTitle(p, 0, 25, 0, ChatColor.GREEN.toString() + ChatColor.BOLD + "5", "");
//                                p.sendTitle(ChatColor.GREEN.toString() + ChatColor.BOLD + "5", "");
                            }
                            break;
                        case 4:
                            for (Player p : Bukkit.getOnlinePlayers()) {
                                MessageController.sendTitle(p, 0, 25, 0, ChatColor.GREEN.toString() + ChatColor.BOLD + "4", "");
                            }
                            break;
                        case 3:
                            for (Player p : Bukkit.getOnlinePlayers()) {
                                MessageController.sendTitle(p, 0, 25, 0, ChatColor.YELLOW.toString() + ChatColor.BOLD + "3", "");
                            }
                            break;
                        case 2:
                            for (Player p : Bukkit.getOnlinePlayers()) {
                                MessageController.sendTitle(p, 0, 25, 0, ChatColor.GOLD.toString() + ChatColor.BOLD + "2", "");
                            }
                            break;
                        case 1:
                            for (Player p : Bukkit.getOnlinePlayers()) {
                                MessageController.sendTitle(p, 0, 25, 0, ChatColor.RED.toString() + ChatColor.BOLD + "1", "");
                            }
                            break;
                    }
                }
            }
        });
    }

}
