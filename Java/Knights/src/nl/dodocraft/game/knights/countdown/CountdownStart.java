package nl.dodocraft.game.knights.countdown;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.scheduler.BukkitRunnable;

import dodonick.ChannelHandler;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.DodoUtils;
import nl.dodocraft.utils.game.countdowns.GameCountdown;
import nl.dodocraft.utils.game.countdowns.announce.CountdownType;
import nl.dodocraft.utils.game.countdowns.check.CountdownCheck;
import nl.dodocraft.utils.game.countdowns.check.CountdownEnd;
import nl.dodocraft.utils.game.countdowns.check.CountdownMethod;
import nl.dodocraft.utils.game.state.GameState;
import nl.dodocraft.utils.scoreboard.controller.ScoreboardController;
import nl.thiemoboven.ncore.SimpleCore;

public class CountdownStart {

    public CountdownStart(Loader plugin) {
        final GameCountdown countdown = new GameCountdown(60, plugin.getGameManager().getMinPlayers(), new CountdownCheck() {
            @Override
            public boolean onCountdownCheck() {
                if (SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers().size() == 0) {
                    return false;
                }
                if (GameState.getState() != GameState.STARTING) {
                    if (SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers().size() < plugin.getGameManager().getMinPlayers()) {
                        return false;
                    } else {
                        GameState.setState(GameState.STARTING);
                    }
                }

                if (!GameState.currentState(GameState.STARTING)) {
                    return false;
                }
                return true;
            }
        }, new CountdownEnd() {
            @Override
            public void onCountdownEnd() {
                GameState.setState(GameState.IN_GAME);
                plugin.getTeamManager().createTeams();
                plugin.getObjectsManager().spawnAllObjects(); //Nexuses, towers, gates inspawnen.
                plugin.getObjectsManager().setActionBar();
                plugin.getObjectsManager().autoDamage();
                
                for(Player player : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) {
                		plugin.getPlayerDataManager().createNewPlayerData(player);
                		String kitName = "";
                		if (plugin.getKitManager().selectedKit.containsKey(player.getName())) {
                			String color = ChatColor.GRAY.toString();
                			if (plugin.getKitManager().selectedAbility.get(player.getName()).getLevel() == 7) {
                				color = ChatColor.GOLD.toString();
                			}
                			kitName = color + "[" + plugin.getKitManager().selectedKit.get(player.getName()).getAfkorting() + "] ";
                			plugin.getPlayerDataManager().getPlayerData(player).setKit(plugin.getKitManager().selectedKit.get(player.getName()).getName().toUpperCase());
                		}
                    player.setPlayerListName(kitName + plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor() + player.getName());
                    ChannelHandler.setColor(player, plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColorCode());
                }
                ChannelHandler.refresh(false);
                
                for (Player player : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) { //START!
                		Location spawn = plugin.getTeamManager().getTeamSpawn(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())).clone().add(0, 1, 0);
                		player.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 1F, 1F);
                		player.teleport(spawn, TeleportCause.PLUGIN);
                		player.setAllowFlight(false);
                		player.setFlying(false);
                		plugin.getPlayerDataManager().startMana(player);
                		plugin.getItemManager().giveItems(player);
                		if(!DodoUtils.getInstance().getGhostAPI().isVanished(player.getUniqueId())) {
                			plugin.getScoreBoard().setGameBoard(player);
                		}
                		plugin.getItemManager().startGoldenApples(player);
                		plugin.getItemManager().autoBlocks(player);
                		
                		plugin.getPlayerDataManager().getPlayerData(player).setTeam(plugin.getTeamManager().getPlayerTeam(player.getUniqueId()));
                		plugin.getPlayerDataManager().getPlayerData(player).saveDataFromCoreToMap();
                		new BukkitRunnable() {
                			@Override
                			public void run() {
                				if (plugin.getEnd()) {
                					this.cancel();
                					return;
                				}
                				if (player.isValid() && player.getGameMode() == GameMode.SURVIVAL && GameState.getState() == GameState.IN_GAME) {
                					plugin.getPlayerDataManager().addMoney(player, 2);
                				}
                				plugin.getPlayerDataManager().addCoins(player, 5);
                			}
                		}.runTaskTimer(plugin, 920L, 920L); //46seconden
                }
                
                for (Team t : Team.values()) {
                		if (t.isActive()) {
                			plugin.getScoreBoard().updateTeamObject(t);
                		}
                }
                
            }
        }, CountdownType.STARTING, "");

        countdown.addMethod(new CountdownMethod() {
            @Override
            public void execute() {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    ScoreboardController.getInstance().getScoreboard(p).setLine(2, ChatColor.GREEN.toString() + countdown.getTimer() + "s");
                }
            }
        });
        plugin.setCountdown(countdown);
    }
}