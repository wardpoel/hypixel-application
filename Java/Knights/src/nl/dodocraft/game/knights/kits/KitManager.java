package nl.dodocraft.game.knights.kits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.AbilityUtils;
import nl.dodocraft.game.knights.abilities.GameAbility;
import nl.dodocraft.game.knights.kits.content.KitElektron;
import nl.dodocraft.game.knights.kits.content.KitHunter;
import nl.dodocraft.game.knights.kits.content.KitMender;
import nl.dodocraft.game.knights.kits.content.KitNinja;
import nl.dodocraft.game.knights.kits.content.KitPirate;
import nl.dodocraft.game.knights.passives.GamePasive;
import nl.dodocraft.utils.game.state.GameState;
import nl.dodocraft.utils.menu.DodoMenu;
import nl.dodocraft.utils.menu.MenuClick;
import nl.dodocraft.utils.scoreboard.controller.ScoreboardController;
import nl.thiemoboven.methods.Method;
import nl.thiemoboven.methods.Methods;

/**
 * Created by Thiemo on 28-12-2015. No part of this publication may be
 * reproduced, distributed, or transmitted in any form or by any means.
 * Copyright © 2015 by Thiemo
 */
public class KitManager implements Listener {

    public final List<KitTemplate> registeredKits = new ArrayList<>();
    public final Map<String, KitTemplate> selectedKit = new HashMap<>();

    public final Map<String, GameAbility> selectedAbility = new HashMap<>();
    public final Map<String, GamePasive> selectedPassive1 = new HashMap<>();

    private final Loader plugin;

    private final AbilityUtils abilityUtils;

    public KitManager(Loader plugin) {
        this.plugin = plugin;
        this.abilityUtils = new AbilityUtils();

        registeredKits.add(new KitPirate());
        registeredKits.add(new KitMender());
        registeredKits.add(new KitHunter());
        registeredKits.add(new KitNinja());
        registeredKits.add(new KitElektron());
        

        for (KitTemplate k : this.registeredKits) {
            DodoCore.getInstance().cachePlayerData(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID());
            DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "KNIGHTS", k.getName().toUpperCase() + "_ABILITY");
            DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "KNIGHTS", k.getName().toUpperCase() + "_PASSIVE1");
        }
        
    }

    @EventHandler
    public void onInteractKit(PlayerInteractEvent event) {
        if (event.getAction() == Action.PHYSICAL) {
            return;
        }
        ItemStack s = event.getPlayer().getInventory().getItemInMainHand();
        if (s != null && s.hasItemMeta() && s.getItemMeta().hasDisplayName()
                && s.getItemMeta().getDisplayName().equals(this.plugin.KIT_SELECTOR)) {
            DodoMenu menu = new DodoMenu("Welke kit wil je gebruiken?", 3);
            int slots[] = new int[]{ 11, 12, 13, 14, 15 };
            final List<Kit> unlockedKits = this.getUnlockedKits(event.getPlayer());
            int slot = 0;

            for (Kit k : unlockedKits) {
                if (slot > unlockedKits.size()) {
                    continue;
                }
                boolean allowCheat = false;
                Rank rank = DodoCore.getInstance().getRank(event.getPlayer().getUniqueId());
                if (rank != null && (rank == Rank.YOUTUBER || rank.getId() >= Rank.ADMIN.getId())) {
                    allowCheat = true;
                }

                final boolean finalAllowCheat = allowCheat;
                k.getTemplate().addMenu(menu, event.getPlayer(), k.getTemplate(), slots[slot], new MenuClick() {
					@Override
					public boolean onItemClick(Player player) {
						if (k.getTemplate().getName().equalsIgnoreCase("Elektron")) {
							if (!hasElektron(player)) {
								player.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD  + "Je kan " + ChatColor.AQUA.toString() + ChatColor.BOLD + "Elektron" + ChatColor.RED.toString() + ChatColor.BOLD + " niet selecteren omdat je hem nog niet hebt vrijgespeeld.");
								return true;
							}
						}
						for (String types : new String[] {k.getTemplate().getName().toUpperCase() + "_ABILITY", k.getTemplate().getName().toUpperCase() + "_PASSIVE1"}) {
                            int level = 1;
                            if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", types, Integer.class) != null && DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", types, Integer.class) > 0) {
                                level = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", types, Integer.class);
                            }
                            if (finalAllowCheat && level < 6)
                                level = 6;

                            if (types.endsWith("_ABILITY")) {
                                selectedAbility.put(player.getName(), abilityUtils.getAbility(player, level, k.getTemplate(), plugin));
                            }
                            else {
                                if (types.endsWith("_PASSIVE1")) {
                                    selectedPassive1.put(player.getName(), abilityUtils.getPassive1(player, level, k.getTemplate(), plugin));
                                }
                            }
                        }
                        selectedKit.put(player.getName(), k.getTemplate());

                        player.closeInventory();
                        player.sendMessage(ChatColor.GRAY + "Je hebt de " + ChatColor.GOLD + k.getTemplate().getName() + " Class " + ChatColor.GRAY + "geselecteerd.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 5F, 5F);

                        if (GameState.getState() != GameState.IN_GAME && GameState.getState() != GameState.IN_PREGAME) {
                            ScoreboardController.getInstance().getScoreboard(player).setLine(5, ChatColor.GREEN + k.getTemplate().getName());
                        }

                        DodoCore.getInstance().setPlayerData(player.getUniqueId(), "KNIGHTS", "KIT", k.getTemplate().getName());
                        return true;
					}

                }, this.plugin);
                slot++;
            }
            menu.openMenu(event.getPlayer());
        }
    }
    
    public boolean hasElektron(Player player) {
		int unlocked = 0; //0 -> false; 1 -> true
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", Integer.class) != null) {
			unlocked = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", Integer.class);
		} else {
			DodoCore.getInstance().setPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", 0);
		}
		if (unlocked == 1 || DodoCore.getInstance().getRank(player.getUniqueId()).getId() >= Rank.ADMIN.getId()) {
			return true;
		}
		return false;
}

    public int getFixedLevel(Player player, String type) {
        boolean allowCheat = false;
        Rank rank = DodoCore.getInstance().getRank(player.getUniqueId());
        if (rank != null && (rank == Rank.YOUTUBER || rank.getId() >= Rank.ADMIN.getId())) {
            allowCheat = true;
        }
        if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) == null) {
            return allowCheat ? 6 : 1;
        }
        int value = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
        return allowCheat && value < 6 ? 6 : value;
    }

    public int getFixedLevelKit(Player player, int type) {
        boolean allowCheat = false;
        Rank rank = DodoCore.getInstance().getRank(player.getUniqueId());
        if (rank != null && (rank == Rank.YOUTUBER || rank.getId() >= Rank.ADMIN.getId())) {
            allowCheat = true;
        }
        if (DodoCore.getInstance().getCachedPlayerData(player.getUniqueId(), new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", type), true) == null) {
            return allowCheat ? 6 : 1;
        }
        int value = DodoCore.getInstance().getCachedPlayerData(player.getUniqueId(), new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", type), true);
        return allowCheat && value < 6 ? 6 : value;
    }

    public List<Kit> getUnlockedKits(Player p) {
        UUID id = p.getUniqueId();

        List<Kit> list = new ArrayList<>();

        boolean allowCheat = false;

        Rank rank = DodoCore.getInstance().getRank(p.getUniqueId());
        if (rank != null && (rank == Rank.YOUTUBER || rank.getId() >= Rank.ADMIN.getId())) {
            allowCheat = true;
        }

        for (KitTemplate k : this.registeredKits) {
            if (DodoCore.getInstance().getCachedPlayerData(id, new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID()), true) != null) {	
                int level = DodoCore.getInstance().getCachedPlayerData(p.getUniqueId(), new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID()), true);
                if (allowCheat) {
                		if (level < 6) {
                        level = 6;
                    }
                }
                list.add(Kit.byId(this.plugin, k.getStartID(), level));
            }
        }
        return list;
    }

    public String toRomain(int v) {
        if (v == 1) {
            return "I";
        } else if (v == 2) {
            return "II";
        } else if (v == 3) {
            return "III";
        } else if (v == 4) {
            return "IV";
        } else if (v == 5) {
            return "V";
        } else if (v == 6) {
            return "VI";
        } else if (v == 7) {
            return "VII";
        } else if (v == 8) {
            return "VIII";
        } else if (v == 9) {
            return "IX";
        } else if (v == 10) {
            return "X";
        } else
            return "";
    }


    public enum Abilities {
        NINJA_1("Dash", 4.0, 9, 2),
        NINJA_2("Dash", 4.0, 10, 2),
        NINJA_3("Dash", 4.0, 11, 2),
        NINJA_4("Dash", 4.0, 12, 2),
        NINJA_5("Dash", 4.0, 13, 2),
        NINJA_6("Dash", 4.0, 14, 2),
        NINJA_7("Dash", 4.3, 16, 2),

        PIRATE_1("Cannonballs", 4.844, 4, 0),
        PIRATE_2("Cannonballs", 5.689, 5, 0),
        PIRATE_3("Cannonballs", 6.533, 6, 0),
        PIRATE_4("Cannonballs", 7.378, 7, 0),
        PIRATE_5("Cannonballs", 8.222, 8, 0),
        PIRATE_6("Cannonballs", 9.067, 9, 0),
        PIRATE_7("Cannonballs", 10.334, 10, 0),
        
        HUNTER_1("Farm Explosion", 1, 10, 10),
        HUNTER_2("Farm Explosion", 2, 11, 11),
        HUNTER_3("Farm Explosion", 3, 12, 12),
        HUNTER_4("Farm Explosion", 4, 13, 13),
        HUNTER_5("Farm Explosion", 5, 14, 14),
        HUNTER_6("Farm Explosion", 6, 15, 15),
        HUNTER_7("Farm Explosion", 8, 16, 17),
        
        MENDER_1("Life Steal", 1, 7, 5),
        MENDER_2("Life Steal", 2, 8, 6),
        MENDER_3("Life Steal", 3, 9, 7),
        MENDER_4("Life Steal", 4, 10, 8),
        MENDER_5("Life Steal", 5, 11, 9),
        MENDER_6("Life Steal", 6, 12, 10),
        MENDER_7("Life Steal", 7, 14, 11),
        
    		ELEKTRON_1("Laser Eyes", 1, 20, 6),
    		ELEKTRON_2("Laser Eyes", 2, 22, 6.5),
    		ELEKTRON_3("Laser Eyes", 3, 24, 7),
    		ELEKTRON_4("Laser Eyes", 4, 26, 7.5),
    		ELEKTRON_5("Laser Eyes", 5, 28, 8),
    		ELEKTRON_6("Laser Eyes", 6, 30, 8.5),
        ELEKTRON_7("Laser Eyes", 7, 55, 10),
        ;

        public String display;
        public double damage;
        public int mana;
        public double count;
        public static int manaElektron = 20;
        
        Abilities(String display, double damage, int mana, double count) {
            this.display = display;
            this.damage = damage;
            this.mana = mana;
            this.count = count;
        }

        public static Abilities getAbility(String ability, int level) {
            for (Abilities a : values()) {
                if (a.toString().startsWith(ability)) {
                    if (a.toString().split("_")[1].equalsIgnoreCase(String.valueOf(level))) {
                        return a;
                    }
                }
            }
            throw new IllegalArgumentException("Invalid ability was given.");
        }
    }

    public enum Passives {
    		PIRATE_1("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "< 4/20 HP"),
    		PIRATE_2("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "< 5/20 HP"),
    		PIRATE_3("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "< 6/20 HP"),
    		PIRATE_4("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "< 7/20 HP"),
    		PIRATE_5("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "< 8/20 HP"),
    		PIRATE_6("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "< 9/20 HP"),
    		PIRATE_7("Sea Rage", "Resistance 3, 8s", "Strength 2, 6s", "< 9/20 HP"),
    	
        NINJA_1("Combo Speed", "Speed 1, 6s", "3", ""),
        NINJA_2("Combo Speed", "Speed 1, 7s", "3", ""),
        NINJA_3("Combo Speed", "Speed 1, 8s", "3", ""),
        NINJA_4("Combo Speed", "Speed 2, 6s", "3", ""),
        NINJA_5("Combo Speed", "Speed 2, 7s", "3", ""),
        NINJA_6("Combo Speed", "Speed 2, 8s", "3", ""),
        NINJA_7("Combo Speed", "Speed 3, 7s", "3", ""),
        
        MENDER_1("Recover", "1 ❤ na een kill", "", ""),
        MENDER_2("Recover", "1.5 ❤ na een kill", "", ""),
        MENDER_3("Recover", "2 ❤ na een kill", "", ""),
        MENDER_4("Recover", "2.5 ❤ na een kill", "", ""),
        MENDER_5("Recover", "3 ❤ na een kill", "", ""),
        MENDER_6("Recover", "3.5 ❤ na een kill", "", ""),
        MENDER_7("Recover", "4.3 ❤ na een kill", "", ""),
        
        HUNTER_1("Triple Shot", "15% kans op", "een Triple Shot", ""),
        HUNTER_2("Triple Shot", "20% kans op", "een Triple Shot", ""),
        HUNTER_3("Triple Shot", "25% kans op", "een Triple Shot", ""),
        HUNTER_4("Triple Shot", "30% kans op", "een Triple Shot", ""),
        HUNTER_5("Triple Shot", "40% kans op", "een Triple Shot", ""),
        HUNTER_6("Triple Shot", "50% kans op", "een Triple Shot", ""),
        HUNTER_7("Triple Shot", "65% kans op", "een Triple Shot", ""),
   
    		ELEKTRON_1("Laser Up", "5% kans op", " een laser", ""),
		ELEKTRON_2("Laser Up", "6% kans op", " een laser", ""),
		ELEKTRON_3("Laser Up", "7% kans op", " een laser", ""),
		ELEKTRON_4("Laser Up", "8% kans op", " een laser", ""),
		ELEKTRON_5("Laser Up", "9% kans op", " een laser", ""),
		ELEKTRON_6("Laser Up", "10% kans op", " een laser", ""),
		ELEKTRON_7("Laser Up", "12% kans op", " een laser", "");

        public String display;
        public String info, info2, info3;

        Passives(String display, String info, String info2, String info3) {
            this.display = display;
            this.info = info;
            this.info2 = info2;
            this.info3 = info3;
        }

        public static Passives getPassive(String ability, int level) {
            for (Passives a : values()) {
                if (a.toString().startsWith(ability)) {
                    if (a.toString().split("_")[1].equalsIgnoreCase(String.valueOf(level))) {
                        return a;
                    }
                }
            }
            throw new IllegalArgumentException("Invalid passive was given.");
        }
    }

    public AbilityUtils getAbilityUtils() {
        return abilityUtils;
    }
}
