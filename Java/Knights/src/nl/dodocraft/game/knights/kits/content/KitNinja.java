package nl.dodocraft.game.knights.kits.content;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.kits.KitTemplate;
import nl.dodocraft.game.knights.lobby.shop.LobbyManager;
import nl.dodocraft.game.knights.lobby.shop.StatsManager;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.menu.DodoMenu;
import nl.dodocraft.utils.menu.MenuClick;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class KitNinja implements KitTemplate {

    @Override
    public int getStartID() {
        return 3;
    }

    @Override
    public String getName() {
        return "Ninja";
    }

    @Override
    public ItemBuilder getSkullItem() {
    		return new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWNjZjI2MjkyNmQ4MmNlZWI1MzJjNzU3OTdiMjg0YzdlODkxYWU1MjQxYWIxODM5Yzg4YmNmNGU2YzIzMmMifX19==").setName(ChatColor.AQUA.toString() + ChatColor.BOLD + this.getName());
    }
    
    @Override
    public void addMenu(DodoMenu menu, Player player, KitTemplate kit, int slot, MenuClick click, Loader plugin) {
        String abilityString = this.getName().toUpperCase() + "_ABILITY";
        String passive1String = this.getName().toUpperCase() + "_PASSIVE1";
        int levelAbility = Loader.getInstance().getKitManager().getFixedLevel(player, abilityString);
        int levelPassive1 = Loader.getInstance().getKitManager().getFixedLevel(player, passive1String);
        LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), levelAbility);
        LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), levelPassive1);

        menu.addMenuClick(new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWNjZjI2MjkyNmQ4MmNlZWI1MzJjNzU3OTdiMjg0YzdlODkxYWU1MjQxYWIxODM5Yzg4YmNmNGU2YzIzMmMifX19==").setName(ChatColor.GREEN.toString() + ChatColor.BOLD + this.getName()).addLoreLines(new String[] {
        		ChatColor.GRAY + "Moeilijkheidsgraad: " + ChatColor.YELLOW + "••" + ChatColor.GRAY + "•",
				"",
				ChatColor.GRAY + "Ability: " + ChatColor.GREEN + ability.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.damage + " HP damage",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " Mana per hit",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + (int)ability.count + " Dashes",
				"",
				ChatColor.GRAY + "Passive: " + ChatColor.GREEN + passive1.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + passive1.info,
				"",
				ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om " + getName() + " te selecteren"
    }).hideAttributes().toItemStack(), click, slot);
    }

    @Override
    public String getAfkorting() {
        return "NJA";
    }
    
    public ItemBuilder getShopItem(Player player) {
		return null;
	}

	@Override
	public ItemBuilder getShopItem() {
		
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 6);
		LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), 6);
		
		return new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWNjZjI2MjkyNmQ4MmNlZWI1MzJjNzU3OTdiMjg0YzdlODkxYWU1MjQxYWIxODM5Yzg4YmNmNGU2YzIzMmMifX19==").setName(ChatColor.AQUA.toString() + ChatColor.BOLD + this.getName()).addLoreLines(new String[] {
				ChatColor.GRAY + "Moeilijkheidsgraad: " + ChatColor.YELLOW + "••" + ChatColor.GRAY + "•",
				"",
				ChatColor.GRAY + "Ability: " + ChatColor.GREEN + ability.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.damage + " HP damage",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " Mana per hit",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + (int)ability.count + " Dashes",
				"",
				ChatColor.GRAY + "Passive: " + ChatColor.GREEN + passive1.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + passive1.info,		
		});
	}
	
	 @Override
	    public String[] getLevelLoreAbility(Player player, int level) {
		 	LobbyManager.Abilities ability;
 		if (level == 1) {
 			ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
 		} else {
 			ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level-1);
 		}
 		LobbyManager.Abilities nextAbility = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
	        String price = LobbyManager.Prices.values()[level - 1].getStringPrice();
	        String[] lore = {
	        		"",
	        		ChatColor.GRAY + "Dash maximaal 2 keer langs jouw",
	        		ChatColor.GRAY + "vijanden heen en doe daarbij 4 HP",
	        		ChatColor.GRAY + "damage als je langs ze vliegt.",
	        		"",
	        		ChatColor.GRAY + "" + ability.mana + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextAbility.mana) + ChatColor.GRAY + " Mana per hit.",
	        		"",
	        		ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price,
	        };
	    		return lore;
	    }
	    
	    @Override
	    public String[] getLevelLorePassive(Player player, int level) {
    		LobbyManager.Passives passive1;
    		if (level == 1) {
    			passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
    		} else {
    			passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level-1);
    		}
    		LobbyManager.Passives nextPassive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
	        String price = LobbyManager.Prices.values()[level - 1].getStringPrice();
	        String[] lore = {
	        		"",
	        		ChatColor.GRAY + "Je krijgt " + passive1.info + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextPassive1.info),
	        		ChatColor.GRAY + "als je een vijand 3 keer achter elkaar",
	        		ChatColor.GRAY + "hit zonder zelf damage te krijgen.",
	        		"",
	        		ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price,
	        };
	    		return lore;
	    }
	
	    @Override
	    public String[] getInfoLore(Player player) {
	    		StatsManager playerStats = new StatsManager(getName(), player);
	    		return playerStats.getInfoLore();
	    }
	
	@Override
	public String[] getMasterLore(Player player) {
		LobbyManager.Abilities currentAbility = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 6);
		LobbyManager.Passives currentPassive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), 6);
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 7);
		LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), 7);
		String price = LobbyManager.Prices.values()[6].getStringPrice();
		return new String[] {
				"",
				ChatColor.GRAY + "Maak jouw favoriete",
				ChatColor.GRAY + "class nog sterker door er",
				ChatColor.GRAY + "een meester in te worden!",
				"",
				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "[" + ChatColor.GOLD + "✮" + ChatColor.GRAY + "] Master TAG in lobbies",
				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Gouden " + ChatColor.GOLD + "[" + getAfkorting() + "]" + ChatColor.GRAY + " TAG in-game",
				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Ability upgrade:",
				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + currentAbility.damage + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + ability.damage + ChatColor.GRAY +" Power",
				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + (int)currentAbility.count + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + (int)ability.count + "" + ChatColor.GRAY + " Dashes",
				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + currentAbility.mana + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + ability.mana + ChatColor.GRAY + " Mana per hit",
				"",
				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Passive upgrade: ",
				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + currentPassive1.info + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + passive1.info,
				"",
				ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price,
				"",
				ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik" + ChatColor.GRAY + " om deze upgrade te kopen"
		};
	}

	@Override
	public String[] getAbilityLore(int level) {
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
		return new String[] {
			"",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.damage + " HP damage",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + (int)ability.count + " Dashes",
			"",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " Mana per hit",
			"",
		};
	}

	@Override
	public String[] getPassiveLore(int level) {
		LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
		return new String[] {
			"",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + passive1.info,	
			"",
		};
	}
	
}
