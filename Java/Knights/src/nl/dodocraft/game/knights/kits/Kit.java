package nl.dodocraft.game.knights.kits;

import nl.dodocraft.game.knights.Loader;

public class Kit {

    private final KitTemplate template;
    private final int level;

    public Kit(KitTemplate template, int level) {
        this.template = template;
        this.level = level;
    }

    public KitTemplate getTemplate() {
        return template;
    }

    public int getLevel() {
        return level;
    }

    public static Kit byId(Loader plugin, int normalID, int level) {
        for (KitTemplate template : plugin.getKitManager().registeredKits) {
            if (template.getStartID() == normalID) {
                return new Kit(template, level);
            }
        }
        return null;
    }
}
