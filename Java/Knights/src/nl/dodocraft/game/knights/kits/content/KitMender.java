package nl.dodocraft.game.knights.kits.content;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.kits.KitTemplate;
import nl.dodocraft.game.knights.lobby.shop.LobbyManager;
import nl.dodocraft.game.knights.lobby.shop.StatsManager;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.menu.DodoMenu;
import nl.dodocraft.utils.menu.MenuClick;

public class KitMender implements KitTemplate {

	@Override
	public int getStartID() {
		return 1;
	}

	@Override
	public String getName() {
		return "Mender";
	}

	@Override
	public String getAfkorting() {
		return "MEN";
	}

	@Override
    public ItemBuilder getSkullItem() {
    		return new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNjFkM2IyODdkZWYwZTNmZjdhMjYzNmMxNDI2Yjk1M2JhZjFlZThiNTJhNzBkZTc1NzIzY2JiMjJkZGU5ZSJ9fX0==").setName(ChatColor.AQUA.toString() + ChatColor.BOLD + this.getName());
    }
	
	@Override
	public void addMenu(DodoMenu menu, Player player, KitTemplate kit, int slot, MenuClick click, Loader plugin) {
		String abilityString = this.getName().toUpperCase() + "_ABILITY";
        String passive1String = this.getName().toUpperCase() + "_PASSIVE1";
        int levelAbility = Loader.getInstance().getKitManager().getFixedLevel(player, abilityString);
        int levelPassive1 = Loader.getInstance().getKitManager().getFixedLevel(player, passive1String);
        LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), levelAbility);
        LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), levelPassive1);

        menu.addMenuClick(new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNjFkM2IyODdkZWYwZTNmZjdhMjYzNmMxNDI2Yjk1M2JhZjFlZThiNTJhNzBkZTc1NzIzY2JiMjJkZGU5ZSJ9fX0==").setName(ChatColor.GREEN.toString() + ChatColor.BOLD + this.getName()).addLoreLines(new String[] {
        			ChatColor.GRAY + "Moeilijkheidsgraad: " + ChatColor.YELLOW + "••" + ChatColor.GRAY + "•",
				"",
				ChatColor.GRAY + "Ability: " + ChatColor.GREEN + ability.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((ability.damage * 0.26), 2) + " D" + ChatColor.DARK_GRAY + " ❚ " + ChatColor.GRAY + round((ability.damage * 0.19) + 0.03, 2) + " H",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.count + "s actief",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " Mana per hit",
				"",
				ChatColor.GRAY + "Passive: " + ChatColor.GREEN + passive1.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GREEN + "✚ " + ChatColor.GRAY + passive1.info,
				"",
				ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om " + getName() + " te selecteren"
        }).hideAttributes().toItemStack(), click, slot);
		
	}

	public ItemBuilder getShopItem(Player player) {
		return null;
	}
	
	@Override
	public ItemBuilder getShopItem() {
		
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 6);
		LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), 6);
		
		return new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNjFkM2IyODdkZWYwZTNmZjdhMjYzNmMxNDI2Yjk1M2JhZjFlZThiNTJhNzBkZTc1NzIzY2JiMjJkZGU5ZSJ9fX0==").setName(ChatColor.AQUA.toString() + ChatColor.BOLD + this.getName()).addLoreLines(new String[] {
				ChatColor.GRAY + "Moeilijkheidsgraad: " + ChatColor.YELLOW + "••" + ChatColor.GRAY + "•",
				"",
				ChatColor.GRAY + "Ability: " + ChatColor.GREEN + ability.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((ability.damage * 0.26), 2) + " D" + ChatColor.DARK_GRAY + " ❚ " + ChatColor.GRAY + round((ability.damage * 0.19) + 0.03, 2) + " H",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.count + "s actief",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " Mana per hit",
				"",
				ChatColor.GRAY + "Passive: " + ChatColor.GREEN + passive1.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GREEN + "✚ " + ChatColor.GRAY + passive1.info,
		});
	}
	
	@Override
    public String[] getLevelLoreAbility(Player player, int level) {
		LobbyManager.Abilities ability;
		if (level == 1) {
			ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
		} else {
			ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level-1);
		}
		LobbyManager.Abilities nextAbility = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
        String price = LobbyManager.Prices.values()[level - 1].getStringPrice();
        String[] lore = {
        		"",
        		ChatColor.GRAY + "Steel de levens van jouw vijanden",
        		ChatColor.GRAY + "als ze binnen de 9 blokken radius",
        		ChatColor.GRAY + "van jouw aanval zitten. Tijdens de",
        		ChatColor.GRAY + "aanval verliezen zij " + round((ability.damage * 0.26), 2) + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + round((nextAbility.damage * 0.26), 2)) + ChatColor.GRAY + " HP",
        		ChatColor.GRAY + "per seconde krijg en krijg jij " + round((ability.damage * 0.19) + 0.03, 2) + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + round((nextAbility.damage * 0.19) + 0.03, 2)) + ChatColor.GRAY + " HP.",
        		ChatColor.GRAY + "Je aanval is " + ability.count + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextAbility.count) + ChatColor.GRAY + "s actief.",
        		"",
        		ChatColor.GRAY + "" + ability.mana + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextAbility.mana) + ChatColor.GRAY + " Mana per hit.",
        		"",
        		ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price,
        };
    		return lore;
    }
    
    @Override
    public String[] getLevelLorePassive(Player player, int level) {
		LobbyManager.Passives passive1;
		if (level == 1) {
			passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
		} else {
			passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level-1);
		}
		LobbyManager.Passives nextPassive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
        String price = LobbyManager.Prices.values()[level - 1].getStringPrice();
        String[] lore = {
        		"",
        		ChatColor.GRAY + "Krijg " + passive1.info.split(" ")[0] + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextPassive1.info.split(" ")[0]) + " ❤" + ChatColor.GRAY + " terug nadat je",
        		ChatColor.GRAY + "een kill hebt gemaakt.",
        		"",
        		ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price,
        };
    		return lore;
    }
	
    @Override
    public String[] getInfoLore(Player player) {
    		StatsManager playerStats = new StatsManager(getName(), player);
    		return playerStats.getInfoLore();
    }
	
	@Override
	public String[] getMasterLore(Player player) {
		LobbyManager.Abilities currentAbility = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 6);
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 7);
		return new String[] {
				"",
				ChatColor.GRAY + "Maak jouw favoriete",
				ChatColor.GRAY + "class nog sterker door er",
				ChatColor.GRAY + "een meester in te worden!",
				"",
				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "[" + ChatColor.GOLD + "✮" + ChatColor.GRAY + "] Master TAG in lobbies",
				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Gouden " + ChatColor.GOLD + "[" + getAfkorting() + "]" + ChatColor.GRAY + " TAG in-game",
				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Ability upgrade:",
				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + round((currentAbility.damage * 0.26), 2) + " D" + ChatColor.DARK_GRAY + " ❚ " + ChatColor.GRAY + round((currentAbility.damage * 0.19) + 0.03, 2) + " H" + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + round((ability.damage * 0.26), 2) + ChatColor.GRAY + " D" + ChatColor.DARK_GRAY + " ❚ " + ChatColor.GREEN + round((ability.damage * 0.19) + 0.03, 2) + ChatColor.GRAY + " H",
				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + currentAbility.count + "s" + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + ability.count + "s" + ChatColor.GRAY + " actief",
				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + currentAbility.mana + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + ability.mana + ChatColor.GRAY + " Mana per hit",
				"",
				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Passive upgrade: ",
				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + "3.5 ❤" + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + "4.3 ❤" + ChatColor.GRAY + " na een kill",
				"",
				ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik" + ChatColor.GRAY + " om deze upgrade te kopen"
		};
	}
	
	public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

	@Override
	public String[] getAbilityLore(int level) {
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
		return new String[] {
			"",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((ability.damage * 0.26), 2) + " D" + ChatColor.DARK_GRAY + " ❚ " + ChatColor.GRAY + round((ability.damage * 0.19) + 0.03, 2) + " H",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.count + "s actief",
			"",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " Mana per hit",
			"",
		};
	}

	@Override
	public String[] getPassiveLore(int level) {
		LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
		return new String[] {
			"",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GREEN + "✚ " + ChatColor.GRAY + passive1.info,
			"",
		};
	}
	
}
