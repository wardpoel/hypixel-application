package nl.dodocraft.game.knights.kits.content;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import nl.dodocraft.common.Rank;
import nl.dodocraft.common.server.ServerType;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.kits.KitTemplate;
import nl.dodocraft.game.knights.lobby.shop.LobbyManager;
import nl.dodocraft.game.knights.lobby.shop.StatsManager;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.menu.DodoMenu;
import nl.dodocraft.utils.menu.MenuClick;
import nl.thiemoboven.ncore.SimpleCore;

public class KitElektron implements KitTemplate {

    @Override
    public int getStartID() {
        return 4;
    }

    @Override
    public String getName() {
        return "Elektron";
    }

    @Override
    public void addMenu(DodoMenu menu, Player player, KitTemplate kit, int slot, MenuClick click, Loader plugin) {
    		String[] lores;
    		if (hasElektron(player)) {
    			lores = getElektronLoreLinesWhenUnlocked(player);
    		} else {
    			lores = getElektronLoreLinesWhenNotUnlocked(player);
    		}
    		
        menu.addMenuClick(new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTI2NTI1NWU4MmVlN2ZkMWFjM2MyYTNmZDNmODkyNjdlMjI4MmY4NjQ3NGNlOTZmZjdmZmYzYzhmZDFlODljIn19fQ==").setName((hasElektron(player) ? ChatColor.GREEN.toString() : ChatColor.RED.toString()) + ChatColor.BOLD + this.getName()).addLoreLines(lores).addLoreLines(new String[] {
				ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om " + getName() + " te selecteren"
        }).hideAttributes().toItemStack(), click, slot);
    }

    @Override
    public String getAfkorting() {
        return "ELK";
    }
    
    @Override
    public ItemBuilder getShopItem(Player player) {
    		String[] lores;
		if (hasElektron(player)) {
			lores = getElektronLoreLinesWhenUnlocked(player);
		} else {
			lores = getElektronLoreLinesWhenNotUnlocked(player);
		}
		return new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTI2NTI1NWU4MmVlN2ZkMWFjM2MyYTNmZDNmODkyNjdlMjI4MmY4NjQ3NGNlOTZmZjdmZmYzYzhmZDFlODljIn19fQ==").setName(ChatColor.AQUA.toString() + ChatColor.BOLD + this.getName()).addLoreLines(lores);
    }
    
    @Override
	public ItemBuilder getShopItem() {
		return new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTI2NTI1NWU4MmVlN2ZkMWFjM2MyYTNmZDNmODkyNjdlMjI4MmY4NjQ3NGNlOTZmZjdmZmYzYzhmZDFlODljIn19fQ==").setName(ChatColor.AQUA.toString() + ChatColor.BOLD + this.getName());
	}
    
    public String[] getElektronLoreLinesWhenNotUnlocked(Player player) {
    		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 6);
        LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), 6);
        String price = LobbyManager.ElektronUpgradePrices.values()[0].stringPrice;
    		return new String[] {
    				ChatColor.DARK_GRAY + "Special class",
    				"",
    				ChatColor.GRAY + "Ability: " + ChatColor.AQUA + ability.display,
    				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((((ability.damage * 10) / 9.63 + 4.2) / 1.35) / 3, 2) + " HP damage per laser",
    				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " blocks range",
    				ChatColor.GRAY + "Ability & Shift: " + ChatColor.AQUA + "Laser Scan",
    				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((ability.damage * 10) / 9.63 + 3.3, 2) + " HP damage per doelwit",
    				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.count + " blocks range",
    				"",
    				ChatColor.GRAY + "Passive: " + ChatColor.AQUA + passive1.display,
    				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + passive1.info + passive1.info2,
    				"",
    				ChatColor.DARK_GRAY + "Alle upgrades hierboven zijn",
    				ChatColor.DARK_GRAY + "level 6. Je ontgrendelt hem dus",
    				ChatColor.DARK_GRAY + "met andere waardes.",
    				"",
    				ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price
    		};
    }
    
    public String[] getElektronLoreLinesWhenUnlocked(Player player) {
    		String abilityString = this.getName().toUpperCase() + "_ABILITY";
        String passive1String = this.getName().toUpperCase() + "_PASSIVE1";
        int levelAbility;
        int levelPassive1;
        if(SimpleCore.getInstance().getInternalAPI().getServer().getPrefix().getServerType() == ServerType.LOBBY) {
        		levelAbility = Loader.getInstance().getLobbyManager().getFixedLevel(player, abilityString);
           	levelPassive1 = Loader.getInstance().getLobbyManager().getFixedLevel(player, passive1String);
        } else {
        		levelAbility = Loader.getInstance().getKitManager().getFixedLevel(player, abilityString);
           	levelPassive1 = Loader.getInstance().getKitManager().getFixedLevel(player, passive1String);
        }
        
    		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), levelAbility);
        LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), levelPassive1);
		return new String[] {
				ChatColor.GRAY + "Moeilijkheidsgraad: " + ChatColor.RED + "•••",
				"",
				ChatColor.GRAY + "Ability: " + ChatColor.GREEN + "Laser Eyes",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((((ability.damage * 10) / 9.63 + 4.2) / 1.35) / 3, 2) + " HP damage per laser",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " blocks range",
				ChatColor.GRAY + "Ability & Shift: " + ChatColor.GREEN + "Laser Scan",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((ability.damage * 10) / 9.63 + 3.3, 2) + " HP damage per doelwit",
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.count + " blocks range",
				"",
				ChatColor.GRAY + "Passive: " + ChatColor.GREEN + passive1.display,
				ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + passive1.info + passive1.info2
		};
    }
    
    @Override
    public String[] getMasterLore(Player player) {
    		LobbyManager.Abilities currentAbility = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 6);
        LobbyManager.Passives currentPassive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), 6);
    		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), 7);
        LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), 7);
        String price = LobbyManager.ElektronUpgradePrices.values()[6].stringPrice;
    		return new String[] {
    				"",
    				ChatColor.GRAY + "Maak jouw favoriete",
    				ChatColor.GRAY + "class nog sterker door er",
    				ChatColor.GRAY + "een meester in te worden!",
    				"",
    				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "[" + ChatColor.GOLD + "✮" + ChatColor.GRAY + "] Master TAG in lobbies",
    				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Gouden " + ChatColor.GOLD + "[" + getAfkorting() + "]" + ChatColor.GRAY + " TAG in-game",
    				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Ability upgrade:",
    				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + round((((currentAbility.damage * 10) / 9.63 + 4.2) / 1.35) / 3, 2) + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + round((((ability.damage * 10) / 9.63 + 4.2) / 1.35) / 3, 2) + ChatColor.GRAY + " HP damage per laser",
    				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + currentAbility.mana + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + ability.mana + ChatColor.GRAY + " blocks range",
    				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Ability & Shift upgrade: ",
    				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + round((currentAbility.damage * 10) / 9.63 + 3.3, 2) + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + round((ability.damage * 10) / 9.63 + 3.3, 2) + ChatColor.GRAY +" HP damage per doelwit",
    				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + currentAbility.count + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + ability.count + ChatColor.GRAY + " blocks range",
    				"",
    				ChatColor.GREEN + "✚ " + ChatColor.GRAY + "Passive upgrade: ",
    				ChatColor.DARK_GRAY + "   ▪ " + ChatColor.GRAY + currentPassive1.info + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + passive1.info + ChatColor.GRAY + passive1.info2,
    				"",
    				ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price,
    				"",
    				ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik" + ChatColor.GRAY + " om deze upgrade te kopen"
    		};
    }
    
    @Override
    public String[] getLevelLoreAbility(Player player, int level) {
    		LobbyManager.Abilities ability;
    		if (level == 1) {
    			ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
    		} else {
    			ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level-1);
    		}
        LobbyManager.Abilities nextAbility = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
        String price = LobbyManager.ElektronUpgradePrices.values()[level - 1].stringPrice;
        String[] lore = {
        		"",
        		ChatColor.GRAY + "Ability: " + ChatColor.GREEN.toString() + ChatColor.BOLD + "Laser Eyes",
        		ChatColor.GRAY + "Schiet kort achter elkaar 3",
        		ChatColor.GRAY + "lasers weg die " + ability.mana + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextAbility.mana) + ChatColor.GRAY + " blocks",
        		ChatColor.GRAY + "ver vliegen. Elke Laser die iemand raakt",
        		ChatColor.GRAY + "doet " + round((((ability.damage * 10) / 9.63 + 4.2) / 1.35) / 3, 2) + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + round((((nextAbility.damage * 10) / 9.63 + 4.2) / 1.35) / 3, 2)) + ChatColor.GRAY + " HP damage.",
        		"",
        		ChatColor.GRAY + "Ability & Shift: " + ChatColor.GREEN.toString() + ChatColor.BOLD + "Laser Scan",
        		ChatColor.GRAY + "Scan de grond om jou heen in",
        		ChatColor.GRAY + "een radius van " + ability.count + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextAbility.count) + ChatColor.GRAY + " blocks. Als",
        		ChatColor.GRAY + "de Laser iemand vindt krijgt deze",
        		ChatColor.GRAY + "persoon " + round((ability.damage * 10) / 9.63 + 3.3, 2) + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + round((nextAbility.damage * 10) / 9.63 + 3.3, 2)) + ChatColor.GRAY + " HP damage",
        		"",
        		ChatColor.GRAY + "" + LobbyManager.Abilities.manaElektron + " Mana per hit.",
        		"",
        		ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price,
        };
    		return lore;
    }
    
    @Override
    public String[] getLevelLorePassive(Player player, int level) {
		LobbyManager.Passives passive1;
		if (level == 1) {
			passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
		} else {
			passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level-1);
		}
		
		int abilityLevel = 1;
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", getName().toUpperCase() + "_ABILITY", Integer.class) != null) {
			abilityLevel = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", getName().toUpperCase() + "_ABILITY", Integer.class);
		}
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), abilityLevel);
		
		LobbyManager.Passives nextPassive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
        String price = LobbyManager.ElektronUpgradePrices.values()[level - 1].stringPrice;
        String[] lore = {
        		"",
        		ChatColor.GRAY + "Maak " + passive1.info.replaceAll("%", "") + (level == 1 ? "" : ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextPassive1.info.replaceAll("%", "")) + ChatColor.GRAY + "% kans op Laser Up, er",
        		ChatColor.GRAY + "schiet dan een Laser omhoog vanonder",
        		ChatColor.GRAY + "jouw vijand die daar " + round((((ability.damage * 10) / 9.63 + 4.2) / 1.35) / 3, 2) + " HP op doet.",
        		"",
        		ChatColor.DARK_GRAY + "De damage van de Laser Up wordt",
        		ChatColor.DARK_GRAY + "bepaald door het level Lasers.",
        		"",
        		ChatColor.GRAY + "Kost: " + ChatColor.GOLD + price,
        };
    		return lore;
    }
    
    @Override
    public ItemBuilder getSkullItem() {
    		return new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTI2NTI1NWU4MmVlN2ZkMWFjM2MyYTNmZDNmODkyNjdlMjI4MmY4NjQ3NGNlOTZmZjdmZmYzYzhmZDFlODljIn19fQ==").setName(ChatColor.AQUA.toString() + ChatColor.BOLD + this.getName());
    }
    
    @Override
    public String[] getInfoLore(Player player) {
    		StatsManager playerStats = new StatsManager(getName(), player);
    		return playerStats.getInfoLore();
    }
    
    private boolean hasElektron(Player player) {
    		int unlocked = 0; //0 -> false; 1 -> true
    		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", Integer.class) != null) {
    			unlocked = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", Integer.class);
    		} else {
    			DodoCore.getInstance().setPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", 0);
    		}
    		if (unlocked == 1 || DodoCore.getInstance().getRank(player.getUniqueId()).getId() >= Rank.ADMIN.getId()) {
    			return true;
    		}
    		return false;
    }
    
    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
	public String[] getAbilityLore(int level) {
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(this.getName().toUpperCase(), level);
		return new String[] {
			"",
			ChatColor.GRAY + "Ability: " + ChatColor.GREEN + "Laser Eyes",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((((ability.damage * 10) / 9.63 + 4.2) / 1.35) / 3, 2) + " HP damage per laser",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.mana + " blocks range",
			ChatColor.GRAY + "Ability & Shift: " + ChatColor.GREEN + "Laser Scan",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + round((ability.damage * 10) / 9.63 + 3.3, 2) + " HP damage per doelwit",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + ability.count + " blocks range",
			"",
		};
	}

    @Override
	public String[] getPassiveLore(int level) {
		LobbyManager.Passives passive1 = LobbyManager.Passives.getPassive(this.getName().toUpperCase(), level);
		return new String[] {
			"",
			ChatColor.DARK_GRAY + "  ▪ " + ChatColor.GRAY + passive1.info + passive1.info2,
			"",
		};
	}
    
}