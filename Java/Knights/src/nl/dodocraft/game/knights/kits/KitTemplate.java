package nl.dodocraft.game.knights.kits;

import org.bukkit.entity.Player;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.menu.DodoMenu;
import nl.dodocraft.utils.menu.MenuClick;

public interface KitTemplate {

	int getStartID();
	String getName();
	String getAfkorting();
	void addMenu(DodoMenu menu, Player player, KitTemplate kit, int slot, MenuClick click, Loader plugin);
	ItemBuilder getShopItem();
	ItemBuilder getShopItem(Player player);
	String[] getLevelLoreAbility(Player player, int level);
	String[] getLevelLorePassive(Player player, int level);
	String[] getMasterLore(Player player);
	String[] getInfoLore(Player player);
	ItemBuilder getSkullItem();
	String[] getAbilityLore(int level);
	String[] getPassiveLore(int level);
	
}
