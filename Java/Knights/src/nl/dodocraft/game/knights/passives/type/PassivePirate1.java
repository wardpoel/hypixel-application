package nl.dodocraft.game.knights.passives.type;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.passives.GamePasive;

public class PassivePirate1 implements GamePasive {

    private enum AbilityStats {
        LVL1("4%DAMAGE_RESISTANCE%INCREASE_DAMAGE%" + (8 * 20) + "%" + (6 * 20)),
        LVL2("5%DAMAGE_RESISTANCE%INCREASE_DAMAGE%" + (8 * 20) + "%" + (6 * 20)),
        LVL3("6%DAMAGE_RESISTANCE%INCREASE_DAMAGE%" + (8 * 20) + "%" + (6 * 20)),
        LVL4("7%DAMAGE_RESISTANCE%INCREASE_DAMAGE%" + (8 * 20) + "%" + (6 * 20)),
        LVL5("8%DAMAGE_RESISTANCE%INCREASE_DAMAGE%" + (8 * 20) + "%" + (6 * 20)),
        LVL6("9%DAMAGE_RESISTANCE%INCREASE_DAMAGE%" + (8 * 20) + "%" + (6 * 20)),
        LVL7("9%DAMAGE_RESISTANCE%INCREASE_DAMAGE%" + (8 * 20) + "%" + (6 * 20));//master

        public final String args;

        AbilityStats(String args) {
            this.args = args;
        }

        public static AbilityStats byLevel(int level) {
            for (AbilityStats s : values()) {
                if (s.toString().replace("LVL", "").equalsIgnoreCase(String.valueOf(level))) {
                    return s;
                }
            }
            return null;
        }
    }

    private Loader plugin;
    private Player player;
    private int level;
    private String args;

    public PassivePirate1(Loader plugin, Player player, int level) {
        this.plugin = plugin;
        this.player = player;
        this.level = level;

        AbilityStats stats = AbilityStats.byLevel(this.level);
        if (stats != null) {
            this.args = stats.args;
        }
    }

    @Override
    public String getArgs() {
        return this.args;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public String getName() {
        return "Protection";
    }

    long lastUsed = 0;

    @Override
    public void onActivate(Player player, Object[] optionalArguments, Event event) {
        if (event instanceof EntityDamageEvent) {
            EntityDamageEvent damageEvent = EntityDamageEvent.class.cast(event);

            if (!(damageEvent.getEntity() instanceof Player)) return;
            if (!(damageEvent.getEntity().getUniqueId().equals(this.player.getUniqueId()))) return;

            Player entity = Player.class.cast(damageEvent.getEntity());

            boolean isEntity = this.plugin.getKitManager().selectedKit.get(entity.getName()).getName().equalsIgnoreCase("Pirate");

            if (!(isEntity))
                return;

            double healthRemaining = player.getHealth() - ((EntityDamageEvent) event).getFinalDamage();

            if (healthRemaining > 0.0 && healthRemaining <= Integer.parseInt(this.getArgs().split("%")[0])) {
                if (lastUsed > System.currentTimeMillis())
                    return;

                //Resistance
                PotionEffectType type = PotionEffectType.getByName(this.getArgs().split("%")[1]);
                int ticks = Integer.valueOf(this.getArgs().split("%")[3]);
                int amplifier = this.level == 7 ? 3 : 2;

                //Speed
                PotionEffectType type2 = PotionEffectType.getByName(this.getArgs().split("%")[2]);
                int ticks2 = Integer.valueOf(this.getArgs().split("%")[4]);
                int amplifier2 = this.level == 7 ? 3 : 2;

                entity.addPotionEffect(new PotionEffect(type, ticks, amplifier, true, false), true);
                entity.addPotionEffect(new PotionEffect(type2, ticks2, amplifier2, true, false), true);

                lastUsed = System.currentTimeMillis() + (16 * 1000);
            }
        }
    }
}
