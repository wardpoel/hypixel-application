package nl.dodocraft.game.knights.passives.type;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.passives.GamePasive;

public class PassiveMender1 implements GamePasive {

    private enum AbilityStats {
        LVL1("1%"),
        LVL2("1.5%"),
        LVL3("2%"),
        LVL4("2.5%"),
        LVL5("3%"),
        LVL6("3.5%"),
        LVL7("4.3%");//master

        public final String args;

        AbilityStats(String args) {
            this.args = args;
        }

        /*public static AbilityStats byLevel(int level) {
            for (AbilityStats s : values()) {
                if (s.toString().replace("LVL", "").equalsIgnoreCase(String.valueOf(level))) {
                    return s;
                }
            }
            return null;
        }*/
    }

    private Loader plugin;
    private int level;
    private String args;

    public PassiveMender1(Loader plugin, Player player, int level) {
        this.plugin = plugin;
        this.level = level;
        this.args = AbilityStats.values()[level - 1].args;
    }

    @Override
    public String getArgs() {
        return this.args;
    }

    @Override
    public int getId() {
        return 3;
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public String getName() {
        return "Recover";
    }

    @Override
    public void onActivate(Player player, Object[] optionalArguments, Event event) {
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent damageEvent = EntityDamageByEntityEvent.class.cast(event);
            if (!(damageEvent.getEntity() instanceof Player)) return;
            if (!(damageEvent.getDamager() instanceof Player)) return;

            Player entity = Player.class.cast(damageEvent.getEntity());
            Player damager = Player.class.cast(damageEvent.getDamager());

            entity.getHealth();
            damageEvent.getDamage();
            
            boolean isDamager = this.plugin.getKitManager().selectedKit.get(damager.getName()).getName().equalsIgnoreCase("Mender");
            if (!isDamager)
                return;

            if (entity.getHealth() - damageEvent.getDamage() <= 0.0) { //Kill
            		double heal = Double.parseDouble(this.getArgs().split("%")[0]) * 2;
            		if (damager.getHealth() + heal <= 20.0 && damager.getHealth() + heal >= 0.0) {
            			damager.setHealth(damager.getHealth() + heal);
            		}
            }           
        }
    }
}
