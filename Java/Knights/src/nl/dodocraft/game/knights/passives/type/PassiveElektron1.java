package nl.dodocraft.game.knights.passives.type;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.Beam;
import nl.dodocraft.game.knights.passives.GamePasive;

public class PassiveElektron1 implements GamePasive {

	private enum AbilityStats {
        LVL1("5%"),
        LVL2("6%"),
        LVL3("7%"),
        LVL4("8%"),
        LVL5("9%"),
        LVL6("10%"),
        LVL7("12%");//master

        public final String args;

        AbilityStats(String args) {
            this.args = args;
        }

        public static AbilityStats byLevel(int level) {
            for (AbilityStats s : values()) {
                if (s.toString().replace("LVL", "").equalsIgnoreCase(String.valueOf(level))) {
                    return s;
                }
            }
            return null;
        }
    }
	
	private Loader plugin;
    private int level;
    private String args;

    public PassiveElektron1(Loader plugin, Player player, int level) {
        this.plugin = plugin;
        this.level = level;

        AbilityStats stats = AbilityStats.byLevel(this.level);
        if (stats != null) {
            this.args = stats.args;
        }
    }

	
	@Override
	public String getArgs() {
		return this.args;
	}

	@Override
	public int getId() {
		return 4;
	}

	@Override
	public int getLevel() {
		return this.level;
	}

	@Override
	public String getName() {
		return "Laser Up";
	}

	@Override
	public void onActivate(Player player, Object[] optionalArguments, Event event) {
		if (event instanceof EntityDamageByEntityEvent) {
			if (((EntityDamageByEntityEvent) event).getEntity() instanceof Player) {
				Player damagedPlayer = (Player) ((EntityDamageByEntityEvent) event).getEntity();
				if (new Random().nextInt(100) <= Integer.parseInt(this.getArgs().split("%")[0])) {
					damagedPlayer.getWorld().playSound(damagedPlayer.getLocation(), Sound.ENTITY_ENDEREYE_LAUNCH, 1, 1);
					for (double i = -0.2; i <= 0.2; i = i + 0.2) {
						Location startPoint = damagedPlayer.getLocation();
						if (i != 0) {
							startPoint.add(-i, 0, i);
						}
						Location endPoint = startPoint.clone().add(0, 0.7, 0);

						Vector vec = new Vector(0, 1, 0);
						
						ArmorStand start = player.getWorld().spawn(startPoint, ArmorStand.class);
						start.setVisible(false);
						ArmorStand target = player.getWorld().spawn(endPoint, ArmorStand.class);
						target.setVisible(false);
						
						start.setGravity(false);
						target.setGravity(false);
						
						Beam laser = new Beam(start.getLocation());
						laser.setTarget(target);
						Bukkit.getOnlinePlayers().forEach(laser::spawn);
						
						if (i == 0) { //Only damage player once
							double damage = plugin.getKitManager().selectedAbility.get(player.getName()).getStrength();
							damagedPlayer.damage(damage / 3);
						}
						
						new BukkitRunnable() {
							@Override
							public void run() {
								if (start.getLocation().distance(startPoint) >= 10 || start.getLocation().getBlock().getType().isSolid()) {
									this.cancel();
									Bukkit.getOnlinePlayers().forEach(laser::despawn);
									start.remove();
									target.remove();
									return;
								}
								Location loc = start.getLocation().clone().add(vec);
								Location loc2 = target.getLocation().clone().add(vec);

								start.teleport(loc, PlayerTeleportEvent.TeleportCause.PLUGIN);
								target.teleport(loc2, PlayerTeleportEvent.TeleportCause.PLUGIN);
								
								laser.startLocation(start.getLocation());
								Bukkit.getOnlinePlayers().forEach(laser::despawn);
								Bukkit.getOnlinePlayers().forEach(laser::spawn);
							}
						}.runTaskTimer(plugin, 0, 0);
					}
					
				}
			}
		}
	}
}
