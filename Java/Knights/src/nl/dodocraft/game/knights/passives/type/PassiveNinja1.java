package nl.dodocraft.game.knights.passives.type;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.passives.GamePasive;

public class PassiveNinja1 implements GamePasive {

    private enum AbilityStats {
        LVL1("SPEED%" + (6 * 20) + "%0"),
        LVL2("SPEED%" + (7 * 20) + "%0"),
        LVL3("SPEED%" + (8 * 20) + "%0"),
        LVL4("SPEED%" + (6 * 20) + "%1"),
        LVL5("SPEED%" + (7 * 20) + "%1"),
        LVL6("SPEED%" + (8 * 20) + "%1"),
        LVL7("SPEED%" + (8 * 20) + "%2");//master

        public final String args;

        AbilityStats(String args) {
            this.args = args;
        }

        public static AbilityStats byLevel(int level) {
            for (AbilityStats s : values()) {
                if (s.toString().replace("LVL", "").equalsIgnoreCase(String.valueOf(level))) {
                    return s;
                }
            }
            return null;
        }
    }

    private Loader plugin;
    private int level;
    private String args;

    public PassiveNinja1(Loader plugin, Player player, int level) {
        this.plugin = plugin;
        this.level = level;

        AbilityStats stats = AbilityStats.byLevel(this.level);
        if (stats != null) {
            this.args = stats.args;
        }
    }

    @Override
    public String getArgs() {
        return this.args;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public String getName() {
        return "Combo Speed";
    }

    Map<String, Integer> hits = new HashMap<>();

    @Override
    public void onActivate(Player player, Object[] optionalArguments, Event event) {
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent damageEvent = EntityDamageByEntityEvent.class.cast(event);
            if (!(damageEvent.getEntity() instanceof Player)) return;
            if (!(damageEvent.getDamager() instanceof Player)) return;

            Player entity = Player.class.cast(damageEvent.getEntity());
            Player damager = Player.class.cast(damageEvent.getDamager());

            boolean isDamager = this.plugin.getKitManager().selectedKit.get(damager.getName()).getName().equalsIgnoreCase("Ninja");

            if (isDamager) {
                hits.putIfAbsent(damager.getName(), 0);

                hits.put(damager.getName(), hits.get(damager.getName()) + 1);

                if (hits.get(damager.getName()) >= 3) {
                    hits.put(damager.getName(), 0);

                    GamePasive pasive = this.plugin.getKitManager().selectedPassive1.get(damager.getName());

                    PotionEffectType type = PotionEffectType.getByName(pasive.getArgs().split("%")[0]);
                    int ticks = Integer.valueOf(pasive.getArgs().split("%")[1]);
                    int amplifier = Integer.valueOf(pasive.getArgs().split("%")[2]);

                    damager.addPotionEffect(new PotionEffect(type, ticks, amplifier, true, false), true);
                }
            }
            if (hits.containsKey(entity.getName())) {
                hits.put(entity.getName(), 0);
            }
        }
    }
}
