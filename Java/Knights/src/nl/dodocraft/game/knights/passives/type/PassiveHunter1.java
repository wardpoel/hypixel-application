package nl.dodocraft.game.knights.passives.type;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.passives.GamePasive;

import java.util.Random;

public class PassiveHunter1 implements GamePasive {

    private enum AbilityStats {
        LVL1("15%"),
        LVL2("20%"),
        LVL3("25%"),
        LVL4("30%"),
        LVL5("40%"),
        LVL6("50%"),
        LVL7("65%");//master

        public final String args;

        AbilityStats(String args) {
            this.args = args;
        }

        public static AbilityStats byLevel(int level) {
            for (AbilityStats s : values()) {
                if (s.toString().replace("LVL", "").equalsIgnoreCase(String.valueOf(level))) {
                    return s;
                }
            }
            return null;
        }
    }

    private Loader plugin;
    private int level;
    private String args;

    public PassiveHunter1(Loader plugin, Player player, int level) {
        this.plugin = plugin;
        this.level = level;

        AbilityStats stats = AbilityStats.byLevel(this.level);
        if (stats != null) {
            this.args = stats.args;
        }
    }

    @Override
    public String getArgs() {
        return this.args;
    }

    @Override
    public int getId() {
        return 2;
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public String getName() {
        return "Triple Shot";
    }

    @Override
    public void onActivate(Player player, Object[] optionalArguments, Event event) {
        if (event instanceof EntityShootBowEvent) {
            EntityShootBowEvent shootBowEvent = EntityShootBowEvent.class.cast(event);
            if (!(shootBowEvent.getEntity() instanceof Player)) return;
            Player entity = Player.class.cast(shootBowEvent.getEntity());
            Arrow a = (Arrow) shootBowEvent.getProjectile();
            boolean isentity = this.plugin.getKitManager().selectedKit.get(entity.getName()).getName().equalsIgnoreCase("Hunter");
            if (!isentity)
                return;

            if (new Random().nextInt(100) >= Integer.parseInt(this.getArgs().split("%")[0]))
                return;

            for (int i = 0; i < 2; i++) {
            		float addYaw;
            		if (i == 0) {
            			addYaw = -8;
            		} else {
            			addYaw = +8;
            		}
            		player.playSound(player.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1F, 1F);
            		
            		Location loc = player.getLocation().clone();
                loc.setYaw(loc.getYaw() + addYaw);
                Vector v = loc.getDirection().normalize();
                v.multiply(a.getVelocity().length());
            		
                Arrow arrow = player.launchProjectile(Arrow.class);
                arrow.setVelocity(v);
                
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (arrow.isDead() || arrow.isOnGround()) {
                            arrow.remove();
                            cancel();
                        }
                    }
                }.runTaskTimer(plugin, 0, 5);
            }
        }
    }
}
