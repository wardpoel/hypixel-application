package nl.dodocraft.game.knights.passives;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public interface GamePasive {

	String getArgs();
	int getId();
	int getLevel();
	String getName();
	void onActivate(Player player, Object[] optionalArguments, Event event);
	
}
