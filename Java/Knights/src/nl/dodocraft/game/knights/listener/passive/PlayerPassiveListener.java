package nl.dodocraft.game.knights.listener.passive;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.utils.game.state.GameState;

/**
 * Created by Thiemo on 12-4-2017.
 * No part of this publication may be reproduced, distributed, or transmitted in any form or by any means.
 * Copyright © 2017 by Thiemo
 */
public class PlayerPassiveListener implements Listener {

    private Loader plugin;

    public PlayerPassiveListener(Loader plugin) {
        this.plugin = plugin;
    }

    public enum PassiveID {
        NINJA(0),
        BERSERKER(1),
        QUAKER(2),
        SHAMAN(3),
        PIRATE(4),
        GOLOU(5),
        ARCHER(6),
        ;

        private final int id;

        PassiveID(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }

//    @EventHandler (priority = EventPriority.HIGH)
//    public void onDamage(EntityDamageEvent e) {
//        if (GameState.getState() != GameState.IN_GAME) {
//            return;
//        }
//        if (!(e.getEntity() instanceof Player)) return;
//
//        Player damager = (Player) e.getEntity();
//
//        if (this.plugin.getKitManager().selectedPassive1.containsKey(damager.getName())) {
//            if (damager.isValid()) {
//                this.plugin.getKitManager().selectedPassive1.get(damager.getName()).onActivate(damager, null, e);
//            }
//        }
//    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onDamage(EntityShootBowEvent e) {
        if (GameState.getState() != GameState.IN_GAME) {
            return;
        }
        if (!(e.getEntity() instanceof Player)) return;

        Player damager = (Player) e.getEntity();

        if (this.plugin.getKitManager().selectedPassive1.containsKey(damager.getName())) {
            if (damager.isValid()) {
                this.plugin.getKitManager().selectedPassive1.get(damager.getName()).onActivate(damager, null, e);
            }
        }
    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onDamage(EntityDamageByEntityEvent e) {
        if (GameState.getState() != GameState.IN_GAME) {
            return;
        }

        if (plugin.getFightListener().getSpawnProtectionList().contains(e.getEntity().getUniqueId())) return;

        if (e.getDamager() instanceof Arrow) {
            if (!(((Arrow) e.getDamager()).getShooter() instanceof Player)) return;
            Player damager = (Player) ((Arrow) e.getDamager()).getShooter();
            if (plugin.getTeamManager().areTeammates(e.getEntity().getUniqueId(), damager.getUniqueId())) return;
            if (this.plugin.getKitManager().selectedPassive1.containsKey(damager.getName())) {
                if (damager.isValid()) {
                    this.plugin.getKitManager().selectedPassive1.get(damager.getName()).onActivate(damager, new Object[] {e.getDamager()}, e);
                }
            }
            return;
        }
        if (!(e.getDamager() instanceof Player)) return;
        if (!(e.getEntity() instanceof Player)) return;

        Player damager = (Player) e.getDamager();
        if (plugin.getTeamManager().areTeammates(e.getEntity().getUniqueId(), damager.getUniqueId())) return;

        if (this.plugin.getKitManager().selectedPassive1.containsKey(damager.getName())) {
            if (damager.isValid()) {
                this.plugin.getKitManager().selectedPassive1.get(damager.getName()).onActivate(damager, null, e);
            }
        }
    }
}
