package nl.dodocraft.game.knights.listener;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupArrowEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.game.knights.fight.PlayerDamage;
import nl.dodocraft.game.knights.objects.Gate;
import nl.dodocraft.game.knights.objects.Nexus;
import nl.dodocraft.game.knights.objects.Tower;
import nl.dodocraft.game.knights.playerdata.PlayerData;
import nl.dodocraft.game.knights.quests.QuestFormat;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.game.knights.utils.DefaultFontInfo;
import nl.dodocraft.utils.chat.messages.MessageController;
import nl.dodocraft.utils.game.state.GameState;
import nl.dodocraft.utils.particles.ParticleEffect;
import nl.dodocraft.utils.scoreboard.DodoBoard;
import nl.dodocraft.utils.scoreboard.controller.ScoreboardController;
import nl.thiemoboven.ncore.SimpleCore;

public class FightListener implements Listener {

	private Loader plugin;
	
	private List<UUID> spawnProtection = new ArrayList<>();
	public Map<String, PlayerDamage> playerDamage = new HashMap<>();
	
	public FightListener(Loader plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerFight(EntityDamageByEntityEvent event) {
		if (GameState.getState() != GameState.IN_GAME && GameState.getState() != GameState.IN_PREGAME) {
			event.setCancelled(true);
			return;
		}
		
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if (spawnProtection.contains(player.getUniqueId())) {
				event.setCancelled(true);
				if (event.getDamager() instanceof Player) {
					Player damager = (Player) event.getDamager();
					damager.playSound(damager.getLocation(), Sound.ITEM_TOTEM_USE, 1F, 5F);
				}
				return;
			}
			
			if (event.getDamager() instanceof Player) {
				Player damager = (Player) event.getDamager();
				
				if (damager.getInventory().getItemInMainHand().getType() == plugin.getItemManager().getSword(player).getMaterial()) {
					int attackDamage = plugin.getItemManager().getSword(player).getAttackDamage();
					int normalAttackDamage = plugin.getItemManager().getSword(player).getNormalAttackDamage();
					event.setDamage(event.getDamage() * ((double)attackDamage) / ((double)normalAttackDamage));
				}

				if (player.isValid() && damager.isValid()) {
					if (plugin.getTeamManager().areTeammates(player.getUniqueId(), damager.getUniqueId())) {
						event.setCancelled(true);
					} else {
						//Add mana
						int mana = 0;
						if (plugin.getKitManager().selectedAbility.containsKey(damager.getName())) {
							mana = plugin.getKitManager().selectedAbility.get(damager.getName()).getManaByHit();
							if (plugin.getKitManager().selectedKit.get(damager.getName()).getName().equalsIgnoreCase("HUNTER")) {
								mana = mana / 3;
							}
						}
						plugin.getPlayerDataManager().addMana(damager, mana);
						
						if (spawnProtection.contains(damager.getUniqueId())) {
							spawnProtection.remove(damager.getUniqueId());
						}
						
						plugin.getPlayerDataManager().getPlayerData(damager).addKitDamage(event.getDamage());
						
						PlayerDamage d;
                        if (this.playerDamage.containsKey(player.getName())) {
                            d = this.playerDamage.get(player.getName());
                        } else {
                            d = new PlayerDamage(player);
                            this.playerDamage.put(player.getName(), d);
                        }

                        if (d != null) {
                            d.addPlayerDamage(damager, event.getDamage());
                            d.setLastDamageTaken(System.currentTimeMillis());
                        } else {
                            throw new UnsupportedOperationException("Player damage could not be registered, invalid damager or player");
                        }
					}
				}
			} else if (event.getDamager() instanceof Projectile && ((Projectile) event.getDamager()).getShooter() instanceof Player) {
				if (event.getDamager() instanceof Arrow) {
					event.setDamage(event.getDamage() * 0.65);
				}
				Player damager = ((Player) ((Projectile) event.getDamager()).getShooter());
				if (plugin.getTeamManager().areTeammates(player.getUniqueId(), damager.getUniqueId())) { //teammates
					event.setCancelled(true);				
				} else { //geen teammates
					//add mana
					int mana = 0;
					if (plugin.getKitManager().selectedAbility.containsKey(damager.getName())) {
						mana = plugin.getKitManager().selectedAbility.get(damager.getName()).getManaByHit();
					}
					plugin.getPlayerDataManager().addMana(damager, mana);
					
					plugin.getPlayerDataManager().getPlayerData(damager).addKitDamage(event.getDamage());
					
					PlayerDamage d;
					if (this.playerDamage.containsKey(player.getName())) {
						d = this.playerDamage.get(player.getName());
					} else {
						d = new PlayerDamage(player);
						this.playerDamage.put(player.getName(), d);
					}

					if (d != null) {
						d.addPlayerDamage(damager, event.getDamage());
						d.setLastDamageTaken(System.currentTimeMillis());

						for (Nexus nexus : plugin.getObjectsManager().getNexuses().values()) {
							if (nexus.getTeam() == plugin.getTeamManager().getPlayerTeam(player.getUniqueId())) {
								if (nexus.isDead()) {
									player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 3 * 20, 1, true, false), true);
								}
							}
						}
					} else {
						throw new UnsupportedOperationException("Player damage could not be registered, invalid damager or player");
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onFallDamage(EntityDamageEvent event) {
		if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
			event.setDamage(event.getDamage() / 2.0);
		}
	}
	
	@EventHandler
    public void onRegen(EntityRegainHealthEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (plugin.getCombotManager().isTagged(player)) {
            		return;
            }
            if (e.getRegainReason() == EntityRegainHealthEvent.RegainReason.REGEN || e.getRegainReason() == EntityRegainHealthEvent.RegainReason.SATIATED || e.getRegainReason() == EntityRegainHealthEvent.RegainReason.EATING) {
                e.setAmount(e.getAmount() * 3);
                plugin.getPlayerDataManager().getPlayerData(player).addHealing(e.getAmount());
            }
        }
    }
	
	@EventHandler
	public void onPlayerDie(PlayerDeathEvent event) {
		event.setDeathMessage(null);
		
		onKill(event.getEntity());
		
		if (event.getEntity() != null && event.getEntity().getInventory() != null) {
			event.getEntity().getInventory().clear();
		}
		event.getDrops().clear();
	}
	
	private void onKill(Player player) {
		if (player != null) {
			if (player.getGameMode() != GameMode.SPECTATOR) {
				plugin.getPlayerDataManager().getPlayerData(player).addDeath();
				boolean elimination = false;
				Team playerTeam = null;
				if (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null) {
					playerTeam = plugin.getTeamManager().getPlayerTeam(player.getUniqueId());
					if (!plugin.getObjectsManager().getNexuses().get(playerTeam).isDead()) { // Nexus is nog niet dood, dus player kan respawnen
						player.setMetadata("gameRespawn", new FixedMetadataValue(plugin, this));
						
						boolean nexusAlive = true;
		                if (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null) {
		                    if (plugin.getObjectsManager().getNexuses().get(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())) != null) {
		                        if (plugin.getObjectsManager().getNexuses().get(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())).isDead()) {
		                            nexusAlive = false;
		                        }
		                    }
		                }
						
		                if (plugin.getGameType() == GameType.MINI) {
		                		if (plugin.getObjectsManager().getGates().get(playerTeam).get(0).isAlive()) {
		                			Gate gate = plugin.getObjectsManager().getGates().get(playerTeam).get(0);
		                			double damage = 12;
		                			if (gate.getHealth() - damage < 50 && gate.getHealth() > 50) {
		                				damage = gate.getHealth() - 50;
		                			} else if (gate.getHealth() - damage < 50){
		                				damage = 0;
		                			}
		                			gate.damage(damage);
		                			gate.updateHoloGram();
		                			Loader.getInstance().getScoreBoard().updateTeamObject(gate.getTeam());
		                		} else if (nexusAlive) {
		                			Nexus nexus = plugin.getObjectsManager().getNexuses().get(playerTeam);
		                			double damage = 12;
		                			if (nexus.getHealth() - damage < 50 && nexus.getHealth() > 50) {
		                				damage = nexus.getHealth() - 50;
		                			} else if (nexus.getHealth() - damage < 50){
		                				damage = 0;
		                			}
		                			nexus.damage(damage);
		                			nexus.updateHoloGram();
		                			Loader.getInstance().getScoreBoard().updateTeamObject(nexus.getTeam());
		                		}
		                }
					} else { //ELIMINATIE
						elimination = true;
						Nexus nexus = plugin.getObjectsManager().getNexuses().get(playerTeam);
						for (Player p : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) {
							DodoBoard sb = ScoreboardController.getInstance().getScoreboard(p);
							sb.setLine(nexus.getTeam().getScoreBoardLine(), nexus.getTeam().getColor().toString() + (plugin.getTeamManager().getTeamPlayers(nexus.getTeam()).size() - 1 <= 0 ? "✗" : ChatColor.BOLD.toString() + (plugin.getTeamManager().getTeamPlayers(nexus.getTeam()).size() - 1)) + ChatColor.WHITE + " " + nexus.getTeam().getName());
						}
						plugin.getPlayerDataManager().getPlayerData(player).setStopTime();
					}
				}
				
				player.spigot().respawn();
				
				boolean nexusAlive = true;
                if (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null) {
                    if (plugin.getObjectsManager().getNexuses().get(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())) != null) {
                        if (plugin.getObjectsManager().getNexuses().get(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())).isDead()) {
                            nexusAlive = false;
                        }
                    }
                }
                
				if (playerDamage.containsKey(player.getName())) {
					PlayerDamage d = playerDamage.get(player.getName());
					if (d != null) {
						Entity killer = d.getLastDamager();
						if (killer == null) {
							killer = player.getKiller();
						}
						if (killer != null) {
							if (killer.getUniqueId().equals(player.getUniqueId())) {
								killer = null;
							}
						}
						
						if (d.isRecentDamage()) {
							if (killer == null) { //ZELFMOORD
								String elim = "";
								if (!nexusAlive) {
									elim += ChatColor.GRAY.toString() + ChatColor.BOLD + "ELIMINATIE: ";
								}
								if (playerTeam == null) {
									elim += ChatColor.RED + player.getName();
								} else {
									elim += playerTeam.getColor() + player.getName();
								}
								for (Player p : Bukkit.getOnlinePlayers()) {
									p.sendMessage(elim + ChatColor.GRAY + " is dood gegaan.");
								}
							} else { //ER IS EEN KILLER
								if (killer instanceof Player) {
									plugin.getPlayerDataManager().addKill((Player)killer);
									plugin.getPlayerDataManager().addMoney((Player)killer, 2);
									if (nexusAlive) { //KILL
										if (plugin.getPlayerDataManager().getPlayerData((Player)killer) != null) {
											if (plugin.getPlayerDataManager().getPlayerData((Player)killer).getKillsOnPlayer(player) <= 8) {
												plugin.getPlayerDataManager().addCoins(player, 32);
												plugin.getPlayerDataManager().getPlayerData((Player)killer).addKillOnPlayer(player);
											}
										}
										
										plugin.addValueToQuest(killer.getUniqueId(), "KILLS");
										
									
										if (plugin.getGameType() == GameType.MEGA) {
											final Player finalKiller = (Player)killer;
											plugin.getObjectsManager().getTowers().get(plugin.getTeamManager().getPlayerTeam(killer.getUniqueId())).stream().forEach(t -> {
												if (t.getLocation().distance(player.getLocation()) <= 16.0) {
													plugin.addValueToQuest(finalKiller.getUniqueId(), "NKILLS");
												}
		                                    	});
										} else {
											final Player finalKiller = (Player)killer;
											Nexus n = plugin.getObjectsManager().getNexuses().get(plugin.getTeamManager().getPlayerTeam(killer.getUniqueId()));
											if (n.getLocation().distance(player.getLocation()) <= 16.0) {
												plugin.addValueToQuest(finalKiller.getUniqueId(), "NKILLS");
											}
										}
									} else {
										plugin.addValueToQuest(killer.getUniqueId(), "EKILLS");
										plugin.getPlayerDataManager().addCoins(player, 64);
									}
									
									String elim = "";
									String killerName = (plugin.getTeamManager().getPlayerTeam(killer.getUniqueId()) != null ? plugin.getTeamManager().getPlayerTeam(killer.getUniqueId()).getColor().toString() : ChatColor.GRAY.toString()) + killer.getName();
									String playerName = (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null ? plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() : ChatColor.GRAY.toString()) + player.getName();
									if (!nexusAlive) {
										elim += ChatColor.GRAY.toString() + ChatColor.BOLD + "ELIMINATIE: ";
									}
									for (Player p : Bukkit.getOnlinePlayers()) {
										p.sendMessage(elim + killerName + ChatColor.GRAY + " heeft " + playerName + ChatColor.GRAY + " vermoord.");
									}
									
								} else if (killer instanceof EnderCrystal) { //Nexus killed the player
									EnderCrystal nexus = (EnderCrystal) killer;
								
									String elim = "";
									String killerName = nexus.getCustomName();
									String playerName = (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null ? plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() : ChatColor.GRAY.toString()) + player.getName();
									if (!nexusAlive) {
										elim += ChatColor.GRAY.toString() + ChatColor.BOLD + "ELIMINATIE: ";
									}
									for (Player p : Bukkit.getOnlinePlayers()) {
										p.sendMessage(elim + playerName + ChatColor.GRAY + " is vermoord door de " + killerName + ChatColor.GRAY + ".");
									}
								}
								
								if (d.getRecentDamagers() != null) { //ASSISTS
									for (UUID uuid : d.getRecentDamagers()) {
										if (!uuid.equals(killer.getUniqueId())) {
											if (Bukkit.getPlayer(uuid) != null) {
												Player p = Bukkit.getPlayer(uuid);
												
												String deadPlayerName = (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null ? plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() : ChatColor.GRAY.toString()) + player.getName();
												p.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Assist! " + ChatColor.GRAY + "Je hebt meegeholpen om " + deadPlayerName + ChatColor.GRAY + " te vermoorden. (" + d.getPercentageDamage(uuid) + "% damage gedaan)");
												
												plugin.getPlayerDataManager().getPlayerData(p).addAssist();
												
												if (d.getPercentageDamage(uuid) >= 50) {
													plugin.getPlayerDataManager().addMoney(p, 2);
												} else {
													plugin.getPlayerDataManager().addMoney(p, 1);
												}
												plugin.getPlayerDataManager().addCoins(p, ((int)d.getPercentageDamage(uuid)/10) * 3);
											}
										}
										
									}
								}
							}
						} else {
							String elim = "";
							String playerName = (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null ? plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() : ChatColor.GRAY.toString()) + player.getName();
							if (!nexusAlive) {
								elim += ChatColor.GRAY.toString() + ChatColor.BOLD + "ELIMINATIE: ";
							}
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(elim + playerName + ChatColor.GRAY + " is dood gegaan.");
							}
						}
					} else {
						String elim = "";
						String playerName = (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null ? plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() : ChatColor.GRAY.toString()) + player.getName();
						if (!nexusAlive) {
							elim += ChatColor.GRAY.toString() + ChatColor.BOLD + "ELIMINATIE: ";
						}
						for (Player p : Bukkit.getOnlinePlayers()) {
							p.sendMessage(elim + playerName + ChatColor.GRAY + " is dood gegaan.");
						}
					}
				} else {
					String elim = "";
					String playerName = (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null ? plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() : ChatColor.GRAY.toString()) + player.getName();
					if (!nexusAlive) {
						elim += ChatColor.GRAY.toString() + ChatColor.BOLD + "ELIMINATIE: ";
					}
					for (Player p : Bukkit.getOnlinePlayers()) {
						p.sendMessage(elim + playerName + ChatColor.GRAY + " is dood gegaan.");
					}
				}
				if (elimination) {
					plugin.getTeamManager().getPlayerTeamHashMap().remove(player.getUniqueId());
				}
			}
		}
		boolean end = checkEndGame();
		if (end) {
			gameEnd();
		}
		int currentDeaths = 0; //DEATH
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "DEATHS", Integer.class) != null ) {
			currentDeaths = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "DEATHS", Integer.class);
		}
		DodoCore.getInstance().setPlayerData(player.getUniqueId(), "KNIGHTS", "DYNAMITE_PLACED", (currentDeaths + 1));
		
	}
	
	public boolean checkEndGame() {
		for (Team team : Team.values()) {
			if (team.isActive()) {
				if (plugin.getTeamManager().getTeamPlayers(team).isEmpty()) {
					team.setActive(false);
				}
			}
		}
		
		int teamsAlive = 0;
		for (Team team : Team.values()) {
			if (team.isActive()) {
				teamsAlive++;
			}
		}
		
		if (teamsAlive == 1) { //Finished
			return true;
		}
		
		return false;
	}
	
	public void gameEnd() {
		if (plugin.getEnd()) {
			return;
		}
		plugin.setEnd(true);
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Player p : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) {
					p.getInventory().clear();
                    if (p.getGameMode().equals(GameMode.SURVIVAL)) {
                        p.setAllowFlight(true);
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                if (!p.isOnline() || !p.getAllowFlight()) {
                                    this.cancel();
                                }

                                if (p.isFlying()) {
                                    ParticleEffect.CLOUD.display(0.1f, 0.02f, 0.1f, 0.04f, 1, p.getLocation());
                                }
                            }
                        }.runTaskTimer(plugin, 0, 0);
                    }
                }
			}
		}.runTaskLater(plugin, 1);
		
		Team winningTeam = null;
		for (Team team : Team.values()) {
			if (team.isActive()) {
				winningTeam = team;
			}
		}
		
		for (PlayerData playerData : plugin.getPlayerDataManager().getPlayerDataList().values()) { //Player player : Bukkit.getOnlinePlayers()
			if (playerData == null) {
				continue;
			}

			boolean playerWon = false;
			if (playerData.getTeam() != null && playerData.getTeam() == winningTeam) {
				playerWon = true;
				playerData.setStopTime();
				int currentWins = playerData.getOldCoreData("WINS");
				setData(playerData.getUUID(), "WINS", (currentWins + 1));
				
				int currentWinsWithKit = playerData.getOldCoreData(playerData.getKit().toUpperCase() + "_WINS");
				setData(playerData.getUUID(), playerData.getKit() + "_WINS", (currentWinsWithKit + 1));
				
				if (playerData.getUUID() != null) {
					playerData.addCoins(240);
					playerData.addXp(50);
				}
			} else {
				if (playerData != null) {
					playerData.addCoins(120);
					playerData.addXp(0);
				}
			}
			
			if (Bukkit.getPlayer(playerData.getUUID()) != null) {
				Player player = Bukkit.getPlayer(playerData.getUUID());
				player.sendMessage("");
				sendCenteredMessage(player, "&7▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
				player.sendMessage("");
				if (playerWon) {
					sendCenteredMessage(player, "&a&lYay! Je hebt gewonnen.&a&l");
				} else {
					sendCenteredMessage(player, "&c&lJe hebt verloren.&c&l");
					sendCenteredMessage(player, "&7Team " + winningTeam.getName() + " heeft gewonnen.&7");
				}
				player.sendMessage("");
				int size = plugin.getPlayerDataManager().getMostKills().size();
				for (int i = 0; i < (size > 3 ? 3 : size); i++) {
					sendCenteredMessage(player, plugin.getPlayerDataManager().getMostKills().get(size - 1 - i));
				}
				player.sendMessage("");
				sendCenteredMessage(player, "&a&lSpeel opnieuw!", true);
				player.sendMessage("");
				sendCenteredMessage(player, "&7▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
				player.sendMessage("");
				player.sendMessage(ChatColor.BOLD + "Beloning:");
				player.sendMessage(ChatColor.GREEN + "✚ " + ChatColor.GOLD + playerData.getCoins() + " Knight Coins");
				player.sendMessage(ChatColor.GREEN + "✚ " + ChatColor.BLUE + playerData.getXp() + " Experience");

			}
			
			/* Save everything in Core */
			int currentGamesPlayed = playerData.getOldCoreData("GAMES_PLAYED");
			setData(playerData.getUUID(), "GAMES_PLAYED", (currentGamesPlayed + 1));
			
			int currentKills = playerData.getOldCoreData("KILLS");
			setData(playerData.getUUID(), "KILLS", (currentKills + playerData.getKills()));
			
			
			int currentAssists = playerData.getOldCoreData("ASSISTS");
			setData(playerData.getUUID(), "ASSISTS", (playerData.getAssists() + currentAssists));
			
			//EXPERIENCE
			if (plugin.getGameManager().getPlayer(playerData.getUUID()) != null) {
				plugin.getGameManager().getPlayer(playerData.getUUID()).giveExperience(playerData.getXp());
			}
			
			int currentDynamite = playerData.getOldCoreData("DYNAMITE_PLACED");
			setData(playerData.getUUID(), "DYNAMITE_PLACED", (currentDynamite + playerData.getTntPlaced()));
			
			/*KIT-SPECIFIC DATA*/
			String kit = playerData.getKit().toUpperCase(); //KIT SPECIFIC DATA

			int currentHoursPlayedWithKit = playerData.getOldCoreData(kit + "_HOURS");
			int secondsPlayedThisGame = (int) ((playerData.getStopTime() - playerData.getStartTime()) / 1000);
			setData(playerData.getUUID(), kit + "_HOURS", (currentHoursPlayedWithKit + secondsPlayedThisGame));
			
			int currentTimesPlayedWithKit = playerData.getOldCoreData(kit + "_TIMES_PLAYED");
			setData(playerData.getUUID(), kit + "_TIMES_PLAYED", (currentTimesPlayedWithKit + 1));
			
			int currentKillsWithKit = playerData.getOldCoreData(kit + "_KILLS");
			setData(playerData.getUUID(), kit + "_KILLS", (currentKillsWithKit + playerData.getKills()));
						
			int currentDeathsWithKit = playerData.getOldCoreData(kit + "_DEATHS");
			setData(playerData.getUUID(), kit + "_DEATHS", (currentDeathsWithKit + playerData.getDeaths()));
			
			int currentEKillsWithKit = playerData.getOldCoreData(kit + "_EKILLS");
			setData(playerData.getUUID(), kit + "_EKILLS", (currentEKillsWithKit + playerData.getEliminationKills()));
			
			int currentEDeathsWithKit = playerData.getOldCoreData(kit + "_EDEATHS");
			setData(playerData.getUUID(), kit + "_EDEATHS", (currentEDeathsWithKit + 1));
			
			int currentAbilitiesWithKit = playerData.getOldCoreData(kit + "_ABILITIES");
			setData(playerData.getUUID(), kit + "_ABILITIES", (currentAbilitiesWithKit + playerData.getAbilities()));
			
			int currentDamageWithKit = playerData.getOldCoreData(kit + "_DAMAGE");
			setData(playerData.getUUID(), kit + "_DAMAGE", (int)(currentDamageWithKit + playerData.getKitDamage()));
			
			int currentHealingWithKit = playerData.getOldCoreData(kit + "_HEALING");
			setData(playerData.getUUID(), kit + "_HEALING", (int)(currentHealingWithKit + playerData.getHealing()));
			
			int currentGateDamageWithKit = playerData.getOldCoreData(kit + "_GATE_DAMAGE");
			setData(playerData.getUUID(), kit + "_GATE_DAMAGE", (int)(currentGateDamageWithKit + playerData.getGateDamage()));
			
			int currentNexusDamageWithKit = playerData.getOldCoreData(kit + "_NEXUS_DAMAGE");
			setData(playerData.getUUID(), kit + "_NEXUS_DAMAGE", (int)(currentNexusDamageWithKit + playerData.getNexusDamage()));
			
			int currentTowerDamageWithKit = playerData.getOldCoreData(kit + "_TOWER_DAMAGE");
			setData(playerData.getUUID(), kit + "_TOWER_DAMAGE", (int)(currentTowerDamageWithKit + playerData.getTowerDamage()));
			
			if (Bukkit.getPlayer(playerData.getUUID()) != null) {
				if (this.plugin.quests.containsKey(playerData.getUUID())) {
		            for (QuestFormat format : plugin.getRegisteredQuests().quests) {
		                for (Map.Entry<String, Integer> entry : this.plugin.quests.get(playerData.getUUID()).entrySet()) {
		                    if (this.plugin.quests.get(playerData.getUUID()).get(entry.getKey()) > 0) {
		                        format.add(playerData.getUUID(), entry.getKey(), this.plugin.quests.get(playerData.getUUID()).get(entry.getKey()));
		                    }
		                }
		            }
		            this.plugin.quests.remove(playerData.getUUID());
		        }
			}
			
			if (Bukkit.getPlayer(playerData.getUUID()) == null) {
				continue;
			}
			
			//DodoBox?
			int c = new Random().nextInt(100) + 1;
            if (c <= 8) {
                Calendar calendar = new Calendar.Builder().build();
                calendar.setTimeInMillis(System.currentTimeMillis());
                int month = calendar.get(2) + 1;

                String cooldownType = "";
                long cooldown = 0;
                if (month == 10)
                    cooldownType = "HCOOLDOWN";
                else if (month == 12)
                    cooldownType = "CCOOLDOWN";

                if (cooldownType != null && cooldownType != "")
                    if (DodoCore.getInstance().getPlayerData(playerData.getUUID(), "LOBBY", cooldownType, Long.class) != null)
                        cooldown = DodoCore.getInstance().getPlayerData(playerData.getUUID(), "LOBBY", cooldownType, Long.class);

                boolean isSpecial = (new Random().nextInt(100) + 1) < 50;

                if (Bukkit.getPlayer(playerData.getUUID()) == null) {
                		continue;
                }
                Player player = Bukkit.getPlayer(playerData.getUUID());
                if (isSpecial && cooldown < System.currentTimeMillis()) {
                    if (month == 10) {
                        if (plugin.getGameManager().getPlayer(playerData.getUUID()) != null) {
                        		player.sendMessage(ChatColor.GREEN + "✚ " + ChatColor.YELLOW + "HalloweenBox");
                            int i = 0;
                            if (DodoCore.getInstance().getPlayerData(playerData.getUUID(), "LOBBY", "HBOXES", Integer.class) != null) {
                                i = DodoCore.getInstance().getPlayerData(playerData.getUUID(), "LOBBY", "HBOXES", Integer.class);
                            }
                            DodoCore.getInstance().setPlayerData(playerData.getUUID(), "LOBBY", "HBOXES", (i + 1));
                        }
                    }
                    else if (month == 12) {
                        if (plugin.getGameManager().getPlayer(playerData.getUUID()) != null) {
                        		player.sendMessage(ChatColor.GREEN + "✚ " + ChatColor.YELLOW + "ChristmasBox");
                            int i = 0;
                            if (DodoCore.getInstance().getPlayerData(playerData.getUUID(), "LOBBY", "CBOXES", Integer.class) != null) {
                                i = DodoCore.getInstance().getPlayerData(playerData.getUUID(), "LOBBY", "CBOXES", Integer.class);
                            }
                            DodoCore.getInstance().setPlayerData(playerData.getUUID(), "LOBBY", "CBOXES", (i + 1));
                        }
                    }
                }
                else {
                    if (plugin.getGameManager().getPlayer(playerData.getUUID()) != null) {
                    		player.sendMessage(ChatColor.GREEN + "✚ " + ChatColor.YELLOW + "DodoBox");
                        plugin.getGameManager().getPlayer(playerData.getUUID()).setEggs(plugin.getGameManager().getPlayer(player.getUniqueId()).getEggs() + 1);
                        DodoCore.getInstance().setPlayerData(playerData.getUUID(), "LOBBY", "EGGS", (plugin.getGameManager().getPlayer(player.getUniqueId()).getEggs()));
                    }
                }
            }
            if (Bukkit.getPlayer(playerData.getUUID()) != null) {
            		Bukkit.getPlayer(playerData.getUUID()).sendMessage("");
            }
			
		}

	SimpleCore.getInstance().getInternalAPI().restartServer(true);

	}
	
	private void setData(UUID uuid, String type, int value) {
		if (Bukkit.getPlayer(uuid) != null) { //Player is online
			DodoCore.getInstance().setPlayerData(uuid, "KNIGHTS", type, value);
		} else { //Player is offline
			DodoCore.getInstance().setPlayerDataOffline(uuid, "KNIGHTS", type, value);
		}
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		Player p = event.getPlayer();
		p.setVelocity(new Vector(0, 0, 0));
		p.setGameMode(GameMode.SPECTATOR);
		if (plugin.getTeamManager().getPlayerTeam(event.getPlayer().getUniqueId()) != null) {
			if (playerDamage.containsKey(p.getName()) && playerDamage.get(p.getName()) != null) {
                if (playerDamage.get(p.getName()).getLastDamager() != null) {
                    event.setRespawnLocation(playerDamage.get(p.getName()).getLastDamager().getLocation().clone());
                } else {
                    event.setRespawnLocation(plugin.getTeamManager().getTeamSpawn(plugin.getTeamManager().getPlayerTeam(p.getUniqueId())).clone());
                }
            } else {
                event.setRespawnLocation(plugin.getTeamManager().getTeamSpawn(plugin.getTeamManager().getPlayerTeam(p.getUniqueId())).clone());
            }
			
			if (p.hasMetadata("gameRespawn")) {
				new BukkitRunnable() {
					double time = plugin.getRespawnTime();
					
					ChatColor color(double time) {
						ChatColor color = ChatColor.GREEN;
						if (time >= 6) {
							color = ChatColor.DARK_RED;
						} else if (time >= 4) {
							color = ChatColor.RED;
						} else if (time >= 2) {
							color = ChatColor.YELLOW;
						}
						return color;
					}
					
					@Override
					public void run() {
						if (time <= 0.0) {
							if (p.isOnline()) {
								respawn(p);
							}
							this.cancel();
						} else {
							if (time == 9) {
								MessageController.sendTitle(p, 2, 25, 0, "", color(time) + "➒");
							} else if (time == 8) {
								MessageController.sendTitle(p, 2, 25, 0, "", color(time) + "➑");
							} else if (time == 7) {
								MessageController.sendTitle(p, 0, 25, 0, "", color(time) + "➐");
							} else if (time == 6) {
								MessageController.sendTitle(p, 0, 25, 0, "", color(time) + "➏");
							} else if (time == 5) {
								MessageController.sendTitle(p, 0, 25, 0, "", color(time) + "➎");
							} else if (time == 4) {
								MessageController.sendTitle(p, 0, 25, 0, "", color(time) + "➍");
							} else if (time == 3) {
								MessageController.sendTitle(p, 0, 25, 0, "", color(time) + "➌");
							} else if (time == 2) {
								MessageController.sendTitle(p, 0, 25, 0, "", color(time) + "➋");
							} else if (time == 1) {
								MessageController.sendTitle(p, 0, 25, 0, "", color(time) + "➊");
							}
							MessageController.getInstance().sendActionBar(p, ChatColor.RED.toString() + ChatColor.BOLD + "NIET LEAVEN!" + ChatColor.YELLOW.toString() + ChatColor.BOLD + " OVER " + color(time).toString() + ChatColor.BOLD + time + "s" + ChatColor.YELLOW + ChatColor.BOLD + " RESPAWN JE!");
							time = round((time - 0.1));
						}
					}
					
				}.runTaskTimer(plugin, 20, 2);
			}
			
		}
	}
	
	private double round(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(1, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

	protected void respawn(Player player) {
		if (player.hasMetadata("gameRespawn")) {
			player.removeMetadata("gameRespawn", plugin);
		}
		
		player.teleport(plugin.getTeamManager().getTeamSpawn(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())).clone().add(0, 1, 0));
		plugin.getItemManager().giveItems(player);
		
		if (plugin.getGameType() == GameType.MEGA) {
			for (Team team : Team.values()) {
				if (team.isActive()) {
					for (Tower tower : plugin.getObjectsManager().getTowers().get(team)) {
						if (!tower.isDead()) {
							tower.updateHoloGram();
						}
					}
				}
			}
		}

		if (!plugin.getObjectsManager().getNexuses().get(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())).isDead()) {
			plugin.getObjectsManager().getNexuses().get(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())).updateHoloGram();
		}

		for (Gate gate : plugin.getObjectsManager().getGates().get(plugin.getTeamManager().getPlayerTeam(player.getUniqueId()))) {
			if (gate.isAlive()) {
				gate.updateHoloGram();
			}
		}
		
		player.setGameMode(GameMode.SURVIVAL);
		this.spawnProtection.add(player.getUniqueId());
		
		new BukkitRunnable() {
			@Override
			public void run() {
				spawnProtection.remove(player.getUniqueId());
			}
		}.runTaskLater(plugin, 200L);
		
	}
	
	@EventHandler
	public void onPlayerDie(EntityDeathEvent event) {
		event.getDrops().clear();
	}
	
	public List<UUID> getSpawnProtectionList() {
		return this.spawnProtection;
	}
	
	@EventHandler
    public void onFallingBlock(EntityChangeBlockEvent e){
        if (e.getBlock().getType() == Material.SOIL) {
            e.setCancelled(true);
        }
        if (e.getEntity().getCustomName() != null || e.getEntity().getCustomName() != "") {
            if (e.getEntity().getCustomName().equalsIgnoreCase("ground.slam")){
                e.setCancelled(true);
                e.getEntity().remove();
            }
        }
    }
	
	@EventHandler
	public void onDropItem(PlayerDropItemEvent event) {
		event.setCancelled(true);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerPickUpItem(PlayerPickupArrowEvent event) {
		event.setCancelled(true);
	}
	
	
	private final static int CENTER_PX = 154;
	
	public static void sendCenteredMessage(Player player, String message) {
		sendCenteredMessage(player, message, false);
	}
	
	public static void sendCenteredMessage(Player player, String message, boolean buttonClick){
        if(message == null || message.equals("")) player.sendMessage("");
                message = ChatColor.translateAlternateColorCodes('&', message);
               
                int messagePxSize = 0;
                boolean previousCode = false;
                boolean isBold = false;
               
                for(char c : message.toCharArray()){
                        if(c == '§'){
                                previousCode = true;
                                continue;
                        }else if(previousCode == true){
                                previousCode = false;
                                if(c == 'l' || c == 'L'){
                                        isBold = true;
                                        continue;
                                }else isBold = false;
                        }else{
                                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                                messagePxSize++;
                        }
                }
               
                int halvedMessageSize = messagePxSize / 2;
                int toCompensate = CENTER_PX - halvedMessageSize;
                int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
                int compensated = 0;
                StringBuilder sb = new StringBuilder();
                while(compensated < toCompensate){
                        sb.append(" ");
                        compensated += spaceLength;
                }
                if (buttonClick) {
                		String playAgain = sb.toString() + message;
                		TextComponent playAgainMessage = new TextComponent(playAgain);
                		playAgainMessage.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/action play " + SimpleCore.getInstance().getInternalAPI().getServer().name()));
                		player.spigot().sendMessage(playAgainMessage);
                } else {
                		player.sendMessage(sb.toString() + message);
                }
                
        }
	
	
}


































