package nl.dodocraft.game.knights.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.TileEntitySkull;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.game.knights.objects.Gate;
import nl.dodocraft.game.knights.objects.Tower;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.chat.messages.MessageController;
import nl.dodocraft.utils.game.state.GameState;
import nl.dodocraft.utils.particles.ParticleEffect;

public class GateListener implements Listener {

	private HashMap<UUID, Integer> dynamiteCooldown = new HashMap<>();
	private List<Location> activeBombs = new ArrayList<>();
	private List<UUID> cooldownDynamiteMessage = new ArrayList<>();

	@EventHandler
	public void onPlaceDynamite(PlayerInteractEvent event) {
		if (GameState.getState() != GameState.IN_GAME) {
			event.setCancelled(true);
			return;
		}
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (event.getHand() == EquipmentSlot.HAND) {
				Player player = event.getPlayer();
				if (player.getInventory().getItemInMainHand().hasItemMeta()) {
					if (player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName() && ChatColor.stripColor(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName()).contains("Dynamiet")) {
						event.setCancelled(true);
						Location l = event.getClickedBlock().getLocation();
						if (!this.dynamiteCooldown.containsKey(player.getUniqueId())) { //kan tnt plaatsen.
							for (Team team : Team.values()) {
								if (team.isActive()) {
									for (Gate gate : Loader.getInstance().getObjectsManager().getGates().get(team)) {
										if (gate.getTeam() != Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId())) {
											if (gate.getLocation().distance(l) <= 9) {
												boolean towersDead = true;
												if (Loader.getInstance().getObjectsManager().getTowers().get(gate.getTeam()) != null) {
													for (Tower tower : Loader.getInstance().getObjectsManager().getTowers().get(gate.getTeam())) {
														if (!tower.isDead()) {
															towersDead = false;
															break;
														}
													}
												}
												
												if (!towersDead) {
													player.sendMessage(ChatColor.RED + "Je kan geen dynamiet plaatsen als de Towers van dit team nog niet kapot zijn.");
													player.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_HURT, 3F, 0F);
													return;
												}
												
												if (gate.isDead()) {
													player.sendMessage(ChatColor.RED + "Je kan geen dynamiet plaatsen als de Gate al kapot is.");
													player.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_HURT, 3F, 0F);
													return;
												}
												
												
												
												for (int y = 0; y < 6; y++) {
													if (event.getClickedBlock().getLocation().clone().subtract(0, y, 0).getBlock().getType() == Material.SPONGE) {
														Location bombLocation = new Location(event.getClickedBlock().getWorld(), event.getClickedBlock().getX(), event.getClickedBlock().getY() + 1, event.getClickedBlock().getZ());
														if (activeBombs.contains(bombLocation)) {
															player.sendMessage(ChatColor.RED + "Je kan geen dynamiet plaatsen als er al een staat.");
														} else {
															activeBombs.add(bombLocation);
															dynamiteCooldown(player);
															spawnBomb(bombLocation, gate, player);
															gate.addWarnBossBar();
															break;
														}
													}
												}
												
												break;
												
											}
										}
									}
								}
							}
						} else {
							if (!cooldownDynamiteMessage.contains(player.getUniqueId())) {
								int timeLeft = this.dynamiteCooldown.get(player.getUniqueId());
								player.sendMessage(ChatColor.GRAY + "Over " + ChatColor.RED.toString() + ChatColor.BOLD + (timeLeft+1) + "s" + ChatColor.GRAY + " kan je weer dynamiet plaatsen.");
								player.updateInventory();
								cooldownDynamiteMessage.add(player.getUniqueId());
								new BukkitRunnable() {
									@Override
									public void run() {
										if (cooldownDynamiteMessage.contains(player.getUniqueId())) {
											cooldownDynamiteMessage.remove(player.getUniqueId());
										}
									}
								}.runTaskLater(Loader.getInstance(), 20L);
							}
							
						}
					}
				}
			}
		}
	}

	private void dynamiteCooldown(Player player) {
		this.dynamiteCooldown.put(player.getUniqueId(), 12);
		new BukkitRunnable() {
			int seconds = 12;
			@Override
			public void run() {
				if (seconds <= 0) {
					this.cancel();
					ItemStack item = player.getInventory().getItem(7);
					if (item != null) {
						ItemMeta meta = item.getItemMeta();
						meta.setDisplayName(ChatColor.RED.toString() + ChatColor.BOLD + "Dynamiet");
						item.setItemMeta(meta);
					}
					dynamiteCooldown.remove(player.getUniqueId());
				} else {
					ItemStack item = player.getInventory().getItem(7);
					if (item != null) {
						ItemMeta meta = item.getItemMeta();
						meta.setDisplayName(ChatColor.RED.toString() + ChatColor.BOLD + "Dynamiet " + ChatColor.GRAY + "(" + seconds + "s)");
						item.setItemMeta(meta);
					}
					dynamiteCooldown.replace(player.getUniqueId(), seconds);
					seconds--;
				}
			}
		}.runTaskTimer(Loader.getInstance(), 0, 20L);
	}

	private void spawnBomb(Location location, Gate gate, Player player) {
		Block block = location.getBlock();
		block.setType(Material.SKULL);
				
		if (block.getState() instanceof Skull) {
            Skull skull = (Skull) block.getState();
            skull.setSkullType(SkullType.PLAYER);
            skull.setRawData((byte) 1);
            skull.update();

            TileEntitySkull tileentityskull = (TileEntitySkull) ((CraftWorld) block.getWorld()).getHandle().getTileEntity(new BlockPosition(block.getX(), block.getY(), block.getZ()));

            byte[] decodedBytes = org.apache.commons.codec.binary.Base64.decodeBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZWI5OTRiNDFmMDdmODdiMzI4MTg2YWNmY2JkYWJjNjk5ZDViMTg0N2ZhYmIyZTQ5ZDVhYmMyNzg2NTE0M2E0ZSJ9fX0");
            String decoded = (new String(decodedBytes)).replace("{\"textures\":{\"SKIN\":{\"url\":\"", "").replace("\"}}}", "");
            byte[] encodedData = org.apache.commons.codec.binary.Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", new Object[]{decoded}).getBytes());

            GameProfile profile = new GameProfile(UUID.randomUUID(), null);

            Property luckyHead = new Property("textures", new String(encodedData));
            profile.getProperties().put("textures", luckyHead);
            tileentityskull.setGameProfile(profile);
            tileentityskull.update();
        }
		
		ArmorStand stand = location.getWorld().spawn(location.clone().add(0.5, -1.4, 0.5), ArmorStand.class);
        stand.setVisible(false);
        stand.setGravity(false);

        new BukkitRunnable() {
            double time = 2.0;

            public void run() {
                if (!stand.isValid()) {
                    this.cancel();
                    if (stand.isValid()) {
                        location.getBlock().setType(Material.AIR);
                        activeBombs.remove(location);
                        stand.remove();
                    }
                    return;
                }
                if (this.time <= 0.0) {
                    this.cancel();
                    if (stand.isValid()) {
                        location.getBlock().setType(Material.AIR);
                        for (Entity e : location.getWorld().getNearbyEntities(location, 3, 3, 3)) {
                            if (e instanceof LivingEntity) {
                                if (e instanceof Player) {
                                		if (!Loader.getInstance().getFightListener().getSpawnProtectionList().contains(((Player)e).getUniqueId())) {
                                			((Player) e).damage(2.0);
                                		}
                                }
                            }
                        }
                        double damage = 20;
                        if (gate.getHealth() - damage <= 0.0) { //gate is dead
                        		damage = gate.getHealth();
                        		String color = gate.getTeam().getColor().toString();
                        		if (gate.isAlive()) {
                        			String message = ChatColor.GRAY + "De " + color + ChatColor.BOLD + gate.getTeam().getBvName() + " Gate" + ChatColor.GRAY + " is vernietigd door " + Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId()).getColor() + player.getName() + ChatColor.GRAY + ".";
        							String teamMessage = ChatColor.RED.toString() + ChatColor.BOLD + "Pas op!" + ChatColor.GRAY + " Jouw Gate is vernietigd. Je " + ChatColor.LIGHT_PURPLE + "Nexus" + ChatColor.GRAY + " kan nu aangevallen worden.";
                        			for (Player p : Bukkit.getOnlinePlayers()) {
        								if (p.getGameMode() != GameMode.SPECTATOR && p.isValid()) {
        									int gold = 5;
        									if (Loader.getInstance().getGameType() == GameType.MEGA) {
        										gold = 8;
        									}
        									Loader.getInstance().getPlayerDataManager().addMoney(p, gold);
        								}
        								p.sendMessage("");
        								p.sendMessage(message);
        								if (Loader.getInstance().getTeamManager().getPlayerTeam(p.getUniqueId()) == gate.getTeam()) {
        									p.sendMessage(teamMessage);
        									p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 2.5F, 0F);
        									MessageController.sendTitle(p, 0, 70, 0, "", ChatColor.RED.toString() + ChatColor.BOLD + "Je Gate is vernietigd!");
        								}
        								p.sendMessage("");
        							}   
                        		}                 		
                        }
                        gate.damage(damage);
                        activeBombs.remove(location);
                        stand.remove();
                        ParticleEffect.EXPLOSION_HUGE.display(0, 0, 0, 0, 1, location);
                        location.getWorld().playSound(location, Sound.ENTITY_GENERIC_EXPLODE, 2.5F, 0F);
                        gate.updateHoloGram();
                        Loader.getInstance().getScoreBoard().updateTeamObject(gate.getTeam());
                        Loader.getInstance().getPlayerDataManager().getPlayerData(player).addGateDamage(damage);
                    }
                    return;
                }

                ChatColor color = ChatColor.DARK_GREEN;
                double rounded = round(this.time);
                if (rounded > 3 && rounded <= 4) {
                    color = ChatColor.GREEN;
                } else if (rounded > 2 && rounded <= 3) {
                    color = ChatColor.YELLOW;
                } else if (rounded > 1 && rounded <= 2) {
                    color = ChatColor.RED;
                } else if (rounded <= 1) {
                    color = ChatColor.DARK_RED;
                }
                if (String.valueOf(rounded).endsWith(".0") || String.valueOf(rounded).endsWith(".5")) {
                    stand.getWorld().playSound(stand.getEyeLocation(), Sound.BLOCK_NOTE_PLING, 1.0f, 10.0f);
                }
                if (!stand.isCustomNameVisible())
                    stand.setCustomNameVisible(true);
                stand.setCustomName(color + ChatColor.translateAlternateColorCodes('&', "&l" + rounded + "s"));
                this.time -= 0.1;
            }

        }.runTaskTimer(Loader.getInstance(), 2, 2);
		
	}

	private Double round(double value) {
        long factor = (long) Math.pow(10.0, 1);
        long tmp = Math.round(value * (double) factor);
        return (double) tmp / (double) factor;
    }
	
}
