package nl.dodocraft.game.knights.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.game.knights.objects.Gate;
import nl.dodocraft.game.knights.objects.Nexus;
import nl.dodocraft.game.knights.objects.Tower;
import nl.dodocraft.utils.chat.messages.MessageController;
import nl.dodocraft.utils.game.state.GameState;
import nl.dodocraft.utils.scoreboard.DodoBoard;
import nl.dodocraft.utils.scoreboard.controller.ScoreboardController;
import nl.thiemoboven.ncore.SimpleCore;

public class CrystalListener implements Listener {

	private Loader plugin = Loader.getInstance();
	
	private final List<UUID> cooldown = new ArrayList<>();
	
	@EventHandler
	public void onTowerDamage(EntityDamageByEntityEvent event) {
		if (GameState.getState() != GameState.IN_GAME) {
			event.setCancelled(true);
			return;
		}
		if (event.getEntity().getType() == EntityType.ENDER_CRYSTAL) {
			event.setCancelled(true);
			if (event.getEntity() instanceof EnderCrystal && event.getDamager() instanceof Player) {
				Player player = (Player) event.getDamager();
				EnderCrystal c = (EnderCrystal) event.getEntity();
				if (!c.isDead()) {
					for (Tower tower : Tower.getTowers().values()) {
						if (tower.getLocation().distance(c.getLocation()) <= 3) {
							if (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null) {
								if (tower.getTeam() != plugin.getTeamManager().getPlayerTeam(player.getUniqueId())) {
									
									if (player.getInventory().getItemInMainHand().getType() == plugin.getItemManager().getSword(player).getMaterial()) {
										int attackDamage = plugin.getItemManager().getSword(player).getAttackDamage();
										int normalAttackDamage = plugin.getItemManager().getSword(player).getNormalAttackDamage();
										event.setDamage(event.getDamage() * ((double)attackDamage) / ((double)normalAttackDamage));
									}
									
									if (cooldown.contains(player.getUniqueId())) {
										return;
									}
									
									boolean towerDead = false;
									double damage = event.getDamage();
									if (tower.getHealth() - damage < 0.0 && !tower.isDead()) { //tower is dead
										towerDead = true;
										damage = tower.getHealth();
										String color = tower.getTeam().getColor().toString();
										String message = ChatColor.GRAY + "Team " + color + ChatColor.BOLD + tower.getTeam().getName() + ChatColor.GRAY + " is punt " + color + ChatColor.BOLD + tower.getName() + ChatColor.GRAY + " verloren door " + plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() + player.getName() + ChatColor.GRAY + ".";
										for (Player p : Bukkit.getOnlinePlayers()) {
											if (p.getGameMode() != GameMode.SPECTATOR && p.isValid()) {
												int gold = 5;
												if (Loader.getInstance().getGameType() == GameType.MEGA) {
													gold = 8;
												}
												plugin.getPlayerDataManager().addMoney(p, gold);
											}
											p.sendMessage(message);
										}
										
										if (plugin.getPlayerDataManager().getPlayerData(player) != null) {
											plugin.getPlayerDataManager().getPlayerData(player).addCoins(30);
										}
										
									}
									
									int mana = 0;
									if (plugin.getKitManager().selectedAbility.containsKey(player.getName())) {
										mana = plugin.getKitManager().selectedAbility.get(player.getName()).getManaByHit();
									}
									plugin.getPlayerDataManager().addMana(player, mana);
									
									player.playSound(player.getLocation(), Sound.BLOCK_GLASS_BREAK, 3F, 0F);
									tower.damage(damage);
									tower.addWarnBossBar();
									Loader.getInstance().getScoreBoard().updateTeamObject(tower.getTeam());
									
									plugin.getPlayerDataManager().getPlayerData(player).addTowerDamage(damage);
									
									for (int i = 0; i < damage; i++) {
										Loader.getInstance().addValueToQuest(player.getUniqueId(), "NDAMAGE");
									}
									
									for (UUID uuid : plugin.getTeamManager().getTeamPlayers(tower.getTeam())) {
										Player p = Bukkit.getPlayer(uuid);
										if (p.isValid() && p.getGameMode() == GameMode.SURVIVAL) {
											p.playSound(p.getLocation(), Sound.BLOCK_GLASS_BREAK, 1F, 5F);
										}
									}
									
									cooldown.add(player.getUniqueId());
									new BukkitRunnable() {
										@Override
										public void run() {
											if (cooldown.contains(player.getUniqueId())) {
												cooldown.remove(player.getUniqueId());
											}
										}
									}.runTaskLater(plugin, 10L);
									
									tower.updateHoloGram();
									if (towerDead) {
										plugin.getItemManager().giveDynamiet(player); //Deze functie kijkt of alle towers dood zijn.
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onNexusDamage(EntityDamageByEntityEvent event) {
		if (GameState.getState() != GameState.IN_GAME) {
			event.setCancelled(true);
			return;
		}
		if (event.getEntity().getType() == EntityType.ENDER_CRYSTAL) {
			event.setCancelled(true);
			if (event.getEntity() instanceof EnderCrystal && event.getDamager() instanceof Player) {
				Player player = (Player) event.getDamager();
				EnderCrystal c = (EnderCrystal) event.getEntity();
				if (!c.isDead()) {
					for (Nexus nexus : Nexus.getNexuses().values()) {
						if (nexus.getLocation().distance(c.getLocation()) <= 3) {
							if (plugin.getTeamManager().getPlayerTeam(player.getUniqueId()) != null) {
								if (nexus.getTeam() != plugin.getTeamManager().getPlayerTeam(player.getUniqueId())) {
									
									if (player.getInventory().getItemInMainHand().getType() == plugin.getItemManager().getSword(player).getMaterial()) {
										int attackDamage = plugin.getItemManager().getSword(player).getAttackDamage();
										int normalAttackDamage = plugin.getItemManager().getSword(player).getNormalAttackDamage();
										event.setDamage(event.getDamage() * ((double)attackDamage) / ((double)normalAttackDamage));
									}
									
									boolean gateDead = false;
									for (Gate gate : Gate.getGates()) { //Check if 1 van de gates dood is
										if (gate.getTeam() == nexus.getTeam()) {
											if (gate.isAlive() == false) {
												gateDead = true; //Als 1 gate kapot is kan je bij de nexus dus true!
											}
										}
									}
									
									boolean towersDead = true;
									if (!Tower.getTowers().isEmpty()) { //Er zijn towers
										towersDead = false;
										for (Tower tower : Tower.getTowers().values()) {
											if (tower.getTeam() == nexus.getTeam()) {
												if (tower.isDead()) {
													towersDead = true;
												} else {
													towersDead = false;
												}
											}
										}
									}
									
									if (gateDead && towersDead) {
										if (cooldown.contains(player.getUniqueId())) {
											return;
										}
										double damage = event.getDamage();
										if (nexus.getHealth() - damage < 0.0 && !nexus.isDead()) { //nexus is dead
											nexus.setDead(true);
											damage = nexus.getHealth();
											String color = nexus.getTeam().getColor().toString();
											String message = ChatColor.GRAY + "De " + color + ChatColor.BOLD + nexus.getTeam().getBvName() + " Nexus" + ChatColor.GRAY + " is vernietigd door " + plugin.getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() + player.getName() + ChatColor.GRAY + ".";
											String teamMessage = ChatColor.RED.toString() + ChatColor.BOLD + "JE " + ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "NEXUS " + ChatColor.RED.toString() + ChatColor.BOLD + "IS VERNIETIGD! RESPAWNEN IS NIET MEER MOGELIJK!";
											for (Player p : Bukkit.getOnlinePlayers()) {
												if (p.getGameMode() != GameMode.SPECTATOR && p.isValid()) {
													int gold = 5;
													if (Loader.getInstance().getGameType() == GameType.MEGA) {
														gold = 8;
													}
													plugin.getPlayerDataManager().addMoney(p, gold);
												}
												p.sendMessage("");
		        									p.sendMessage(message);
		        									if (Loader.getInstance().getTeamManager().getPlayerTeam(p.getUniqueId()) == nexus.getTeam()) {
		        										p.sendMessage(teamMessage);
		        										p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 2.5F, 0F);
		        										MessageController.sendTitle(p, 0, 50, 0, "", ChatColor.RED.toString() + ChatColor.BOLD + "Je " + ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Nexus" + ChatColor.RED.toString() + ChatColor.BOLD + " is vernietigd!");
		        									}
		        									p.sendMessage("");
											}
											
											if (plugin.getPlayerDataManager().getPlayerData(player) != null) {
												plugin.getPlayerDataManager().getPlayerData(player).addCoins(30);
											}
											
											for (Player p : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) { //Update scoreboard
												DodoBoard sb = ScoreboardController.getInstance().getScoreboard(p);
												sb.setLine(nexus.getTeam().getScoreBoardLine(), nexus.getTeam().getColor().toString() + ChatColor.BOLD + (plugin.getTeamManager().getTeamPlayers(nexus.getTeam()).size() == 0 ? "✗" : ChatColor.BOLD.toString() + plugin.getTeamManager().getTeamPlayers(nexus.getTeam()).size()) + ChatColor.WHITE + " " + nexus.getTeam().getName());
											}
												
										}
										
										nexus.damage(damage);
										Loader.getInstance().getScoreBoard().updateTeamObject(nexus.getTeam());
										nexus.addWarnBossBar();
										nexus.updateHoloGram();
										
										plugin.getPlayerDataManager().getPlayerData(player).addNexusDamage(damage);
										
										//Heal damager his nexus with 2hp
										plugin.getObjectsManager().getNexuses().get(plugin.getTeamManager().getPlayerTeam(player.getUniqueId())).addHealth(2);
										
										for (UUID uuid : plugin.getTeamManager().getTeamPlayers(nexus.getTeam())) {
											Player p = Bukkit.getPlayer(uuid);
											if (p.isValid() && p.getGameMode() == GameMode.SURVIVAL) {
												p.playSound(p.getLocation(), Sound.BLOCK_GLASS_BREAK, 1F, 5F);
											}
										}
										
										for (int i = 0; i < damage; i++) {
											Loader.getInstance().addValueToQuest(player.getUniqueId(), "NDAMAGE");
										}
										
										int mana = 0;
										if (plugin.getKitManager().selectedAbility.containsKey(player.getName())) {
											mana = plugin.getKitManager().selectedAbility.get(player.getName()).getManaByHit();
										}
										plugin.getPlayerDataManager().addMana(player, mana);
										
										cooldown.add(player.getUniqueId());
										new BukkitRunnable() {
											@Override
											public void run() {
												if (cooldown.contains(player.getUniqueId())) {
													cooldown.remove(player.getUniqueId());
												}
											}
										}.runTaskLater(plugin, 10L);
										
										player.playSound(player.getLocation(), Sound.BLOCK_GLASS_BREAK, 3F, 0F);
									} else if (towersDead) {
										player.sendMessage(ChatColor.RED + "Je kunt de nexus nog niet aanvallen omdat de gate nog niet kapot is. Schakel eerst de gate uit voordat je de nexus probeert aan te vallen!");
									} else {
										player.sendMessage(ChatColor.RED + "Je kunt de nexus nog niet aanvallen omdat de torens van Team " + nexus.getTeam().getName() + " nog levend zijn. Schakel de torens eerst uit voor jij de nexus aan probeert te vallen!");
									}
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	
}
