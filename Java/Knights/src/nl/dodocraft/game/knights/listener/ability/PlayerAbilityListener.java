package nl.dodocraft.game.knights.listener.ability;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.GameAbility;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.game.state.GameState;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.particles.ParticleEffect;

public class PlayerAbilityListener implements Listener {

	private Loader plugin;

	public PlayerAbilityListener(Loader plugin) {
		this.plugin = plugin;
	}

	public enum AbilityID {
		PIRATE(0), MENDER(1), HUNTER(2), NINJA(3), ELEKTRON(4);

		private final int id;

		AbilityID(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	@EventHandler
	public void onShootArrow(EntityShootBowEvent e) {
		if (GameState.getState() != GameState.IN_GAME) {
			return;
		}
		if (!plugin.getKitManager().selectedKit.containsKey(((Player) e.getEntity()).getName())) {
			return;
		}
		if (e.getEntity() instanceof Player && e.getProjectile() instanceof Arrow) {
			Player p = ((Player) e.getEntity());
			if (this.plugin.getKitManager().selectedAbility.containsKey(e.getEntity().getName())) {
				if (p.getGameMode() == GameMode.SURVIVAL) {
					if (this.plugin.getKitManager().selectedAbility.get(p.getName()).getId() == AbilityID.HUNTER.getId()
							&& p.hasMetadata("knights.hunter")) {
						plugin.getItemManager().giveArrow(p);
						Arrow arrow = (Arrow) e.getProjectile();

						Entity animal = getAnimal(arrow);

						((Ageable) animal).setBaby();
						animal.setVelocity(arrow.getVelocity().multiply(1.2));

						new BukkitRunnable() {
							@Override
							public void run() {
								ParticleEffect.CLOUD.display(0.1f, 0.02f, 0.1f, 0.04f, 1, animal.getLocation());
								if (animal.isOnGround()) {
									this.cancel();
									arrow.remove();
									animal.remove();
									ParticleEffect.EXPLOSION_NORMAL.display(0.1f, 0.02f, 0.1f, 0.04f, 1, animal.getLocation().clone());
									spawnAnimalFood(animal, plugin.getKitManager().selectedAbility.get(p.getName()), plugin.getTeamManager().getPlayerTeam(p.getUniqueId()));
								}
								for (Entity e : animal.getNearbyEntities(0.5, 0.5, 0.5)) {
									if (e instanceof Player) {
										if ((Player) e != p) {
											this.cancel();
											arrow.remove();
											animal.remove();
											ParticleEffect.EXPLOSION_NORMAL.display(0.1f, 0.02f, 0.1f, 0.04f, 1, animal.getLocation().clone());
											spawnAnimalFood(animal, plugin.getKitManager().selectedAbility.get(p.getName()), plugin.getTeamManager().getPlayerTeam(p.getUniqueId()));
											break;
										}
									}
								}
							}
						}.runTaskTimer(plugin, 0, 0);

					} else {
						new BukkitRunnable() {
							@Override
							public void run() {
								plugin.getItemManager().giveArrow(p);
							}
						}.runTaskLater(plugin, 14L);
					}
				}
			}
		}
	}

	public Entity getAnimal(Arrow arrow) {
		Entity animal = null;

		double r = new Random().nextDouble();

		if (r < 0.25) {
			animal = arrow.getLocation().clone().add(0, 0.7, 0).getWorld().spawnEntity(arrow.getLocation(),
					EntityType.COW);
		} else if (r < 0.50) {
			animal = arrow.getLocation().clone().add(0, 0.7, 0).getWorld().spawnEntity(arrow.getLocation(),
					EntityType.SHEEP);
		} else if (r < 0.75) {
			animal = arrow.getLocation().clone().add(0, 0.7, 0).getWorld().spawnEntity(arrow.getLocation(),
					EntityType.CHICKEN);
		} else {
			animal = arrow.getLocation().clone().add(0, 0.7, 0).getWorld().spawnEntity(arrow.getLocation(),
					EntityType.PIG);
		}
		return animal;
	}

	public void spawnAnimalFood(Entity animal, GameAbility ability, Team team) {
		Location loc = animal.getLocation();
		double count = ability.getLevel();
		ItemBuilder builder;
		int amplifier = 1;
		if (ability.getLevel() >= 7) {
			amplifier = 3;
		} else if (ability.getLevel() >= 4) {
			amplifier = 2;
		}
		if (animal instanceof Cow) {
			builder = new ItemBuilder(Material.COOKED_BEEF).addLoreLine("" + ability.getStrength() * 1.75).addLoreLine("" + amplifier)
					.addLoreLine(team.getName());
		} else if (animal instanceof Pig) {
			builder = new ItemBuilder(Material.GRILLED_PORK).addLoreLine("" + ability.getStrength() / 2.75).addLoreLine("" + 0)
					.addLoreLine(team.getName());
		} else if (animal instanceof Sheep) {
			builder = new ItemBuilder(Material.COOKED_MUTTON).addLoreLine("" + ability.getStrength() * 1.56).addLoreLine("" + amplifier)
					.addLoreLine(team.getName());
		} else {
			builder = new ItemBuilder(Material.COOKED_CHICKEN).addLoreLine("" + ability.getStrength() * 2.25).addLoreLine("" + (ability.getLevel() == 7 ? 2 : 1))
					.addLoreLine(team.getName());
		}
		drop(builder, loc, count);
	}

	public void drop(ItemBuilder builder, Location location, double count) {
		double angle, x, y = 0.48, z;
		for (int i = 0; i < count; i++) {
			angle = i * Math.PI / (count / 2.0);

			x = Math.cos(angle) * 0.1125;
			z = Math.sin(angle) * 0.1125;

			Location loc = location.clone().add(x, y, z);
			Vector vec = loc.toVector().subtract(location.clone().toVector()).multiply(1.23);

			ItemStack stack = builder.clone().setName(ChatColor.RED + "Item_" + i + "_" + System.currentTimeMillis()).toItemStack();
			Item item = location.getWorld().dropItem(location, stack);
			item.setVelocity(vec);

			new BukkitRunnable() {
				@Override
				public void run() {
					item.remove();
				}
			}.runTaskLater(plugin, 12 * 20L);

		}
	}

	public static List<UUID> cooldown = new ArrayList<>();

	private HashSet<Material> cancels = new HashSet<>(Arrays.asList(new Material[] { Material.ANVIL, Material.COMMAND,
			Material.BED, Material.BEACON, Material.BED_BLOCK, Material.BREWING_STAND, Material.BURNING_FURNACE,
			Material.CAKE_BLOCK, Material.CHEST, Material.DIODE, Material.DIODE_BLOCK_OFF, Material.DIODE_BLOCK_ON,
			Material.DISPENSER, Material.DROPPER, Material.ENCHANTMENT_TABLE, Material.ENDER_CHEST, Material.FENCE_GATE,
			Material.FENCE_GATE, Material.FURNACE, Material.HOPPER, Material.IRON_DOOR, Material.IRON_DOOR_BLOCK,
			Material.ITEM_FRAME, Material.LEVER, Material.REDSTONE_COMPARATOR, Material.REDSTONE_COMPARATOR_OFF,
			Material.REDSTONE_COMPARATOR_ON, Material.STONE_BUTTON, Material.TRAP_DOOR, Material.TRAPPED_CHEST,
			Material.WOODEN_DOOR, Material.WOOD_BUTTON, Material.WOOD_DOOR, Material.WORKBENCH }));

	private List<String> cd = new ArrayList<>();

	@EventHandler
	public void onRightClick(PlayerInteractEvent e) {
		if (GameState.getState() != GameState.IN_GAME
				|| !plugin.getKitManager().selectedKit.containsKey(e.getPlayer().getName())) {
			return;
		}
		if (e.getPlayer() != null) {
			Player p = e.getPlayer();
			boolean archer = this.plugin.getKitManager().selectedKit.get(e.getPlayer().getName())
					.getStartID() == AbilityID.HUNTER.getId();
			boolean use = false;
			use = e.getAction().toString().contains("RIGHT") && !archer;
			if (!use)
				use = e.getAction().toString().contains("LEFT") && archer;

			if (use) {
				if (e.getHand() != EquipmentSlot.HAND) {
					return;
				}

				if (cooldown.contains(p.getUniqueId())) {
					return;
				}

				if (e.getClickedBlock() != null && !e.getClickedBlock().getType().equals(Material.AIR)) {
					if (cancels.contains(e.getClickedBlock().getType())) {
						return;
					}
				}
				if (this.plugin.getKitManager().selectedAbility.containsKey(e.getPlayer().getName())) {
					if (p.isValid()) {
						if (p.getInventory().getItemInMainHand() != null) {
							if (((p.getInventory().getItemInMainHand().getType().toString().contains("_AXE")
									|| p.getInventory().getItemInMainHand().getType().toString().contains("SWORD"))
									&& !archer)
									|| (p.getInventory().getItemInMainHand().getType().equals(Material.BOW)
											&& archer)) {
								if (this.plugin.getPlayerDataManager().getMana(p) >= this.plugin.getPlayerDataManager()
										.getMaxMana()) {
									cooldown.add(p.getUniqueId());
									this.plugin.getKitManager().selectedAbility.get(p.getName()).onActivate(p);
									Loader.getInstance().addValueToQuest(p.getUniqueId(), "ABILITYU");
									new BukkitRunnable() {
										@Override
										public void run() {
											cooldown.remove(p.getUniqueId());
										}
									}.runTaskLater(this.plugin, 1);
								} else {

									if (cd.contains(e.getPlayer().getName())) {
										e.getPlayer().playSound(e.getPlayer().getLocation(),
												Sound.BLOCK_FIRE_EXTINGUISH, 0.2f, 2.0f);
									} else {
										p.sendMessage(ChatColor.RED
												+ "Je kan alleen je ability gebruiken als je 100 mana hebt.");
										e.getPlayer().playSound(e.getPlayer().getLocation(),
												Sound.BLOCK_FIRE_EXTINGUISH, 1.0f, 2.0f);
										cd.add(e.getPlayer().getName());
										new BukkitRunnable() {
											@Override
											public void run() {
												cd.remove(e.getPlayer().getName());
											}
										}.runTaskLater(plugin, 16 * 20);
									}
								}
							}
						}
					}
				}
			}
		}
	}

}
