package nl.dodocraft.game.knights.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.game.knights.kits.KitTemplate;
import nl.dodocraft.game.knights.objects.Gate;
import nl.dodocraft.game.knights.objects.Nexus;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.game.events.PlayerJoinGameEvent;
import nl.dodocraft.utils.game.events.PlayerLeaveGameEvent;
import nl.dodocraft.utils.game.state.GameState;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.particles.ParticleEffect;
import nl.dodocraft.utils.scoreboard.DodoBoard;
import nl.dodocraft.utils.scoreboard.controller.ScoreboardController;
import nl.dodocraft.utils.tools.PacketAPI;
import nl.thiemoboven.ncore.SimpleCore;

public class GameListener implements Listener {

	private List<Location> placedBlocks = new ArrayList<>();
	
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinGameEvent e) {
        Player player = e.getPlayer();
        player.teleport(Loader.getInstance().getTeamManager().getTeamSpawn(Team.RED));
        if (GameState.currentState(GameState.IN_LOBBY) || GameState.currentState(GameState.STARTING)) {
            e.announceJoin();
        }

        player.getInventory().setItem(0, new ItemBuilder(Material.COMPASS).setName(Loader.getInstance().TEAM_SELECTOR).toItemStack());
        player.getInventory().setItem(1, new ItemBuilder(Material.BLAZE_POWDER).setName(Loader.getInstance().KIT_SELECTOR).toItemStack());

        /* Set player settings */
        player.setAllowFlight(true);
        player.setHealthScale(20);
        player.setHealth(player.getHealthScale());
        player.setWalkSpeed(0.2F);

        /* Cloud effect */
        new BukkitRunnable() {
            @Override
            public void run() {
                if (player == null || !player.isOnline() || !player.getAllowFlight()) {
                    this.cancel();
                }

                if (player.isFlying()) {
                    ParticleEffect.CLOUD.display(0.1f, 0.02f, 0.1f, 0.04f, 1, player.getLocation());
                }
            }
        }.runTaskTimer(Loader.getInstance(), 0, 0);

        /* Set player kit */
        if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "KIT", String.class) == null)
            DodoCore.getInstance().setPlayerData(player.getUniqueId(), "KNIGHTS", "KIT", "Pirate");

        for (KitTemplate k : Loader.getInstance().getKitManager().registeredKits) {
            if (!DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "KIT", String.class).equalsIgnoreCase(k.getName())) {
            		continue;
            }

            boolean allowCheat = false;
            Rank rank = DodoCore.getInstance().getRank(player.getUniqueId());
            if (rank != null && (rank == Rank.YOUTUBER || rank.getId() >= Rank.ADMIN.getId())) {
                allowCheat = true;
            }

            for (String types : new String[]{k.getName().toUpperCase() + "_ABILITY", k.getName().toUpperCase() + "_PASSIVE1"}) {
                int level = 1;
                if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", types, Integer.class) != null && DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", types, Integer.class) != 0) {
                    level = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", types, Integer.class);
                }
                if (allowCheat && level < 3)
                    level = 3;

                if (types.endsWith("_ABILITY")) {
                    Loader.getInstance().getKitManager().selectedAbility.put(player.getName(), Loader.getInstance().getKitManager().getAbilityUtils().getAbility(player, level, k, Loader.getInstance()));
                } else {
                    if (types.endsWith("_PASSIVE1")) {
                        Loader.getInstance().getKitManager().selectedPassive1.put(player.getName(), Loader.getInstance().getKitManager().getAbilityUtils().getPassive1(player, level, k, Loader.getInstance()));
                    }
                }
            }
            Loader.getInstance().getKitManager().selectedKit.put(player.getName(), k);
        }

        /* Set wait scoreboard */
        Loader.getInstance().getScoreBoard().setWaitBoard(player);

        for (Player pl : Bukkit.getOnlinePlayers()) {
        		DodoBoard sb = ScoreboardController.getInstance().getScoreboard(pl);
        		sb.setLine(10, "Spelers: " + ChatColor.GREEN + SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers().size() + "/" + Loader.getInstance().getGameManager().getMaxPlayers());
        }


    }

    @EventHandler
    public void onPlayerQuit(PlayerLeaveGameEvent e) {
        if (GameState.currentState(GameState.IN_LOBBY) || GameState.currentState(GameState.STARTING)) {
            e.announceQuit(e.getQuitMessage());
            for (Player p : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) {
				DodoBoard sb = ScoreboardController.getInstance().getScoreboard(p);
				sb.setLine(10, "Spelers: " + ChatColor.GREEN + (SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers().size() - 1) + "/" + Loader.getInstance().getGameManager().getMaxPlayers());
			}
            if (Loader.getInstance().getTeamManager().getPlayerTeam(e.getPlayer().getUniqueId()) != null) {
            		Loader.getInstance().getTeamManager().getPlayerTeamHashMap().remove(e.getPlayer().getUniqueId());
            }
            return;
        } else {
            e.announceQuit(e.getQuitMessageSilent());
            Loader.getInstance().getTeamManager().getPlayerTeamHashMap().remove(e.getPlayer().getUniqueId());
            if (Loader.getInstance().getFightListener().checkEndGame() && !Loader.getInstance().getEnd()) {
            		Loader.getInstance().getFightListener().gameEnd();
            } else {
            		Team team = Loader.getInstance().getTeamManager().getPlayerTeam(e.getPlayer().getUniqueId());
            		Loader.getInstance().getTeamManager().getPlayerTeamHashMap().remove(e.getPlayer().getUniqueId());
            		if (team != null) {
            			for (Player p : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) {
						DodoBoard sb = ScoreboardController.getInstance().getScoreboard(p);
						sb.setLine(team.getScoreBoardLine(), team.getColor().toString() + (Loader.getInstance().getTeamManager().getTeamPlayers(team).size() == 0 ? "✗" : ChatColor.BOLD.toString() + Loader.getInstance().getTeamManager().getTeamPlayers(team).size()) + ChatColor.WHITE + " " + team.getName());
					}
            			if (Loader.getInstance().getTeamManager().getTeamPlayers(team).size() == 0) {
            				//Destroy Gate
            				Loader.getInstance().getObjectsManager().getGates().get(team).get(0).removeHoloGram();
            				Loader.getInstance().getObjectsManager().getGates().get(team).get(0).destroyGateByLeave();
            				
            				//Destroy Nexus
            				Loader.getInstance().getObjectsManager().getNexuses().get(team).removeHoloGram();
            				Nexus n = Loader.getInstance().getObjectsManager().getNexuses().get(team);
            				n.removeNexus();
            				team.setActive(false);
            				
            				//Send team elimination message
            				Bukkit.broadcastMessage("");
            				Bukkit.broadcastMessage(team.getColor().toString() + ChatColor.BOLD + "Het " + team.getBvName() + " Team is geëlimineerd omdat alle Knights de game hebben verlaten.");
            				Bukkit.broadcastMessage("");
            			}
            		}
            }
            if (Loader.getInstance().getPlayerDataManager().getPlayerData(e.getPlayer()) != null) {
            		Loader.getInstance().getPlayerDataManager().getPlayerData(e.getPlayer()).setStopTime();
            }
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
    		event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBreakFinal(BlockBreakEvent event) {
        if (GameState.currentState(GameState.IN_LOBBY) || GameState.currentState(GameState.STARTING) || GameState.currentState(GameState.IN_PREGAME)) {
        		event.setCancelled(true);
        		return;
        }

        if (event.getBlock().getType() == Material.LONG_GRASS || event.getBlock().getType() == Material.YELLOW_FLOWER || event.getBlock().getType() == Material.RED_ROSE) {
            event.setCancelled(true);
            event.getBlock().setType(Material.AIR);
            return;
        }

        Block b = event.getBlock();
       	if (placedBlocks.contains(b.getLocation())) {
       		b.setType(Material.WOOL);
       	} else {
       		event.setCancelled(true);
       	}
    }
    
    @EventHandler
    public void onPlayerPickUp(EntityPickupItemEvent event) {
    		if (event.getEntity() instanceof Player) {
    			event.setCancelled(true);
    			Player player = (Player) event.getEntity();
    			if (player.getGameMode() == GameMode.SPECTATOR || !GameState.currentState(GameState.IN_GAME)) {
    				event.setCancelled(true);
    				return;
    			}
    			if (event.getItem().getItemStack().getType() == Material.WOOL) {
    				event.getItem().getItemStack().setDurability((byte) Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId()).getBlockData());
    				if (!player.getInventory().contains(Material.WOOL)) {
    					player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 3F, 0F);
    					event.getItem().remove();
    					player.getInventory().setItem(8, event.getItem().getItemStack());
    				} else if (player.getInventory().getItem(8).getAmount() >= 64) {
    					event.setCancelled(true);
    				} else {
    					int amount = event.getItem().getItemStack().getAmount();
    					if (amount + player.getInventory().getItem(8).getAmount() > 64) {
    						amount = 64 - player.getInventory().getItem(8).getAmount();
    					}
    					event.getItem().getItemStack().setAmount(amount);
    					event.getItem().remove();
    					player.getInventory().addItem(event.getItem().getItemStack());
    				}
    				return;
    			} else {
    				if (event.getItem().getItemStack().getItemMeta().hasLore() && event.getItem().getItemStack().getItemMeta().getLore().size() >= 2) {
    					if (event.getItem().getItemStack().getItemMeta().getLore().get(2).equalsIgnoreCase(Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId()).getName())) {
        					event.setCancelled(true);
        					return;
        				}
        				
        				ParticleEffect.EXPLOSION_NORMAL.display(0, 0, 0.3f, 1, 1, player.getLocation());
        				
        				event.getItem().remove();
        				Material type = event.getItem().getItemStack().getType();
        				double time = Double.parseDouble(event.getItem().getItemStack().getItemMeta().getLore().get(0));
        				int amplifier = Integer.parseInt(event.getItem().getItemStack().getItemMeta().getLore().get(1));
        				if (type == Material.COOKED_BEEF) {
        					player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, (int) (time * 20), amplifier));
        				} else if (type == Material.GRILLED_PORK) {
        					player.damage(time);
        				} else if (type == Material.COOKED_MUTTON) {
        					player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, (int) (time * 20), amplifier));
        				} else {
        					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) (time * 20), amplifier));
        				}
    				}
    			}
    		}
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
    		event.setCancelled(true);
    }

    @EventHandler
    public void placeBlockEvent(BlockPlaceEvent event) {
    		for (Team team : Team.values()) {
    			if (team.isActive()) {
    				if (Loader.getInstance().getGameType() == GameType.MINI) {
    					for (Gate gate : Loader.getInstance().getObjectsManager().getGates().get(team)) {
    						if (gate.isAlive()) {
    							if (gate.getLocation().distance(event.getBlock().getLocation()) <= 8) {
            						event.getPlayer().sendMessage(ChatColor.GRAY + "Je kan niet zo dicht bij een Gate bouwen.");
            						event.setCancelled(true);
            						return;
            					}
    						}
        				}
    					Nexus nexus = Loader.getInstance().getObjectsManager().getNexuses().get(team);
        				if (!nexus.isDead() && nexus.getLocation().distance(event.getBlock().getLocation()) <= 8) {
        					event.getPlayer().sendMessage(ChatColor.GRAY + "Je kan niet zo dicht bij een Nexus bouwen.");
        					event.setCancelled(true);
        					return;
        				}
    				}    				
    			}
    		}
    		placedBlocks.add(event.getBlock().getLocation());
    }

    @EventHandler
    public void preventFlyKick(PlayerKickEvent e) {
        if (e.getReason().contains("Flying is not enabled")) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
    		event.setCancelled(true);
    		Player player = event.getPlayer();
    		String input = event.getMessage();
    		String prefix = "";
    		Team team = null;
    		if (player.getGameMode() == GameMode.SPECTATOR && !player.hasMetadata("gameRespawn")) {
    			prefix += ChatColor.GRAY + "[UITGESCHAKELD] ";
    		} else {
    			team = Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId());
    		}

    		Rank rank = DodoCore.getInstance().getRank(player.getUniqueId());

    		if (rank == null) {
    			return;
    		}
    		if (GameState.getState() == GameState.IN_GAME && team != null) {
    			if (input.startsWith("!") || Loader.getInstance().getTeamManager().getTeamPlayers(team).size() == 1) { //Shout chat & shout chat when only 1 player in a team
        			input = input.replaceFirst("!", "");
        			prefix = team.getColor() + "[SHOUT] ";
        			if (rank.getId() > Rank.NORMAL.getId()) {
        				String chatColor = ChatColor.GRAY.toString();
        				if (rank.getId() > Rank.BUILDER.getId()) {
        					chatColor = ChatColor.WHITE.toString();
        				}
        				prefix += rank.getColorPrefix() + rank.getShortName() + " " + chatColor + player.getName() + ": ";
        			} else {
        				prefix += ChatColor.GRAY + player.getName() + ": ";
        			}
        			String message = prefix + rank.getChatColor() + input;

        			for (Player p : Bukkit.getOnlinePlayers()) {
        				p.sendMessage(message);
        			}

        		} else { //Team chat
        			prefix = team.getColor() + "[" + team.getName().toUpperCase() + "] ";
        			if (rank.getId() > Rank.NORMAL.getId()) {
        				String chatColor = ChatColor.GRAY.toString();
        				if (rank.getId() > Rank.BUILDER.getId()) {
        					chatColor = ChatColor.WHITE.toString();
        				}
        				prefix += rank.getColorPrefix() + rank.getShortName() + " " + chatColor + player.getName() + ": ";
        			} else {
        				prefix += ChatColor.GRAY + player.getName() + ": ";
        			}
        			String message = prefix + rank.getChatColor() + input;

        			for (UUID uuid : Loader.getInstance().getTeamManager().getTeamPlayers(team)) {
        				Bukkit.getPlayer(uuid).sendMessage(message);
        			}
        		}
    		} else {
    			if (rank.getId() > Rank.NORMAL.getId()) {
    				prefix += rank.getColorPrefix() + rank.getShortName() + " " + ChatColor.WHITE + player.getName() + ": ";
    			} else {
    				prefix += ChatColor.GRAY + player.getName() + ": ";
    			}
    			String message = prefix + input;
    			for (Player p : Bukkit.getOnlinePlayers()) {
    				p.sendMessage(message);
    			}
    		}
    }
    
	@EventHandler
    public void onPlayerTracking(PlayerMoveEvent e) {
    		if (GameState.getState() == GameState.IN_GAME) {
    			Player player = e.getPlayer();
        		Player target = null;
        		double distance = Double.POSITIVE_INFINITY;
        		for (Player ent : Bukkit.getOnlinePlayers()) {
        			if (ent.getGameMode() != GameMode.SPECTATOR && !Loader.getInstance().getTeamManager().areTeammates(player.getUniqueId(), ((Player)ent).getUniqueId())) {
        				double distanceto = player.getLocation().distance(ent.getLocation());
                    if (distanceto < distance) {
                    		distance = distanceto;
                        target = (Player) ent;
                    }
        			}
        		}

        		if (target != null) {
        			player.setCompassTarget(target.getLocation());
        			if (player.getInventory().getItemInMainHand().getType() == Material.COMPASS) {
                			double distanceBetween = Math.round(player.getLocation().distance(target.getLocation()));
                			String health;
                			double h = target.getHealth();
                			if (h >= 14.5) {
                				health = ChatColor.GREEN + "❤";
                			} else if (h > 8.6) {
                				health = ChatColor.YELLOW + "❤";
                			} else if (h > 3.6) {
                				health = ChatColor.RED + "❤";
                			} else {
                				health = ChatColor.DARK_RED + "❤";
                			}

                			String message = ChatColor.WHITE + "Target: " + Loader.getInstance().getTeamManager().getPlayerTeam(target.getUniqueId()).getColor() + target.getDisplayName() + ChatColor.GRAY + " ▪ " + ChatColor.WHITE + "Afstand: " + ChatColor.GREEN + ChatColor.BOLD + distanceBetween + " block(s)" + ChatColor.RESET + ChatColor.GRAY + " ▪ " + ChatColor.WHITE + "Health: " + health;
                			PacketAPI.sendActionBar(player, message);
            		}
        		}
    		}
    }

    @EventHandler
    public void onInteractDisbaledItem(PlayerInteractEvent event) {
    		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
    			Material m = event.getClickedBlock().getType();
    			if (m == Material.CHEST || m == Material.TRAP_DOOR || m == Material.FURNACE || m == Material.ENCHANTMENT_TABLE || m == Material.WORKBENCH) {
    				event.setCancelled(true);
    			}
    		}
    }

    @EventHandler
    public void onHunterDamage(EntityDamageEvent event) {
    		if (event.getEntity() instanceof Pig || event.getEntity() instanceof Cow || event.getEntity() instanceof Sheep || event.getEntity() instanceof Chicken) {
    			event.setCancelled(true);
    		}
    		if (GameState.getState() != GameState.IN_GAME) {
    			event.setCancelled(true);
    		}
    }
    
    @EventHandler
    public void onInteractArmorStand(PlayerArmorStandManipulateEvent event) {
    		event.setCancelled(true);
    }
    
    @EventHandler
    public void onSwapHand(PlayerSwapHandItemsEvent event) {
    		event.setCancelled(true);
    }
    
}
