package nl.dodocraft.game.knights.team;

import org.bukkit.Color;
import org.bukkit.boss.BarColor;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.objects.Gate;
import nl.dodocraft.game.knights.objects.Nexus;
import nl.dodocraft.game.knights.objects.Tower;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;

public enum Team {
	
	RED(1, "Rood", "Rode", ChatColor.RED, 14, Color.RED, 12, "&c", BarColor.RED),
	YELLOW(4, "Geel", "Gele", ChatColor.YELLOW, 4, Color.YELLOW, 9, "&e", BarColor.YELLOW),
	GREEN(3, "Groen", "Groene", ChatColor.GREEN, 13, Color.GREEN, 10, "&a", BarColor.GREEN),
	BLUE(2, "Blauw", "Blauwe", ChatColor.BLUE, 11, Color.BLUE, 11, "&9", BarColor.BLUE);
	
	private int id;
	private String name;
	private String bvName;
	private ChatColor color;
	private int blockData;
	private Color fireworkColor;
	private boolean active;
	private boolean dead;
	private int scoreboardLine;
	private String colorCode;
	private BarColor barColor;
	
	Team(int id, String name, String bvName, ChatColor color, int woolData, Color fireworkColor, int scoreboardLine, String colorCode, BarColor barColor) {
		this.id = id;
		this.name = name;
		this.bvName = bvName;
		this.color = color;
		this.blockData = woolData;
		this.fireworkColor = fireworkColor;
		this.scoreboardLine = scoreboardLine;
		this.colorCode = colorCode;
		this.barColor = barColor;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isActive() {
		return this.active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getBvName() {
		return this.bvName;
	}

	public ChatColor getColor() {
		return color;
	}

	public void setColor(ChatColor color) {
		this.color = color;
	}

	public int getBlockData() {
		return blockData;
	}

	public void setBlockData(int blockData) {
		this.blockData = blockData;
	}

	public Color getFireworkColor() {
		return fireworkColor;
	}

	public void setFireworkColor(Color fireworkColor) {
		this.fireworkColor = fireworkColor;
	}
	
	public Color getArmorColor() {
		return this.fireworkColor;
	}

	public String getColorCode() {
		return this.colorCode;
	}
	
	public List<Team> getOtherActiveTeams() {
		List<Team> otherTeams = new ArrayList<>();
		
		for (Team t : Team.values()) {
			if (t.getId() != this.getId() && t.isActive()) {
				otherTeams.add(t);
			}
		}
		
		return otherTeams;
	}

	public boolean isDead() {
		return this.dead;
	}
	
	public int getScoreBoardLine() {
		return this.scoreboardLine;
	}
	
	public BarColor getBarColor() {
		return this.barColor;
	}
	
	public String getScoreBoardObjectAndHealth() {
		String output = "";
		if (Loader.getInstance().getObjectsManager().allTowersAreDown(this)) {
			if (Loader.getInstance().getObjectsManager().oneOfTheGatesIsDead(this)) {
				Nexus n = Loader.getInstance().getObjectsManager().getNexuses().get(this);
				output = ChatColor.GRAY + "(N: " + n.getHealth() + ")";
			} else { //GATES ARE NOT DOWN
				Gate g = Loader.getInstance().getObjectsManager().getGates().get(this).get(0);
				output = ChatColor.GRAY + "(G: " + g.getHealth() + ")";
			}
		} else { //TOWERS ARE NOT DOWN
			Tower t;
			if (Loader.getInstance().getObjectsManager().getTowers().get(this).get(0).isDead()) {
				t = Loader.getInstance().getObjectsManager().getTowers().get(this).get(1); //BETA TOWER
			} else {
				t = Loader.getInstance().getObjectsManager().getTowers().get(this).get(0); //ALPHA TOWER
			}
			output = ChatColor.GRAY + "(" + t.getName().substring(0, 1) + ": " + t.getHealth() + ")";
		}
		return output;
	}
	
}
