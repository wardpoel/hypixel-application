package nl.dodocraft.game.knights.team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import dodonick.DodoNickHandler;
import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.utils.game.state.GameState;
import nl.dodocraft.utils.game.world.util.LocationSerializer;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.menu.DodoMenu;
import nl.dodocraft.utils.menu.MenuClick;
import nl.thiemoboven.ncore.SimpleCore;


public class TeamManager implements Listener {
	
	private HashMap<Team, List<Location>> teamSpawns = new HashMap<>();
	private HashMap<UUID, Team> playerTeam = new HashMap<>();	
	
	private Loader plugin;
	
	public TeamManager(Loader plugin) {
		this.plugin = plugin;
		for (Team t : Team.values()) { //spawns van team instellen
			teamSpawns.put(t, new ArrayList<>());
			if (plugin.getConfig().contains("spawns." + t.name())) {
                for (String string : plugin.getConfig().getConfigurationSection("spawns." + t.name()).getKeys(false)) {
                    Location loc = LocationSerializer.deserialize(plugin.getConfig().getString("spawns." + t.name() + "." + string));
                    List<Location> ta = teamSpawns.get(t);
                    ta.add(loc);
                    teamSpawns.put(t, ta);
                }
            }
		}
	}
	
	public HashMap<Team, List<Location>> getTeamSpawns() {
		return this.teamSpawns;
	}
	
	public Team getPlayerTeam(UUID uuid) {
		return this.playerTeam.get(uuid);
	}
	
	public HashMap<UUID, Team> getPlayerTeamHashMap() {
		return this.playerTeam;
	}

	public void createTeams() {
		for (Player p : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) {
			if (getPlayerTeam(p.getUniqueId()) == null) {
				Team teamToJoin = getMostEmptyTeam();
				if (!isFull(teamToJoin)) {
					playerTeam.put(p.getUniqueId(), teamToJoin);
				}
			}
		}
		setTeamsActiveWhenNotEmpty();
	}
	
	private void setTeamsActiveWhenNotEmpty() {
		for (Team team : Team.values()) {
			if (this.getTeamPlayers(team).isEmpty()) {
				team.setActive(false);
			} else {
				team.setActive(true);
			}
		}
	}

	public boolean areTeammates(UUID id1, UUID id2) {
		if (getPlayerTeam(id1) == getPlayerTeam(id2)) {
			return true;
		} else {
			return false;
		}
	}
	
	public List<UUID> getTeamPlayers(Team team) {
		List<UUID> list = new ArrayList<>();
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (getPlayerTeam(player.getUniqueId()) != null && getPlayerTeam(player.getUniqueId()) == team) {
				list.add(player.getUniqueId());
			}
		}
		return list;
	}
	
	public Location getTeamSpawn(Team team) {				
		if (!plugin.getObjectsManager().oneOfTheGatesIsDead(team)) { //spawn before gate
			return this.teamSpawns.get(team).get(0);
		} else { //spawn by nexus
			return this.teamSpawns.get(team).get(1);
		}
	}
	
	@EventHandler
	public void onOpenTeamSelector(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (GameState.getState() != GameState.IN_GAME) {
			if (event.getHand() == EquipmentSlot.HAND) {
				if (player.getInventory().getItemInMainHand() != null) {
					if (player.getInventory().getItemInMainHand().hasItemMeta() && player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName()) {
						if (player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.TEAM_SELECTOR)) {
							openTeamSelector(player);
						}
					}
				}
			}
		}
		
	}

	private void openTeamSelector(Player player) {
		DodoMenu menu = new DodoMenu("In welk team wil je?", 3);
		
		int teams;
		int maxTeamSize = Loader.getInstance().getConfig().getInt("teamsize");
		int[] slots = new int[Team.values().length];
		
		if (plugin.getGameType() == GameType.MINI) { //MINI
			teams = 4;
			slots[0] = 10;
			slots[1] = 12;
			slots[2] = 14;
			slots[3] = 16;
		} else { //MEGA
			teams = 2;
			slots[0] = 11;
			slots[1] = 15;
		}
		
		for (int i = 0; i < teams; i++) {
			Team team = Team.values()[i];
						
			ItemStack item = getTeamItemStack(team);
				
			menu.addMenuClick(item, new MenuClick() {

				@Override
				public boolean onItemClick(Player p) {
					if (getTeamPlayers(team) != null && getTeamPlayers(team).size() < maxTeamSize) { //Join team
						if (playerTeam.containsKey(p.getUniqueId())) {
							playerTeam.remove(p.getUniqueId());
						}
						playerTeam.put(p.getUniqueId(), team);
						p.sendMessage(ChatColor.GRAY + "Je zit nu in " + team.getColor() + "Team " + team.getName());
						openTeamSelector(player);
					} else { //Team is full;
						p.sendMessage(ChatColor.RED + "Team " + team.getColor() + team.getName() + ChatColor.RED + " is al vol.");
					}
					return false;
				}
				
			}, slots[i]);
		}
		
		
		menu.openMenu(player);
	}
	
	private ItemStack getTeamItemStack(Team team) {
		String teamName = team.getColor().toString() + ChatColor.BOLD + "Team " + team.getName();
		List<String> memberList = new ArrayList<>();
		ItemStack item;
		int maxTeamSize = Loader.getInstance().getConfig().getInt("teamsize");
				
		if (getTeamPlayers(team) != null && getTeamPlayers(team).size() >= maxTeamSize) { //Full
			memberList.add("");
			memberList.add(ChatColor.GRAY + "Teamgenoten (" + maxTeamSize + "/" + maxTeamSize + "):");
			for (UUID uuid : getTeamPlayers(team)) {
				memberList.add(ChatColor.DARK_GRAY + " ▪ " + getPlayerNameWithRankColor(uuid));
			}
			memberList.add("");
			memberList.add(ChatColor.RED + "Dit team zit al vol.");
			item = new ItemBuilder(Material.SULPHUR).setName(teamName).addLoreLines(memberList).toItemStack();
		} else { //Not full
			memberList.add("");
			if (getTeamPlayers(team) != null && getTeamPlayers(team).size() == 1) {
				memberList.add(ChatColor.GRAY + "Teamgenoten (" + getTeamPlayers(team).size() + "/" + maxTeamSize + "):");
			}
			if (getTeamPlayers(team).isEmpty()) {
				memberList.add(ChatColor.GRAY + "Er zit nog niemand in dit team.");
			} else {
				for (UUID uuid : getTeamPlayers(team)) {
					memberList.add(ChatColor.DARK_GRAY + " ▪ " + getPlayerNameWithRankColor(uuid));
				}
			}
			memberList.add("");
			memberList.add(ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om dit team te joinen.");
			item = new ItemBuilder(Material.STAINED_CLAY).setName(teamName).setDurability((short) team.getBlockData()).addLoreLines(memberList).toItemStack();
		}
		return item;
	}
	
	private String getPlayerNameWithRankColor(UUID uuid) {
		Rank rank = DodoCore.getInstance().getRank(uuid);
        boolean isDisguised = DodoNickHandler.getInstance().isDisguised(uuid);
        if (isDisguised) {
            rank = Rank.NORMAL;
        }
        return rank.getColorPrefix().toString() + (rank.getId() != Rank.NORMAL.getId() ? ChatColor.BOLD : "") + Bukkit.getPlayer(uuid).getName();
	}
	
	public boolean isFull(Team team) {
		int maxSize = plugin.getConfig().getInt("teamsize");
		if (this.getTeamPlayers(team).size() != maxSize) {
			return false;
		}
		return true;
	}
	
	public Team getMostEmptyTeam() {
		Team mostEmptyTeam = null;
		for (Team t : Team.values()) {
			if (mostEmptyTeam == null || plugin.getTeamManager().getTeamPlayers(t).size() < plugin.getTeamManager().getTeamPlayers(mostEmptyTeam).size()) {
				mostEmptyTeam = t;
			}
		}
		return mostEmptyTeam;
	}
		
}

















