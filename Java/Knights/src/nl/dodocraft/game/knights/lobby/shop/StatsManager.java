package nl.dodocraft.game.knights.lobby.shop;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;

public class StatsManager {

	private Player player;
	private String kitName;

	public StatsManager(String kitName, Player player) {
		this.player = player;
		this.kitName = kitName;
	}

	private String getName() {
		return this.kitName;
	}

	public double getProcess() {
		String abilityString = this.getName().toUpperCase() + "_ABILITY";
		String passive1String = this.getName().toUpperCase() + "_PASSIVE1";
		int levelAbility = Loader.getInstance().getLobbyManager().getFixedLevel(player, abilityString);
		int levelPassive1 = Loader.getInstance().getLobbyManager().getFixedLevel(player, passive1String);
		int sum = levelAbility + levelPassive1;
		if (sum == 14) {
			sum = 13;
		} else {
			sum = sum - 2;
		}
		return (sum / 13.0) * 100;
	}
	
	public String getProcessField() {
		double process = getProcess();
		String showProcess = "";
		if (process == 100.0) {
			showProcess = ChatColor.GOLD.toString() + ChatColor.BOLD + "MASTER";
		} else {
			showProcess += ChatColor.DARK_GRAY + "[";
    			for (int i = 0; i < 20; i++) {
    				if (i < (process / 5)) {
    					showProcess += ChatColor.GREEN + "|";
    				} else {
    					showProcess += ChatColor.GRAY + "|";
    				}
    			}
    			showProcess += ChatColor.DARK_GRAY + "]";
    			showProcess += ChatColor.GREEN.toString() + ChatColor.BOLD + " " + round(process, 2) + "%";
		}
		return showProcess;
	}
	
	public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

	public int getHoursPlayed() {
		int hours = 0;
		String type = this.getName().toUpperCase() + "_HOURS";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			hours = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return hours / 3600;
	}

	public int getTimesPlayed() {
		int times = 0;
		String type = this.getName().toUpperCase() + "_TIMES_PLAYED";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			times = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return times;
	}

	public int getKills() {
		int kills = 0;
		String type = this.getName().toUpperCase() + "_KILLS";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			kills = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return kills;
	}

	public int getAssists() {
		int assists = 0;
		String type = this.getName().toUpperCase() + "_ASSISTS";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			assists = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return assists;
	}

	public int getDamage() {
		int damage = 0;
		String type = this.getName().toUpperCase() + "_DAMAGE";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			damage = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return damage;
	}

	public int getHealing() {
		int healing = 0;
		String type = this.getName().toUpperCase() + "_HEALING";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			healing = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return healing;
	}

	public int getNexusDamage() {
		int nexusDamage = 0;
		String type = this.getName().toUpperCase() + "_NEXUS_DAMAGE";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			nexusDamage = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return nexusDamage;
	}

	public int getGateDamage() {
		int gateDamage = 0;
		String type = this.getName().toUpperCase() + "_GATE_DAMAGE";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			gateDamage = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return gateDamage;
	}

	public int getAbilities() {
		int abilities = 0;
		String type = this.getName().toUpperCase() + "_ABILITIES";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			abilities = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return abilities;
	}

	public double getKillDeathRatio() {
		int kills = getKills();

		int deaths = 0;
		String type = this.getName().toUpperCase() + "_DEATHS";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			deaths = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}

		double output = ((double) kills) / deaths;
		if (deaths == 0) {
			output = Double.NaN;
		}
		return output;
	}

	public double getEKillDeathRatio() {
		int kills = 0;
		String type = this.getName().toUpperCase() + "_EKILLS";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			kills = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}

		int deaths = 0;
		type = this.getName().toUpperCase() + "_EDEATHS";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			deaths = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}

		double output = ((double) kills) / deaths;
		if (deaths == 0) {
			output = Double.NaN;
		}
		return output;
	}

	public int getWins() {
		int wins = 0;
		String type = this.getName().toUpperCase() + "_WINS";
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) != null) {
			wins = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
		}
		return wins;
	}
	
    public String[] getInfoLore() {    		
    		double winLossRatio = ((double)this.getWins()) / (this.getTimesPlayed() - this.getWins());
    		if ((this.getTimesPlayed() - this.getWins()) == 0) {
    			winLossRatio = Double.NaN;
    		}
    		String[] lore = {
    			"",
    			ChatColor.GRAY + "Upgrade progressie:",
    			this.getProcessField(),
    			"",
//    			ChatColor.GRAY + "Uren gespeeld: " + ChatColor.AQUA.toString() + ChatColor.BOLD + format(this.getHoursPlayed()),
    			ChatColor.GRAY + "Aantal keer gespeeld: " + ChatColor.AQUA.toString() + ChatColor.BOLD + format(this.getTimesPlayed()),
    			"",
    			ChatColor.GRAY + "Kill/Death ratio: " + (this.getKillDeathRatio() >= 1 ? ChatColor.GREEN.toString() : ChatColor.RED.toString()) + (Double.isNaN(this.getKillDeathRatio()) ? "N/A" : ChatColor.BOLD.toString() + format(this.getKillDeathRatio(), 2)),
    			ChatColor.GRAY + "Eliminatie K/D ratio: " + (this.getEKillDeathRatio() >= 1 ? ChatColor.GREEN.toString() : ChatColor.RED.toString()) + (Double.isNaN(this.getEKillDeathRatio()) ? "N/A" : ChatColor.BOLD.toString() + format(this.getEKillDeathRatio(), 2)),
    			ChatColor.GRAY + "Win/Loss ratio: " + (winLossRatio >= 1 ? ChatColor.GREEN.toString() : ChatColor.RED.toString()) + (Double.isNaN(winLossRatio) ? "N/A" : ChatColor.BOLD.toString() + format(winLossRatio, 2)),
    			"",
    			ChatColor.GRAY + "Kills: " + ChatColor.AQUA.toString() + ChatColor.BOLD + format(this.getKills()),
    			ChatColor.GRAY + "Assists: " + ChatColor.AQUA.toString() + ChatColor.BOLD + format(this.getAssists()),
    			ChatColor.GRAY + "Wins: " + ChatColor.AQUA.toString() + ChatColor.BOLD + format(this.getWins()),
    			ChatColor.GRAY + "Abilities: " + ChatColor.AQUA.toString() + ChatColor.BOLD + format(this.getAbilities()),
    			ChatColor.GRAY + "Damage: " + ChatColor.RED.toString() + ChatColor.BOLD + format(this.getDamage()),
    			ChatColor.GRAY + "Healing: " + ChatColor.GREEN.toString() + ChatColor.BOLD + format(this.getHealing()),
    			"",
    			ChatColor.GRAY + "Nexus damage: " + ChatColor.AQUA.toString() + ChatColor.BOLD + format(this.getNexusDamage()),
    			ChatColor.GRAY + "Gate damage: " + ChatColor.AQUA.toString() + ChatColor.BOLD + format(this.getGateDamage())
    		};
    		return lore;
    }
    
    private String format(double value, int decimals) {
    		if (value < 1000) {
			return "" + String.format("%." + decimals + "f", value);
		}
		int exp = (int) (Math.log(value) / Math.log(1000));
	    return String.format("%." + decimals + "f%c", value / Math.pow(1000, exp), "kMGTPEZYB".charAt(exp-1));
    }
    
    private String format(double value) {
    		return format(value, 0);
    }

}
