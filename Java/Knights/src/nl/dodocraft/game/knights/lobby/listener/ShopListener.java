package nl.dodocraft.game.knights.lobby.listener;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import dodonick.ChannelHandler;
import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.kits.KitTemplate;
import nl.thiemoboven.methods.Method;
import nl.thiemoboven.methods.Methods;

public class ShopListener implements Listener {
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		UUID id = player.getUniqueId();
		int stars = 0;
		for (KitTemplate k : Loader.getInstance().getLobbyManager().registeredKits) {
			if (DodoCore.getInstance().getCachedPlayerData(id, new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID()), true) != null) {
				int level = DodoCore.getInstance().getCachedPlayerData(id, new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID()), true);
				if (level == 7) {
					stars++;
				}
			}
		}
		
		if (stars > 0) {
			Rank rank = DodoCore.getInstance().getRank(player.getUniqueId());
			String prefix = ChatColor.GRAY + "[" + ChatColor.GOLD.toString();
			if (stars < 4) {
				for (int i = 0; i < stars; i++) {
					prefix += "✮";
				}
			} else {
				prefix += stars + "✮";
			}
			prefix += ChatColor.GRAY + "] " + rank.getColorPrefix();
			
			player.setPlayerListName(prefix + player.getName());
			ChannelHandler.refresh(false);
		}
	}
}













