package nl.dodocraft.game.knights.lobby.shop;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.kits.KitTemplate;
import nl.dodocraft.lobby.shop.GameShop;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.menu.DodoMenu;
import nl.dodocraft.utils.menu.MenuClick;
import nl.sandergielisse.coreclient.purchases.PurchaseResultCallback;
import nl.thiemoboven.methods.Method;
import nl.thiemoboven.methods.Methods;
import nl.thiemoboven.packetcollection.packets.transaction.PurchaseResult;

public class KnightsShop extends GameShop {

	private List<UUID> cooldown = new ArrayList<>();
	
	@Override
	public String getName() {
		return "Knights Shop";
	}
	
	private void openShop(Player player) {
		int[] slots = new int[] {11, 12, 13, 14, 15};
		DodoMenu shop = new DodoMenu(getName(), 5);
		
		int index = 0;
		for (KitTemplate k : Loader.getInstance().getLobbyManager().registeredKits) {
			ItemBuilder builder;
			if (k.getName().equalsIgnoreCase("Elektron")) {
				builder = k.getShopItem(player);
				if (!hasElektron(player)) {
					builder.addLoreLines(new String[] {"", ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om " + k.getName() + " te unlocken."});
				} else {
					builder.addLoreLines(new String[] {"", ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om " + k.getName() + " te upgraden."});
				}
			} else {
				builder = k.getShopItem();
				builder.addLoreLines(new String[] {"", ChatColor.GREEN.toString() + ChatColor.BOLD + "Klik " + ChatColor.GRAY + "om " + k.getName() + " te upgraden."});
			}
			shop.addMenuClick(builder.toItemStack(), new MenuClick() {

				@Override
				public boolean onItemClick(Player p) {
					p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 5F, 5F);
					if (k.getName().equalsIgnoreCase("Elektron") && !hasElektron(p)) {
						unlockElektron(p, k);
					} else {
						openKitShop(p, k);
					}
					return true;
				}
				
			}, slots[index]);
			index++;
		}
		
		shop.addMenuClick(new ItemBuilder(Material.DOUBLE_PLANT).setName(ChatColor.GOLD.toString() + ChatColor.BOLD + getPlayerMoney(player) + " Coins").addLoreLines(new String[]{ChatColor.GRAY + "Heb je niet genoeg coins?", ChatColor.GRAY + "Koop een Coins Booster op", ChatColor.GREEN + "shop.dodocraft.nl" + ChatColor.GRAY + " om de", ChatColor.GRAY + "inkomsten van iedereen te", ChatColor.GRAY + "verhogen! In ruil daarvoor krijg", ChatColor.GRAY + "jij" + ChatColor.GOLD.toString() + ChatColor.BOLD + " duizenden coins" + ChatColor.GRAY + "!"}).toItemStack(), new MenuClick() {
			
			@Override
			public boolean onItemClick(Player player) {
				return true;
			}
			
		}, 31);
		
		shop.openMenu(player);
	}
	
	protected void unlockElektron(Player p, KitTemplate k) {
		/*Get coins*/
		int coins = getPlayerMoney(p);
		
		LobbyManager.ElektronUpgradePrices unlock = LobbyManager.ElektronUpgradePrices.LEVEL_1;
		
		if (coins >= unlock.price) { //CAN UNLOCK
			DodoMenu unlockElektron = new DodoMenu("Elektron unlocken?", 3);
			unlockElektron.addMenuClick(new ItemBuilder(Material.STAINED_CLAY).setDurability((byte) 14).setName(ChatColor.RED.toString() + ChatColor.BOLD + "Annuleren").addLoreLines(new String[]{
                    "",
                    "&7Als je Elektron niet",
                    "&7wil vrijspelen kan je op dit",
                    "&7item klikken. Je annuleert",
                    "&7dan de betaling."
            }).toItemStack(), new MenuClick() {
                @Override
                public boolean onItemClick(Player player) {
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 5F, 0F);
                    openShop(player);
                    return true;
                }
            }, 15);
			
			unlockElektron.addMenuClick(new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 5).setName(ChatColor.GREEN.toString() + ChatColor.BOLD + "Bevestigen").hideAttributes().toItemStack(), new MenuClick() {
				@Override
				public boolean onItemClick(Player p) {
					if (!cooldown.contains(p.getUniqueId())) {
						DodoCore.getInstance().changeBalance(p.getUniqueId(), "KNIGHTCOINS", -unlock.price, new PurchaseResultCallback() {
							@Override
							public void call(PurchaseResult purchaseResult) {
								if (purchaseResult == PurchaseResult.SUCCESS) {
									p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5F, 5F);
									String message = ChatColor.GREEN.toString() + ChatColor.BOLD + "Unlock! " + ChatColor.GREEN + "Je hebt " + ChatColor.AQUA + ChatColor.BOLD + "Elektron" + ChatColor.GREEN + " geunlocked.";
									DodoCore.getInstance().setPlayerData(p.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_ABILITY", 1);
									DodoCore.getInstance().setPlayerData(p.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_PASSIVE1", 1);
									DodoCore.getInstance().unlockPlayerKit(p.getUniqueId(), "KNIGHTS", k.getStartID(), 1);
									DodoCore.getInstance().setPlayerData(p.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", 1);
									p.sendMessage(message);
									p.closeInventory();
								}
							}
						}, true);
					} else {
						p.sendMessage(ChatColor.RED + "Er is nog een aankoop bezig...");
					}
					return true;
				}
				
			}, 11);
			unlockElektron.openMenu(p);
		} else { //CANT UNLOCK
			p.sendMessage(ChatColor.RED + "Je voldoet niet aan de benodigdheden om " + ChatColor.AQUA + ChatColor.BOLD + "Elektron" + ChatColor.RED + " te unlocken.");
		}
	}

	private void openKitShop(Player player, KitTemplate k) {
		DodoMenu kitShop = new DodoMenu(k.getName(), 4);
		
		int abilityLevel = getCurrentAbilityLevel(player, k);				
		int passiveLevel = getCurrentPassiveLevel(player, k);
		
		if (DodoCore.getInstance().getRank(player.getUniqueId()).getId() >= Rank.ADMIN.getId()) {//allowCheat
			if (abilityLevel < 6) {
				abilityLevel = 6;
			}
			if (passiveLevel < 6) {
				passiveLevel = 6;
			}
		}
		
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(k.getName().toUpperCase(), abilityLevel);
		LobbyManager.Passives passive = LobbyManager.Passives.getPassive(k.getName().toUpperCase(), passiveLevel);
		
		int[] slots = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25};
		
		kitShop.addMenuClick(k.getSkullItem().addLoreLines(k.getInfoLore(player)).toItemStack(), new MenuClick() { //KIT SKULL
			@Override
			public boolean onItemClick(Player p) {
				return true;
			}
		}, 0);
		
		int index = 0;
		kitShop.addMenuClick(new ItemBuilder(Material.IRON_SWORD).hideAttributes().setName(ChatColor.GRAY.toString() + "Ability: " + ChatColor.GREEN + ChatColor.BOLD + ability.display).addLoreLines(k.getAbilityLore(abilityLevel)).toItemStack(), new MenuClick() {
			@Override
			public boolean onItemClick(Player p) {
				return true;
			}
		}, slots[index]);
		
		index++;
		
		for (int i = 1; i <= 6; i++) { //ABILITY'S
			short dur;
			String color;
			if (abilityLevel >= i) { dur = 5; color = ChatColor.GREEN.toString(); } else if (abilityLevel+1 == i) { dur = 4; color = ChatColor.YELLOW.toString(); } else { dur = 7; color = ChatColor.RED.toString(); }
			String [] lores = k.getLevelLoreAbility(player, i);
			kitShop.addMenuClick(new ItemBuilder(Material.STAINED_CLAY).setName(color + ChatColor.BOLD + ability.display + " "+ toRomain(i)).setDurability(dur).addLoreLines(lores).toItemStack(), new MenuClick() {
				final int finalDur = (int) dur;
			
				@Override
				public boolean onItemClick(Player p) {
					if (finalDur == 4) { //ABILITY TO BUY
						buyUpgrade(p, k, "ABILITY");
					} else if (finalDur == 5) {
						p.sendMessage(ChatColor.GRAY + "Je hebt dit level al gekocht.");
					} else {
						p.sendMessage(ChatColor.RED + "Je kan dit level nog niet kopen.");
					}
					return true;
				}
				
			}, slots[index]);
			index++;
		}
		
		kitShop.addMenuClick(new ItemBuilder(Material.MAGMA_CREAM).setName(ChatColor.GRAY.toString() + "Passive: " + ChatColor.GREEN + ChatColor.BOLD + passive.display).addLoreLines(k.getPassiveLore(passiveLevel)).toItemStack(), new MenuClick() {
			@Override
			public boolean onItemClick(Player p) {
				return true;
			}
		}, slots[index]);
		
		index++;
		
		for (int i = 1; i <= 6; i++) { //PASSIVES
			short dur;
			String color;
			if (passiveLevel >= i) { dur = 5; color = ChatColor.GREEN.toString(); } else if (passiveLevel+1 == i) { dur = 4; color = ChatColor.YELLOW.toString(); } else { dur = 7; color = ChatColor.RED.toString(); }
			String [] lores = k.getLevelLorePassive(player, i);
			kitShop.addMenuClick(new ItemBuilder(Material.STAINED_CLAY).setName(color + ChatColor.BOLD + passive.display + " "+ toRomain(i)).setDurability(dur).addLoreLines(lores).toItemStack(), new MenuClick() {
				final int finalDur = (int) dur;
				@Override
				public boolean onItemClick(Player p) {
					if (finalDur == 4) { //PASSIVE TO BUY
						buyUpgrade(p, k, "PASSIVE1");
					} else if (finalDur == 5) {
						p.sendMessage(ChatColor.GRAY + "Je hebt dit level al gekocht.");
					} else {
						p.sendMessage(ChatColor.RED + "Je kan dit level nog niet kopen.");
					}
					return true;
				}
				
			}, slots[index]);
			index++;
		}
		
		//BACK TO OVERVIEW (LobbyShop)
		kitShop.addMenuClick(new ItemBuilder(Material.ARROW).setName(ChatColor.RED.toString() + ChatColor.BOLD + "◄ Ga terug").toItemStack(), new MenuClick() {
			@Override
			public boolean onItemClick(Player p) {
				openShop(p);
				return true;
			}
		}, 28);

		//MASTER
		kitShop.addMenuClick(new ItemBuilder(Material.SKULL_ITEM, 1, (byte)3).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTc1MWIyYzMxMWFjZDQ4ZTA2ZjcxNTA4YzY4NzM1ZmU5ZjNhNGY0ZDc1YjllZjFhMjRmYTEzMzNkNzcifX19").setName(ChatColor.GOLD.toString() + ChatColor.BOLD + k.getName() + " Master").addLoreLines(k.getMasterLore(player)).toItemStack(), new MenuClick() {
			@Override
			public boolean onItemClick(Player p) {
				if (checkCanBuyMaster(p, k)) {
					buyMaster(p, k);
				} else {
					p.sendMessage(ChatColor.RED + "Je moet eerst je ability en passive maximaal upgraden, voordat je Master kan worden");
				}
				return true;
			}
		}, 31);
		
		kitShop.openMenu(player);
		
	}

	

	/*private String[] getPassiveLores(short dur, int abilityLevel) {
		return new String[] {};
	}

	private String[] getAbilityLores(short dur, int abilityLevel, KitTemplate k) {
		LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(k.getName().toUpperCase(), abilityLevel);
		
		if (abilityLevel <= 6) {
			LobbyManager.Abilities nextAbility = LobbyManager.Abilities.getAbility(k.getName().toUpperCase(), abilityLevel + 1);
			return new String[] {
					"",
					ChatColor.GRAY + ability.display,
					ChatColor.DARK_GRAY + "  ▪ " + ability.damage + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextAbility.damage + ChatColor.GRAY + " HP",
					"",
					
			};
		} else {
			return null;
		}
	}*/
	
	private String getBezitsVormName(String name) {
		if (name.endsWith("s")) {
			return name + "\'";
		} else if (name.endsWith("a") || name.endsWith("e") || name.endsWith("u") || name.endsWith("i") || name.endsWith("o")) {
			return name + "\'s";
		} else {
			return name + "s";
		}
	}

	protected void buyMaster(Player player, KitTemplate k) {
		int levelToBuy = 7;
		int money = getPlayerMoney(player);
		int priceOfUpgrade = LobbyManager.Prices.values()[levelToBuy - 1].getPrice();
		if (DodoCore.getInstance().getCachedPlayerData(player.getUniqueId(), new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID()), true) != null) {
			int level = DodoCore.getInstance().getCachedPlayerData(player.getUniqueId(), new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID()), true);
			if (level == 7) {
				player.sendMessage(ChatColor.RED.toString() + "Je hebt " + getBezitsVormName(k.getName()) + " Master level al bereikt.");
				return;
			}
		}
		if (money >= priceOfUpgrade) {// CAN BUY
			DodoMenu areYouSureYouWantToBuyThisMenu = new DodoMenu("Aankoop", 3);
			areYouSureYouWantToBuyThisMenu.addMenuClick(new ItemBuilder(Material.STAINED_CLAY).setDurability((byte) 14).setName(ChatColor.RED.toString() + ChatColor.BOLD + "Annuleren").addLoreLines(new String[]{
                    "",
                    "&7Als je deze upgrade niet",
                    "&7wil hebben kan je op dit",
                    "&7item klikken. Je annuleert",
                    "&7dan de betaling."
            }).toItemStack(), new MenuClick() {
                @Override
                public boolean onItemClick(Player player) {
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 5F, 0F);
                    openKitShop(player, k);
                    return true;
                }
            }, 15);
			
			areYouSureYouWantToBuyThisMenu.addMenuClick(new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 5).setName(ChatColor.GREEN.toString() + ChatColor.BOLD + "Bevestigen").hideAttributes().toItemStack(), new MenuClick() {
				
				@Override
				public boolean onItemClick(Player p) {
					if (!cooldown.contains(p.getUniqueId())) {
						DodoCore.getInstance().changeBalance(p.getUniqueId(), "KNIGHTCOINS", -priceOfUpgrade, new PurchaseResultCallback() {
							@Override
							public void call(PurchaseResult purchaseResult) {
								if (purchaseResult == PurchaseResult.SUCCESS) {
									Bukkit.getServer().broadcastMessage(ChatColor.AQUA + "✮   ✮   " + ChatColor.GREEN.toString() + ChatColor.BOLD + p.getName() + ChatColor.YELLOW + " is nu een " + ChatColor.GOLD.toString() + ChatColor.BOLD + k.getName() + " Master" + ChatColor.YELLOW + "!" + ChatColor.AQUA + "   ✮   ✮");
									p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5F, 5F);
									DodoCore.getInstance().setPlayerData(p.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_ABILITY", levelToBuy);
									DodoCore.getInstance().setPlayerData(p.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_PASSIVE1", levelToBuy);
									DodoCore.getInstance().unlockPlayerKit(p.getUniqueId(), "KNIGHTS", k.getStartID(), levelToBuy);
									p.closeInventory();
								}
							}
						}, true);
					} else {
						p.sendMessage(ChatColor.RED + "Er is nog een aankoop bezig...");
					}
					return true;
				}
			}, 11);
			
			areYouSureYouWantToBuyThisMenu.openMenu(player);
		} else {
			player.sendMessage(ChatColor.RED + "Je hebt niet genoeg coins om deze upgrade te kopen!");
		}
	}
	
	protected boolean checkCanBuyMaster(Player p, KitTemplate k) {
		int abilityLevel = getCurrentAbilityLevel(p, k);				
		int passiveLevel = getCurrentPassiveLevel(p, k);
		
		if (DodoCore.getInstance().getRank(p.getUniqueId()).getId() >= Rank.ADMIN.getId()) {//allowCheat
			if (abilityLevel < 6) {
				abilityLevel = 6;
			}
			if (passiveLevel < 6) {
				passiveLevel = 6;
			}
		}
		
		if (abilityLevel >= 6 && passiveLevel >= 6) {
			return true;
		}
		return false;
	}

	private String toRomain(int v) {
        if (v == 1) {
            return "I";
        } else if (v == 2) {
            return "II";
        } else if (v == 3) {
            return "III";
        } else if (v == 4) {
            return "IV";
        } else if (v == 5) {
            return "V";
        } else if (v == 6) {
            return "VI";
        } else if (v == 7) {
            return "VII";
        } else if (v == 8) {
            return "VIII";
        } else if (v == 9) {
            return "IX";
        } else if (v == 10) {
            return "X";
        } else
            return "";
    }

	protected void buyUpgrade(Player player, KitTemplate k, String type) {
		int levelToBuy;
		if (type.equalsIgnoreCase("ABILITY")) {
			levelToBuy = getCurrentAbilityLevel(player, k) + 1;
		} else {
			levelToBuy = getCurrentPassiveLevel(player, k) + 1;
		}
		int money = getPlayerMoney(player);
		int priceOfUpgrade = LobbyManager.Prices.values()[levelToBuy - 1].getPrice();
		if (money >= priceOfUpgrade) {// CAN BUY
			DodoMenu areYouSureYouWantToBuyThisMenu = new DodoMenu("Aankoop", 3);
			areYouSureYouWantToBuyThisMenu.addMenuClick(new ItemBuilder(Material.STAINED_CLAY).setDurability((byte) 14).setName(ChatColor.RED.toString() + ChatColor.BOLD + "Annuleren").addLoreLines(new String[]{
                    "",
                    "&7Als je deze upgrade niet",
                    "&7wil hebben kan je op dit",
                    "&7item klikken. Je annuleert",
                    "&7dan de betaling."
            }).toItemStack(), new MenuClick() {
                @Override
                public boolean onItemClick(Player player) {
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 5F, 0F);
                    openKitShop(player, k);
                    return true;
                }
            }, 15);
			
			areYouSureYouWantToBuyThisMenu.addMenuClick(new ItemBuilder(Material.STAINED_CLAY).setDurability((short) 5).setName(ChatColor.GREEN.toString() + ChatColor.BOLD + "Bevestigen").hideAttributes().toItemStack(), new MenuClick() {
				
				@Override
				public boolean onItemClick(Player p) {
					if (!cooldown.contains(p.getUniqueId())) {
						DodoCore.getInstance().changeBalance(p.getUniqueId(), "KNIGHTCOINS", -priceOfUpgrade, new PurchaseResultCallback() {
							@Override
							public void call(PurchaseResult purchaseResult) {
								if (purchaseResult == PurchaseResult.SUCCESS) {
									p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5F, 5F);
									TextComponent message = new TextComponent(ChatColor.GREEN.toString() + ChatColor.BOLD + "Upgrade! ");
									if (type.equalsIgnoreCase("ABILITY")) {
										int abilityLevel = getCurrentAbilityLevel(player, k); //OLD LEVEL
										LobbyManager.Abilities ability = LobbyManager.Abilities.getAbility(k.getName().toUpperCase(), abilityLevel);
										LobbyManager.Abilities nextAbility = LobbyManager.Abilities.getAbility(k.getName().toUpperCase(), levelToBuy);
										message.addExtra(ChatColor.GRAY + ability.display + " " + toRomain(abilityLevel) + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextAbility.display + " " + toRomain(levelToBuy));
									} else {
										int passiveLevel = getCurrentPassiveLevel(player, k);
										LobbyManager.Passives passive = LobbyManager.Passives.getPassive(k.getName().toUpperCase(), passiveLevel);
										LobbyManager.Passives nextPassive = LobbyManager.Passives.getPassive(k.getName().toUpperCase(), levelToBuy);
										message.addExtra(ChatColor.GRAY + passive.display + " " + toRomain(passiveLevel) + ChatColor.DARK_GRAY + " ➜ " + ChatColor.GREEN + nextPassive.display + " " + toRomain(levelToBuy));
									}
									DodoCore.getInstance().setPlayerData(p.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_" + type.toUpperCase(), levelToBuy);
									DodoCore.getInstance().unlockPlayerKit(p.getUniqueId(), "KNIGHTS", k.getStartID(), levelToBuy);
									message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Hoi\nLeuk").create()));
									p.spigot().sendMessage(message);
									p.closeInventory();
								}
							}
						}, true);
					} else {
						p.sendMessage(ChatColor.RED + "Er is nog een aankoop bezig...");
					}
					return true;
				}
				
			}, 11);
			
			areYouSureYouWantToBuyThisMenu.openMenu(player);
		} else {
			player.sendMessage(ChatColor.RED + "Je hebt niet genoeg coins om deze upgrade te kopen!");
		}
	}
	
	private int getCurrentAbilityLevel(Player player, KitTemplate k) {
		int abilityLevel = 1;
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_ABILITY", Integer.class) != null && DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_ABILITY", Integer.class) > 0) {
			abilityLevel = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_ABILITY", Integer.class);
		}
		return abilityLevel;
	}
	
	private int getCurrentPassiveLevel(Player player, KitTemplate k) {
		int passiveLevel = 1;
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_PASSIVE1", Integer.class) != null && DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_PASSIVE1", Integer.class) > 0) {
			passiveLevel = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", k.getName().toUpperCase() + "_PASSIVE1", Integer.class);
		}
		return passiveLevel;
	}
	
	private int getPlayerMoney(Player player) {		
		int money = 0;
		if (DodoCore.getInstance().getCachedBalance(player.getUniqueId(), "KNIGHTCOINS") > 0) {
			money = DodoCore.getInstance().getCachedBalance(player.getUniqueId(), "KNIGHTCOINS");
		}
		return money;
	}

	@Override
	public String[] getLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onOpen(Player player) {
		openShop(player);
	}
	
	private boolean hasElektron(Player player) {
		int unlocked = 0; //0 -> false; 1 -> true
		if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", Integer.class) != null) {
			unlocked = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", Integer.class);
		} else {
			DodoCore.getInstance().setPlayerData(player.getUniqueId(), "KNIGHTS", "ELEKTRON_UNLOCKED", 0);
		}
		if (unlocked == 1 || DodoCore.getInstance().getRank(player.getUniqueId()).getId() >= Rank.ADMIN.getId()) {
			return true;
		}
		return false;
}
	
}











