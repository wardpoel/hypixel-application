package nl.dodocraft.game.knights.lobby.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;

import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.AbilityUtils;
import nl.dodocraft.game.knights.kits.Kit;
import nl.dodocraft.game.knights.kits.KitTemplate;
import nl.dodocraft.game.knights.kits.content.*;
import nl.dodocraft.lobby.Main;
import nl.thiemoboven.methods.Method;
import nl.thiemoboven.methods.Methods;

public class LobbyManager {

	 private final Loader plugin;
	 private final AbilityUtils abilityUtils;
	 public final List<KitTemplate> registeredKits = new ArrayList<>();
	
	public LobbyManager(Loader plugin) {
		this.plugin = plugin;
        this.abilityUtils = new AbilityUtils();

        registeredKits.add(new KitPirate());
        registeredKits.add(new KitMender());
        registeredKits.add(new KitHunter());
        registeredKits.add(new KitNinja());
        registeredKits.add(new KitElektron());
        

        for (KitTemplate k : this.registeredKits) {
            DodoCore.getInstance().cachePlayerData(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID());
            DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "KNIGHTS", k.getName().toUpperCase() + "_ABILITY");
            DodoCore.getInstance().cachePlayerData(Methods.CUSTOM_PLAYER_DATA, "KNIGHTS", k.getName().toUpperCase() + "_PASSIVE1");
        }
        
        Main.getInstance().getShopManager().registerShop(new KnightsShop());
	}
	
	public int getFixedLevel(Player player, String type) {
        boolean allowCheat = false;
        Rank rank = DodoCore.getInstance().getRank(player.getUniqueId());
        if (rank != null && (rank == Rank.YOUTUBER || rank.getId() >= Rank.ADMIN.getId())) {
            allowCheat = true;
        }
        if (DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class) == null) {
            return allowCheat ? 6 : 1;
        }
        int value = DodoCore.getInstance().getPlayerData(player.getUniqueId(), "KNIGHTS", type, Integer.class);
        return allowCheat && value < 6 ? 6 : value;
    }

    public List<Kit> getUnlockedKits(Player p) {
        UUID id = p.getUniqueId();

        List<Kit> list = new ArrayList<>();

        boolean allowCheat = false;

        Rank rank = DodoCore.getInstance().getRank(p.getUniqueId());
        if (rank != null && (rank == Rank.YOUTUBER || rank.getId() >= Rank.ADMIN.getId())) {
            allowCheat = true;
        }

        for (KitTemplate k : this.registeredKits) {
            if (DodoCore.getInstance().getCachedPlayerData(id, new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID()), true) != null) {	
                int level = DodoCore.getInstance().getCachedPlayerData(p.getUniqueId(), new Method<>(Methods.GET_KIT_LEVEL, "KNIGHTS", k.getStartID()), true);
                if (allowCheat) {
                		if (level < 6) {
                        level = 6;
                    }
                }
                list.add(Kit.byId(this.plugin, k.getStartID(), level));
            }
        }
        return list;
    }
    
    public enum Prices {
		LEVEL_1(0, "Gratis"),
		LEVEL_2(5000, "5.000 Knight Coins"),
		LEVEL_3(25000, "25.000 Knight Coins"),
		LEVEL_4(50000, "50.000 Knight Coins"),
		LEVEL_5(100000, "100.000 Knight Coins"),
		LEVEL_6(250000, "250.000 Knight Coins"),
		LEVEL_7(1000000, "1.000.000 Knight Coins");
		
		private int price;
		private String stringPrice;
		
		Prices(int price, String stringPrice) {
			this.price = price;
			this.stringPrice = stringPrice;
		}
		
		public int getPrice() {
			return this.price;
		}
		
		public String getStringPrice() {
			return this.stringPrice;
		}
		
}

public enum ElektronUpgradePrices {
		LEVEL_1(15000, "15.000 Knight Coins"),
		LEVEL_2(25000, "25.000 Knight Coins"),
		LEVEL_3(50000, "50.000 Knight Coins"),
		LEVEL_4(75000, "75.000 Knight Coins"),
		LEVEL_5(150000, "150.000 Knight Coins"),
		LEVEL_6(300000, "300.000 Knight Coins"),
		LEVEL_7(1250000, "1.250.000 Knight Coins");
		
		public int price;
		public String stringPrice;
		
		ElektronUpgradePrices(int p, String sP) {
			this.price = p;
			this.stringPrice = sP;
		}
}


public enum Abilities {
    NINJA_1("Dash", 4, 9, 2),
    NINJA_2("Dash", 4, 10, 2),
    NINJA_3("Dash", 4, 11, 2),
    NINJA_4("Dash", 4, 12, 2),
    NINJA_5("Dash", 4, 13, 2),
    NINJA_6("Dash", 4, 14, 2),
    NINJA_7("Dash", 4.3, 16, 3),

    PIRATE_1("Cannonballs", 4.844, 4, 0),
    PIRATE_2("Cannonballs", 5.689, 5, 0),
    PIRATE_3("Cannonballs", 6.533, 6, 0),
    PIRATE_4("Cannonballs", 7.378, 7, 0),
    PIRATE_5("Cannonballs", 8.222, 8, 0),
    PIRATE_6("Cannonballs", 9.067, 9, 0),
    PIRATE_7("Cannonballs", 10.334, 10, 0),
    
    HUNTER_1("Farm Explosion", 1, 10, 10),
    HUNTER_2("Farm Explosion", 2, 11, 11),
    HUNTER_3("Farm Explosion", 3, 12, 12),
    HUNTER_4("Farm Explosion", 4, 13, 13),
    HUNTER_5("Farm Explosion", 5, 14, 14),
    HUNTER_6("Farm Explosion", 6, 15, 15),
    HUNTER_7("Farm Explosion", 8, 16, 17),
    
    MENDER_1("Life Steal", 1, 7, 5),
    MENDER_2("Life Steal", 2, 8, 6),
    MENDER_3("Life Steal", 3, 9, 7),
    MENDER_4("Life Steal", 4, 10, 8),
    MENDER_5("Life Steal", 5, 11, 9),
    MENDER_6("Life Steal", 6, 12, 10),
    MENDER_7("Life Steal", 7, 14, 11),
    
		ELEKTRON_1("Lasers", 1, 20, 6),
		ELEKTRON_2("Lasers", 2, 22, 6.5),
		ELEKTRON_3("Lasers", 3, 24, 7),
		ELEKTRON_4("Lasers", 4, 26, 7.5),
		ELEKTRON_5("Lasers", 5, 28, 8),
		ELEKTRON_6("Lasers", 6, 30, 8.5),
		ELEKTRON_7("Lasers", 7, 55, 10),
    ;

    public String display;
    public double damage;
    public int mana;
    public double count;
    public static int manaElektron = 10;
    
    Abilities(String display, double damage, int mana, double count) {
        this.display = display;
        this.damage = damage;
        this.mana = mana;
        this.count = count;
    }

    public static Abilities getAbility(String ability, int level) {
        for (Abilities a : values()) {
            if (a.toString().startsWith(ability)) {
                if (a.toString().split("_")[1].equalsIgnoreCase(String.valueOf(level))) {
                    return a;
                }
            }
        }
        throw new IllegalArgumentException("Invalid ability was given.");
    }
}

public enum Passives {
		PIRATE_1("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "Minder dan 4 HP"),
		PIRATE_2("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "Minder dan 5 HP"),
		PIRATE_3("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "Minder dan 6 HP"),
		PIRATE_4("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "Minder dan 7 HP"),
		PIRATE_5("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "Minder dan 8 HP"),
		PIRATE_6("Sea Rage", "Resistance 2, 8s", "Strength 1, 6s", "Minder dan 9 HP"),
		PIRATE_7("Sea Rage", "Resistance 3, 8s", "Strength 2, 6s", "Minder dan 9 HP"),
	
    NINJA_1("Combo Speed", "Speed 1, 6s", "3", ""),
    NINJA_2("Combo Speed", "Speed 1, 7s", "3", ""),
    NINJA_3("Combo Speed", "Speed 1, 8s", "3", ""),
    NINJA_4("Combo Speed", "Speed 2, 6s", "3", ""),
    NINJA_5("Combo Speed", "Speed 2, 7s", "3", ""),
    NINJA_6("Combo Speed", "Speed 2, 8s", "3", ""),
    NINJA_7("Combo Speed", "Speed 3, 8s", "3", ""),
    
    MENDER_1("Recover", "1 ❤ na een kill", "", ""),
    MENDER_2("Recover", "1.5 ❤ na een kill", "", ""),
    MENDER_3("Recover", "2 ❤ na een kill", "", ""),
    MENDER_4("Recover", "2.5 ❤ na een kill", "", ""),
    MENDER_5("Recover", "3 ❤ na een kill", "", ""),
    MENDER_6("Recover", "3.5 ❤ na een kill", "", ""),
    MENDER_7("Recover", "4.3 ❤ na een kill", "", ""),
    
    HUNTER_1("Triple Shot", "15%", "kans op een Triple Shot", ""),
    HUNTER_2("Triple Shot", "20%", "kans op een Triple Shot", ""),
    HUNTER_3("Triple Shot", "25%", "kans op een Triple Shot", ""),
    HUNTER_4("Triple Shot", "30%", "kans op een Triple Shot", ""),
    HUNTER_5("Triple Shot", "40%", "kans op een Triple Shot", ""),
    HUNTER_6("Triple Shot", "50%", "kans op een Triple Shot", ""),
    HUNTER_7("Triple Shot", "65%", "kans op een Triple Shot", ""),

	ELEKTRON_1("Laser Up", "5%", " kans op een laser", ""),
	ELEKTRON_2("Laser Up", "6%", " kans op een laser", ""),
	ELEKTRON_3("Laser Up", "7%", " kans op een laser", ""),
	ELEKTRON_4("Laser Up", "8%", " kans op een laser", ""),
	ELEKTRON_5("Laser Up", "9%", " kans op een laser", ""),
	ELEKTRON_6("Laser Up", "10%", " kans op een laser", ""),
	ELEKTRON_7("Laser Up", "12%", " kans op een laser", "");

    public String display;
    public String info, info2, info3;

    Passives(String display, String info, String info2, String info3) {
        this.display = display;
        this.info = info;
        this.info2 = info2;
        this.info3 = info3;
    }

    public static Passives getPassive(String ability, int level) {
        for (Passives a : values()) {
            if (a.toString().startsWith(ability)) {
                if (a.toString().split("_")[1].equalsIgnoreCase(String.valueOf(level))) {
                    return a;
                }
            }
        }
        throw new IllegalArgumentException("Invalid passive was given.");
    }
}

public AbilityUtils getAbilityUtils() {
    return abilityUtils;
}
	
	
}
