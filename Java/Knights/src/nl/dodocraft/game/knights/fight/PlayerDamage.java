package nl.dodocraft.game.knights.fight;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Created by Thiemo on 26-3-2017. No part of this publication may be
 * reproduced, distributed, or transmitted in any form or by any means.
 * Copyright © 2017 by Thiemo
 */
public class PlayerDamage {

	private final Player player;
	private final Map<UUID, Damage> damagers = new HashMap<>();
	private double totalDamage;

	private long lastDamageTaken;
	private Entity lastDamager;

	public long getLastDamageTaken() {
		return lastDamageTaken;
	}

	public boolean isRecentDamage() {
		return (((System.currentTimeMillis() - this.lastDamageTaken) / 1000) <= 20);
	}

	public void setLastDamageTaken(long lastDamageTaken) {
		this.lastDamageTaken = lastDamageTaken;
	}

	public Entity getLastDamager() {
		return lastDamager;
	}

	private class Damage {
		public long lastHitTime;
		public double damage = 0.0;

		Damage(Entity entity, double damage) {
			this.lastHitTime = System.currentTimeMillis();
			this.damage = damage;
		}
	}

	public PlayerDamage(Player player) {
		this.player = player;
		this.totalDamage = 0.0;
	}

	public Player getPlayer() {
		return player;
	}

	public Map<UUID, Damage> getDamagers() {
		return damagers;
	}

	public List<UUID> getRecentDamagers() {
		List<UUID> l = new ArrayList<>();
		for (Map.Entry<UUID, Damage> m : this.damagers.entrySet()) {
			long start = m.getValue().lastHitTime;
			if (((System.currentTimeMillis() - start) / 1000) <= 20) {
				l.add(m.getKey());
			}
		}
		return l;
	}

	public float getPercentageDamage(UUID player) {
		if (!this.damagers.containsKey(player)) {
			return 0f;
		}
		return (float) (this.round((this.damagers.get(player).damage * 100) / (this.totalDamage), 1));
	}

	public void addPlayerDamage(Entity entity, double damage) {
		double c = 0.0;
		if (this.damagers.containsKey(entity.getUniqueId())) {
			c += this.damagers.get(entity.getUniqueId()).damage;
		}
		this.increaseDamage(damage);
		this.lastDamager = entity;
		// set in damage map
		this.damagers.put(entity.getUniqueId(), new Damage(entity, c + damage));
	}

	public double getTotalDamage() {
		return totalDamage;
	}

	private void increaseDamage(double damage) {
		damage = this.round(damage, 2);
		this.totalDamage += damage;
	}

	public double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public void setLastDamager(Player lastDamager) {
		this.lastDamager = lastDamager;
	}
}
