package nl.dodocraft.game.knights.scoreboard;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.countdown.gametype.GameType;
import nl.dodocraft.game.knights.team.Team;
import nl.dodocraft.utils.DodoUtils;
import nl.dodocraft.utils.scoreboard.DodoBoard;
import nl.dodocraft.utils.scoreboard.controller.ScoreboardController;
import nl.thiemoboven.ncore.SimpleCore;

public class ScoreBoard {
	
	public void setWaitBoard(Player player) {
		DodoBoard sb = ScoreboardController.getInstance().getScoreboard(player);
		String name = "Knights" + (Loader.getInstance().getGameType() == GameType.MEGA ? " Mega" : " Mini");
		sb.setDisplayName(ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + name);
		sb.setBlank(11);
		sb.setLine(10, "Spelers: " + ChatColor.GREEN + SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers().size() + "/" + Loader.getInstance().getGameManager().getMaxPlayers());
		sb.setBlank(9);
		sb.setLine(8, "Map: " + ChatColor.GREEN + getMapName());
		sb.setBlank(7);
		sb.setLine(6, "Geselecteerde class:");
		sb.setLine(5, ChatColor.GREEN + Loader.getInstance().getKitManager().selectedKit.get(player.getName()).getName());
		sb.setBlank(4);
		sb.setLine(3, "De game start over:");
		sb.setLine(2, ChatColor.GRAY + "Onbekend");
		sb.setBlank(1);
		sb.setLine(0, ChatColor.AQUA + "play.dodocraft.nl");
	}
	
	private String getMapName() {
		String map;
		if (Loader.getInstance().getConfig().contains("map")) {
			map = Loader.getInstance().getConfig().getString("map");
		} else {
			map = "Onbekend";
		}
		return map;
	}
	
	public void setGameBoard(Player player) {
		DodoBoard sb = ScoreboardController.getInstance().getScoreboard(player);
		for (int i : sb.getLines().keySet()) {
			sb.removeLine(i);
		}
		String name = "Knights" + (Loader.getInstance().getGameType() == GameType.MEGA ? " Mega" : " Mini");
		if (Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId()) != null) {
			sb.setDisplayName(Loader.getInstance().getTeamManager().getPlayerTeam(player.getUniqueId()).getColor().toString() + ChatColor.BOLD + name);
		} else {
			sb.setDisplayName(ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + name);
		}
		sb.setLine(14, ChatColor.GRAY + (Loader.getInstance().getGameType() == GameType.MEGA ? "Mega " : "Mini ") + new SimpleDateFormat("dd-MM-yyyy").format(new Date()).replace("-", "/").replace(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)), "'" + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2, 4)));
        sb.setBlank(13);
        for (Team team : Team.values()) {
			if (team.isActive()) {
				sb.setLine(team.getScoreBoardLine(), team.getColor() + "✔ " + ChatColor.WHITE + team.getName() + ChatColor.GRAY + "");
			}
		}
        sb.setBlank(8);
        sb.setLine(7, "Kills: " + ChatColor.GREEN + Loader.getInstance().getPlayerDataManager().getPlayerKills(player));
        sb.setLine(6, "Gold: " + ChatColor.GREEN + Loader.getInstance().getPlayerDataManager().getPlayerCoins(player));
        sb.setBlank(5);
        sb.setLine(0, ChatColor.AQUA + "play.dodocraft.nl");
	}

	public void updateMoney(Player player) {
		DodoBoard sb = ScoreboardController.getInstance().getScoreboard(player);
		sb.setLine(6, "Gold: " + ChatColor.GREEN + Loader.getInstance().getPlayerDataManager().getPlayerMoney(player));
	}
	
	public void updateKills(Player player) {
		DodoBoard sb = ScoreboardController.getInstance().getScoreboard(player);
		sb.setLine(7, "Kills: " + ChatColor.GREEN + Loader.getInstance().getPlayerDataManager().getPlayerKills(player));
	}
	
	public void updateTeamObject(Team team) {
		for (Player player : SimpleCore.getInstance().getMain().getGameServerManager().getGamePlayers()) {
			if(DodoUtils.getInstance().getGhostAPI().isVanished(player.getUniqueId())) return; //Do not update scoreboard
			DodoBoard sb = ScoreboardController.getInstance().getScoreboard(player);
			sb.setLine(team.getScoreBoardLine(), team.getColor() + "✔ " + ChatColor.WHITE + team.getName() + " " + team.getScoreBoardObjectAndHealth());
		}
	}
	
}
