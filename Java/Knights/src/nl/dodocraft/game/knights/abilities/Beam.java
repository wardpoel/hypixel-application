package nl.dodocraft.game.knights.abilities;

import java.lang.reflect.Field;
import java.util.Random;
import java.util.UUID;

import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class Beam {

    protected int entityId;
    protected DataWatcher dataWatcher;

    private Location location;

    public Beam(Location location) {
        entityId = new Random().nextInt(Integer.MAX_VALUE);
        dataWatcher = new DataWatcher(null);

        byte data = (byte) calcData(0, 0, false); //onFire
        data = (byte) calcData(data, 1, false); //Crouched
        data = (byte) calcData(data, 3, false); //Sprinting
        data = (byte) calcData(data, 4, false); //Eating/Drinking/Block
        data = (byte) calcData(data, 5, true);  //Invisible

        dataWatcher.register(new DataWatcherObject<>(0, DataWatcherRegistry.a), data);
        dataWatcher.register(new DataWatcherObject<>(7, DataWatcherRegistry.c), 20F);

        //int type = calcType(0, 2, false); //Is Elderly
        //type = calcType(type, 4, false);  //Is retracting spikes

        dataWatcher.register(new DataWatcherObject<>(12, DataWatcherRegistry.h), false);

        this.location = location;
    }

    private void sendPacket(Packet packet){
        for(Player player : Bukkit.getOnlinePlayers()){
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
        }
    }

    private void sendPacket(Player player,Packet packet){
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public void startLocation(Location loc) {
    		this.location = loc;
    }
    
    public void spawn(Player show){
        try {
            PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving();
            set(packet, "a", entityId);
            set(packet, "b", UUID.randomUUID());
            set(packet, "c", 68);
            set(packet, "d", location.getX());
            set(packet, "e", location.getY());
            set(packet, "f", location.getZ());
            set(packet, "g", (int)toPackedByte(location.getYaw()));
            set(packet, "h", (int)toPackedByte(location.getPitch()));
            set(packet, "i", (int)toPackedByte(location.getPitch()));
            set(packet, "j", (byte) 0);
            set(packet, "k", (byte) 0);
            set(packet, "l", (byte) 0);
            set(packet, "m", dataWatcher);
            if(show == null){
                sendPacket(packet);
            }else{
                sendPacket(show,packet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTarget(LivingEntity target){
        try {
            spawn(null);
            PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata();
            set(packet, "a", entityId);
            dataWatcher.register(new DataWatcherObject<>(13, DataWatcherRegistry.b), ((CraftEntity) target).getHandle().getId());
            set(packet, "b", dataWatcher.b());
            sendPacket(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTarget(Player show,LivingEntity target){
        try {
            spawn(show);
            PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata();
            set(packet, "a", entityId);
            dataWatcher.register(new DataWatcherObject<>(13, DataWatcherRegistry.b), ((CraftEntity) target).getHandle().getId());
            set(packet, "b", dataWatcher.b());
            sendPacket(show,packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void despawn(Player show) {
        PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(new int[] { entityId });
        if (show == null) {
            sendPacket(packet);
        } else {
            sendPacket(show, packet);
        }
    }


    protected int calcData(int data, int id, boolean flag) {
        if (flag) { return Integer.valueOf(data | 1 << id);
        } else {  return Integer.valueOf(data & ~(1 << id)); }
    }

    protected int calcType(int type, int id, boolean flag) {
        if (flag) { return Integer.valueOf(type | id);
        } else {  return Integer.valueOf(type & ~id); }
    }

    protected byte toPackedByte(float f) {
        return (byte)((int)(f * 256.0F / 360.0F));
    }

    protected int toFixedPointNumber(Double d) {
        return (int)(d * 32D);
    }

    protected void set(Object instance, String name, Object value) throws Exception {
        Field field = instance.getClass().getDeclaredField(name);
        field.setAccessible(true);
        field.set(instance, value);
    }

}