/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.util.Vector
 */
package nl.dodocraft.game.knights.abilities.utils;

import org.bukkit.util.Vector;

import java.util.Random;

public class VectorUtils {
    private static Random random = new Random();

    public static Vector randomVector() {
        Vector vec = Vector.getRandom().multiply(2);
        vec.setX(vec.getX() - 1.0);
        vec.setY(vec.getY() - 1.0);
        vec.setZ(vec.getZ() - 1.0);
        return vec;
    }

    public static Double randomDouble() {
        return random.nextDouble() * 2.0 - 1.0;
    }

    public static final Vector rotateAroundAxisX(Vector v, double angle) {
        double cos = Math.cos(angle);
        double sin = Math.sin(angle);
        double y = v.getY() * cos - v.getZ() * sin;
        double z = v.getY() * sin + v.getZ() * cos;
        return v.setY(y).setZ(z);
    }

    public static final Vector rotateAroundAxisY(Vector v, double angle) {
        double cos = Math.cos(angle);
        double sin = Math.sin(angle);
        double x = v.getX() * cos + v.getZ() * sin;
        double z = v.getX() * (-sin) + v.getZ() * cos;
        return v.setX(x).setZ(z);
    }

    public static final Vector rotateAroundAxisZ(Vector v, double angle) {
        double cos = Math.cos(angle);
        double sin = Math.sin(angle);
        double x = v.getX() * cos - v.getY() * sin;
        double y = v.getX() * sin + v.getY() * cos;
        return v.setX(x).setY(y);
    }

    public static final Vector rotateVector(Vector v, double angleX, double angleY, double angleZ) {
        VectorUtils.rotateAroundAxisX(v, angleX);
        VectorUtils.rotateAroundAxisY(v, angleY);
        VectorUtils.rotateAroundAxisZ(v, angleZ);
        return v;
    }

    public static final double angleToXAxis(Vector vector) {
        return Math.atan2(vector.getX(), vector.getY());
    }
}

