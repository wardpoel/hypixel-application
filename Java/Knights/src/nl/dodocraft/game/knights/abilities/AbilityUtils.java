package nl.dodocraft.game.knights.abilities;

import org.bukkit.entity.Player;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.type.*;
import nl.dodocraft.game.knights.passives.type.*;
import nl.dodocraft.game.knights.kits.KitTemplate;
import nl.dodocraft.game.knights.passives.GamePasive;

public class AbilityUtils {

    public GameAbility getAbility(Player player, int level, KitTemplate kit, Loader plugin) {
        if (kit.getStartID() == 0) {
            return new AbilityPirate(plugin, level);
        }
        if (kit.getStartID() == 1) {
            return new AbilityMender(plugin, level);
        }
        if (kit.getStartID() == 2) {
            return new AbilityHunter(plugin, level);
        }
        if (kit.getStartID() == 3) {
            return new AbilityNinja(plugin, level);
        }
        if (kit.getStartID() == 4) {
            return new AbilityElektron(plugin, level);
        }
        return null;
    }

    public GamePasive getPassive1(Player player, int level, KitTemplate kit, Loader plugin) {
    	if (kit.getStartID() == 0) {
            return new PassivePirate1(plugin, player, level);
        }
        if (kit.getStartID() == 1) {
            return new PassiveMender1(plugin, player, level);
        }
        if (kit.getStartID() == 2) {
            return new PassiveHunter1(plugin, player, level);
        }
        if (kit.getStartID() == 3) {
            return new PassiveNinja1(plugin, player, level);
        }
        if (kit.getStartID() == 4) {
            return new PassiveElektron1(plugin, player, level);
        }
        return null;
    }
}
