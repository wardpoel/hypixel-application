package nl.dodocraft.game.knights.abilities.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.GameAbility;
import nl.dodocraft.utils.inventory.ItemBuilder;
import nl.dodocraft.utils.particles.ParticleEffect;

public class AbilityPirate implements GameAbility {

	private enum AbilityStats {
		LVL1(4.844, 4),
		LVL2(5.689, 5),
		LVL3(6.533, 6),
		LVL4(7.378, 7),
		LVL5(8.222, 8),
		LVL6(9.067, 9),
		LVL7(10.334, 10); //Master

		public final double strength;
		public final int mana;

		AbilityStats(double strength, int mana) {
			this.strength = strength;
			this.mana = mana;
		}

		public double getStrength() {
			return this.strength;
		}

		public int getMana() {
			return this.mana;
		}

	}

	private Loader plugin;
	private int level;
	private double strength;
	private int mana;

	public AbilityPirate(Loader plugin, int level) {
		this.plugin = plugin;
		this.level = level;
		this.strength = AbilityStats.values()[level - 1].getStrength();
		this.mana = AbilityStats.values()[level - 1].getMana();

	}

	@Override
	public double getStrength() {
		return this.strength;
	}

	@Override
	public int getId() {
		return 0;
	}

	@Override
	public int getLevel() {
		return this.level;
	}

	@Override
	public String getName() {
		return "Cannonballs";
	}

	@Override
	public void onActivate(Player player) {
		plugin.getPlayerDataManager().getPlayerData(player).addAbility();
		plugin.getPlayerDataManager().resetMana(player);
		List<UUID> hittedPlayers = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			Location loc = player.getLocation();
			if (i == 0) {
				loc.setYaw(loc.getYaw() + 6);
			} else if (i == 1) {
				loc.setYaw(loc.getYaw() - 6);
			}

			final Vector vec = loc.getDirection().multiply(1);
			final ArmorStand stand = player.getWorld().spawn(player.getLocation().add(vec.clone().multiply(0.3333333)), ArmorStand.class);
			stand.setVisible(false);
			stand.setRemoveWhenFarAway(true);
			stand.setGravity(false);
			stand.setHelmet(new ItemBuilder(Material.SKULL_ITEM, 1, (byte) SkullType.PLAYER.ordinal()).setSkullOwnerNMS("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTk2NzU0ZDMzMDQzNTM0NWFhZTNhOWYwNjNjZmNhNDJhZmIyOGI3YzVjNGJiOWYyOTRlZDI1MjdkOTYxIn19fQ==").toItemStack());

			new BukkitRunnable() {
				boolean exploded = false;

				void explode(LivingEntity e) {
					this.exploded = true;
					ParticleEffect.EXPLOSION_LARGE.display(0, 0, 0, 0, 1, stand.getEyeLocation());
					stand.getWorld().createExplosion(stand.getEyeLocation().getX(), stand.getEyeLocation().getY(), stand.getEyeLocation().getZ(), 0.0f, false, false);
					ParticleEffect.EXPLOSION_HUGE.display(0.0f, 0.0f, 0.0f, 0.0f, 1, stand.getEyeLocation().clone().add(0.0, 0.1, 0.0));
					stand.remove();

					ParticleEffect.SMOKE_NORMAL.display(0.0f, 0.0f, 0.0f, 0.28f, 40, stand.getEyeLocation().clone().add(0.0, 0.1, 0.0));
					ParticleEffect.SMOKE_LARGE.display(0.0f, 0.0f, 0.0f, 0.2f, 24, stand.getEyeLocation().clone().add(0.0, 0.1, 0.0));
					ParticleEffect.FLAME.display(0.0f, 0.0f, 0.0f, 0.35f, 30, stand.getEyeLocation());

					double radius = 4;

					for (Player p : getNearbyPlayers(stand.getEyeLocation(), radius)) {
						if (!plugin.getTeamManager().areTeammates(player.getUniqueId(), p.getUniqueId())) { // No teammates
							if (!plugin.getFightListener().getSpawnProtectionList().contains(p.getUniqueId())) { // No spawnprotection
								if (!hittedPlayers.contains(p.getUniqueId())) {
									p.damage(strength);
									plugin.getPlayerDataManager().getPlayerData(player).addKitDamage(strength);
									hittedPlayers.add(p.getUniqueId());
								}
							} else {
								player.playSound(player.getLocation(), Sound.ITEM_TOTEM_USE, 1F, 5F);
							}
						}
					}
				}

				@Override
				public void run() {
					if (!stand.isValid() || stand.getTicksLived() > 15 * 20 || this.exploded) {
						this.cancel();
						stand.remove();
						if (stand.isValid() && (this.exploded)) {
							ParticleEffect.FLAME.display(0.0f, 0.0f, 0.0f, 0.25f, 5, stand.getEyeLocation());
						}
						return;
					}
					Location loc = stand.getLocation().clone().add(vec);
					Location checkLoc = stand.getEyeLocation().clone().add(vec);

					if (checkLoc.clone().subtract(0.0, 0.2, 0.0).getBlock().getType().isSolid()) {
						this.explode(stand);
					}

					for (Entity entity : stand.getWorld().getNearbyEntities(stand.getLocation(), 2, 2, 2)) {
						if (entity instanceof Player && !entity.equals(player) && !plugin.getTeamManager().areTeammates(player.getUniqueId(), entity.getUniqueId())) {
							this.explode(stand);
							return;
						}
					}

					stand.teleport(loc, PlayerTeleportEvent.TeleportCause.PLUGIN);
					ParticleEffect.FLAME.display(0.0f, 0.0f, 0.0f, 0.035f, 1, stand.getEyeLocation().clone().add(0.0, 0.1, 0.0));
					ParticleEffect.SMOKE_NORMAL.display(0.0f, 0.0f, 0.0f, 0.035f, 2, stand.getEyeLocation().clone().add(0.0, 0.1, 0.0));
					ParticleEffect.SMOKE_LARGE.display(0.0f, 0.0f, 0.0f, 0.035f, 1, stand.getEyeLocation().clone().add(0.0, 0.1, 0.0));
					if (new Random().nextBoolean()) {
						ParticleEffect.SMOKE_LARGE.display(0.0f, 0.0f, 0.0f, 0.015f, 1, stand.getEyeLocation().clone().add(0.0, 0.1, 0.0));
					}
				}
			}.runTaskTimer(plugin, 0, 0);
		}
	}

	@Override
	public int getManaByHit() {
		return this.mana;
	}

	public List<Player> getNearbyPlayers(Location loc, double range) {
        List<Player> list = new ArrayList<>();
        for (Entity entity : loc.getWorld().getNearbyEntities(loc, range, range, range)) {
            if (entity instanceof Player) {
                Player casted = Player.class.cast(entity);
                if ((casted.getGameMode() == GameMode.SURVIVAL || ((Player) entity).getGameMode() == GameMode.ADVENTURE) && !casted.isDead()) {
                    if (casted.isValid()) {
                        list.add(casted);
                    }
                }
            }
        }
        return list;
    }


}
