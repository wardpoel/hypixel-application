package nl.dodocraft.game.knights.abilities.type;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.Beam;
import nl.dodocraft.game.knights.abilities.GameAbility;

public class AbilityElektron implements GameAbility {

	private enum AbilityStats {
		LVL1(4.338, 3.88, 20, 6, 10), LVL2(5.376, 4.649, 22, 6.5, 10), LVL3(6.415, 5.418, 24, 7, 10), LVL4(7.453, 6.187,
				26, 7.5,
				10), LVL5(8.492, 6.957, 28, 8, 10), LVL6(9.530, 7.726, 30, 8.5, 10), LVL7(10.568, 8.495, 55, 10, 10); // Master

		private final double eyesDamage;
		private final double scanDamage;
		private final double eyesRange;
		private final double scanRange;
		private final int mana;

		AbilityStats(double eyesDamage, double scanDamage, double eyesRange, double scanRange, int mana) {
			this.eyesDamage = eyesDamage;
			this.scanDamage = scanDamage;
			this.eyesRange = eyesRange;
			this.scanRange = scanRange;
			this.mana = mana;
		}

		public double getEyesDamage() {
			return eyesDamage;
		}

		public double getScanDamage() {
			return scanDamage;
		}

		public double getEyesRange() {
			return eyesRange;
		}

		public double getScanRange() {
			return scanRange;
		}

		public int getMana() {
			return mana;
		}
	}

	private Loader plugin;
	private final double eyesDamage;
	private final double scanDamage;
	private final double eyesRange;
	private final double scanRange;
	private final int mana;
	private final int level;

	public AbilityElektron(Loader plugin, int level) {
		this.plugin = plugin;
		this.eyesDamage = AbilityStats.values()[level - 1].getEyesDamage();
		this.scanDamage = AbilityStats.values()[level - 1].getScanDamage();
		this.eyesRange = AbilityStats.values()[level - 1].getEyesRange();
		this.scanRange = AbilityStats.values()[level - 1].getScanRange();
		this.mana = AbilityStats.values()[level - 1].getMana();
		this.level = level;
	}

	@Override
	public double getStrength() {
		return this.eyesDamage;
	}

	@Override
	public int getId() {
		return 4;
	}

	@Override
	public int getLevel() {
		return this.level;
	}

	@Override
	public String getName() {
		return "Lasers";
	}

	@Override
	public void onActivate(Player player) {
		this.plugin.getPlayerDataManager().resetMana(player);
		plugin.getPlayerDataManager().getPlayerData(player).addAbility();
		if (player.isSneaking()) {
			laserScan(player);
		} else {

			laserEyes(player);
		}
	}

	public void laserEyes(Player player) {
		new BukkitRunnable() {
			int times = 0;

			@Override
			public void run() {
				if (times >= 3 || player.getGameMode() != GameMode.SURVIVAL) {
					this.cancel();
					return;
				}
				Location startPoint = player.getLocation();
				Vector direction = player.getLocation().getDirection();
				Location endPoint = startPoint.clone().add(direction.multiply(1));
				final Vector vec = direction.multiply(1);

				ArmorStand start = player.getWorld().spawn(startPoint.clone().add(0, 0.6, 0), ArmorStand.class);
				start.setVisible(false);
				start.setGravity(false);

				ArmorStand target = player.getWorld().spawn(endPoint.clone(), ArmorStand.class);
				target.setVisible(false);
				target.setGravity(false);

				Beam laser = new Beam(start.getLocation().clone());
				laser.setTarget(target);

				Bukkit.getOnlinePlayers().forEach(laser::spawn);

				new BukkitRunnable() {
					@Override
					public void run() {
						if (start.getLocation().distance(startPoint) >= eyesRange
								|| start.getLocation().clone().add(0, 1.5, 0).getBlock().getType().isSolid()) {
							this.cancel();
							Bukkit.getOnlinePlayers().forEach(laser::despawn);
							start.remove();
							target.remove();
							return;
						}
						Location loc = start.getLocation().clone().add(vec);
						Location loc2 = target.getLocation().clone().add(vec);

						for (Entity entity : loc.getWorld().getNearbyEntities(loc, 0.9, 0.9, 0.9)) {
							if (entity instanceof Player) {
								Player casted = Player.class.cast(entity);
								if ((casted.getGameMode() == GameMode.SURVIVAL) && !casted.isDead()
										&& casted != player) {
									if (casted.isValid() && !plugin.getTeamManager().areTeammates(player.getUniqueId(),
											casted.getUniqueId())) {
										if (!plugin.getFightListener().getSpawnProtectionList().contains(casted.getUniqueId())) {
											casted.damage(eyesDamage / 3);
											plugin.getPlayerDataManager().getPlayerData(player).addKitDamage(eyesDamage / 3);
										} else {
											player.playSound(player.getLocation(), Sound.ITEM_TOTEM_USE, 1F, 5F);
										}

									}
								}
							}
						}

						target.teleport(loc2, PlayerTeleportEvent.TeleportCause.PLUGIN);
						start.teleport(loc, PlayerTeleportEvent.TeleportCause.PLUGIN);

						laser.startLocation(start.getLocation());
						Bukkit.getOnlinePlayers().forEach(laser::despawn);
						Bukkit.getOnlinePlayers().forEach(laser::spawn);
					}
				}.runTaskTimer(plugin, 0, 0);
				times++;
			}
		}.runTaskTimer(plugin, 0, 14L);
	}

	/*
	public void laserScan(Player player) {
		List<UUID> hitted = new ArrayList<>();
		Location c = player.getLocation();
		for (int times = 0; times <= 10; times++) {
			Location center = c.clone();
			center.setYaw(center.getYaw() + (times * 36));
			final float radius = (float) scanRange;
			final float radPerSec = 8f;
			final float radPerTick = radPerSec / 20f;

			ArmorStand stand = center.getWorld().spawn(center, ArmorStand.class);
			stand.setVisible(false);
			stand.setGravity(false);

			stand.teleport(center);

			Beam laser = new Beam(center);
			laser.setTarget(stand);
			Bukkit.getOnlinePlayers().forEach(laser::spawn);

			new BukkitRunnable() {
				int tick = 0;

				public void run() {
					Location loc = getLocationAroundCircle(center, radius, radPerTick * tick);
					stand.setVelocity(new Vector(1, 0, 0));
					stand.teleport(loc);
					if (tick >= 20 * 3) {
						this.cancel();
						Bukkit.getOnlinePlayers().forEach(laser::despawn);
						stand.remove();
						return;
					}

					if (tick % 10 == 0) { // Damage players in scan
						for (Player p : getNearbyPlayers(center, scanRange)) {
							if (p != player && !plugin.getFightListener().getSpawnProtectionList().contains(p.getUniqueId()) && p.getGameMode() == GameMode.SURVIVAL) {
								if (!hitted.contains(p.getUniqueId())) {
									if (!plugin.getFightListener().getSpawnProtectionList().contains(p.getUniqueId())) {
										p.damage(scanDamage);
										plugin.getPlayerDataManager().getPlayerData(player).addKitDamage(scanDamage);
									}
									hitted.add(p.getUniqueId());
								}
							}
						}
					}
					tick++;
				}
			}.runTaskTimer(plugin, 0, 1);

		}

	}
	*/
	
	public void laserScan(Player player) {
		List<UUID> hitted = new ArrayList<>();
		Location c = player.getLocation();
		Location center = c.clone();
		final float radius = (float) scanRange;
		final float radPerSec = 8f;
		final float radPerTick = radPerSec / 20f;

		ArmorStand stand = center.getWorld().spawn(center, ArmorStand.class);
		stand.setVisible(false);
		stand.setGravity(false);
		stand.teleport(center);

		Guardian laser = center.getWorld().spawn(center, Guardian.class);
		laser.setTarget(stand);
		
		new BukkitRunnable() {
			int tick = 0;
			public void run() {
				Location loc = getLocationAroundCircle(center, radius, radPerTick * tick);
				stand.setVelocity(new Vector(1, 0, 0));
				stand.teleport(loc);
				laser.setTarget(stand);
				if (tick >= 20 * 3) {
					this.cancel();
					laser.remove();
					stand.remove();
					return;
				}

				if (tick % 10 == 0) { // Damage players in scan
					for (Player p : getNearbyPlayers(center, scanRange)) {
						if (p != player && !plugin.getFightListener().getSpawnProtectionList().contains(p.getUniqueId()) && p.getGameMode() == GameMode.SURVIVAL) {
							if (!hitted.contains(p.getUniqueId())) {
								if (!plugin.getFightListener().getSpawnProtectionList().contains(p.getUniqueId())) {
									p.damage(scanDamage);
									plugin.getPlayerDataManager().getPlayerData(player).addKitDamage(scanDamage);
								}
								hitted.add(p.getUniqueId());
							}
						}
					}
				}
				tick++;
			}
		}.runTaskTimer(plugin, 0, 1);
	}

	public Location getLocationAroundCircle(Location center, double radius, double angleInRadian) {
		double x = center.getX() + radius * Math.cos(angleInRadian);
		double z = center.getZ() + radius * Math.sin(angleInRadian);
		double y = center.getY();

		Location loc = new Location(center.getWorld(), x, y, z);
		Vector difference = center.toVector().clone().subtract(loc.toVector()); // this sets the returned location's
																				// direction toward the center of the
																				// circle
		loc.setDirection(difference);

		return loc;
	}

	public List<Player> getNearbyPlayers(Location loc, double range) {
		List<Player> list = new ArrayList<>();
		for (Entity entity : loc.getWorld().getNearbyEntities(loc, range, range, range)) {
			if (entity instanceof Player) {
				Player casted = Player.class.cast(entity);
				if ((casted.getGameMode() == GameMode.SURVIVAL || ((Player) entity).getGameMode() == GameMode.ADVENTURE)
						&& !casted.isDead()) {
					if (casted.isValid()) {
						list.add(casted);
					}
				}
			}
		}
		return list;
	}

	@Override
	public int getManaByHit() {
		return this.mana;
	}

}
