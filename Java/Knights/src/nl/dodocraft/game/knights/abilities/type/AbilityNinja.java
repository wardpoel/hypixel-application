package nl.dodocraft.game.knights.abilities.type;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.GameAbility;
import nl.dodocraft.utils.chat.messages.MessageController;
import nl.dodocraft.utils.particles.ParticleEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AbilityNinja implements GameAbility {

    private enum AbilityStats {
        LVL1(4.0, 9, 2),
        LVL2(4.0, 10, 2),
        LVL3(4.0, 11, 2),
        LVL4(4.0, 12, 2),
        LVL5(4.0, 13, 2),
        LVL6(4.0, 14, 2),
        LVL7(4.0, 16, 3);//master

        public final double strength;
        public final int mana;
        public final int maxDashes;

        AbilityStats(double strength, int mana, int maxDashes) {
            this.strength = strength;
            this.mana = mana;
            this.maxDashes = maxDashes;
        }

        public double getStrength() {
			return this.strength;
		}

		public int getMaxDashes() {
			return this.maxDashes;
		}

		public int getMana() {
			return this.mana;
		}

    }

    private Loader plugin;
    private int level;
    private double strength;
    private int mana;
    private int maxDashes;

    public AbilityNinja(Loader plugin, int level) {
        this.plugin = plugin;
        this.level = level;
        this.strength = AbilityStats.values()[level -1].getStrength();
        this.mana = AbilityStats.values()[level -1].getMana();
        this.maxDashes = AbilityStats.values()[level -1].getMaxDashes();
    }

    @Override
    public double getStrength() {
        return this.strength;
    }

    @Override
    public int getId() {
        return 3;
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public String getName() {
        return "Dash";
    }

    private int dash = 0;
    private List<UUID> hitted = new ArrayList<>();

    @Override
    public void onActivate(Player player) {
    	plugin.getPlayerDataManager().getPlayerData(player).addAbility();
        if (dash >= maxDashes) {
            return;
        }

        if (dash == 0) {
            new BukkitRunnable() {
                int i = 0;
                int dashNow = dash;
                boolean cancel = false;

                @Override
                public void run() {
                    if (dashNow == dash) {
                        cancel = true;
                    }
                    else {
                        dashNow = dash;
                    }

                    if (++i >= 3) {
                        cancel = true;
                    }

                    if (cancel) {
                        this.cancel();
                        hitted.clear();
                        dash = 0;
                        plugin.getPlayerDataManager().resetMana(player);
                    }
                    else {
                        if (dash == 1)
                            MessageController.sendTitle(player, 0, 30, 0, "", ChatColor.translateAlternateColorCodes('&', "&b&lDASH &8▪ &3Rechtermuisknop &dom nog een keer te dashen."));
                        else
                            MessageController.sendTitle(player, 0, 30, 0, "", ChatColor.translateAlternateColorCodes('&', "&a&lDASH &8▪ &3Rechtermuisknop &dom nog een keer te dashen."));
                    }
                }
            }.runTaskTimer(this.plugin, 10, 10);
        }

        dash++;
        //dash
        int maxTime = 5;

        for (float f = 0.1f; f <= 0.3f; f+=0.1) {
            ParticleEffect.SMOKE_NORMAL.display(0.3f, 0, 0.3f, f, 40, player.getLocation());
            ParticleEffect.SMOKE_LARGE.display(0.3f, 0, 0.3f, f, 20,player.getLocation());
        }

        {
            //ArmorStand stand = player.getWorld().spawn()
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, maxTime, 0, true, false), true);
        new BukkitRunnable() {
            boolean isValid(Player player) {
                if (player == null || player.getGameMode() == GameMode.SPECTATOR || player.isDead()) {
                   this.cancel();
                   return false;
                }
                return true;
            }
            int i = 0;
            Vector vector = player.getLocation().getDirection().multiply(1.1);
            @Override
            public void run() {
                if (isValid(player)) {
                    i++;
                    if (i > maxTime) {
                        this.cancel();
                        for (float f = 0.1f; f <= 0.3f; f+=0.1) {
                            ParticleEffect.SMOKE_NORMAL.display(0.3f, 0, 0.3f, f, 40, player.getLocation());
                            ParticleEffect.SMOKE_LARGE.display(0.3f, 0, 0.3f, f, 20,player.getLocation());
                        }
                        return;
                    }

                    player.setVelocity(vector);
                    for (Player players : getNearbyPlayers(player.getLocation(), 2)) {
                        if (plugin.getTeamManager().areTeammates(player.getUniqueId(), players.getUniqueId())) continue;
                        if (plugin.getFightListener().getSpawnProtectionList().contains(players.getUniqueId())) {
                        		player.playSound(player.getLocation(), Sound.ITEM_TOTEM_USE, 1F, 5F);
                        		continue;
                        }
                        if (hitted.contains(players.getUniqueId())) continue;
                        hitted.add(players.getUniqueId());

                        double totalDamage = players.getHealth() - strength < 0.0 ? players.getHealth() : strength;
                        players.damage(totalDamage);
                        plugin.getPlayerDataManager().getPlayerData(player).addKitDamage(totalDamage);
                    }
                }
            }
        }.runTaskTimer(this.plugin, 0, 0);
    }

    public List<Player> getNearbyPlayers(Location loc, double range) {
        List<Player> list = new ArrayList<>();
        for (Entity entity : loc.getWorld().getNearbyEntities(loc, range, range, range)) {
            if (entity instanceof Player) {
                Player casted = Player.class.cast(entity);
                if ((casted.getGameMode() == GameMode.SURVIVAL || ((Player) entity).getGameMode() == GameMode.ADVENTURE) && !casted.isDead()) {
                    if (casted.isValid()) {
                        list.add(casted);
                    }
                }
            }
        }
        return list;
    }

	@Override
	public int getManaByHit() {
		return this.mana;
	}
}
