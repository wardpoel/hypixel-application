package nl.dodocraft.game.knights.abilities.type;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.GameAbility;
import nl.dodocraft.utils.particles.ParticleEffect;

public class AbilityMender implements GameAbility {

	private enum AbilityStats {
		LVL1(0.26, 4, 5, 0.22),
		LVL2(0.52, 5, 6, 0.41),
		LVL3(0.78, 6, 7, 0.60),
		LVL4(1.04, 7, 8, 0.79),
		LVL5(1.3, 8, 9, 0.98),
		LVL6(1.56, 9, 10, 1.17),
		LVL7(1.82, 10, 11, 1.36); //Master

		public final double strength;
		public final int mana;
		public final int seconds;
		public final double heal;

		AbilityStats(double strength, int mana, int seconds, double heal) {
			this.strength = strength;
			this.mana = mana;
			this.seconds = seconds;
			this.heal = heal;
		}

		public double getStrength() {
			return this.strength;
		}

		public int getMana() {
			return this.mana;
		}

		public int getSeconds() {
			return this.seconds;
		}

		public double getHeal() {
			return this.heal;
		}
	}

	private Loader plugin;
	private int level;
	private double strength;
	private int mana;
	private int seconds;
	private double heal;

	public AbilityMender(Loader plugin, int level) {
		this.plugin = plugin;
		this.level = level;
		this.strength = AbilityStats.values()[level - 1].getStrength();
		this.mana = AbilityStats.values()[level - 1].getMana();
		this.seconds = AbilityStats.values()[level - 1].getSeconds();
		this.heal = AbilityStats.values()[level - 1].getHeal();
	}

	@Override
	public double getStrength() {
		return this.strength;
	}

	@Override
	public int getId() {
		return 1;
	}

	@Override
	public int getLevel() {
		return this.level;
	}

	@Override
	public String getName() {
		return "Life Steal";
	}

	@Override
	public void onActivate(Player player) {
		plugin.getPlayerDataManager().getPlayerData(player).addAbility();
		Location startPoint = player.getLocation();
		List<Player> players = getNearbyPlayers(startPoint, 9.5);
		if (players.size() > 1) {
			this.plugin.getPlayerDataManager().resetMana(player);

			for (Player p : players) {
				if (p != player) {
					new BukkitRunnable() {
						int times = 0;
						@Override
						public void run() {
							if (times >= (seconds * 20) || p.getLocation().distance(player.getLocation()) > 12 || player.isDead() || p.isDead() || p.getGameMode() != GameMode.SURVIVAL || player.getGameMode() != GameMode.SURVIVAL) {
								this.cancel();
								return;
							}
							if (times % 20 == 0) {
								if (!plugin.getFightListener().getSpawnProtectionList().contains(p.getUniqueId())) {
									if (player.getHealth() + heal > 20) {
										heal = 20 - player.getHealth();
									}
									player.setHealth(player.getHealth() + heal);
									plugin.getPlayerDataManager().getPlayerData(player).addHealing(heal);
									p.damage(strength);
									plugin.getPlayerDataManager().getPlayerData(player).addKitDamage(strength);

									if (p.isDead() || p.getHealth() - strength <= 0) {
										this.cancel();
										return;
									}

									//Green and Red particles
									Location start = player.getLocation().clone().add(0, 1, 0);
									Location playerLoc = p.getLocation().add(0, 1, 0);
									Vector vec = playerLoc.toVector().subtract(start.clone().toVector()).normalize();
									for (double i = 0; i < playerLoc.distance(start.clone()); i += 0.2) {
										ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(255, 0, 0), start.clone().add(vec.clone().multiply(i)), 0);
										ParticleEffect.VILLAGER_HAPPY.display(0, 0, 0, 0.001f, 1, start.clone().add(vec.clone().multiply(i)));
									}
								} else {
									player.playSound(player.getLocation(), Sound.ITEM_TOTEM_USE, 1F, 5F);
								}
							}
							times++;
							Location start = player.getLocation().clone().add(0, 1, 0);
							Location playerLoc = p.getLocation().add(0, 1, 0);
							Vector vec = playerLoc.toVector().subtract(start.clone().toVector()).normalize();
							for (double i = 0; i < playerLoc.distance(start.clone()); i += 0.2) {
								ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(255, 255, 255), start.clone().add(vec.clone().multiply(i)), 0);
							}
						}
					}.runTaskTimer(plugin, 0, 1);
				}
			}
		} else {
			player.sendMessage(ChatColor.RED + "Er is geen vijand in de buurt om " + ChatColor.AQUA + ChatColor.BOLD + getName() + ChatColor.RED + " op te gebruiken.");
		}
	}

	@Override
	public int getManaByHit() {
		return this.mana;
	}

	public List<Player> getNearbyPlayers(Location loc, double range) {
        List<Player> list = new ArrayList<>();
        for (Entity entity : loc.getWorld().getNearbyEntities(loc, range, range, range)) {
            if (entity instanceof Player) {
                Player casted = Player.class.cast(entity);
                if ((casted.getGameMode() == GameMode.SURVIVAL || ((Player) entity).getGameMode() == GameMode.ADVENTURE) && !casted.isDead()) {
                    if (casted.isValid()) {
                        list.add(casted);
                    }
                }
            }
        }
        return list;
    }


}
