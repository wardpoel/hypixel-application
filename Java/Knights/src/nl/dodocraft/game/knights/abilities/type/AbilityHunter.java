package nl.dodocraft.game.knights.abilities.type;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.abilities.GameAbility;

public class AbilityHunter implements GameAbility {

	private enum AbilityStats {
		LVL1(1.0, 10, 10),
		LVL2(2.0, 11, 11),
		LVL3(3.0, 12, 12),
		LVL4(4.0, 13, 13),
		LVL5(5.0, 14, 14),
		LVL6(6.0, 15, 15),
		LVL7(7.5, 17, 16); //Master
		
		public final double strength;
		public final int activeTime;
		public final int mana;
		
		AbilityStats(double strength, int activeTime, int mana) {
			this.strength = strength;
			this.activeTime = activeTime;
			this.mana = mana;
		}
		
		public double getStrength() {
			return this.strength;
		}
		
		public int getActiveTime() {
			return this.activeTime;
		}
		
		public int getMana() {
			return this.mana;
		}
		
	}
	
	private Loader plugin;
	private int level;
	private double strength;
	private int mana;
	private int activeTime;
	
	public AbilityHunter(Loader plugin, int level) {
		this.plugin = plugin;
		this.level = level;
		this.strength = AbilityStats.values()[level - 1].getStrength();
		this.mana = AbilityStats.values()[level - 1].getMana();
		this.activeTime = AbilityStats.values()[level - 1].getActiveTime();
	}
	
	@Override
	public double getStrength() {
		return this.strength;
	}

	@Override
	public int getId() {
		return 2;
	}

	@Override
	public int getLevel() {
		return this.level;
	}

	@Override
	public String getName() {
		return "Farm Explosion";
	}

	@Override
	public void onActivate(Player player) {
		plugin.getPlayerDataManager().getPlayerData(player).addAbility();
		player.setMetadata("knights.hunter", new FixedMetadataValue(this.plugin, true));
		new BukkitRunnable() {
			@Override
			public void run() {
				if (player.hasMetadata("knights.hunter")) {
					player.removeMetadata("knights.hunter", plugin);
				}
			}
		}.runTaskLater(this.plugin, Math.round(this.activeTime * 20));
		
		this.plugin.getPlayerDataManager().resetMana(player);
	}

	@Override
	public int getManaByHit() {
		return this.mana;
	}

}
