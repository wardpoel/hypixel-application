package nl.dodocraft.game.knights.abilities;

import org.bukkit.entity.Player;

public interface GameAbility {
	
	double getStrength();
	
	int getId();
	
	int getLevel();
	
	String getName();
	
	void onActivate(Player player);
	
	int getManaByHit();
	
}
