package nl.dodocraft.game.knights.commands;

import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Jesper on 9-9-2017.
 * Don't use this code for own creations.
 * Copyright © 2017 Jesper van den Munckhof
 */

public class CmdSetCenter implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player p = (Player) sender;
        if (DodoCore.getInstance().getRank(p.getUniqueId()).getId() >= Rank.DEVELOPER.getId()) {
            Loader.getInstance().getConfig().set("center", serialize(p.getLocation()));
            p.sendMessage(ChatColor.GREEN + "Center is geplaatst.");
            Loader.getInstance().saveConfig();
        }
        return true;
    }

    private String serialize(Location loc) {
        return loc.getWorld().getName() + "_" + (loc.getBlockX() + 0.5) + "_" + (loc.getBlockY() + 0.5) + "_" + (loc.getBlockZ() + 0.5) + "_" + 0 + "_" + 0;
    }
}
