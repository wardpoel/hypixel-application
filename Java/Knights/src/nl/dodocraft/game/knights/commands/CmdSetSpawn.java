package nl.dodocraft.game.knights.commands;

import nl.dodocraft.common.Rank;
import nl.dodocraft.core.DodoCore;
import nl.dodocraft.game.knights.Loader;
import nl.dodocraft.game.knights.team.Team;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CmdSetSpawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player p = (Player) sender;
        if (DodoCore.getInstance().getRank(p.getUniqueId()).getId() >= Rank.DEVELOPER.getId()) {
            try {
                if (args.length == 2) {
                    Loader.getInstance().getConfig().set("spawns." + Team.valueOf(args[0].toUpperCase()) + "." + args[1], this.serialize(p.getLocation()));
                }
                else {
                		Loader.getInstance().getConfig().set("spawns." + Team.valueOf(args[0].toUpperCase()) + ".1", this.serialize(p.getLocation()));
                }
                p.sendMessage(ChatColor.GREEN + "Spawn voor team " + args[0] + " is geplaatst.");
                Loader.getInstance().saveConfig();
            } catch (IllegalArgumentException e) {
                p.sendMessage(ChatColor.RED + "Ongeldig Team. Geldige teams zijn: " + Arrays.toString(Team.values()));
            }
        }
        return true;
    }

    private String serialize(Location loc) {
        return loc.getWorld().getName() + "_" + (loc.getBlockX() + 0.5) + "_" + (loc.getBlockY() + 0.5) + "_" + (loc.getBlockZ() + 0.5) + "_" + loc.getYaw() + "_" + loc.getPitch();
    }
}
